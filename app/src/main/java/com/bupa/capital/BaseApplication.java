
package com.bupa.capital;

import android.app.Application;

import com.sgb.capital.utils.CapitalInit;


public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CapitalInit.init(this, BuildConfig.BUILD_TYPE);
    }
}
