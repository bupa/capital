package com.sgb.capital.base;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Description:
 * Author zhengkewen
 * Time 2017/12/11
 */
public class AppFragment extends BaseFragment {


    @Override
    protected boolean registerNet() {
        return false;
    }

    @Override
    protected void connectNetWork() {

    }

    @Override
    protected void disconnectNetWork() {

    }



    @Override
    public void onMultiClick(View v) {

    }

    protected void closeKeybord() {
        if (getActivity().getCurrentFocus() != null) {
            ((InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(getActivity().getCurrentFocus()
                                    .getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void loading() {

    }

    public void error() {

    }

    public void success() {

    }
}
