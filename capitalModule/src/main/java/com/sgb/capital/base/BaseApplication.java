
package com.sgb.capital.base;

import android.app.Application;

import com.sgb.capital.BuildConfig;
import com.sgb.capital.utils.CapitalInit;


public class BaseApplication extends Application {
    public static BaseApplication mApp = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        CapitalInit.init(this, BuildConfig.BUILD_TYPE);
    }

    /**
     * BaseApplication 的单列
     *
     * @return
     */
    public static BaseApplication getApp() {
        if (mApp == null) {
            mApp = new BaseApplication();
        }
        return mApp;
    }
}
