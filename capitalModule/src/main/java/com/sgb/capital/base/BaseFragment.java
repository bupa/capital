package com.sgb.capital.base;

import android.os.Bundle;
import android.view.View;

import com.sgb.capital.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


/**
 * Description:
 * Author zhengkewen
 * Time 2017/12/11
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {
    private static final int MIN_CLICK_DELAY_TIME = 2000;
    private static long lastClickTime;

    protected abstract boolean registerNet();

    protected abstract void connectNetWork();

    protected abstract void disconnectNetWork();


    public abstract void onMultiClick(View v);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            lastClickTime = curClickTime;
            onMultiClick(v);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
