package com.sgb.capital.base;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.sgb.capital.R;
import com.sgb.capital.utils.ResolutionUtil;
import com.sgb.capital.view.pop.PopViewHolder;


public abstract class BasePopWindow<D> extends PopupWindow {
    protected static final int DIR_DEFUALT = 0;
    protected static final int DIR_RIGHT = 1;
    protected static final int DIR_DEFULAT_WRAP = 2;
    protected static final int DIR_DOWN_UP = 3;
    protected static final int DIR_RIGHT_MATCH = 4;
    protected static final int DIR_DOWN_UP_MATCH = 5;
    protected static final int DIR_RIGHT_MATCH_HEIGHT = 6;
    protected Context mContext;
    protected View bgView;
    protected OnResultClick onResultClick;
    protected OnDisMiss onDisMiss;
    private int dir = DIR_DEFUALT;   //弹出方向默认从下到下 1表示从右到左

    public BasePopWindow(Context context) {
        this(context, null, 0);
    }

    public BasePopWindow(Context context, int dir) {
        this(context, null, dir);
    }

    public BasePopWindow(Context context, AttributeSet attrs, int dir) {
        this(context, attrs, 0, dir);
    }

    public BasePopWindow(Context context, AttributeSet attrs, int defStyleAttr, int dir) {
        this(context, attrs, defStyleAttr, 0, dir);
    }

    public BasePopWindow(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, int dir) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext = context;
        this.dir = dir;
        init();
    }

    public void setBgView(View bgView) {
        this.bgView = bgView;
        bgView.setOnClickListener(v -> dissMiss());
    }

    public void setBgNoClickView(View bgView) {
        this.bgView = bgView;
        bgView.setOnClickListener(v -> {
            if (true) return;
        });

    }

    private void init() {
        PopViewHolder popViewHolder = PopViewHolder.get(mContext, null, popLayout());
        setContentView(popViewHolder.getPop());
        setFocusable(true);
        setOutsideTouchable(true);
        ColorDrawable dw = new ColorDrawable(0000000);
        this.setBackgroundDrawable(dw);
        if (dir == 0) {
            setAnimationStyle(R.style.popwindow_dropdown);
            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        } else if (dir == 1) {
            setAnimationStyle(R.style.popwindow_right);
            setWidth((ResolutionUtil.getScreenWidth(mContext) / 6) * 5);
            setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        } else if (dir == 2) {
            setAnimationStyle(R.style.popwindow_dropdown);
            //    setWidth((ResolutionUtil.getScreenWidth(mContext) / 3));
            setWidth((ViewGroup.LayoutParams.WRAP_CONTENT));
            setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        } else if (dir == 3) {
            setAnimationStyle(R.style.popwindow_down_up);
            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        } else if (dir == 4) {
            setAnimationStyle(R.style.popwindow_right);
            setWidth((ResolutionUtil.getScreenWidth(mContext)));
            setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        } else if (dir == 5) {
            setAnimationStyle(R.style.popwindow_down_up);
            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        } else if (dir == 6) {
            setAnimationStyle(R.style.popwindow_right);
            setWidth((ResolutionUtil.getScreenWidth(mContext) / 6) * 5);
            setHeight(ResolutionUtil.getScreenHeight(mContext));
        }
        initView(popViewHolder);
    }

    public void showPopwindow(View view, boolean alpha) {
        if (alpha) {
            setAlpha();
        }
        if (!this.isShowing()) {
            showAsDropDown(view, 0, 20);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }


    public void showPopwindow(View view, boolean alpha,int y) {
        if (alpha) {
            setAlpha();
        }
        if (!this.isShowing()) {
            showAsDropDown(view, 0, y);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }

    public void showPopwindowGoods(View view) {
        if (!this.isShowing()) {
            showAsDropDown(view, 0, 0);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }

    public void showPopwindow(View view) {
        if (!this.isShowing()) {
            showAsDropDown(view, 0, 20);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }

    public void showPopwindow0(View view) {
        if (!this.isShowing()) {
            showAsDropDown(view, 0, 0);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
//                        window.setBackgroundDrawableResource(com.swgk.core.R.color.transparent); //设置对话框背景为透明
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }

    public void showPopwindow(View view, boolean alpha,int xCoordinate,int yCoordinate) {
        if (alpha) {
            setAlpha();
        }
        if (!this.isShowing()) {
            showAsDropDown(view, xCoordinate, yCoordinate);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }

    public void showPopwindowshare(View view) {
        if (!this.isShowing()) {
            showAsDropDown(view, 0, 0);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }

    public void showPopwindow2(View view) {
        setWidth(view.getWidth());
        if (!this.isShowing()) {
            showAsDropDown(view, 0, 0);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }

    /**
     * 底部弹窗
     * @param alpha
     */
    public void showDownPopwindow(boolean alpha) {
        if(null != bgView) {
            showDownPopwindow(bgView, alpha);
        } else {
            try {
                throw new RuntimeException("你需要先设置 pop.setBgView(v) 背景视图!");
            } catch (Throwable e) {
                // 输出错误异常栈信息
                e.printStackTrace();
            }
        }
    }

    public void showDownPopwindow(View view, boolean alpha) {
        if (!this.isShowing()) {
            showAtLocation(view, Gravity.BOTTOM, 0, 0);
            if (bgView != null) {
                if (alpha) {
                    setAlpha();
                }
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }

    private void setAlpha() {
        Window window = ((Activity) mContext).getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = 0.4f;
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setAttributes(lp);
        setTouchable(true);
        setFocusable(true);
        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
    }

    protected void clearAlpha() {
        Window window = ((Activity) mContext).getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = 1f;
        window.setAttributes(lp);
    }

    public void backgroundAlpha(float Alpha) {
        Window window = ((Activity) mContext).getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = Alpha;
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setAttributes(lp);
        setTouchable(true);
        setFocusable(true);
        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
    }

    public void showPopwindowRight(View view) {
        if (!this.isShowing()) {
            showAtLocation(view, Gravity.RIGHT | Gravity.TOP, 0, 0);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 400);
            }
        } else {
            dissMiss();
        }
    }

    public void showPopwindowBottom(View view) {
        if (!this.isShowing()) {
            int[] location = new int[2];
            view.getLocationOnScreen(location);
            showAtLocation(view, Gravity.LEFT | Gravity.BOTTOM, 0, -location[1]);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 400);
            }
        } else {
            dissMiss();
        }
    }

    public void showPopwindow(View view, int x, int y) {
        if (!this.isShowing()) {
            showAsDropDown(view, x, y);
            if (bgView != null) {
                bgView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bgView.setVisibility(View.VISIBLE);
                        bgView.removeCallbacks(this);
                    }
                }, 0);
            }
        } else {
            dissMiss();
        }
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        if (Build.VERSION.SDK_INT >= 24) {
            Rect rect = new Rect();
            parent.getGlobalVisibleRect(rect);
            int h = parent.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            //  setHeight(h);
        }
        super.showAtLocation(parent, gravity, x, y);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        if (Build.VERSION.SDK_INT >= 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            //  setHeight(h);
        }
        super.showAsDropDown(anchor, xoff, yoff);
    }

    protected void dissMiss() {
        if (bgView != null) {
            bgView.setVisibility(View.GONE);
        }
        if (this.isShowing()) {
            this.dismiss();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (onDisMiss != null) {
            onDisMiss.disMiss();
        }
        clearAlpha();
        if (bgView != null) {
            bgView.setVisibility(View.GONE);
        }
    }

    protected OnResultCallBack onResultCallBack;

    public interface OnResultCallBack<D> {
        void result(D result);
    }

    public void setOnResultCallBack(OnResultCallBack<D> onResultCallBack) {
        this.onResultCallBack = onResultCallBack;
    }

    public interface OnResultClick {
        void result(Object key1, Object key2, Object key3);
    }

    public void setOnResultClick(OnResultClick onResultClick) {
        this.onResultClick = onResultClick;
    }

    public interface OnDisMiss {
        void disMiss();
    }

    public void setOnDisMissClick(OnDisMiss onDisMiss) {
        this.onDisMiss = onDisMiss;
    }

    protected abstract int popLayout();

    protected abstract void initView(PopViewHolder holder);


}
