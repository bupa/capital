package com.sgb.capital.base;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sgb.capital.utils.AdapterOnItemClick;
import com.sgb.capital.utils.ItemViewHolder;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Description:
 * Author zhengkewen
 * Time 2017/12/13
 */
public abstract class BaseRecycleAdapter<T> extends RecyclerView.Adapter<ItemViewHolder> {

    protected final static int GET = 1;
    protected final static int POST = 2;
    protected Context context;
    protected int layoutId;
    protected List<T> datas;
    protected LayoutInflater inflater;
    /**
     * 设置是否显示EmptyView
     */
    protected boolean showEmptyView = false;
    /**
     * 标识是否显示EmptyView
     */
    protected boolean isShowEmptyView = false;
    /**
     * 全部加载完毕是否显示底部View
     */
    protected boolean isShowEndView = false;
    /**
     * 是否添加了显示底部View的数据
     */
    protected boolean isAddShowEndViewData = false;
    /**
     * 是否添设置item选中状态
     */
    protected boolean isItemSelect = false;
    /**
     * 是否添设置item多选
     */
    protected boolean multiSelect = false;
    /**
     * 最多选择个数
     */
    protected int most = -1;

    protected int count = 0;
    protected OnItemClick onItemClick;
    protected OnItemMultiClick onItemMultiClick;
    protected OnItemMultiClick2 onItemMultiClick2;
    private int defItem = -1;
    private SparseBooleanArray checkList = new SparseBooleanArray();
    private List<T> checkDatas = new ArrayList<>();

    public BaseRecycleAdapter(Context context, int layoutId, List<T> datas) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.layoutId = layoutId;
        this.datas = datas;
        if (this.datas == null) {
            this.datas = new ArrayList<>();
        }
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        if (isItemSelect) {
            if (defItem == position) {
                holder.getItem().setSelected(true);
            } else {
                holder.getItem().setSelected(false);
            }
        }
        if (multiSelect) {
            if (checkList.size() > 0) {
                if (checkList.get(position)) {
                    holder.getItem().setSelected(true);
                } else {
                    holder.getItem().setSelected(false);
                }
            } else {
                holder.getItem().setSelected(false);
            }
        }
        holder.getmBinding().getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isItemSelect) {
//                    if (defItem == position) {
//                        defItem = -1;
//                        if (onItemClick != null) {
//                            onItemClick.onItemClick(null, position);
//                        }
//                    } else {
                    defItem = position;
                    if (onItemClick != null) {
                        onItemClick.onItemClick(datas.get(position), position);
                    }
//                    }
                    notifyDataSetChanged();
                } else if (multiSelect) {
                    if (!holder.getItem().isSelected()) {
                        if (checkDatas.size() + 1 > most && most != -1) {
                            Toast.makeText(context, "最多能选择" + most + "个选项", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        checkList.put(position, true);
                        checkDatas.add(datas.get(position));
                    } else {
                        checkList.put(position, false);
                        checkDatas.remove(datas.get(position));
                    }
                    if (onItemMultiClick != null) {
                        onItemMultiClick.onItemMultiClick(checkDatas, position);
                    }
                    if (onItemMultiClick2 != null) {
                        onItemMultiClick2.onItemMultiClick(checkDatas, position, holder);
                    }
                    notifyDataSetChanged();
                } else {
                    if (onItemClick != null) {
                        onItemClick.onItemClick(datas.get(position), position);
                    }
                }
            }
        });
        try {
            convert(holder, datas.get(position));
        } catch (Exception e) {
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ItemViewHolder.get(context, parent, layoutId);
    }

    public abstract void convert(ItemViewHolder holder, T t);

    @Override
    public int getItemCount() {
        if (getCount() != 0) {
            return datas.size() < getCount() ? datas.size() : getCount();
        }
        return datas.size();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> list) {
        if (list != null ) {
            int size = datas.size();
            datas.clear();
            notifyItemRangeRemoved(0, size);
            datas.addAll(list);
            notifyItemRangeChanged(0, list.size());
        }
        if (this.datas == null) {
            this.datas = new ArrayList<>();
            isShowEmptyView = true;
        } else {
            isShowEmptyView = false;
        }
        notifyDataSetChanged();
    }

    public void setDatasMayNull(List<T> list) {
        if (list != null) {
            int size = datas.size();
            datas.clear();
            notifyItemRangeRemoved(0, size);
            datas.addAll(list);
            notifyItemRangeChanged(0, list.size());
            isShowEmptyView = false;
        } else {
            if (this.datas == null) {
                isShowEmptyView = true;
            } else {
                isShowEmptyView = false;
            }
            this.datas = new ArrayList<>();
        }
        notifyDataSetChanged();
    }

    public void insertDataAtTop(T newData) {
        if (datas != null) {
            datas.add(0, newData);
        } else {
            datas = new ArrayList<>();
            datas.add(newData);
        }
        notifyDataSetChanged();
    }

    public void addDatas(List<T> newDatas) {
        if (this.datas == null) {
            this.datas = new ArrayList<>();
        }
        int last = datas.size();
        this.datas.addAll(newDatas);
        //      notifyItemInserted(last + 1);
        notifyDataSetChanged();
    }

    public void addData(T newData) {
        if (this.datas == null) {
            this.datas = new ArrayList<>();
        }
        this.datas.add(newData);
        notifyDataSetChanged();
    }

    public void addData(T newData, int index) {
        if (this.datas == null) {
            this.datas = new ArrayList<>();
        }
        this.datas.add(index, newData);
        notifyDataSetChanged();
    }

    public void setShowEmptyView(boolean showEmptyView) {
        this.showEmptyView = showEmptyView;
    }

    public void setIsShowEmptyView(boolean show) {
        if (!showEmptyView) return;
        isShowEmptyView = show;
        if (isShowEmptyView) {
            this.datas.clear();
            insertDataAtTop((T) new Object());
        }

    }

    public void setMost(int most) {
        this.most = most;
    }

    public void setItemSelect(boolean itemSelect) {
        isItemSelect = itemSelect;
    }

    public void setMultiSelect(boolean multiSelect) {
        this.multiSelect = multiSelect;
    }

    public boolean isShowEndView() {
        return isShowEndView;
    }

    public void setShowEndView(boolean showEndView) {
        isShowEndView = showEndView;
    }

    public void addEndViewData() {
        if (isAddShowEndViewData || !isShowEndView) return;
        isAddShowEndViewData = true;
        if (datas == null) {
            datas = new ArrayList<>();
        }
        datas.add((T) new Object());
    }

    public void removeEndViewData() {
        if (!isAddShowEndViewData || !isShowEndView) return;
        isAddShowEndViewData = false;
        if (datas != null && datas.size() > 0) {
            datas.remove(datas.size() - 1);
        }
    }

    public void removeItem(int position) {
        if (datas != null && datas.size() > position) {
            datas.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeItem(T t) {
        if (datas != null && datas.size() > 0) {
            datas.remove(t);
            notifyDataSetChanged();
        }
    }

    public void setDefItem(int defItem) {
        this.defItem = defItem;
        notifyDataSetChanged();
    }

    public void clearDatas() {
        this.datas.clear();
        notifyDataSetChanged();
    }

    public void setCheckDefault(int position) {
        checkList.put(position, true);
        checkDatas.add(datas.get(position));
        notifyDataSetChanged();
    }

    public void checkALLDatas() {
        if (datas == null) {
            return;
        }
        this.checkList.clear();
        this.checkDatas.clear();
        this.checkDatas.addAll(datas);
        for (int i = 0; i < datas.size(); i++) {
            checkList.put(i, true);
        }
        notifyDataSetChanged();
    }

    public void checkDatas(int i) {
        if (datas == null) {
            return;
        }
        checkDatas.add(datas.get(i));
        checkList.put(i, true);
        notifyDataSetChanged();
    }

    public List<T> getCheckDatas() {
        return checkDatas;
    }

    public void setCheckDefultDatas(List<T> checkDatas, SparseBooleanArray checkList) {
        this.checkDatas = checkDatas;
        this.checkList = checkList;
        notifyDataSetChanged();
    }

    public SparseBooleanArray getCheckList() {
        return checkList;
    }

    public void resetCheckDatas() {
        this.checkDatas.clear();
        this.checkList.clear();
        notifyDataSetChanged();
    }

    public void setOnItemClick(AdapterOnItemClick<T> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public void setOnItemMultiClick(OnItemMultiClick<T> onItemMultiClick) {
        this.onItemMultiClick = onItemMultiClick;
    }

    public void setOnItemMultiClick2(OnItemMultiClick2<T> onItemMultiClick2) {
        this.onItemMultiClick2 = onItemMultiClick2;
    }


    public interface OnItemClick<T> {
        void onItemClick(T t, int position);
    }

    public interface OnItemMultiClick<T> {
        void onItemMultiClick(List<T> tList, int position);
    }

    public interface OnItemMultiClick2<T> {
        void onItemMultiClick(List<T> tList, int position, ItemViewHolder view);
    }
}
