package com.sgb.capital.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.WindowManager;

import com.gyf.barlibrary.ImmersionBar;
import com.sgb.capital.utils.Utils;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


/**
 * 作者:张磊
 * 日期:2019/6/5
 * 说明:这个是不含状态栏的Activity基类
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected Context mContext;
    private ImmersionBar immersionBar;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        immersionBar = ImmersionBar.with(this)
                .statusBarDarkFont(false)
                .fitsSystemWindows(false)
                .keyboardEnable(false)
                .keyboardMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        immersionBar.init();
        mContext = this;
        Utils.setStatusBarLightMode(this, true);
        initView();
        // 初始化数据
        initData();
        // 初始化监听
        initListener();
        // 初始化观察者
        initObserve();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    protected abstract void initView();


    public void initData() {
    }


    public void initListener() {
    }

    public void initObserve() {
    }

}