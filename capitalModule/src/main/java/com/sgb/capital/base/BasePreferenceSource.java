package com.sgb.capital.base;

import com.sgb.capital.api.AllFavor;
import com.sgb.capital.api.Default;
import com.sgb.capital.api.Favor;

/**
 * Description:
 * Author zhengkewen
 * Time 2017/12/11
 */
@AllFavor
public interface BasePreferenceSource {

    @Favor("token")
    @Default("")
    String getToken();

    @Favor("token")
    void setToken(String token);

    @Favor("jwt")
    @Default("")
    String getJwt();

    @Favor("jwt")
    void setJwt(String jwt);
}
