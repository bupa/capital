package com.sgb.capital.callback;

import android.text.Editable;
import android.text.TextWatcher;


/**
 * author:张磊
 * createDate:2020/1/15
 * introduce: 输入监听回调封装
 * modifier:
 * modifiyDate:2020/12/10
 * modifiyContent:
 */


public abstract class MyZTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
