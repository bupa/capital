package com.sgb.capital.callback;

import com.sgb.capital.BuildConfig;

public class Constants {
    public static final int MODIFY_THE_PHONE_NUMBER = 0;
    public static final int EVENT_CLICK = 1;
    public static final int EVENT_REFRESH = 2;
    public static final int EVENT_HIDE = 3;
    public static final int EVENT_ALTER = 4;
    public static final int EVENT_UNBIND = 5;
    public static final int EVENT_SELECT = 6;
    public static final int EVENT_ALTER_COMPANY = 7;
    public static final int EVENT_FINISH = 8;//放弃支付关闭页面
    /** 确认发票跳转发票界面 */
    public static final int EVENT_CONFIRM_INVOICE_JUMP_ASIA_RECORD = 9;

    public static final String PAY_LICENCE = BuildConfig.BASE_URL + "v1/app/common/personRegisterProtocol";//支付协议
    public static final int EVENT_REFRESH_TIP = 9;
    public static final int ACTIVATE = 1000000001;
    public static final int CLICK_ZJ = 1000000002;
    public static final int CLICK_PXT = 1000000003;
    public static final int CLICK_XXB = 1000000004;
    public static final int CLICK_LL = 1000000005;
    public static final int EVENT_REFRESH_ORDER = 1000000006;
    public static final int TIMEOUT = 1000000007;
    public static final int SB_JSON = 1000000008;
    public static final int CLICK_HT = 1000000009;
    public static final int EVENT_HIDE_ORDERID = 1000000010;
    public static final int EVENT_OPERATE = 1000000011;
}
