package com.sgb.capital.callback;

/**
 * 作者:张磊
 * 日期:2019/8/20
 * 说明:点击确定,取消
 *
 */
public abstract class AbstractOnClickPopListener implements OnClickPopListener {

    @Override
    public void onClickSure() {}

    @Override
    public void onClickCancel() {}
}
