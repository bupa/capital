package com.sgb.capital.callback;

/**
 * 作者:张磊
 * 日期:2019/2/22 0022
 * 说明:用于事件接收消息类型
 */
public class ZMessageEvent {

    public int code; // 消息类型
    public Object data; // 消息数据
}
