package com.sgb.capital.callback;

/**
 * 作者:张磊
 * 日期:2019/8/20
 * 说明:点击确定,取消
 *
 */
public interface OnClickPopListener {

    void onClickSure();

    void onClickCancel();
}
