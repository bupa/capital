package com.sgb.capital.callback;

import com.google.android.material.tabs.TabLayout;

/**
 * 作者:张磊
 * 日期:2020/12/26 0026
 * 说明:
 */
public abstract class OnZTabSelectedListener implements TabLayout.OnTabSelectedListener{
    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}