package com.sgb.capital.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class ResolutionUtil {

    private static DisplayMetrics metrics;

    /**
     * dp2px(dp转px)
     */
    public static int dp2px(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }
    public static int dp2px(Context context, float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }
    /**
     * px2dp(px转dp)
     */
    public static int px2dp(Context context, int px) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px,
                context.getResources().getDisplayMetrics());
    }

    private static DisplayMetrics getDisplayMetrics(Context context) {
        if (metrics != null) {
            return metrics;
        }
        metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay()
                .getMetrics(metrics);
        return metrics;
    }

    public static int getScreenWidth(Context context) {
        return getDisplayMetrics(context).widthPixels; // 屏幕宽度（像素）
    }

    public static int getScreenHeight(Context context) {
        return getDisplayMetrics(context).heightPixels;// 屏幕高度（像素）
    }

    public static float getScreenDensity(Context context) {
        return getDisplayMetrics(context).density; // 屏幕密度（0.75 / 1.0 / 1.5）
    }

    public static int getScreenDensityDpi(Context context) {
        return getDisplayMetrics(context).densityDpi;// 屏幕密度DPI（120 / 160 / 240）
    }
}
