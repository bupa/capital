package com.sgb.capital.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * SP工具类
 */
public class SPreferenceUtil {
    public static final String resourcePCToken = "resourcePCToken";
    public static final String resourceAPPToken = "resourceAPPToken";
    public static final String PREFERENCE_NAME = "saveInfo";
    public static final String PREFERENCE_COMMON = "common";
    //当前城市id
    public static final String PROVINCE_ID = "provinceId";
    //当前城市的经度
    public static final String LONGITUDE_ID = "longitudeId";
    public static final String LONGITUDE = "longitude";
    //当前城市的维度
    public static final String LATITUDE_ID = "latitudeId";
    public static final String LATITUDE = "latitude";
    //登录token
    public static final String LOGIN_TOKEN = "loginToken";
    //pc token
    public static final String LOGIN_PC_TOKEN = "LOGIN_PC_TOKEN";
    //登录电话
    public static final String LOGIN_PHONE = "loginPhone";
    //登录人角色
    public static final String LOGIN_ROLE = "loginRole";
    //登录人姓名
    public static final String REAL_NAME = "realName";
    //登录人ID
    public static final String REAL_ID = "realId";
    //登录人身份证号
    public static final String ID_CARD = "idCard";
    //店铺id
    public static final String SHOP_ID = "shopId";
    //个人中心显示名称
    public static final String USER_NAME = "userName";
    //个人中心用户no
    public static final String USER_NO = "userNo";
    //个人中心公司名称
    public static final String COMPANY_NAME = "companyName";
    //个人中心公司no
    public static final String COMPANY_NO = "companyNo";
    //类型 0个人 1企业 2团队
    public static final String COM_TYPE = "com_type";
    //个人中心公司名称
    public static final String COMP_LOGO = "compLogo";
    //是否个人实名认证(boolean)
    public static final String PERSON_AUTH = "personAuth";
    //企业no
    public static final String ENTERPRISE_NO = "enterpriseNo";

    //是否司机认证
    public static final String DRIVER_AUTH = "driverAuth";

    //企业认证状态四种(NONE,STANDBY,FAIL,SUCCESS)
    public static final String ENTERPRISE_AUTH = "enterpriseAuth";

    //工队发布 保存的数据
    public static final String PACKERS_PUBLISH_DATA = "packers_publish_data";
    //设备发布 保存的数据
    public static final String DEVICE_PUBLISH_DATA = "device_publish_data";
    //机械发布 保存的数据
    public static final String MACHINE_PUBLISH_DATA = "machine_publish_data";

    //首页自定义 频道 保存的数据
    public static final String HOME_CHANNEL_DATA = "home_channel_data";
    //首页自定义 频道 服务器数据
    public static final String SYS_CHANNEL_DATA = "sys_channel_data";
    //首页自定义 切换样式
    public static final String SYS_HOME_DATA = "sys_home_data";

    //分享自定义 频道 保存的数据
    public static final String SHARE_CHANNEL_DATA = "share_channel_data";

    //货物类型
    public static final String GOODS_INFO_TYPE = "goodsInfoType";

    //货车类型
    public static final String CARS_INFO_TYPE = "carsInfoType";

    //货车长度
    public static final String CARS_INFO_LENGTH = "carsInfoLength";

    //运输方式
    public static final String TRANSPORT_TYPE = "transportType";

    //装卸方式
    public static final String CARS_HANDLING_TYPE = "carsHandlingType";

    //付款方式
    public static final String PAY_TYPE = "payType";

    //分享侧滑用户id
    public static final String SHARE_PALY_USERNO = "sharePalyUserno";

    //发票方式
    public static final String INVOICE_TYPE = "invoiceType";

    public static final String DEP_NAME = "DEP_NAME";
    // 司机认证的车牌号
    public static final String CAR_PUBLISH_NO_DRIVER_AUTHENTICATION = "CAR_PUBLISH_NO_DRIVER_AUTHENTICATION";

    // 切換地址
    public static final String URL = "url_set";
    //集采-搜索——历史搜索词汇
    public static final String JICAI_SEARCH_HISTORY = "JiCaiSearchHistoryWord";    // 分享推荐下标保存
    //租赁-搜索-历史搜索词汇
    public static final String ZULIN_SEARCH_HISTORY="ZuLinSearchHistoryWord";
    public static final String SHARE_HOME_POSITION = "ShareHomePosition";
    //手动刷新
    public static final String MANUAL_REFRESH = "manual_refresh";
    // 历史搜索
    public static final String HISTORY_SEARCH = "historySearch";
    // relevance 是否关联企业 0:未关联1:已关联
    public static final String RELEVANCE = "relevance";
    // 存储所有权限json数据,用于判断个人中心数据
    public static final String IS_DATA = "isData";
    // 存储所有子菜单权限json数据,用于判断子菜单权限
    public static final String CHILD_MENU = "childMenu";
    // 存储招采大搜索的本地历史纪录
    public static final String PICK_SEARCH = "pickSearch";

    //定位的城市
    public static final String City="city";
    public static final String Province="province";
    public static final String Area="area";
    public static final String AoiName="AoiName";

    public static final String FLAG_MESSAGE_NOTICE = "flag_message_notice"; //收到消息是否提示
    public static final String FLAG_PUSH_NOTICE = "flag_push_notice";       //推送通知是否提示
    public static final String APP_IS_FOREGROUND = "app_is_foreground";     //应用是否在前台
    public static final String IS_LIMIT_MULTI_LOGIN = "isLimitMultiLogin";     //是否被挤掉了

    public static final String GROUP_NUM = "GROUP_NUM";//群人数（用于显示当前群内消息当前的未读数计算）

    private static SharedPreferences mSharedPreferences;
    private static SharedPreferences commonSharedPreferences;
    private static SPreferenceUtil mSharedpreferenceUtil;
    private static SharedPreferences.Editor editor;
    private static SharedPreferences.Editor commonEditor;

    private SPreferenceUtil(Context cxt) {
        mSharedPreferences = cxt.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = mSharedPreferences.edit();

        commonSharedPreferences = cxt.getSharedPreferences(PREFERENCE_COMMON, Context.MODE_PRIVATE);
        commonEditor = commonSharedPreferences.edit();
    }

    public static synchronized void init(Context cxt) {
        if (mSharedpreferenceUtil == null) {
            mSharedpreferenceUtil = new SPreferenceUtil(cxt);
        }
    }

    /**
     * 单例模式，获取instance实例
     *
     * @return
     */
    public synchronized static SPreferenceUtil getInstance() {
        if (mSharedpreferenceUtil == null) {
            throw new RuntimeException("please init first!");
        }

        return mSharedpreferenceUtil;
    }

    final public void saveData(String key, String value) {
        editor.putString(key, value).commit();
    }

    final public void saveData(String key, int value) {
        editor.putInt(key, value).commit();
    }

    final public void saveData(String key, boolean value) {
        editor.putBoolean(key, value).commit();
    }

    final public void saveData(String key, float value) {
        editor.putFloat(key, value).commit();
    }

    final public void saveData(String key, long value) {
        editor.putLong(key, value).commit();
    }

    //
    final public void saveString(String key, String value) {
        editor.putString(key, value).commit();
    }

    final public String getString(String key, String defaultValue) {
        return mSharedPreferences.getString(key, defaultValue);
    }

    final public void saveBoolean(String key, boolean value) {
        editor.putBoolean(key, value).commit();
    }

    final public boolean getBoolean(String key, boolean defaultValue) {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    final public String getSaveStringData(String key, String defaultValue) {
        return mSharedPreferences.getString(key, defaultValue);
    }

    final public int getSaveIntData(String key, int defaultValue) {
        return mSharedPreferences.getInt(key, defaultValue);
    }

    final public boolean getSaveBooleanData(String key, boolean defaultValue) {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    final public float getSaveFloatData(String key, float defaultValue) {
        return mSharedPreferences.getFloat(key, defaultValue);
    }

    final public long getSaveLongData(String key, long defaultValue) {
        return mSharedPreferences.getLong(key, defaultValue);
    }

    final public void cleatData() {
        editor.clear().commit();
    }



    //字符串转数组
    final public String[] stringToArry(String str) {
        String[] arr = str.split(","); // 用,分割
        return arr;
    }
}
