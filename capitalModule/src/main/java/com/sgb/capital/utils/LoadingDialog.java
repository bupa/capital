package com.sgb.capital.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieCompositionFactory;
import com.sgb.capital.R;

import androidx.annotation.NonNull;

/**
 * Created by LT on 2018/5/12.
 */
public class LoadingDialog extends Dialog {

    private static final String TAG = "LoadingDialog";

    private String mMessage;
    private int mImageId;
    private boolean mCancelable;
    private boolean mIsH5;
    private RotateAnimation mRotateAnimation;
    private LottieAnimationView lottieAnimationView;
    private Context mContext;

    public LoadingDialog(@NonNull Context context, String message, int imageId) {
        this(context, R.style.LoadingDialog, message, imageId, false, false);
    }

    public LoadingDialog(@NonNull Context context, String message, int imageId, boolean isH5) {
        this(context, R.style.LoadingDialog, message, imageId, false, isH5);
    }

    public LoadingDialog(@NonNull Context context, int themeResId, String message, int imageId, boolean cancelable, boolean isH5) {
        super(context, themeResId);
        mMessage = message;
        mImageId = imageId;
        mCancelable = cancelable;
        mContext = context;
        mIsH5 = isH5;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        initView();
    }

    //清理数据
    public void removeAnim() {
        if (lottieAnimationView != null) {
            lottieAnimationView.clearAnimation();
        }
    }

    //设置动画
    public void setAnim(int rawResId) {
        LottieCompositionFactory.fromRawRes(getContext(), rawResId).addListener(result -> lottieAnimationView.setComposition(result));
    }

    private void initView() {
        // 设置窗口大小
        WindowManager windowManager = getWindow().getWindowManager();
        int screenWidth = windowManager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        //        attributes.alpha = 0.4f;
        if (mIsH5) {
            setContentView(R.layout.dialog_loading_ae_h5);
            attributes.width = screenWidth;
            attributes.height = attributes.width / 3;
        } else {
            setContentView(R.layout.dialog_loading_ae);
            attributes.width = screenWidth / 5;
            attributes.height = attributes.width;
        }
        setCancelable(mCancelable);

        TextView tv_loading = findViewById(R.id.tv_loading);
        lottieAnimationView = findViewById(R.id.lottieAnimationView);
        tv_loading.setText(mMessage);
        lottieAnimationView.measure(0, 0);

    }

    public void setMessage(String msg) {
        mMessage = msg;
    }

    @Override
    public void dismiss() {
//        mRotateAnimation.cancel();
        lottieAnimationView.cancelAnimation();
        super.dismiss();
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 屏蔽返回键
            return mCancelable;
        }
        return super.onKeyDown(keyCode, event);
    }
}
