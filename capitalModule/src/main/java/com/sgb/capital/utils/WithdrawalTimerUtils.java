package com.sgb.capital.utils;

import android.os.CountDownTimer;
import android.widget.TextView;

import com.sgb.capital.R;

public class WithdrawalTimerUtils extends CountDownTimer {
    private TextView mTextView; //显示倒计时的文字

    /**
     * @param textView          The TextView
     * @param millisInFuture     millisInFuture  从开始调用start()到倒计时完成
     *                           并onFinish()方法被调用的毫秒数。（译者注：倒计时时间，单位毫秒）
     * @param countDownInterval 接收onTick(long)回调的间隔时间。（译者注：单位毫秒）
     */
    public WithdrawalTimerUtils(TextView textView, long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
        this.mTextView = textView;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        mTextView.setClickable(false); //设置不可点击
        mTextView.setText(millisUntilFinished / 1000+"秒后可点此重新发送");  //设置倒计时时间
        mTextView.setTextColor(Utils.getColor(R.color.color_969696));
    }

    @Override
    public void onFinish() {
        mTextView.setText("重新获取");
        mTextView.setTextColor(Utils.getColor(R.color.color_5792FD));
        mTextView.setClickable(true);//重新获得点击
    }
}