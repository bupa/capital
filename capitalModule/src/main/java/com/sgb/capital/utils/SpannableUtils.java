package com.sgb.capital.utils;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

/**
 * Spannable 工具类
 * @author cyj
 */
public class SpannableUtils {

    public static SpannableString changePartTextColor(String content, String colorStr, int start, int end) {
        return changePartTextColor(content, colorStr, start, end, false);
    }

    /**
     * Spannable改变部分文字颜色
     * @param content    内容
     * @param colorStr   颜色字符串，如"FF4B10"
     * @param start      颜色改变开始位置
     * @param end        颜色改变结束位置
     * @return  SpannableString
     */
    public static SpannableString changePartTextColor(String content, String colorStr, int start, int end, boolean isBold) {
        SpannableString spanString = new SpannableString(content);
        spanString.setSpan(new ForegroundColorSpan(Color.parseColor(colorStr)),
                start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if(isBold) {
            spanString.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return spanString;// StyleSpan styleSpan_B  = new StyleSpan(Typeface.BOLD);
    }

    /**
     * Spannable改变最后一个文字颜色
     * @param content    内容
     * @param colorStr   颜色字符串，如"FF4B10"
     * @return  SpannableString
     */
    public static SpannableString changePartTextColorForLastOnce(String content, String colorStr) {
        return changePartTextColor(content, colorStr, content.length() - 1, content.length());
    }
}
