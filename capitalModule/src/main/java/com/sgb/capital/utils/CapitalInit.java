package com.sgb.capital.utils;

import android.app.Application;

import com.sgb.capital.api.UrlConfig;

/**
 * 作者:张磊
 * 日期:2021/5/25 0025
 * 说明:初始化组件
 */
public class CapitalInit {
    /**
     *
     * @param application
     * @param debugType
     */
    public static void init(Application application, String debugType) {
        Utils.init(application);
        SPreferenceUtil.init(application);
        UrlConfig.initUrlConfig(debugType);
        CryptoUtils.AES_KEY = debugType.contains("release") ? "gt3ZRqZuB5bdQxQ4" : "gt3ZRqZuB5bdQxQ4";
        System.out.println("key==" + CryptoUtils.AES_KEY);
        if (debugType.contains("release")) {
            MLog.DEBUG_LOG = false;
        } else {
            MLog.DEBUG_LOG = true;
        }
    }
} 