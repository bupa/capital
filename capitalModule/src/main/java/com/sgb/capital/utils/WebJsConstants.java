package com.sgb.capital.utils;

/**
 * js调用 方法
 */
public class WebJsConstants {
    public static final String Sync_Token = "syncToken";
    public static final String SEND_TITLE = "sendTitle";
    public static final String RECV_TOKEN = "receivedToken";
    public static final String CLOSE_PAGE = "closePage";
    public static final String KEY_PC_TOKEN = "resourcePCToken";
    public static final String KEY_APP_TOKEN = "resourceAPPToken";
    public static final String KEY_JWT_TOKEN = "loginToken";
    public static final String GO_TO_BID = "goToBid";
    public static final String GO_TO_COMPANY = "goToCompany";
    public static final String GO_TO_PHONE = "goToCallPhone";
    public static final String GO_TO_LOGIN = "toLogin";
    public static final String GO_BACK_LIST = "GoBackList";
    public static final String GO_TO = "goTo";
    public static final String GO_TO_MAP = "goMap";
    public static final String GO_TO_REPLY = "goToReply";
    public static final String GO_TO_REPORT = "goToReport";
    public static final String GO_TO_REPLY_RENT = "goToReplyRent";
    public static final String GO_TO_SERVICE = "goToRentService";
    public static final String GO_SERVICE = "goToService";
    public static final String GO_TO_TENDER = "goToTender"; //跳转招标详情
    public static final String OWNER_INQUIRY = "ownerInquiry";
    public static final String OWNERPLACE_ORDER = "ownerplaceOrder";
    public static final String WANTMAKEOFFER = "wantMakeOffer";
    public static final String WANTTAKEORDER = "wantTakeOrder";
    public static final String JUDGE_CALL_PHON = "judgeCallPhone";
    public static final String LEASE_ASK_QUOTE = "Quote";
    public static final String GO_TO_TEAM_MAP = "goTeamMap";
    public static final String GO_TO_ACCESSORY = "accessory";
    public static final String SHOP_PRICE_DETAILS = "amendQuote";
    public static final String COMPANYAPPROVE = "companyApprove";
    public static final String PROPERTY_EDIT = "propertyEdit";
    public static final String GO_TO_IM = "goToIMCommunication";
    public static final String OWNER_DEL_Order = "ownerDeleteOrder";
    public static final String OWNER_CONFIRM_ORDER = "cargoOwnerConfirmOrder";
    public static final String OWNER_CANCEL_ORDER = "cargoOwnerCancellation";
    public static final String OWNER_CANCE_LLATION = "ownerCancellation";
    public static final String OWNER_DEPARTAURE = "ownerDeparture";
    public static final String OWNER_ARRIVAL = "ownerArrival";
    public static final String GO_WEBSITE = "goWebsite";
    public static final String GO_INVITATION = "goInvitation";
    public static final String GO_NATIVE_SHEBEI = "nativeSheBei";
    public static final String GO_NATIVE_JIXIE = "nativeJiXie";
    public static final String GO_TO_COMPANY_SHOP = "goToCompanyShop";
    public static final String GO_TO_OFFER_OR_JOIN = "goToOfferOrJoin";
    public static final String GO_TO_PLACE_ORDER = "placeOrder";//H5详情下单事件
    public static final String GO_TO_ORDER_LIST = "goOrderList";//确认接单后,需要跳转至出租订单列表页
    public static final String GO_BACK_LEASE_LIST = "goBackLeaseList";//H5刷新个人中心列表
    public static final String GO_EDIT_INFO = "editInfo";  //编辑工队工人基础信息
    public static final String GO_EDIT_WORKER = "editworker"; //编辑工队工人工作经验
    public static final String GO_EDIT_FILE = "editfile";   //编辑工队工人资格证书
    public static final String GO_ADD_Worker = "addworker";//编辑工队工人工作经验
    public static final String GO_ADD_File = "addfile";  //编辑工队工人资格证书
    public static final String GO_GET_INVOLVED_NOW = "goGetInvolvedNow";//厂家集采详情拉起原生立即参与页面
    public static final String CALLEDHANDLER = "calledHandler";//原生主动调用H5方法
    public static final String CALLBACKHANDLER = "callbackHandler";//H5主动调用原生方法
    public static final String BACK_TO_DO_LIST="BackToDoList";//返回审批待办事件列表
    public static final String GO_APPLY_INFO="goApplyInfo";//申请信息页面
    public static final String GO_APPLY_APPROVAL_INFO="goApplyApprovalInfo";//申请审批信息页面
    public static final String GO_INVITE_RECORD="goInvitRecord";//邀请记录页面
    public static final String GO_APPLY_RECORD="goApplyRecord";//申请记录页面

    public static final String GO_Team_Edit="goTeamEdit";//工队编辑
    public static final String GO_Team_Performance="goTeamPerformance";//工队业绩详情
    public static final String GO_Team_Personnel="goTeamPersonnel";//工队人员
    public static final String GO_Team_Equipment="goTeamEquipment";//工队设备



}
