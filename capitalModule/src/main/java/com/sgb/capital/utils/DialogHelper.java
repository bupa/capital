package com.sgb.capital.utils;

import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.text.TextUtils;
import android.util.Log;

import java.lang.ref.WeakReference;

public class DialogHelper {

    private static WeakReference<LoadingDialog> sProgressDialogRef;

    public static LoadingDialog showProgressDialog(Context context, String message) {
        return showProgressDialog(context, null, message, 0, true, null);
    }

    public static LoadingDialog showProgressDialog(Context context, String message, int imageId) {
        return showProgressDialog(context, null, message, imageId, true, null);
    }

    public static LoadingDialog showProgressDialog(Context context, String message, int imageId, boolean cancelable) {
        return showProgressDialog(context, null, message, imageId, cancelable, null);
    }

    @Deprecated
    public static LoadingDialog showProgressDialog(Context context, String title, String message, int imageId,
                                                   boolean canCancelable, OnCancelListener listener) {
        LoadingDialog dialog = getDialog();
        if (dialog != null && dialog.getContext() != context) {
            // maybe existing dialog is running in a destroyed activity cotext we should recreate one
            dismissProgressDialog();
            MLog.e("dialog", "there is a leaked window here,orign context: " + dialog.getContext() + " now: " + context);
            dialog = null;
        }

        if (dialog == null) {
            dialog = new LoadingDialog(context, message, imageId);
            sProgressDialogRef = new WeakReference<>(dialog);
        }

        if (!TextUtils.isEmpty(title)) {
            dialog.setTitle(title);
        }
        if (!TextUtils.isEmpty(message)) {
            dialog.setMessage(message);
        }
        dialog.setCancelable(canCancelable);
        dialog.setOnCancelListener(listener);
        dialog.show();
        return dialog;
    }

    @Deprecated
    public static LoadingDialog showProgressDialog(Context context, String title, String message, int imageId,
                                                   boolean canCancelable, boolean isH5, OnCancelListener listener) {
        LoadingDialog dialog = getDialog();
        if (dialog != null && dialog.getContext() != context) {
            // maybe existing dialog is running in a destroyed activity cotext we should recreate one
            dismissProgressDialog();
            MLog.e("dialog", "there is a leaked window here,orign context: " + dialog.getContext() + " now: " + context);
            dialog = null;
        }

        if (dialog == null) {
            dialog = new LoadingDialog(context, message, imageId, isH5);
            sProgressDialogRef = new WeakReference<>(dialog);
        }

        if (!TextUtils.isEmpty(title)) {
            dialog.setTitle(title);
        }
        if (!TextUtils.isEmpty(message)) {
            dialog.setMessage(message);
        }
        dialog.setCancelable(canCancelable);
        dialog.setOnCancelListener(listener);
        dialog.show();
        return dialog;
    }

    public static void dismissProgressDialog() {
        LoadingDialog dialog = getDialog();
        if (null == dialog) {
            return;
        }
        sProgressDialogRef.clear();
        if (dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e("异常", e.getMessage());
                // maybe we catch IllegalArgumentException here.
            }
        }

    }

    public static void setMessage(String message) {
        LoadingDialog dialog = getDialog();
        if (null != dialog && dialog.isShowing() && !TextUtils.isEmpty(message)) {
            dialog.setMessage(message);
        }
    }


    public static boolean isShowing() {
        LoadingDialog dialog = getDialog();
        return (dialog != null && dialog.isShowing());
    }

    private static LoadingDialog getDialog() {
        return sProgressDialogRef == null ? null : sProgressDialogRef.get();
    }
}
