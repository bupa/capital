package com.sgb.capital.utils;

import java.security.SecureRandom;
import java.util.Random;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;

/**
 * 加解密工具类
 */
public class CryptoUtils {

    public String PRIVATE_KEY;

    public String PUBLIC_KEY;

    public static String AES_KEY;


    /**
     * RSA 解密
     *
     * @param str
     * @return
     */
    public String decryptRSA(String str) {
        PRIVATE_KEY = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJ4ZVQlJELXdIDFdFp8F4akTOA7E9WGVoCIfwrleKu6eOGxTSyiCZ4LqOOzX+u44hn5S2i+CX0Z8qWyA5BKhGS3AFcncglo2LtZa6BbAOLY1/GlkII/yRuPrQci+jzb1LA+LzV/lHXCng0/v6tAeJOpkkaes9zrH3PefMsJHIA0JAgMBAAECgYADc8/psZ7j68nezha2/wbCHY1zNB2cJYtvR1SGHf5vwsR+aBS7dBqMzy7STDaacmFMe6RwVmdcd48vE4L3fa1aK70wSh2lPE2caYkWF0gbh+RpGytGl3m+YPejJabfXCR20jc3JS8QP0GtA4GqqOVWdz5hsTN1ySEi01j67hDroQJBANFMjeJCIc9VeQYik4teLHluMGw9f+1GElF0TG6C+tyvrIwIX4OwSQeO0UhncSj/2SwXxeiIwuCnBtuUDiTFwSECQQDBYCjOJWo07JmzCqv+DvYMb6JhsYLFX6a+W8OtzMZeDpsATLUAJTCwwt/MZbbSQ3qzXyxSv4xYuWc1SdHujIbpAkEAub/aUApnHS37KZG4JkEe8Wn0eaufBEJi8X1oc8+0ufUDCohH1IS4W4fJfxum1z8xSyuSDgyJ/3zc3jUo1EPZYQJBAJjN7pCvWhSGHhIvzacNlXpQQIVoKsjig7WKd77/wISUkAEVnDWILciPxR8UBPpz3iKdgz3EJRf7McYXFi+llfkCQHd9SAidd8ZB5laR6xPXJWmlAt9/MSWbahlG1dME2BSYYjDMwnyxYeBnZLcu2cTkst5PZG5gNBX+RUd9e4BBjEc=";
        RSA rsa = new RSA(PRIVATE_KEY, null);
        byte[] decrypt = rsa.decrypt(str, KeyType.PrivateKey);
        return StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8);
    }

    /**
     * RSA 加密
     *
     * @param str
     * @return
     */
    public String encryptRSA(String str, String publicKey) {
        RSA rsa = new RSA(null, publicKey);
        byte[] encrypt = rsa.encrypt(str, KeyType.PublicKey);
        return Base64.encode(encrypt);
    }

    /**
     * AES 解密
     *
     * @param str 加密报文
     * @param key 密钥
     * @return
     */
    public String decryptAES(String str, String key) {
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, Base64.decode(key));
        return aes.decryptStr(str);
    }

    /**
     * AES 加密
     *
     * @param str
     * @param key
     * @return
     */
    public String encryptAES(String str, String key) {
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, Base64.decode(key));
        return aes.encryptBase64(str);
    }

    /**
     * AES/ECB 解密
     */
    public static String decryptAES_ECB(String str, String key) {
        AES aes = new AES(Mode.ECB, Padding.PKCS5Padding, key.getBytes());
        return aes.decryptStr(str);
    }

    /**
     * AES/ECB 加密
     */
    public static String encryptAES_ECB(String str, String key) {
        AES aes = new AES(Mode.ECB, Padding.PKCS5Padding, key.getBytes());
        return aes.encryptBase64(str);
    }

    /**
     * AES/ECB 解密
     */
    public static String decryptAES_ECB(String str) {
        AES aes = new AES(Mode.ECB, Padding.PKCS5Padding, AES_KEY.getBytes());
        return StringUtils.isEmpty(str) ? "" : aes.decryptStr(str);
    }

    /**
     * AES/ECB 加密
     */
    public static String encryptAES_ECB(String str) {
        AES aes = new AES(Mode.ECB, Padding.PKCS5Padding, AES_KEY.getBytes());
        return StringUtils.isEmpty(str) ? "" : aes.encryptBase64(str);
    }

    /**
     * 获取随机生成AES Key
     *
     * @return
     */
    public String getAESKey() {
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
        return Base64.encode(key);
    }


    /**
     * 获取key
     */
    public static String getKey() {
        StringBuilder uid = new StringBuilder();
        //产生16位的强随机数
        Random rd = new SecureRandom();
        for (int i = 0; i < 128 / 8; i++) {
            //产生0-2的3位随机数
            int type = rd.nextInt(3);
            switch (type) {
                case 0:
                    //0-9的随机数
                    uid.append(rd.nextInt(10));
                    break;
                case 1:
                    //ASCII在65-90之间为大写,获取大写随机
                    uid.append((char) (rd.nextInt(25) + 65));
                    break;
                case 2:
                    //ASCII在97-122之间为小写，获取小写随机
                    uid.append((char) (rd.nextInt(25) + 97));
                    break;
                default:
                    break;
            }
        }
        return uid.toString();
    }

    public static void main(String[] args) {
        CryptoUtils cryptoUtils = new CryptoUtils();
        String content = "hello";
//        // 随机生成AES 密钥 tCktxTGmvlkwQBqLO097raytuEJYkytn
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();

        String aesKey = Base64.encode(key);
//        String aesKey = StrUtil.str(key,CharsetUtil.CHARSET_UTF_8);
        System.out.println("AES密钥是:" + aesKey);
        String aesKey2 = cryptoUtils.getAESKey();
        System.out.println("ase2:" + aesKey2);
//
//        // AES加密
        String encryptAES = cryptoUtils.encryptAES("{\"id\":\"1\",\"name\":\"zhao\"}", aesKey2);
        System.out.println("AES加密内容：" + encryptAES);
        // AES解密
        System.out.println("AES解密内容：" + cryptoUtils.decryptAES("madOYKsUEwp7opXn3mRK3Nq1GtEvcptwnFeenC/+gqR18U68kaFiAO4Ez0EnCNdV2wYZGWpkt9oSvr6FrSHMprQM9d4JF9abM8vegJr1Bdqw3cKA1nYGXbXnoao44jcLVeex/Nh6roPebA5FrO4Nlj9lWJ5GnlGPXdk5IDdd2J3O5MO3AFzI6nsferO0qPogM9euso1pFaqh1K+UzHIvwt2SRI9pu3oMSMQ1j6p5+XqoQZf6eOom6of5VYD9cnftjNcxbOI3OKOwDGbTLOv/fD3nDg/PX7suiESaFO3RiPmrxWRHS9922+qK5M2EhOUkbd5T4FQuLBpy9VIbIN1pKJK/sH1CW+c8euDsxICwynj+/oYPdm0I4xwxtMSeu0yVsQXANVTKjkcgLKdNDYAmmS3sOD6Wm54DCKs2D3cQ8peAJnEBfPzofxVNZDdOEHT9yqEL9xLFnXRDzmlTG8mXb7c7BM4V+T7Nnpm/6s2Q1QyK+B7xq6eL8FoLf33SNxCQxClkh4H/YiNcKiAAeVXhlEGnfO+xeyB2hvy57Cp7uuzWWbi20/VrZQ58c9+XqNNRpRtktBbGWJCinwWN94XBh5xry28ieoHBJLda9RdRhAL18TketXHXSLHeQuEMQhN7jeRIWoravqprsWvG1b5NcO5kYdlh2KICu6rbR3fOIcQaZ0NjmxUM23WhDzzb3N7JcCoiRlS0hecICyeDHzuEs9ywhWm0PVTM0mUZzjQcRQ5BcBT++IwLTKxnlh6a4w7a", "gt3ZRqZuB5bdQxQ4jfgw3g=="));
        // RSA加密
        String encryptRSA = cryptoUtils.encryptRSA(content, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCeGVUJSRC13SAxXRafBeGpEzgOxPVhlaAiH8K5XirunjhsU0sogmeC6jjs1/ruOIZ+Utovgl9GfKlsgOQSoRktwBXJ3IJaNi7WWugWwDi2NfxpZCCP8kbj60HIvo829SwPi81f5R1wp4NP7+rQHiTqZJGnrPc6x9z3nzLCRyANCQIDAQAB");
        System.out.println("RSA加密内容：" + encryptRSA);
        // RSA解密
        String decryptRSA = cryptoUtils.decryptRSA("UTbSkmvYVYxsGwpKF2/nVAXqM+qyNl06QSTrKwK/BoZEiKzmtIp+oge7Xn8VHlQtHv6fyqrwcvj3LBIvkkYJyVUIQhXf1qmFMnUb462wdpqgFV6HRA1ZcFUosvow0kjmT8sIH8WE71/38ZG0aHkM1N8A9mGcfcBXvsHtV6V0mwE=");
        System.out.println("RSA解密内容：" + decryptRSA);


        // 构建
        AES aes = new AES(Mode.ECB, Padding.PKCS5Padding, "gt3ZRqZuB5bdQxQ4jfgw3g==".getBytes());
        // 加密
        byte[] encrypt = aes.encrypt(content);
        String encode = Base64.encode(encrypt);
        System.out.println("AES/ECB加密内容1：" + encode);
        // 解密
        String s = new String(aes.decrypt(Base64.decode(encode)));
        System.out.println("AES/ECB解密内容1：" + s);
        String s1 = cryptoUtils.encryptAES_ECB(content, "gt3ZRqZuB5bdQxQ4jfgw3g==");
        System.out.println("AES/ECB加密内容2：" + s1);
        String s2 = cryptoUtils.decryptAES_ECB(s1, "gt3ZRqZuB5bdQxQ4jfgw3g==");
        System.out.println("AES/ECB解密内容2：" + s2);
    }


}
