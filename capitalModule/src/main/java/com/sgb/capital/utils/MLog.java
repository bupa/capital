package com.sgb.capital.utils;

import android.util.Log;


/**
 * Description:
 * Author zhengkewen
 * Time 2017/12/11
 */
public class MLog {

    public static boolean DEBUG_LOG = true;
    public static final String symbol = "------>";
    public static final String tag = "sgb统计";

    public static void v(String msg) {
        if (DEBUG_LOG) {
            Log.v(tag, symbol + msg);
        }
    }

    public static void d(String msg) {
        if (DEBUG_LOG) {
            Log.d(tag, symbol + msg);
        }
    }

    public static void i(String msg) {
        if (DEBUG_LOG) {
            Log.i(tag, symbol + msg);
        }
    }

    public static void w(String msg) {
        if (DEBUG_LOG) {
            Log.w(tag, symbol + msg);
        }
    }

    public static void e(String msg) {
        if (DEBUG_LOG) {
            Log.e(tag, symbol + msg);
        }
    }

    public static void v(String tag, String msg) {
        if (DEBUG_LOG) {
            Log.v(tag, symbol + msg);
        }
    }

    public static void d(String tag, String msg) {
        if (DEBUG_LOG) {
            Log.d(tag, symbol + msg);
        }
    }

    public static void i(String tag, String msg) {
        if (DEBUG_LOG) {
            Log.i(tag, symbol + msg);
        }
    }

    public static void w(String tag, String msg) {
        if (DEBUG_LOG) {
            Log.w(tag, symbol + msg);
        }
    }

    public static void e(String tag, String msg) {
        if (DEBUG_LOG) {
            Log.e(tag, symbol + msg);
        }
    }
}
