package com.sgb.capital.api;

import com.sgb.capital.api.service.CapitalAPI;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * 作者:张磊
 * 日期:2021/4/12 0012
 * 说明:资金中心专属api
 */
public class FileManager {
    public static final String RESPONSE_OK = "ok";
    public static final String RESPONSE_CODE = "200";
    private static FileManager apiManager;

    /**
     * apiClass
     */
    private CapitalAPI mCapitalAPI;


    /**
     * 短响应时间请求 10秒超时
     */
    private OkHttpClient okHttpClient;
    private Retrofit localTestRetrofit;
    /**
     * 长响应时间请求 150秒超时
     */
    private OkHttpClient uploadHttpClient;

    /**
     * 接口初始化入口
     *
     * @return
     */
    public static FileManager getInstance() {
        if (apiManager == null) {
            apiManager = new FileManager();
        }
        return apiManager;
    }

    /**
     * 初始化
     * 使用默认的baseUrl
     */
    private FileManager() {
        if (okHttpClient == null || uploadHttpClient == null) {
            initOkHttpClient();
        }
        initRetrofit(UrlConfig.BASE_URL);
        initServiceClass();
    }


    /**
     * 初始化  OkHttpClient
     */
    private void initOkHttpClient() {
        OkHttpClient.Builder okHttpBuilder =
                new OkHttpClient.Builder()
                        .addInterceptor(new ZInterceptor(false));
        okHttpClient = okHttpBuilder.connectTimeout(10, TimeUnit.SECONDS).readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS).build();

        uploadHttpClient = okHttpBuilder.connectTimeout(150, TimeUnit.SECONDS).readTimeout(150, TimeUnit.SECONDS)
                .writeTimeout(150, TimeUnit.SECONDS).build();
    }

    /**
     * 初始化 Retrofit
     *
     * @param baseUrl 当前接口访问的域名
     */
    private void initRetrofit(String baseUrl) {
        localTestRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(CryptoConverterFactory.create())
                .build();

    }

    private void initServiceClass() {
        mCapitalAPI = localTestRetrofit.create(CapitalAPI.class);
    }

    public CapitalAPI getCapitalAPI() {
        return this.mCapitalAPI;
    }
}
