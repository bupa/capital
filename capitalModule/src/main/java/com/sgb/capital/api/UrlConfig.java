package com.sgb.capital.api;

import com.sgb.capital.BuildConfig;
import com.sgb.capital.utils.MLog;
import com.sgb.capital.utils.Utils;

public class UrlConfig {
    //服务器地址
    public static String BASE_URL = BuildConfig.BASE_URL;
    //资金中心单独域名
    public static String PAY_URL = BuildConfig.PAY_URL;

    public static void initUrlConfig(String type) {
        if (Utils.isEmpty(type)) return;
        if (type.contains("release")) {
            BASE_URL = "http://gateway.shigongbang.com/sgb-app-android/";
            //sgb-app
            PAY_URL = "http://gateway.shigongbang.com/";
        } else if (type.contains("pre")) {
            BASE_URL = "http://gateway.shigongbang.com/sgb-app-pre/";
            PAY_URL = "http://gateway.shigongbang.com/";
        } else {
            BASE_URL = "http://api.sgbwl.com/sgb-app/";
            PAY_URL = "http://api.sgbwl.com/";
        }
        MLog.d("接口", "BASE_URL==" + BASE_URL + "\t" + PAY_URL);
    }
}