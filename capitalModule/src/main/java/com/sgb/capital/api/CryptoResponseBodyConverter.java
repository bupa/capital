package com.sgb.capital.api;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.sgb.capital.utils.MLog;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * Description:
 * Author zhengkewen
 * Time 2017/12/11
 */
public class CryptoResponseBodyConverter<T> implements Converter<ResponseBody, T> {

    private final TypeAdapter<T> adapter;
    private final Gson gson;

    CryptoResponseBodyConverter(Gson gson, TypeAdapter<T> adapter) {
        this.gson = gson;
        this.adapter = adapter;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {


        String result = value.string();
        MLog.d("api", "response value : " + result);
        return adapter.fromJson(result);
    }
}
