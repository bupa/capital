package com.sgb.capital.api.service;

import com.sgb.capital.model.AccountBean;
import com.sgb.capital.model.AddAndEditCardsBean;
import com.sgb.capital.model.AsiaRecordBean;
import com.sgb.capital.model.BalanceDetailsEntity;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.model.BankTradeLimitBean;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.BindBankCardListEntity;
import com.sgb.capital.model.BindEnterpriseBankBean;
import com.sgb.capital.model.BindEnterpriseBankEntity;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.model.CapitalEntity;
import com.sgb.capital.model.CommoditiesBean;
import com.sgb.capital.model.CompanyEntity;
import com.sgb.capital.model.ContractBean;
import com.sgb.capital.model.ContractDetailBean;
import com.sgb.capital.model.CostQueryBean;
import com.sgb.capital.model.H5ReEntity;
import com.sgb.capital.model.InvoiceApplyBean;
import com.sgb.capital.model.InvoiceCompanyInfoBean;
import com.sgb.capital.model.InvoiceDetailBean;
import com.sgb.capital.model.InvoiceFeesBean;
import com.sgb.capital.model.InvoiceStatisticInfo;
import com.sgb.capital.model.ListBean;
import com.sgb.capital.model.LogBean;
import com.sgb.capital.model.LoginEntity;
import com.sgb.capital.model.OpenBusinessBean;
import com.sgb.capital.model.PayCardEntity;
import com.sgb.capital.model.PayDealBean;
import com.sgb.capital.model.PayDetailBean;
import com.sgb.capital.model.PayDetailEntity;
import com.sgb.capital.model.PayFlowBean;
import com.sgb.capital.model.PayFlowStatisticsEntity;
import com.sgb.capital.model.PayInfoEntity;
import com.sgb.capital.model.PayOrderBean;
import com.sgb.capital.model.PayOrderPageEntity;
import com.sgb.capital.model.PersonalBean;
import com.sgb.capital.model.PhoneBean;
import com.sgb.capital.model.QRcodeEntity;
import com.sgb.capital.model.SelBean;
import com.sgb.capital.model.UserAddressBean;
import com.sgb.capital.model.UserInfoEntity;
import com.sgb.capital.model.VCodeEntity;
import com.sgb.capital.model.WithdrawalBankCarEntity;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * 作者:张磊
 * 日期:2021/4/12 0012
 * 说明:
 */
public interface CapitalAPI {

    //上传单个文件
    @Multipart
    @POST("v1/app/common/fileUpload")
    Call<BaseEntity> uploadFile(@PartMap Map<String, RequestBody> files);

    //获取用户开户信息审核状态
    @GET("zjkj-pay/app/business/getProcess")
    Call<BaseEntity<CapitalEntity>> getProcess();

    //获取企业联动商户信息
    @POST("zjkj-pay/app/business/getBusinessInfoByCompany")
    Call<BaseEntity<BusinessInfoByCompanyEntity>> getBusinessInfoByCompany();

    //企业未开户时，远程调用个人中心获取企业信息
    @GET("zjkj-pay/app/business/getBusinessCompanyInfo")
    Call<BaseEntity<BusinessInfoByCompanyEntity>> getBusinessCompanyInfo();

    //获取流水统计
    @GET("zjkj-pay/app/payFlow/getPayFlowStatistics/{month}")
    Call<BaseEntity<PayFlowStatisticsEntity>> getPayFlowStatistics(@Path("month") String month);

    //绑定企业对公结算账户
    @POST("zjkj-pay/app/bind/bindEnterpriseBank")
    Call<BaseEntity<BindEnterpriseBankEntity>> bindEnterpriseBank(@Body Map map);

    //获取绑定结算账户列表
    @GET("zjkj-pay/app/bind/getBindBankCardList")
    Call<BaseEntity<List<BindBankCardListEntity>>> getBindBankCardList();

    //余额提现 获取验证码
    @GET("zjkj-pay/app/withdrawal/apply/sendCode")
    Call<BaseEntity> sendCode(@Query("phone") String phone);

    @POST("zjkj-pay/app/withdrawal/apply/verify")
    Call<BaseEntity> apply(@Body Map map);

    //获取快捷支付卡列表
    @GET("zjkj-pay/app/payBankCard/list/quick")
    Call<BaseEntity<List<PayCardEntity>>> getQuick();

    //获取流水详情
    @GET("zjkj-pay/app/payFlow/getPayFlowDetails")
    Call<BaseEntity<BalanceDetailsEntity>> getPayFlowDetails(@Query("flowNo") String flowNo);

    //快捷支付申请验证码
    @POST("zjkj-pay/app/pay/quickPayOrderApply")
    Call<BaseEntity> quickPayOrderApply(@Body Map map);

    //快捷支付确认验证码
    @POST("zjkj-pay/app/pay/quickPayOrderConfirm")
    Call<BaseEntity> quickPayOrderConfirm(@Body Map map);


    // 获取个人联动商户信息
    @POST("zjkj-pay/app/business/getBusinessInfoByUser")
    Call<BaseEntity<BusinessInfoByCompanyEntity>> getBusinessInfoByUser();

    // 生成收款二维码
    @POST("zjkj-pay/app/pay/receiveQRcode")
    Call<BaseEntity<QRcodeEntity>> receiveQRcode(@Body QRcodeEntity entity);

    // 轮询查询交易是否支付完成
    @GET("zjkj-pay/app/pay/queryPayState")
    Call<BaseEntity> queryPayState(@Query("tradeNo") String tradeNo);


    //竞价详情H5
    @FormUrlEncoded
    @POST("zjkj-pay/v1/app/common/getUrlSetting")
    Call<BaseEntity<H5ReEntity>> getUrlSetting(@Field("param") String param);

    // 流水收款查询
    @POST("zjkj-pay/app/payFlow/getReceivePaymentPayFlowList")
    Call<BaseEntity<PayFlowBean>> getReceivePaymentPayFlowList(@Body PayFlowBean bean);

    // 流水支付查询
    @POST("zjkj-pay/app/payFlow/getPaymentPayFlowList")
    Call<BaseEntity<PayFlowBean>> getPaymentPayFlowList(@Body PayFlowBean bean);

    // 流水详情
    @POST("zjkj-pay/app/payFlow/list")
    Call<BaseEntity<PayFlowBean>> list(@Body PayFlowBean bean);

    // 企业钱包，支付银行卡列表
    @GET("zjkj-pay/app/payBankCard/list")
    Call<BaseEntity<List<BankBean>>> payBankCardList();

    // 提现银行卡列表
    @GET("zjkj-pay/app/bind/getBindBankCardList")
    Call<BaseEntity<List<BankBean>>> getBindBankCardLists();

    // 个人中心 银行卡管理列表
    @GET("zjkj-pay/app/payBankCard/personalList")
    Call<BaseEntity<List<BankBean>>> personalList();

    // 解绑
    @DELETE("zjkj-pay/app/payBankCard")
    Call<BaseEntity> payBankCard(@Query("id") String id);

    // 账户明细
    @POST("zjkj-pay/app/business/tradingQuery")
    Call<BaseEntity<AccountBean>> tradingQuery(@Body AccountBean bean);

    // 获取费用列表
    @POST("zjkj-pay/app/fees/list")
    Call<BaseEntity<CostQueryBean>> feeslist(@Body CostQueryBean bean);

    // 待付订单
    @POST("zjkj-pay/app/orderPay/getPayOrderList")
    Call<BaseEntity<PayOrderPageEntity>> getPayOrderList(@Body PayOrderBean bean);

    // 待付订单支付数据
    @GET("zjkj-pay/app/pay/payInfo")
    Call<BaseEntity<PayInfoEntity>> payInfo(@Query("orderId") String orderId);

    // 待付订单支付数据
    @GET("zjkj-pay/app/orderPay/getPayOrderDetails")
    Call<BaseEntity<PayDetailEntity>> getPayOrderDetails(@Query("orderId") String orderId);

    // 异常订单
    @POST("zjkj-pay/app/orderPay/getAbnormalOrderList")
    Call<BaseEntity<PayOrderPageEntity>> getAbnormalOrderList(@Body PayOrderBean bean);

    // 添加和编辑银行卡
    @POST("zjkj-pay/app/payBankCard/save")
    Call<BaseEntity> save(@Body AddAndEditCardsBean bean);

    // 根据id查询银行卡信息
    @GET("zjkj-pay/app/payBankCard/detail")
    Call<BaseEntity<AddAndEditCardsBean>> detail(@Query("id") String id);

    // 根据id获取 提现银行卡信息
    @GET("zjkj-pay/app/bind/getBindBankCardById")
    Call<BaseEntity<WithdrawalBankCarEntity>> getBindBankCardById(@Query("id") String id);

    // 查询借记卡快捷支付银行列表
    @GET("zjkj-pay/app/query/getQpBank")
    Call<BaseEntity<List<BankEntity>>> getQpBank();

    // 企业子商户注册
    @POST("zjkj-pay/app/business/preAudit")
    Call<BaseEntity> preAudit(@Body OpenBusinessBean openBusinessBean);

    //查询个人信息
    @GET("memberCenter/api/user/getRealNameInfo")
    Call<BaseEntity<UserInfoEntity>> getRealNameInfo();

    //获取验证码
    @POST("zjkj-pay/app/bind/orderPersonalBank")
    Call<BaseEntity> getVCode(@Body VCodeEntity vCodeEntity);

    //确认绑卡
    @POST("zjkj-pay/app/bind/confirmPersonalBank")
    Call<BaseEntity> confirmPersonalBank(@Body Map map);

    //获取修改预留手机号验证码
    @GET("zjkj-pay/app/bind/modifyCode")
    Call<BaseEntity> modifyCode(@Query("phone") String phone);

    //获取修改预留手机号验证码
    @POST("zjkj-pay/app/bind/modifyCardPhone")
    Call<BaseEntity> modifyCardPhone(@Body Bean bean);


    // 获取注册个人账户信息验证码
    @POST("zjkj-pay/app/business/register/sendCode")
    Call<BaseEntity> send2Code(@Query("phone") String phone);

    // 获取注册个人账户信息验证码
    @POST("zjkj-pay/app/business/register/personal")
    Call<BaseEntity> personal(@Body PersonalBean bean);

    // 企业提现卡-获取绑定预留银行卡手机号验证码
    @GET("zjkj-pay/app/bind/applyCode")
    Call<BaseEntity> applyCode(@Query("phone") String phone);

    // 绑定企业提现卡支持银行列表
    @GET("zjkj-pay/app/query/getPublicBank")
    Call<BaseEntity<List<BankEntity>>> getPublicBank();

    // 绑定企业提现卡支持银行列表
    @POST("zjkj-pay/app/bind/bindEnterpriseBank")
    Call<BaseEntity> bindEnterpriseBank(@Body BindEnterpriseBankBean bean);

    // 获取注册个人账户信息验证码
    @GET("zjkj-pay/app/payBankCard/hasToBindBank")
    Call<BaseEntity> hasToBindBank(@Query("id") String id);

    // 个人提现卡-绑定个人提现银行卡,申请绑卡获取验证码
    @POST("zjkj-pay/app/bind/orderPersonalBank")
    Call<BaseEntity> orderPersonalBank(@Body PhoneBean bean);


    // 添加和编辑银行卡
    @POST("zjkj-pay/app/payBankCard/save")
    Call<BaseEntity> saveBankCard(@Body BankBean bean);


    // 当前用户是否企业用户
    @GET("zjkj-pay/business/isCompany")
    Call<BaseEntity> isCompany();

    // 待付订单金额和数量统计
    @GET("zjkj-pay/app/orderPay/statisticsNumberAndMoney")
    Call<BaseEntity<Bean>> statisticsNumberAndMoney();

    // 获取交易类型
    @GET("zjkj-pay/app/payFlow/getTradeTypeTree")
    Call<BaseEntity<List<SelBean>>> getTradeTypeTree();

    // 订单类型
    @GET("zjkj-pay/app/orderPay/getOrderType")
    Call<BaseEntity<List<Bean>>> getOrderType();


    //用户登陆
    @FormUrlEncoded
    @POST("v1/app/login")
    Call<BaseEntity<LoginEntity>> login(@Field("username") String username, @Field("password") String password);


    //我的界面获取账号所有企业
    @POST("v1/app/userInfo/getUserCompany")
    Call<BaseEntity<List<CompanyEntity>>> getUserCompany();


    //设置主企业
    @FormUrlEncoded
    @POST("v1/app/userInfo/settingMainCompany")
    Call<BaseEntity<LoginEntity>> settingMainCompany(@Field("no") String no, @Field("type") int type);


    //充值订单-快捷支付申请验证码
    @POST("zjkj-pay/app/recharge/quickPayOrderApply")
    Call<BaseEntity> rechargeQuickPayOrderApply(@Body Map map);

    //余额支付申请短信验证码
    @POST("/zjkj-pay/app/pay/balancePayApply")
    Call<BaseEntity> balancePayApply(@Body Map map);


    // 充值订单-收银台获取详情
    @GET("zjkj-pay/app/pay/recharge/payInfo")
    Call<BaseEntity<PayInfoEntity>> rechargePayInfo(@Query("orderId") String orderId);

    //充值申请
    @POST("zjkj-pay/app/recharge/apply")
    Call<BaseEntity> getApply(@Body Map map);


    //充值订单-快捷支付确认验证码
    @POST("zjkj-pay/app/recharge/quickPayOrderConfirm")
    Call<BaseEntity> rechargeQuickPayOrderConfirm(@Body Map map);

    //余额支付确认支付验证短信验证码
    @POST("/zjkj-pay/app/pay/balancePayConfirm")
    Call<BaseEntity> balancePayConfirm(@Body Map map);


    //资金中心-待付订单是否异常
    @GET("/zjkj-pay/app/orderPay/isAbnormalOrder")
    Call<BaseEntity> isAbnormalOrder(@Query("orderId") String orderId);


    //订单是否超过24小时
    @GET("/zjkj-pay/pay/orderOvertime/{orderId}")
    Call<BaseEntity> orderOvertime(@Path("orderId") String orderId);

    // 获取费用开票列表
    @POST("zjkj-pay/invoice/fees/list")
    Call<BaseEntity<InvoiceFeesBean>> invoiceFeesList(@Body InvoiceFeesBean bean);

    // 获取开票信息公司信息
    @GET("/zjkj-pay/app/invoice/getInvoiceCompanyInfo?page=1&limit=20")
    Call<BaseEntity<InvoiceCompanyInfoBean>> getInvoiceCompanyInfoList();

    // 查询收货地址（分页）pc端接口
    @GET("/zjkj-pay/app/invoice/getAddressList")
    Call<BaseEntity<List<UserAddressBean.Records>>> queryUserAddress();

    // 统计可开具流水数量和金额
    @POST("/zjkj-pay/invoice/getInvoiceStatisticInfo")
    Call<BaseEntity<InvoiceStatisticInfo>> getInvoiceStatisticInfo(@Body InvoiceStatisticInfo bean);

    // 开具发票申请
    @POST("/zjkj-pay/invoice/apply")
    Call<BaseEntity<InvoiceApplyBean>> invoiceApply(@Body InvoiceApplyBean bean);

    //发票列表
    @POST("/zjkj-pay/app/invoice/list")
    Call<BaseEntity<AsiaRecordBean>> invoiceList(@Body Map map);

    //获取状态下拉框接口
    @GET("/zjkj-pay/app/invoice/getSelectOption")
    Call<BaseEntity<List<SelBean>>> getSelectOption();

    //获取详情
    @GET("/zjkj-pay/app/invoice/getInvoiceDetail/{invoiceId}")
    Call<BaseEntity<InvoiceDetailBean>> getInvoiceDetail(@Path("invoiceId") String invoiceId);

    //检验状态是否改变
    @GET("/zjkj-pay/app/invoice/verifyStatusChange/{invoiceId}/{targetStatus}")
    Call<BaseEntity> verifyStatusChange(@Path("invoiceId") String invoiceId, @Path("targetStatus") String targetStatus);

    //撤销申请
    @POST("/zjkj-pay/app/invoice/revoke")
    Call<BaseEntity> revoke(@Body Map map);

    //待付订单列表
    @POST("/zjkj-pay/app/bill/getToBePayOrderList")
    Call<BaseEntity<PayOrderPageEntity>> getToBePayOrderList(@Body PayOrderBean bean);

    //已付订单列表
    @POST("/zjkj-pay/app/bill/getPayOrderList")
    Call<BaseEntity<PayOrderPageEntity>> getPay2OrderList(@Body PayOrderBean bean);

    //应收订单列表
    @POST("/zjkj-pay/app/bill/getReceivableOrderList")
    Call<BaseEntity<PayOrderPageEntity>> getReceivableOrderList(@Body PayOrderBean bean);

    //已收订单列表
    @POST("/zjkj-pay/app/bill/getReceivedOrderList")
    Call<BaseEntity<PayOrderPageEntity>> getReceivedOrderList(@Body PayOrderBean bean);

    //获取筛选类型
    @GET("/zjkj-pay/app/bill/getSelectTree")
    Call<BaseEntity<List<SelBean>>> getSelectTree();

    //已付转账列表
    @POST("/zjkj-pay/app/bill/getTransferInfoList")
    Call<BaseEntity<PayOrderPageEntity>> getTransferInfoList(@Body PayOrderBean bean);

    //已收转账列表
    @POST("/zjkj-pay/app/bill/getReceivedTransferOrderList")
    Call<BaseEntity<PayOrderPageEntity>> getReceivedTransferOrderList(@Body PayOrderBean bean);

    // 账单详情
    @GET("/zjkj-pay/app/bill/getPayOrderDetails/{orderId}")
    Call<BaseEntity<PayDetailBean>> getBillPayOrderDetails(@Path("orderId") String orderId);

    // 订单号 查询订单商品列表
    @GET("/zjkj-app/v1/app/user/order/commodities/{id}")
    Call<BaseEntity<List<CommoditiesBean>>> commodities(@Path("id") String id);

    // 合同
    @POST("/zjkj-app/v1/contract/search")
    Call<BaseEntity<ContractDetailBean>> contract(@Body ContractBean bean);

    // 日志
    @GET("/zjkj-pay/app/bill/getOrderLog")
    Call<BaseEntity<List<LogBean>>> getOrderLog(@Query("orderId") String orderId);

    // 订单解冻
    @POST("/zjkj-pay/app/bill/finish")
    Call<BaseEntity> finish(@Body Map map);

    // 查询
    @GET("/zjkj-pay/app/bill/getPayStatus/{orderId}")
    Call<BaseEntity> getPayStatus(@Path("orderId") String orderId);

    // 查询
    @GET("/zjkj-pay/app/bill/getTransferInfo/{id}")
    Call<BaseEntity<PayDetailBean>> getTransferInfo(@Path("id") String id);

    // 个人未开户时，远程调用个人中心获取个人信息
    @GET("/zjkj-pay/app/business/getBusinessUserInfo")
    Call<BaseEntity<BusinessInfoByCompanyEntity>> getBusinessUserInfo();


    // 个人未开户时，远程调用个人中心获取个人信息
    @GET("/zjkj-pay/app/pay/payInfo")
    Call<BaseEntity<PayDealBean>> payInfo(@Query("orderId") String orderId, @Query("payType") String payType);

    // 复核人员名称
    @GET("/zjkj-pay/app/reviewer/list")
    Call<BaseEntity<List<ListBean>>> list();


    // 支付风险校验
    @GET("/zjkj-pay/app/pay/payValidation")
    Call<BaseEntity> payValidation(@Query("tradeNo") String tradeNo ,@Query("reviewerNo") String reviewerNo   );


    // 交易限额
    @GET("/zjkj-pay/app/bankTradeLimit/list")
    Call<BaseEntity<List<BankTradeLimitBean>>> bankTradeLimitList();

    // 订单是实时状态
    @GET("/zjkj-pay/app/bill/getOrderPayStatus/{orderId}")
    Call<BaseEntity> getOrderPayStatus(@Path("orderId") String orderId);
}
