package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.model.BalanceDetailsEntity;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.view.ui.activity.WithdrawalDetailActivity;
import com.sgb.capital.view.widget.MToast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:chent
 * 日期:2021/5/12 提现详情
 * 说明:资金中心
 */
public class WithdrawalDetailModel extends ViewModel {
    public MutableLiveData<BalanceDetailsEntity> mData = new MutableLiveData<>();

    public void getPayFlowDetails(WithdrawalDetailActivity activity,String flowNo) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getPayFlowDetails(flowNo).enqueue(new Callback<BaseEntity<BalanceDetailsEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BalanceDetailsEntity>> call, Response<BaseEntity<BalanceDetailsEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mData.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BalanceDetailsEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }
}