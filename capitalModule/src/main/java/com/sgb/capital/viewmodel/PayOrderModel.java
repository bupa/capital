package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.PayDetailBean;
import com.sgb.capital.model.PayDetailEntity;
import com.sgb.capital.model.PayInfoEntity;
import com.sgb.capital.model.PayOrderBean;
import com.sgb.capital.model.PayOrderEntity;
import com.sgb.capital.model.PayOrderPageEntity;
import com.sgb.capital.model.SelBean;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.MToast;

import java.util.List;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * chent 2021/5/6
 */
public class PayOrderModel extends ViewModel {

    public MutableLiveData<List<PayOrderEntity>> mPayOrderList = new MutableLiveData<>();
    public MutableLiveData<List<Bean>> mBean = new MutableLiveData<>();
    public MutableLiveData<String> mCode = new MutableLiveData<>();
    public MutableLiveData<PayDetailEntity> payDetails = new MutableLiveData<>();
    public MutableLiveData<PayDetailBean> mPayDetailBean = new MutableLiveData<>();
    public MutableLiveData<PayInfoEntity> mPayInfo = new MutableLiveData<>();
    public MutableLiveData<List<SelBean>> mSelBean = new MutableLiveData<>();

    /**
     * 待付订单列表
     *
     * @param context
     * @param bean
     */
    public void getPayOrderList(BaseActivity context, PayOrderBean bean) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getPayOrderList(bean).enqueue(new Callback<BaseEntity<PayOrderPageEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayOrderPageEntity>> call, Response<BaseEntity<PayOrderPageEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayOrderList.setValue(response.body().getData().list);
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayOrderPageEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }

    /**
     * 获取支付详情
     *
     * @param context
     * @param orderId
     */
    public void getPayOrderDetails(BaseActivity context, String orderId) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getPayOrderDetails(orderId).enqueue(new Callback<BaseEntity<PayDetailEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayDetailEntity>> call, Response<BaseEntity<PayDetailEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    payDetails.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayDetailEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }

    /**
     * 订单详情
     *
     * @param context
     * @param orderId
     */
    public void getBillPayOrderDetails(BaseActivity context, String orderId) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBillPayOrderDetails(orderId).enqueue(new Callback<BaseEntity<PayDetailBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayDetailBean>> call, Response<BaseEntity<PayDetailBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayDetailBean.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayDetailBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }

    /**
     * 获取支付信息
     *
     * @param context
     * @param orderId
     */
    public void payInfo(BaseActivity context, String orderId) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().payInfo(orderId).enqueue(new Callback<BaseEntity<PayInfoEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayInfoEntity>> call, Response<BaseEntity<PayInfoEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayInfo.setValue(response.body().getData());
                } else {
                    mPayInfo.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayInfoEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }


    /**
     * 获取业务类型
     *
     * @param activity
     */
    public void getOrderType(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getOrderType().enqueue(new Callback<BaseEntity<List<Bean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<Bean>>> call, Response<BaseEntity<List<Bean>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBean.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<Bean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 资金中心-待付订单是否异常
     *
     * @param activity
     */
    public void isAbnormalOrder(BaseActivity activity, String orderId) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().isAbnormalOrder(orderId).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                } else {
                 /*   mPayInfo.setValue(null);
                    MToast.showToast(activity, response.body().getMsg());*/
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 待付订单
     *
     * @param activity
     */
    public void getToBePayOrderList(BaseActivity activity, PayOrderBean payOrderBean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getToBePayOrderList(payOrderBean).enqueue(new Callback<BaseEntity<PayOrderPageEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayOrderPageEntity>> call, Response<BaseEntity<PayOrderPageEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayOrderList.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayOrderPageEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 已付订单列表
     *
     * @param activity
     */
    public void getPay2OrderList(BaseActivity activity, PayOrderBean payOrderBean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getPay2OrderList(payOrderBean).enqueue(new Callback<BaseEntity<PayOrderPageEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayOrderPageEntity>> call, Response<BaseEntity<PayOrderPageEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayOrderList.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayOrderPageEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 应收订单列表
     *
     * @param activity
     */
    public void getReceivableOrderList(BaseActivity activity, PayOrderBean payOrderBean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getReceivableOrderList(payOrderBean).enqueue(new Callback<BaseEntity<PayOrderPageEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayOrderPageEntity>> call, Response<BaseEntity<PayOrderPageEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayOrderList.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayOrderPageEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 已收订单列表
     *
     * @param activity
     */
    public void getReceivedOrderList(BaseActivity activity, PayOrderBean payOrderBean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getReceivedOrderList(payOrderBean).enqueue(new Callback<BaseEntity<PayOrderPageEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayOrderPageEntity>> call, Response<BaseEntity<PayOrderPageEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayOrderList.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayOrderPageEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 获取筛选类型
     *
     * @param activity
     */
    public void getSelectTree(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getSelectTree().enqueue(new Callback<BaseEntity<List<SelBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<SelBean>>> call, Response<BaseEntity<List<SelBean>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mSelBean.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<SelBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 已付转账列表
     *
     * @param activity
     */
    public void getTransferInfoList(BaseActivity activity, PayOrderBean payOrderBean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getTransferInfoList(payOrderBean).enqueue(new Callback<BaseEntity<PayOrderPageEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayOrderPageEntity>> call, Response<BaseEntity<PayOrderPageEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayOrderList.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayOrderPageEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 已收转账列表
     *
     * @param activity
     */
    public void getReceivedTransferOrderList(BaseActivity activity, PayOrderBean payOrderBean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getReceivedTransferOrderList(payOrderBean).enqueue(new Callback<BaseEntity<PayOrderPageEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayOrderPageEntity>> call, Response<BaseEntity<PayOrderPageEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayOrderList.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayOrderPageEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 已收转账列表
     *
     * @param activity
     */
    public void finish(BaseActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().finish(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCode.setValue("1");
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }
}
