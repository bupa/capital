package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.view.ui.fragment.BankCardsFragment;
import com.sgb.capital.view.widget.MToast;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/4/7 0007
 * 说明:企业银行卡
 */
public class BankCardsModel extends ViewModel {


    public MutableLiveData<List<BankBean>> mListMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<List<BankBean>> mBindBankCardLists = new MutableLiveData<>();
    public MutableLiveData<BusinessInfoByCompanyEntity> mBusinessInfoByCompanyEntity = new MutableLiveData<>();
    /**
     * 企业资金中心，支付银行卡列表
     *
     * @param fragment
     */
    public void payBankCardList(BankCardsFragment fragment) {
        BaseActivity activity = (BaseActivity) fragment.getActivity();
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().payBankCardList().enqueue(new Callback<BaseEntity<List<BankBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<BankBean>>> call, Response<BaseEntity<List<BankBean>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mListMutableLiveData.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<BankBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 提现银行卡列表
     *
     * @param fragment
     */
    public void getBindBankCardLists(BankCardsFragment fragment) {
        // tip : 修改方法名,实体类
        BaseActivity activity = (BaseActivity) fragment.getActivity();
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBindBankCardLists().enqueue(new Callback<BaseEntity<List<BankBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<BankBean>>> call, Response<BaseEntity<List<BankBean>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBindBankCardLists.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<BankBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    public void payBankCard(BankCardsFragment fragment, String id) {
        // tip : 修改方法名,实体类
        BaseActivity activity = (BaseActivity) fragment.getActivity();
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().payBankCard(id).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    fragment.payBankCard(true);
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }



    // 获取企业信息
    public void getBusinessInfoByCompany(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByCompany().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBusinessInfoByCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

}