package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.model.BankTradeLimitBean;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.view.widget.MToast;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/9/18 0018
 * 说明:
 */
public class DealLimitModel extends ViewModel {
    public MutableLiveData<List<BankTradeLimitBean>> mDatas = new MutableLiveData<>();

    /**
     * 交易限额
     *
     * @param activity
     */
    public void bankTradeLimitList(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().bankTradeLimitList().enqueue(new Callback<BaseEntity<List<BankTradeLimitBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<BankTradeLimitBean>>> call, Response<BaseEntity<List<BankTradeLimitBean>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mDatas.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<BankTradeLimitBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

} 