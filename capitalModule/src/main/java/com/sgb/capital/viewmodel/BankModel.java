package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.model.AddAndEditCardsBean;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.BindEnterpriseBankBean;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.model.CapitalEntity;
import com.sgb.capital.model.PhoneBean;
import com.sgb.capital.model.UserInfoEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.MToast;

import java.util.List;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/4/7 0007
 * 说明:企业银行卡
 */
public class BankModel extends ViewModel {

    public MutableLiveData<CapitalEntity> mCapitalEntity = new MutableLiveData<>();
    public MutableLiveData<List<BankEntity>> mBank = new MutableLiveData<>();
    public MutableLiveData<BaseEntity> mBaseEntity = new MutableLiveData<>();
    public MutableLiveData<String> mIsWithdraw = new MutableLiveData<>();
    public MutableLiveData<String> mCode = new MutableLiveData<>();
    public MutableLiveData<BusinessInfoByCompanyEntity> mBusinessInfoByCompanyEntity = new MutableLiveData<>();
    public MutableLiveData<UserInfoEntity> mUserInfoEntity = new MutableLiveData<>();

    /**
     * 查询个人快捷支付银行列表
     */
    public void getQpBank(BaseActivity context) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getQpBank().enqueue(new Callback<BaseEntity<List<BankEntity>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<BankEntity>>> call, Response<BaseEntity<List<BankEntity>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBank.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<BankEntity>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }


    /**
     * 添加银行卡
     *
     * @param activity
     */
    public void save(BaseActivity activity, AddAndEditCardsBean bean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().save(bean).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBaseEntity.setValue(response.body());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 编辑或添加银行卡
     *
     * @param activity
     */
    public void saveBankCard(BaseActivity activity, BankBean bean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().saveBankCard(bean).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBaseEntity.setValue(response.body());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 企业提现卡-获取绑定预留银行卡手机号验证码
     *
     * @param activity
     */
    public void applyCode(BaseActivity activity, String phone) {
        CapitalManager.getInstance().getCapitalAPI().applyCode(phone).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(Utils.getContext(), "验证码已发送");
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 绑定企业提现卡支持银行列表
     *
     * @param activity
     */
    public void getPublicBank(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getPublicBank().enqueue(new Callback<BaseEntity<List<BankEntity>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<BankEntity>>> call, Response<BaseEntity<List<BankEntity>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBank.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<BankEntity>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 绑定企业提现银行卡
     *
     * @param activity
     */
    public void bindEnterpriseBank(BaseActivity activity, BindEnterpriseBankBean bean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().bindEnterpriseBank(bean).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(activity, response.message());
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBaseEntity.setValue(response.body());
                } else {
                    if (response.body().getCode().contains("E00000")) {
                        if (response.body().getMsg().contains("未查到开户行支行")) {
                            mBaseEntity.setValue(null);
                        } else {
                            MToast.showToast(Utils.getContext(), response.body().getMsg());
                        }
                    } else {
                        mCode.setValue("0");
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    // 获取企业信息
    public void getBusinessInfoByCompany(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByCompany().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBusinessInfoByCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    public void getProcess(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getProcess().enqueue(new Callback<BaseEntity<CapitalEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<CapitalEntity>> call, Response<BaseEntity<CapitalEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCapitalEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(Utils.getContext(), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<CapitalEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 是否可设置为提现银行卡
     */
    public void hasToBindBank(BaseActivity activity, String id) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().hasToBindBank(id).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mIsWithdraw.setValue("1");
                } else {
                    DialogHelper.dismissProgressDialog();
                    mIsWithdraw.setValue("0");
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
            }
        });
    }


    /**
     * 个人提现卡-绑定个人提现银行卡,确认绑卡
     *
     * @param activity
     */
    public void confirmPersonalBank(BaseActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().confirmPersonalBank(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(activity, response.message());
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCode.setValue("1");
                } else {
                    if (response.body().getCode().contains("E00000")) {
                        if (response.body().getMsg().contains("未查到开户行支行")) {
                        } else {
                            MToast.showToast(Utils.getContext(), response.body().getMsg());
                        }
                    } else {
                        mCode.setValue("0");
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 设置提现银行卡发送验证码
     *
     * @param activity
     */
    public void orderPersonalBank(BaseActivity activity, PhoneBean bean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().orderPersonalBank(bean).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "银行卡信息校验失败，请检查银行卡信息是否正确！");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mIsWithdraw.setValue("6");
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

}