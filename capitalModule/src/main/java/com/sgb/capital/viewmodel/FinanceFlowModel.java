package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.model.BalanceDetailsEntity;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.PayFlowBean;
import com.sgb.capital.model.RecordsBean;
import com.sgb.capital.model.SelBean;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.fragment.FinanceFlowFragment;
import com.sgb.capital.view.widget.MToast;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/4/7 0007
 * 说明:财务流水
 */
public class FinanceFlowModel extends ViewModel {


    public MutableLiveData<List<RecordsBean>> mPayFlowBeanMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<List<Bean>> mBean = new MutableLiveData<>();
    public MutableLiveData<List<SelBean>> mSelBean = new MutableLiveData<>();
    public MutableLiveData<BalanceDetailsEntity> mData = new MutableLiveData<>();
    /**
     * 财务流水列表
     *
     * @param fragment
     * @param bean
     */
    public void list(FinanceFlowFragment fragment, PayFlowBean bean) {
        // tip : 修改方法名,实体类
        BaseActivity activity = (BaseActivity) fragment.getActivity();
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().list(bean).enqueue(new Callback<BaseEntity<PayFlowBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayFlowBean>> call, Response<BaseEntity<PayFlowBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    mPayFlowBeanMutableLiveData.setValue(null);
                    MToast.showToast(activity, "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayFlowBeanMutableLiveData.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayFlowBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 获取业务类型
     *
     * @param activity
     */
    public void getTradeType(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getTradeTypeTree().enqueue(new Callback<BaseEntity<List<SelBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<SelBean>>> call, Response<BaseEntity<List<SelBean>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mSelBean.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<SelBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 获取流水详情
     *
     * @param activity
     */
    public void getPayFlowDetails(BaseActivity activity,String flowNo) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getPayFlowDetails(flowNo).enqueue(new Callback<BaseEntity<BalanceDetailsEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BalanceDetailsEntity>> call, Response<BaseEntity<BalanceDetailsEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null){
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mData.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BalanceDetailsEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

}