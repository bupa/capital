package com.sgb.capital.viewmodel;


import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.model.PersonalBean;
import com.sgb.capital.model.QRcodeEntity;
import com.sgb.capital.model.UserInfoEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.activity.QrCodeActivity;
import com.sgb.capital.view.widget.MToast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/3/15 0015
 * 说明:资金中心1.3.9
 */
public class QrCodeModel extends ViewModel {
    // 生成收款二维码
    public MutableLiveData<QRcodeEntity> mQRcodeEntityMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<BusinessInfoByCompanyEntity> mBusinessInfoByCompanyEntity = new MutableLiveData<>();
    public MutableLiveData<UserInfoEntity> mUserInfoEntity = new MutableLiveData<>();
    public MutableLiveData<BaseEntity> mBaseEntity = new MutableLiveData<>();


    public void receiveQRcode(QrCodeActivity activity, QRcodeEntity entity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().receiveQRcode(entity).enqueue(new Callback<BaseEntity<QRcodeEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<QRcodeEntity>> call, Response<BaseEntity<QRcodeEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mQRcodeEntityMutableLiveData.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<QRcodeEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 轮询查询交易是否支付完成
    public void queryPayState(QrCodeActivity activity, String tradeNo) {
        CapitalManager.getInstance().getCapitalAPI().queryPayState(tradeNo).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    activity.queryPayState((Boolean) response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    // 获取修改预留手机号验证码
    public void modifyCode(String phone) {
        CapitalManager.getInstance().getCapitalAPI().modifyCode(phone).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(Utils.getContext(), "验证码已发送");
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
            }
        });
    }

    // 修改预留手机号
    public void modifyCardPhone(BaseActivity activity, Bean bean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().modifyCardPhone(bean).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                Utils.sendMsg(Constants.ACTIVATE, null);
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBaseEntity.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
            }
        });
    }


    /**
     * 获取个人商户信息
     *
     * @param activity
     */
    public void getBusinessInfoByUser(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByUser().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBusinessInfoByCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 获取用户未开户信息
     *
     * @param activity
     */
    public void getRealNameInfo(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getRealNameInfo().enqueue(new Callback<BaseEntity<UserInfoEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<UserInfoEntity>> call, Response<BaseEntity<UserInfoEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getCode().equals(CapitalManager.RESPONSE_CODE)) {
                    mUserInfoEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<UserInfoEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 个人注册发送验证码
     *
     * @param activity
     */
    public void send2Code(BaseActivity activity, String phone) {
        CapitalManager.getInstance().getCapitalAPI().send2Code(phone).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(activity, "验证码已发送");
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 个人子商户 注册提交
     *
     * @param activity
     */
    public void personal(BaseActivity activity, PersonalBean bean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().personal(bean).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                Utils.sendMsg(Constants.ACTIVATE, null);
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBaseEntity.setValue(response.body());
                } else {
                    mBaseEntity.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 认证资料
     *
     * @param activity
     */
    public void getBusinessInfoByCompany(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByCompany().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBusinessInfoByCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }
}