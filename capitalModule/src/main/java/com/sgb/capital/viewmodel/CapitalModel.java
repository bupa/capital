package com.sgb.capital.viewmodel;

import android.util.Base64;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.api.FileManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.model.CapitalEntity;
import com.sgb.capital.model.CompanyEntity;
import com.sgb.capital.model.LoginEntity;
import com.sgb.capital.model.PayFlowStatisticsEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.SPreferenceUtil;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.activity.CapitalActivity;
import com.sgb.capital.view.widget.MToast;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/4/12 0012
 * 说明:资金中心
 */

public class CapitalModel extends ViewModel {
    public MutableLiveData<CapitalEntity> mCapitalEntity = new MutableLiveData<>();
    public MutableLiveData<Bean> mBean = new MutableLiveData<>();
    public MutableLiveData<BusinessInfoByCompanyEntity> mBusinessInfoByCompanyEntity = new MutableLiveData<>();
    public MutableLiveData<BusinessInfoByCompanyEntity> mUserBusinessInfoByCompanyEntity = new MutableLiveData<>();
    public MutableLiveData<PayFlowStatisticsEntity> mPayFlowStatisticsEntity = new MutableLiveData<>();
    public MutableLiveData<Boolean> mIsCompany = new MutableLiveData<>();
    public MutableLiveData<List<CompanyEntity>> mCompanyEntity = new MutableLiveData<>();

    // 开户信息
    public void getProcess(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getProcess().enqueue(new Callback<BaseEntity<CapitalEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<CapitalEntity>> call, Response<BaseEntity<CapitalEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCapitalEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(Utils.getContext(), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<CapitalEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 企业信息
    public void getBusinessInfoByCompany(CapitalActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByCompany().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBusinessInfoByCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 支付流水
    public void getPayFlowStatistics(CapitalActivity activity, String month) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getPayFlowStatistics(month).enqueue(new Callback<BaseEntity<PayFlowStatisticsEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayFlowStatisticsEntity>> call, Response<BaseEntity<PayFlowStatisticsEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayFlowStatisticsEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayFlowStatisticsEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    // 1.3.8 个人 资金中心接口
    public void getBusinessInfoByUser(CapitalActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByUser().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mUserBusinessInfoByCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    public void isCompany(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().isCompany().enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mIsCompany.setValue((Boolean) response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 待付订单金额和数量统计
     *
     * @param activity
     */
    public void statisticsNumberAndMoney(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().statisticsNumberAndMoney().enqueue(new Callback<BaseEntity<Bean>>() {
            @Override
            public void onResponse(Call<BaseEntity<Bean>> call, Response<BaseEntity<Bean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBean.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<Bean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    // 登录
    public void login(BaseActivity activity, String account, String pwd) {
        DialogHelper.showProgressDialog(activity, null, "登录中...", 0, false, null).setCanceledOnTouchOutside(false);
        FileManager.getInstance().getCapitalAPI().login(account, Base64.encodeToString(pwd.getBytes(), Base64.DEFAULT)).enqueue(new Callback<BaseEntity<LoginEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<LoginEntity>> call, Response<BaseEntity<LoginEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                SPreferenceUtil.getInstance().saveData(SPreferenceUtil.resourceAPPToken, response.body().getData().getToken());
                SPreferenceUtil.getInstance().saveData(SPreferenceUtil.resourcePCToken, response.body().getData().getResourcePCToken());
                mIsCompany.setValue(true);
            }

            @Override
            public void onFailure(Call<BaseEntity<LoginEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 公司列表信息
     */
    public void getCompanyList(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "登录中...", 0, false, null).setCanceledOnTouchOutside(false);
        FileManager.getInstance().getCapitalAPI().getUserCompany().enqueue(new Callback<BaseEntity<List<CompanyEntity>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<CompanyEntity>>> call, Response<BaseEntity<List<CompanyEntity>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<CompanyEntity>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 设置主企业
     *
     * @param companyEntity
     */
    public void settingMainCompany(BaseActivity activity, CompanyEntity companyEntity) {
        FileManager.getInstance().getCapitalAPI().settingMainCompany(companyEntity.getCompNo(), companyEntity.getType()).enqueue(new Callback<BaseEntity<LoginEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<LoginEntity>> call, Response<BaseEntity<LoginEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    SPreferenceUtil.getInstance().saveData(SPreferenceUtil.resourcePCToken, response.body().getData().getResourcePCToken());
                    getProcess(activity);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<LoginEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }
}