package com.sgb.capital.viewmodel;


import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.BindBankCardListEntity;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.view.ui.activity.WithdrawalBalanceActivity;
import com.sgb.capital.view.widget.MToast;

import java.util.List;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WithdrawalBalanceModel extends ViewModel {

    public MutableLiveData<String> applyCode = new MutableLiveData<>();//申请验证码
    public MutableLiveData<BusinessInfoByCompanyEntity> balance = new MutableLiveData<>();//余额
    public MutableLiveData<Bean> applyWithdrawal = new MutableLiveData<>();//申请提现
    public MutableLiveData<List<BindBankCardListEntity>> mBankCardListEntityMutableLiveData = new MutableLiveData<>();//银行卡列表
    // 获取个人余额
    public void getPersonBalance(WithdrawalBalanceActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByUser().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    balance.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }
    // 获取企业余额
    public void getCompanyBalance(WithdrawalBalanceActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByCompany().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    balance.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    public void getBindBankCardList(WithdrawalBalanceActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBindBankCardList().enqueue(new Callback<BaseEntity<List<BindBankCardListEntity>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<BindBankCardListEntity>>> call, Response<BaseEntity<List<BindBankCardListEntity>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBankCardListEntityMutableLiveData.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<BindBankCardListEntity>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 申请验证码
    public void applyCode(WithdrawalBalanceActivity activity, String phone) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().sendCode(phone).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    applyCode.setValue("1");
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }
    // 余额提现
    public void apply(WithdrawalBalanceActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().apply(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    Bean bean = new Bean(response.body().getData().toString(),1);
                    applyWithdrawal.setValue(bean);
                } else {
                    DialogHelper.dismissProgressDialog();
                    Bean bean = new Bean(response.body().getCode(),2);
                    applyWithdrawal.setValue(bean);
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

}
