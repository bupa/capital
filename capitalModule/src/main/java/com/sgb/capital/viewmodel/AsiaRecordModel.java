package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.AsiaRecordBean;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.InvoiceDetailBean;
import com.sgb.capital.model.SelBean;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.MToast;

import java.util.List;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/4/7 0007
 * 说明:个人银行卡
 */
public class AsiaRecordModel extends ViewModel {

    public MutableLiveData<List<AsiaRecordBean.ListDTO>> mAsiaRecordBean = new MutableLiveData<>();
    public MutableLiveData<List<SelBean>> mSelBean = new MutableLiveData<>();
    public MutableLiveData<InvoiceDetailBean> mInvoiceDetailBean = new MutableLiveData<>();
    public MutableLiveData<String> mCode = new MutableLiveData<>();


    /**
     * 发票列表
     *
     * @param activity
     */
    public void invoiceList(BaseActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().invoiceList(map).enqueue(new Callback<BaseEntity<AsiaRecordBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<AsiaRecordBean>> call, Response<BaseEntity<AsiaRecordBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mAsiaRecordBean.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<AsiaRecordBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 获取状态下拉框接口
     *
     * @param activity
     */
    public void getSelectOption(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getSelectOption().enqueue(new Callback<BaseEntity<List<SelBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<SelBean>>> call, Response<BaseEntity<List<SelBean>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mSelBean.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<SelBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 获取详情
     *
     * @param activity
     */
    public void getInvoiceDetail(BaseActivity activity, String invoiceId) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getInvoiceDetail(invoiceId).enqueue(new Callback<BaseEntity<InvoiceDetailBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<InvoiceDetailBean>> call, Response<BaseEntity<InvoiceDetailBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mInvoiceDetailBean.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<InvoiceDetailBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 检验状态是否改变
     *
     * @param activity
     */
    public void verifyStatusChange(BaseActivity activity, String invoiceId, String targetStatus) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().verifyStatusChange(invoiceId, targetStatus).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCode.setValue((boolean) response.body().getData() ? "1" : "0");
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 撤销申请
     *
     * @param activity
     */
    public void revoke(BaseActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().revoke(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(activity, "您已成功撤销申请!");
                    Utils.sendMsg(Constants.EVENT_REFRESH, null);
                    activity.finish();
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


}