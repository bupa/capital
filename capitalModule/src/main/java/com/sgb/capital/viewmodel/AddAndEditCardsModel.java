package com.sgb.capital.viewmodel;

import android.content.Context;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.AddAndEditCardsBean;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.CapitalEntity;
import com.sgb.capital.model.UserInfoEntity;
import com.sgb.capital.model.VCodeEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.activity.AddPersonBankCardActivity;
import com.sgb.capital.view.widget.MToast;

import java.util.List;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:陈涛
 * 日期:2021/5/10
 * 说明:收银台银行卡
 */
public class AddAndEditCardsModel extends ViewModel {

    public MutableLiveData<AddAndEditCardsBean> mCarInfo = new MutableLiveData<>();
    public MutableLiveData<List<BankEntity>> mBank = new MutableLiveData<>();
    public MutableLiveData<UserInfoEntity> mUserInfo = new MutableLiveData<>();
    public MutableLiveData<BaseEntity> mBaseEntity = new MutableLiveData<>();
    public MutableLiveData<String> saveSuccess = new MutableLiveData<>();
    public MutableLiveData<String> setWithdrawal = new MutableLiveData<>();
    public MutableLiveData<String> code = new MutableLiveData<>();
    public MutableLiveData<CapitalEntity> openAccount = new MutableLiveData<>();

    /**
     * 添加和编辑收银台银行卡
     */
    public void payBankCardList(BaseActivity context, AddAndEditCardsBean bean) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().save(bean).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                Utils.sendMsg(Constants.ACTIVATE, null);
                if (response.body() == null) {
                    MToast.showToast(context, response.message());
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    saveSuccess.setValue("1");
                } else {
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }

    /**
     * 添加和编辑收银台银行卡
     */
    public void payBankCardList(AddPersonBankCardActivity context, AddAndEditCardsBean bean) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().save(bean).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(context, response.message());
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    Utils.sendMsg(Constants.EVENT_REFRESH, null);
                    context.finish();
                } else {
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }

    /**
     * 查询银行卡信息
     */
    public void getCarInfo(BaseActivity context, String id) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().detail(id).enqueue(new Callback<BaseEntity<AddAndEditCardsBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<AddAndEditCardsBean>> call, Response<BaseEntity<AddAndEditCardsBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCarInfo.setValue(response.body().getData());
                } else {
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<AddAndEditCardsBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }

    /**
     * 查询快捷支付银行列表
     */
    public void getQpBank(BaseActivity context) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getQpBank().enqueue(new Callback<BaseEntity<List<BankEntity>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<BankEntity>>> call, Response<BaseEntity<List<BankEntity>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(context, response.message());
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBank.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<BankEntity>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }

    /**
     * 查询快捷支付银行列表
     */
    public void getQpBank(AddPersonBankCardActivity context) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getQpBank().enqueue(new Callback<BaseEntity<List<BankEntity>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<BankEntity>>> call, Response<BaseEntity<List<BankEntity>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(context, response.message());
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBank.setValue(response.body().getData());
                } else {
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<BankEntity>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }

    /**
     * 查询用户信息
     */
    public void getUserInfo() {
        CapitalManager.getInstance().getCapitalAPI().getRealNameInfo().enqueue(new Callback<BaseEntity<UserInfoEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<UserInfoEntity>> call, Response<BaseEntity<UserInfoEntity>> response) {
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode().equals(CapitalManager.RESPONSE_CODE)) {
                    mUserInfo.setValue(response.body().getData());
                } else {
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<UserInfoEntity>> call, Throwable t) {

            }
        });
    }

    /**
     * 获取短信验证码
     */
    public void getVCode(BaseActivity context, VCodeEntity vCodeEntity) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);

        CapitalManager.getInstance().getCapitalAPI().getVCode(vCodeEntity).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "银行卡信息校验失败，请检查银行卡信息是否正确！");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    code.setValue("1");
                    MToast.showToast(context, "验证码已发送");
                } else {
                    MToast.showToast(Utils.getContext(), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
            }
        });
    }

    /**
     * 是否可设置为提现银行卡
     */
    public void hasToBindBank(Context context, String id) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);

        CapitalManager.getInstance().getCapitalAPI().hasToBindBank(id).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), response.message());
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    setWithdrawal.setValue("1");
                } else {
                    DialogHelper.dismissProgressDialog();
                    setWithdrawal.setValue("0");
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
            }
        });
    }

    //获取开户状态
    public void getProcess(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getProcess().enqueue(new Callback<BaseEntity<CapitalEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<CapitalEntity>> call, Response<BaseEntity<CapitalEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), response.message());
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    openAccount.setValue(response.body().getData());
                } else {
                    MToast.showToast(Utils.getContext(), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<CapitalEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 绑定提现银行卡，验证
     *
     * @param activity
     */
    public void bandCar(BaseActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);

        CapitalManager.getInstance().getCapitalAPI().confirmPersonalBank(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(activity, response.message());
                    return;
                }
                if (response.body().getCode().equals(CapitalManager.RESPONSE_CODE)) {
                    mBaseEntity.setValue(response.body());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                MToast.showToast(activity, t.getMessage());
                DialogHelper.dismissProgressDialog();
            }
        });
    }
}