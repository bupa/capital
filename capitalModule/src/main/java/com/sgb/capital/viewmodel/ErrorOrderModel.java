package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.PayOrderBean;
import com.sgb.capital.model.PayOrderEntity;
import com.sgb.capital.model.PayOrderPageEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.view.widget.MToast;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ErrorOrderModel extends ViewModel {

    public MutableLiveData<List<PayOrderEntity>> mErrorOrderList = new MutableLiveData<>();
    public MutableLiveData<List<Bean>> mBean = new MutableLiveData<>();
    public void getAbnormalOrderList(BaseActivity activity, PayOrderBean bean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getAbnormalOrderList(bean).enqueue(new Callback<BaseEntity<PayOrderPageEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayOrderPageEntity>> call, Response<BaseEntity<PayOrderPageEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    mErrorOrderList.setValue(new ArrayList<>());
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mErrorOrderList.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayOrderPageEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 获取业务类型
     *
     * @param activity
     */
    public void getOrderType(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getOrderType().enqueue(new Callback<BaseEntity<List<Bean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<Bean>>> call, Response<BaseEntity<List<Bean>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mBean.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<Bean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }



}
