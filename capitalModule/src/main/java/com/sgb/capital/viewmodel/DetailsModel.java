package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.api.File2Manager;
import com.sgb.capital.api.FileManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.model.CommoditiesBean;
import com.sgb.capital.model.ContractBean;
import com.sgb.capital.model.ContractDetailBean;
import com.sgb.capital.model.ListBean;
import com.sgb.capital.model.LogBean;
import com.sgb.capital.model.PayDealBean;
import com.sgb.capital.model.PayDetailBean;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.activity.PayDealActivity;
import com.sgb.capital.view.widget.MToast;

import java.util.List;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/9/6 0006
 * 说明:详情
 */
public class DetailsModel extends ViewModel {
    public MutableLiveData<List<CommoditiesBean>> mDatas = new MutableLiveData<>();
    public MutableLiveData<List<LogBean>> mLogBean = new MutableLiveData<>();
    public MutableLiveData<List<ContractDetailBean.ListDTO>> mContractDetailBean = new MutableLiveData<>();
    public MutableLiveData<PayDetailBean> mPayDetailBean = new MutableLiveData<>();
    public MutableLiveData<Double> mCode = new MutableLiveData<>();
    public MutableLiveData<String> mState = new MutableLiveData<>();
    public MutableLiveData<BusinessInfoByCompanyEntity> infoByCompanyEntityMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<BusinessInfoByCompanyEntity> mUserBusinessInfoByCompanyEntity = new MutableLiveData<>();
    public MutableLiveData<PayDealBean> mPayDealBean = new MutableLiveData<>();
    public MutableLiveData<List<ListBean>> mListBean = new MutableLiveData<>();

    /**
     * 商品
     *
     * @param activity
     */
    public void commodities(BaseActivity activity, String id) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        FileManager.getInstance().getCapitalAPI().commodities(id).enqueue(new Callback<BaseEntity<List<CommoditiesBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<CommoditiesBean>>> call, Response<BaseEntity<List<CommoditiesBean>>> data) {
                DialogHelper.dismissProgressDialog();
                if (data.body() == null) return;
                if (data.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mDatas.setValue(data.body().getData());
                } else {
                    MToast.showToast(activity, data.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<CommoditiesBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 合同
     *
     * @param activity
     */
    public void contract(BaseActivity activity, ContractBean bean) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        File2Manager.getInstance().getCapitalAPI().contract(bean).enqueue(new Callback<BaseEntity<ContractDetailBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<ContractDetailBean>> call, Response<BaseEntity<ContractDetailBean>> data) {
                DialogHelper.dismissProgressDialog();
                if (data.body() == null) return;
                if (data.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mContractDetailBean.setValue(data.body().getData().list);
                } else {
                    MToast.showToast(activity, data.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<ContractDetailBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 日志
     *
     * @param activity
     */
    public void getOrderLog(BaseActivity activity, String orderId) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getOrderLog(orderId).enqueue(new Callback<BaseEntity<List<LogBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<LogBean>>> call, Response<BaseEntity<List<LogBean>>> data) {
                DialogHelper.dismissProgressDialog();
                if (data.body() == null) return;
                if (data.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mLogBean.setValue(data.body().getData());
                } else {
                    MToast.showToast(activity, data.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<LogBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 查询订单状态
     *
     * @param activity
     */
    public void getPayStatus(BaseActivity activity, String orderId) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getPayStatus(orderId).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> data) {
                DialogHelper.dismissProgressDialog();

                if (data.body() == null) return;
                if (data.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCode.setValue((Double) data.body().getData());
                } else {
                    MToast.showToast(activity, data.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 已收转账列表
     *
     * @param activity
     */
    public void finish(BaseActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().finish(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCode.setValue((double) 7);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 订单详情
     *
     * @param context
     * @param orderId
     */
    public void getBillPayOrderDetails(BaseActivity context, String orderId) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBillPayOrderDetails(orderId).enqueue(new Callback<BaseEntity<PayDetailBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayDetailBean>> call, Response<BaseEntity<PayDetailBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayDetailBean.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(context, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayDetailBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }


    // 企业未开户时，远程调用个人中心获取企业信息
    public void getBusinessCompanyInfo(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessCompanyInfo().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    infoByCompanyEntityMutableLiveData.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                    // TODO: 2021/6/23 0023
                    Utils.postDelay(() -> {
                        Utils.sendMsg(Constants.EVENT_HIDE, null);
                        activity.finish();
                    }, 1000);
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
            }
        });
    }


    // 企业未开户时，远程调用个人中心获取企业信息
    public void getTransferInfo(BaseActivity activity, String id) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getTransferInfo(id).enqueue(new Callback<BaseEntity<PayDetailBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayDetailBean>> call, Response<BaseEntity<PayDetailBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayDetailBean.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                    // TODO: 2021/6/23 0023
                    Utils.postDelay(() -> {
                        Utils.sendMsg(Constants.EVENT_HIDE, null);
                        activity.finish();
                    }, 1000);

                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayDetailBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
            }
        });
    }


    // 1.3.8 个人 资金中心接口
    public void getBusinessUserInfo(BaseActivity activity) {
        CapitalManager.getInstance().getCapitalAPI().getBusinessUserInfo().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mUserBusinessInfoByCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 支付信息
    public void payInfo(PayDealActivity activity, String orderId, String payType) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().payInfo(orderId, payType).enqueue(new Callback<BaseEntity<PayDealBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayDealBean>> call, Response<BaseEntity<PayDealBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayDealBean.setValue(response.body().getData());
                } else {
                    activity.show2ErrorCode(5, "重复充值");
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayDealBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 复核人员名称
    public void list(BaseActivity activity) {
        CapitalManager.getInstance().getCapitalAPI().list().enqueue(new Callback<BaseEntity<List<ListBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<ListBean>>> call, Response<BaseEntity<List<ListBean>>> response) {
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mListBean.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<ListBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 支付风险校验
    public void payValidation(BaseActivity activity, String tradeNo,String reviewerNo) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().payValidation(tradeNo,reviewerNo).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body()==null)return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mState.setValue("");
                } else {
                    mState.setValue(response.body().getCode());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    // 余额支付申请短信验证码
    public void balancePayApply(PayDealActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().balancePayApply(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "联动订单编号不能为空");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(activity, "验证码已发送");
                    activity.setApplyCount(response.body().getData() + "");
                } else {
                    activity.showErrorCode(response.body().getCode(),response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    public void getBusinessInfoByUser(BaseActivity activity) {
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByUser().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mUserBusinessInfoByCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 余额支付确认支付验证短信验证码
    public void balancePayConfirm(PayDealActivity mActivity, Map map) {
        DialogHelper.showProgressDialog(mActivity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().balancePayConfirm(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mActivity.set2Confirm();
                } else {
                    if ("E00466311".equals(response.body().getCode())) {
                        mActivity.show2ErrorCode(2, "");
                    } else if ("E00466337".equals(response.body().getCode())) {
                        mActivity.show2ErrorCode(6, response.body().getMsg());
                    } else if ("E00466464".equals(response.body().getCode())) {
                        mActivity.show2ErrorCode(4, "验证码已过期");
                    } else if ("E000007".equals(response.body().getCode())) {
                        mActivity.show2ErrorCode(5, "重复充值");
                    } else {
                        mActivity.show2ErrorCode(3, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(mActivity, t.getMessage());
            }
        });
    }



    /**
     * 查询订单状态
     *
     * @param activity
     */
    public void getOrderPayStatus(BaseActivity activity, String orderId) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getOrderPayStatus(orderId).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> data) {
                DialogHelper.dismissProgressDialog();
                if (data.body() == null) return;
                if (data.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCode.setValue((Double) data.body().getData());
                } else {
                    MToast.showToast(activity, data.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


}