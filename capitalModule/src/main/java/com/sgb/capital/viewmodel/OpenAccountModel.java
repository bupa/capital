package com.sgb.capital.viewmodel;

import android.content.Context;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.api.FileManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.model.OpenBusinessBean;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.MToast;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:chentao
 * 日期:2021/5/12
 * 说明:资金中心
 */
public class OpenAccountModel extends ViewModel {
    public MutableLiveData<BusinessInfoByCompanyEntity> infoByCompanyEntityMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<String> stringMutableLiveData = new MutableLiveData<>();

    // 企业未开户时，远程调用个人中心获取企业信息
    public void getBusinessCompanyInfo(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessCompanyInfo().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    infoByCompanyEntityMutableLiveData.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                    // TODO: 2021/6/23 0023
                    Utils.postDelay(() -> {
                        Utils.sendMsg(Constants.EVENT_HIDE, null);
                        activity.finish();
                    },1000);

                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
            }
        });
    }

    //企业子商户提交预审
    public void preAudit(OpenBusinessBean bean, Context activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);

        CapitalManager.getInstance().getCapitalAPI().preAudit(bean).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                Utils.sendMsg(Constants.ACTIVATE, null);
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    Utils.sendMsg(Constants.EVENT_HIDE, null);
                    Utils.sendMsg(Constants.EVENT_REFRESH, null);
                    Utils.sendMsg(Constants.EVENT_REFRESH_TIP, null);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
            }
        });
    }

    //上传多图片
    public void uploadMutiFileModel(File file, int completeNum, Context activity) {
        Map<String, RequestBody> photos = new HashMap<>();
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        photos.put("file" + "\"; filename=\"" + completeNum + "image.jpg\"", requestBody);
        DialogHelper.showProgressDialog(activity, null, "图片上传中" + completeNum + "/" + 1 + "...", 0, false, null).setCanceledOnTouchOutside(false);
        FileManager.getInstance().getCapitalAPI().uploadFile(photos).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    // http://api.sgbwl.com/v1/app/common/fileUpload
                    // http://api.sgbwl.com/sgb-app/v1/app/common/fileUpload
                    stringMutableLiveData.setValue(response.body().getData().toString());
                } else {
                    stringMutableLiveData.setValue("");
                    MToast.showToast(activity, "图片上传失败");
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }
}