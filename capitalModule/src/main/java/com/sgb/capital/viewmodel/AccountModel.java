package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.model.AccountBean;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.view.ui.fragment.AccountFragment;
import com.sgb.capital.view.widget.MToast;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/4/7 0007
 * 说明:个人银行卡
 */
public class AccountModel extends ViewModel {

     public MutableLiveData<List<AccountBean>> mTradingQuery = new MutableLiveData<>();


    public void tradingQuery(AccountFragment fragment, AccountBean bean) {
        // tip : 修改方法名,实体类
        DialogHelper.showProgressDialog(fragment.getActivity(), null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().tradingQuery(bean).enqueue(new Callback<BaseEntity<AccountBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<AccountBean>> call, Response<BaseEntity<AccountBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mTradingQuery.setValue(response.body().getData().list);
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(fragment.getActivity(), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<AccountBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(fragment.getActivity(), t.getMessage());
            }
        });
    }


}