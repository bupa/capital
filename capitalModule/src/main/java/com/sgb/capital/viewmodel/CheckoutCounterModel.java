package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.model.PayCardEntity;
import com.sgb.capital.model.PayInfoEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.activity.CheckoutCounterActivity;
import com.sgb.capital.view.ui.activity.PayCounterActivity;
import com.sgb.capital.view.widget.MToast;

import java.util.List;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutCounterModel extends ViewModel {
    public MutableLiveData<List<PayCardEntity>> mPayCardList = new MutableLiveData<>();
    public MutableLiveData<String> mCode = new MutableLiveData<>();
    public MutableLiveData<Bean> mBean = new MutableLiveData<>();
    public MutableLiveData<PayInfoEntity> mPayinfoentitymutablelivedata = new MutableLiveData<>();
    public MutableLiveData<BusinessInfoByCompanyEntity> mUserBusinessInfoByCompanyEntity = new MutableLiveData<>();
    public MutableLiveData<PayInfoEntity> mPayInfo = new MutableLiveData<>();
    public MutableLiveData<String> mState = new MutableLiveData<>();

    public void getQuick(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getQuick().enqueue(new Callback<BaseEntity<List<PayCardEntity>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<PayCardEntity>>> call, Response<BaseEntity<List<PayCardEntity>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayCardList.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }
            @Override
            public void onFailure(Call<BaseEntity<List<PayCardEntity>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    // 快捷支付申请验证码
    public void quickPayOrderApply(CheckoutCounterActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().quickPayOrderApply(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(activity, "验证码已发送");
                    activity.setApplyCount(response.body().getData() + "");
                } else {
                    activity.showErrorCode(response.body().getCode(), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 快捷支付确认验证码
    public void quickPayOrderConfirm(CheckoutCounterActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().quickPayOrderConfirm(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(activity, "验证码已发送");
                    activity.setApplyCount(response.body().getData() + "");
                } else {
                    activity.showErrorCode(response.body().getCode(), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    // 快捷支付申请验证码
    public void rechargeQuickPayOrderApply(PayCounterActivity mActivity, Map map) {
        DialogHelper.showProgressDialog(mActivity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().rechargeQuickPayOrderApply(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                String msg = response.body().getMsg();
                System.out.println("msg==" + msg);
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(mActivity, "验证码已发送");
                    mActivity.setApplyCount(response.body().getData() + "");
                } else {
                    if ("E000009".equals(response.body().getCode())) {
                        mActivity.showErrorCode(0, response.body().getMsg());
                    } else if ("E00463031".equals(response.body().getCode())) {
                        mActivity.showErrorCode(1, response.body().getMsg());
                    } else if ("E000007".equals(response.body().getCode())) {
                        mActivity.showErrorCode(5, "重复支付");
                    } else if ("E00466352".equals(response.body().getCode())) {
                        mActivity.showErrorCode(0, response.body().getMsg());
                    } else {
                        MToast.showToast(Utils.getContext(), response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(mActivity, t.getMessage());
            }
        });
    }

    /**
     * 充值订单-收银台获取详情
     *
     * @param activity
     */
    public void payInfo(BaseActivity activity, String orderId) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().rechargePayInfo(orderId).enqueue(new Callback<BaseEntity<PayInfoEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayInfoEntity>> call, Response<BaseEntity<PayInfoEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPayinfoentitymutablelivedata.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayInfoEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 充值申请
     *
     * @param activity
     */
    public void apply(BaseActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getApply(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mCode.setValue((String) response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    // 充值订单-快捷支付确认验证码
    public void rechargeQuickPayOrderConfirm(PayCounterActivity mActivity, Map map) {
        DialogHelper.showProgressDialog(mActivity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().rechargeQuickPayOrderConfirm(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mActivity.setConfirm();
                } else {
                    if ("E00466311".equals(response.body().getCode())) {
                        mActivity.showErrorCode(2, "");
                    } else if ("E00466337".equals(response.body().getCode())) {
                        mActivity.showErrorCode(6, response.body().getMsg());
                    } else if ("E00466464".equals(response.body().getCode()) || "E00466317".equals(response.body().getCode())) {
                        mActivity.showErrorCode(4, "验证码已过期");
                    } else if ("E000007".equals(response.body().getCode())) {
                        mActivity.showErrorCode(5, "重复充值");
                    } else {
                        mActivity.showErrorCode(3, "");
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(mActivity, t.getMessage());
            }
        });
    }


    // 1.3.8 个人 资金中心接口
    public void getBusinessInfoByUser(BaseActivity activity) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().getBusinessInfoByUser().enqueue(new Callback<BaseEntity<BusinessInfoByCompanyEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Response<BaseEntity<BusinessInfoByCompanyEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mUserBusinessInfoByCompanyEntity.setValue(response.body().getData());
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<BusinessInfoByCompanyEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 余额支付申请短信验证码
    public void balancePayApply(CheckoutCounterActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().balancePayApply(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    MToast.showToast(Utils.getContext(), "联动订单编号不能为空");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(activity, "验证码已发送");
                    activity.setApplyCount(response.body().getData() + "");
                } else {
                    activity.showErrorCode(response.body().getCode(), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    // 余额支付确认支付验证短信验证码
    public void balancePayConfirm(CheckoutCounterActivity activity, Map map) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().balancePayConfirm(map).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    MToast.showToast(activity, "验证码已发送");
                    activity.setApplyCount(response.body().getData() + "");
                } else {
                    activity.showErrorCode(response.body().getCode(), response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    /**
     * 获取支付信息
     *
     * @param context
     * @param orderId
     */
    public void pay2Info(BaseActivity context, String orderId) {
        DialogHelper.showProgressDialog(context, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().payInfo(orderId).enqueue(new Callback<BaseEntity<PayInfoEntity>>() {
            @Override
            public void onResponse(Call<BaseEntity<PayInfoEntity>> call, Response<BaseEntity<PayInfoEntity>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    if (response.body().getData() == null) return;
                    mPayInfo.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    mPayInfo.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<PayInfoEntity>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(context, t.getMessage());
            }
        });
    }


    /**
     * 订单是否超过24小时
     *
     * @param activity
     */
    public void orderOvertime(BaseActivity activity, String orderId) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().orderOvertime(orderId).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    if ((boolean) response.body().getData()) {
                        mCode.setValue("1");
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }


    // 支付风险校验
    public void payValidation(BaseActivity activity, String tradeNo, String reviewerNo) {
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().payValidation(tradeNo, reviewerNo).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mState.setValue("");
                } else {
                    mState.setValue(response.body().getCode());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }
}

