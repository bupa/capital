package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.CostQueryBean;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.view.widget.MToast;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/4/7 0007
 * 说明:费用查询
 */
public class CostQueryModel extends ViewModel {

    public MutableLiveData<List<CostQueryBean>> mFeesList = new MutableLiveData<>();

    public void feesList(BaseActivity activity, CostQueryBean bean) {
        // tip : 修改方法名,实体类
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().feeslist(bean).enqueue(new Callback<BaseEntity<CostQueryBean>>() {
            @Override
            public void onResponse(Call<BaseEntity<CostQueryBean>> call, Response<BaseEntity<CostQueryBean>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) {
                    mFeesList.setValue(null);
                    MToast.showToast(activity, "登录已失效,请重新登录");
                    return;
                }
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mFeesList.setValue(response.body().getData().list);
                } else {
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<CostQueryBean>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }
}