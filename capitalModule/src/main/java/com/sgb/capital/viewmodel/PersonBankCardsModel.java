package com.sgb.capital.viewmodel;

import com.sgb.capital.api.CapitalManager;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.utils.DialogHelper;
import com.sgb.capital.view.ui.activity.PersonBankCardsActivity;
import com.sgb.capital.view.widget.MToast;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:张磊
 * 日期:2021/4/7 0007
 * 说明:个人银行卡
 */
public class PersonBankCardsModel extends ViewModel {


    public MutableLiveData<List<BankBean>> mPersonalList = new MutableLiveData<>();

    /**
     * 企业资金中心，支付银行卡列表
     *
     * @param activity
     */
    public void personalList(PersonBankCardsActivity activity) {
        // tip : 修改方法名,实体类
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().personalList().enqueue(new Callback<BaseEntity<List<BankBean>>>() {
            @Override
            public void onResponse(Call<BaseEntity<List<BankBean>>> call, Response<BaseEntity<List<BankBean>>> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    mPersonalList.setValue(response.body().getData());
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity<List<BankBean>>> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

    /**
     * 解绑银行卡
     * @param activity
     * @param id
     */
    public void payBankCard(PersonBankCardsActivity activity, String id) {
        // tip : 修改方法名,实体类
        DialogHelper.showProgressDialog(activity, null, "数据加载中", 0, true, null).setCanceledOnTouchOutside(false);
        CapitalManager.getInstance().getCapitalAPI().payBankCard(id).enqueue(new Callback<BaseEntity>() {
            @Override
            public void onResponse(Call<BaseEntity> call, Response<BaseEntity> response) {
                DialogHelper.dismissProgressDialog();
                if (response.body() == null) return;
                if (response.body().getState().equals(CapitalManager.RESPONSE_OK)) {
                    activity.payBankCard(true);
                } else {
                    DialogHelper.dismissProgressDialog();
                    MToast.showToast(activity, response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<BaseEntity> call, Throwable t) {
                DialogHelper.dismissProgressDialog();
                MToast.showToast(activity, t.getMessage());
            }
        });
    }

}