package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import com.bigkoo.pickerview.adapter.ArrayWheelAdapter;
import com.contrarywind.view.WheelView;
import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:发票
 */
public class InvoicePop extends BasePopupWindow {


    public String mTip = "发票信息错误";
    public List<String> mList;
    private WheelView mWheelView;

    public InvoicePop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.cut_pop);

        mWheelView = v.findViewById(R.id.wv);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> dismiss());
        mList = new ArrayList<>();
        mList.add("发票类型错误");
        mList.add("发票信息错误");
        mList.add("其他");
        mWheelView.setCurrentItem(1);
        mWheelView.setCyclic(false);
        mWheelView.setAdapter(new ArrayWheelAdapter(mList));

        mWheelView.setOnItemSelectedListener(index -> {
            mTip = mList.get(index);
        });
        TextView tv_sure = v.findViewById(R.id.tv_sure);
        tv_sure.setOnClickListener(v1 -> {
            dismiss();
            Utils.sendMsg(Constants.EVENT_SELECT, mTip);
        });
        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }


    public void show(int index) {
        mWheelView.setCurrentItem(index);
        showPopupWindow();
    }
}