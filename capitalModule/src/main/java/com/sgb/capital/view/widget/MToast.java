package com.sgb.capital.view.widget;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sgb.capital.R;


/**
 * Description:
 * Author zhengkewen
 * Time 2017/12/11
 */
public class MToast {
    private static Toast mToast = null;

    public static void showToast(Context context, String text) {
        if (TextUtils.isEmpty(text)) return;
        if (null == context) return;

        if (mToast == null) {
            mToast = Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_SHORT);
        }
        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.custom_toast, null);
        TextView tv = (TextView) v.findViewById(R.id.message);
        tv.setText(text);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.setView(v);
        mToast.show();
    }

    public static void showLongToast(Context context, String text) {
        if (null == context) return;
        if (mToast == null) {
            mToast = Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_LONG);
        }
        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.custom_toast, null);
        TextView tv = (TextView) v.findViewById(R.id.message);
        tv.setText(text);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.setDuration(Toast.LENGTH_LONG);
        mToast.setView(v);
        mToast.show();
    }

    /**
     * 自定义toast显示位置
     * @param context
     * @param text
     * @param load
     *  Gravity.CENTER：中间
     *
     *  Gravity.BOTTOM：下方
     *
     *  Gravity.TOP：上方
     *
     *  Gravity.RIGHT：右边
     *
     *  Gravity.LEFT：左边
     */
    public static void showLongToast(Context context, String text,int load) {
        if (null == context) return;
        if (mToast == null) {
            mToast = Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_LONG);
        }
        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.custom_toast, null);
        TextView tv = (TextView) v.findViewById(R.id.message);
        tv.setText(text);
        mToast.setView(v);
        mToast.setGravity(load, 0, 0);
        mToast.show();
    }

    public static void showLongToast1(Context context, String text,int load) {
        if (null == context) return;
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_LONG);
        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.custom_toast, null);
        TextView tv = (TextView) v.findViewById(R.id.message);
        tv.setText(text);
        mToast.setView(v);
        mToast.setDuration(3*1000);
        mToast.setGravity(load, 0, 0);
        mToast.show();
    }

    public static void showToast(Context context, int res) {
        if (mToast == null) {
            mToast = Toast.makeText(context.getApplicationContext(), context.getString(res), Toast.LENGTH_SHORT);
        }
        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.custom_toast, null);
        TextView tv = (TextView) v.findViewById(R.id.message);
        tv.setText(context.getString(res));
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setView(v);
        mToast.show();
    }
}
