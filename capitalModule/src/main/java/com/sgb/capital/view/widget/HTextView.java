package com.sgb.capital.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;

/**
 * 作者:张磊
 * 日期:2018/6/13
 * 说明:
 */
public abstract class HTextView extends androidx.appcompat.widget.AppCompatTextView {
    public HTextView(Context context) {
        this(context, null);
    }

    public HTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public abstract void setAnimationListener(Animation.AnimationListener listener);

    public abstract void setProgress(float progress);

    public abstract void animateText(CharSequence text);
}

