package com.sgb.capital.view.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.sgb.capital.R;
import com.sgb.capital.databinding.CostqueryFragmentBinding;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.CostQueryBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.SelectPop;
import com.sgb.capital.view.ui.adapter.CostQueryAdapter;
import com.sgb.capital.view.widget.xrecyclerview.ZXRecyclerView;
import com.sgb.capital.viewmodel.CostQueryModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:费用查询
 */
public class CostQueryFragment extends AppLazyFragment {

    CostQueryModel mModel;
    private String mTime;
    private SelectPop mSelectPop;
    private CostqueryFragmentBinding mBinding;
    private List<Bean> mTimeBeans;
    private CostQueryAdapter mAdapter;
    private CostQueryBean mBean;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.costquery_fragment, container, false);
        return mBinding.getRoot();
    }

    @Override
    protected void initView() {
        mBinding.rvList.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mSelectPop = new SelectPop(getActivity());
        mSelectPop.setBgView(mBinding.grayLayout);
        mModel = new ViewModelProvider(this).get(CostQueryModel.class);
    }

    @Override
    public void initObserve() {
        mModel.mFeesList.observe(this, data -> {
            mBinding.rvList.refreshComplete();
            if (data == null) return;
            if (mBean.page == 1) {
                mBinding.rvList.setVisibility(data.size() != 0 ? View.VISIBLE : View.GONE);
                mBinding.errorView.setVisibility(data.size() != 0 ? View.GONE : View.VISIBLE);
                mBinding.rvList.setLoadingMoreEnabled(true);
                mBinding.rvList.refreshComplete();
                mAdapter.setDatas(data);
                if (data.size() < 20) {
                    mBinding.rvList.setNoMore(true);
                }
            } else {
                if (data.size() != 0) {
                    mBinding.rvList.loadMoreComplete();
                    mAdapter.addDatas(data);
                } else {
                    mBinding.rvList.setNoMore(true);
                }
            }
            mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void initListener() {
        mBinding.etSs.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mBean.page = 1;
                mBean.searchValue = mBinding.etSs.getText().toString().trim();
                mModel.feesList(mActivity, mBean);
            }
            return false;
        });
        mBinding.rvList.setLoadingListener(new ZXRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                mBean.page = 1;
                mModel.feesList(mActivity, mBean);
            }

            @Override
            public void onLoadMore() {
                mBean.page++;
                mModel.feesList(mActivity, mBean);
            }
        });
        mSelectPop.setOnDismissListener(() -> {
            mBinding.img.setRotation(0);
            if (mSelectPop.mIsClick) {
                mBinding.tvName.setText(mSelectPop.mPos1 == 0 ? "全部" : mSelectPop.mBean.name);
                mBinding.tvName.setTextColor(Utils.getColor(mSelectPop.mPos1 != 0 ? R.color.color_EF4033 : R.color.color_666666));
                mBinding.img.setImageResource(mSelectPop.mPos1 != 0 ? R.mipmap.down : R.mipmap.ic_down_arrow_n);
                if (mSelectPop.mPos1 != 0) {
                    mTime = mSelectPop.mBean.name.contains("今日") ? Utils.getStartAndEndTime(0, 0, "今日")
                            : mSelectPop.mBean.name.contains("最近一周") ? Utils.getStartAndEndTime(1, 7, "最近一周")
                            : mSelectPop.mBean.name.contains("最近一个月") ? Utils.getStartAndEndTime(2, 1, "最近一个月")
                            : Utils.getStartAndEndTime(3, 1, "最近一年");
                    mBean.startTime = mTime.split("-")[0];
                    mBean.endTime = mTime.split("-")[1];
                } else {
                    mBean.startTime = null;
                    mBean.endTime = null;
                }
                mSelectPop.mIsClick = false;
                mBean.page = 1;
                mModel.feesList(mActivity, mBean);
            }
        });
        mBinding.llView.setOnClickListener(v -> {
            mBinding.img.setRotation(180);
            mSelectPop.showPopwindow(mBinding.view, 0, 0);
        });

    }

    @Override
    protected void onLazyLoad() {
        mTimeBeans = new ArrayList<>();
        Utils.setBeans(mTimeBeans, new String[]{"全部", "今日", "最近一周", "最近一个月", "最近一年"});
        mSelectPop.setData(mTimeBeans);
        mBean = new CostQueryBean();
        mModel.feesList(mActivity, mBean);
        mAdapter = new CostQueryAdapter(mActivity, null);
        mBinding.rvList.setAdapter(mAdapter);
    }
}