package com.sgb.capital.view.ui.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.AsiaRecordBean;
import com.sgb.capital.utils.ItemViewHolder;
import com.sgb.capital.utils.Utils;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:发票的item
 */
public class AsiaRecordAdapter extends BaseRecycleAdapter<AsiaRecordBean.ListDTO> {


    public AsiaRecordAdapter(Context context, List<AsiaRecordBean.ListDTO> beans) {
        super(context, R.layout.asiarecord_adapter, beans);
    }

    @Override
    public void convert(ItemViewHolder holder, AsiaRecordBean.ListDTO item) {

        //状态(1.待审核 2.已驳回 3.待寄出 4.已寄出 5.已撤销 6.未开票 7.已开票 )
        holder
                .setText(R.id.tv_invoiceCode, item.invoiceId)
                .setText(R.id.tv_invoiceType, item.invoiceType==1?"增值税专用发票":"增值税普通发票")
                .setText(R.id.tv_totalFees, "￥"+ Utils.getDecimalFormat(item.totalFees))
                .setText(R.id.tv_invoiceHead, item.invoiceHead)
                .setText(R.id.tv_created, Utils.getTime(item.created))
                .setText(R.id.tv_invoiceStatus_tip,

                        (item.invoiceStatus == 1 || item.invoiceStatus == 2) ? "未开票" :
                                (item.invoiceStatus == 3 || item.invoiceStatus == 4) ? "已开票" : "已撤销")
                .setText(R.id.tv_invoiceStatus, item.invoiceStatus == 1 ? "待审核" :
                        item.invoiceStatus == 2 ? "已驳回" :
                                item.invoiceStatus == 3 ? "待寄出" :
                                        item.invoiceStatus == 4 ? "已寄出" :
                                                item.invoiceStatus == 5 ? "已撤销" :
                                                        item.invoiceStatus == 6 ? "未开票" : "已开票"
                );
        TextView tv_invoiceStatus = holder.getView(R.id.tv_invoiceStatus);
        tv_invoiceStatus.setTextColor(Utils.getColor(item.invoiceStatus == 2 ? R.color.color_EF4033 : R.color.color_5792FD));
        tv_invoiceStatus.setVisibility(item.invoiceStatus == 5 ? View.GONE : View.VISIBLE);
        TextView tv_invoiceStatus_tip = holder.getView(R.id.tv_invoiceStatus_tip);
        tv_invoiceStatus_tip.setTextColor(Utils.getColor((item.invoiceStatus == 1 || item.invoiceStatus == 2 || item.invoiceStatus == 5) ? R.color.color_333333 : R.color.color_06C764));
    }

}
