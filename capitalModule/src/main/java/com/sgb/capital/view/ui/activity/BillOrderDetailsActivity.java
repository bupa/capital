package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.OnZTabSelectedListener;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.CapitalTobepaidbillshopdetailsActivityBinding;
import com.sgb.capital.model.BaseEntity;
import com.sgb.capital.model.PayDetailBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.ui.adapter.ViewPagerAdapter;
import com.sgb.capital.view.ui.fragment.BillContractDetailsFragment;
import com.sgb.capital.view.ui.fragment.BillLogDetailsFragment;
import com.sgb.capital.view.ui.fragment.BillPayDetailsFragment;
import com.sgb.capital.view.ui.fragment.BillShopDetailsFragment;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.DetailsModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import androidx.core.util.Pair;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

/**
 * 作者:张磊
 * 日期:2021/9/1 0001
 * 说明:待付订单详情
 */
public class BillOrderDetailsActivity extends BaseActivity {
    private CapitalTobepaidbillshopdetailsActivityBinding mBinding;
    public int mType;
    private DetailsModel mModel;
    public PayDetailBean mPayDetailBean;
    public String mRealName;
    private TipPop mTip;

    public static void start(Context context, int type, String orderId, String id, String title) {
        Intent intent = new Intent(context, BillOrderDetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("type", type);
        intent.putExtra("orderId", orderId);
        intent.putExtra("id", id);
        intent.putExtra("title", title);
        context.startActivity(intent);
    }


    @Override
    protected void initView() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mBinding = DataBindingUtil.setContentView(this, R.layout.capital_tobepaidbillshopdetails_activity);
        mModel = new ViewModelProvider(this).get(DetailsModel.class);
        mTip = new TipPop(this, "温馨提示", "复核人员为空，请在电脑端-业务管理-复核配置进行相关人员的配置!", null, "知道了");

    }

    public void initData() {
        mType = getIntent().getIntExtra("type", 1);
        String s = mType == 1 ? "待付账单" : mType == 2 ? "已付账单" :
                mType == 3 ? "应收帐单" : mType == 4 ? "已收账单" : mType == 5 ? "已付帐单" :
                        "已收账单";
        String title = getIntent().getStringExtra("title");
        mBinding.tvTitle.setText(title.contains("企") ? "企服" + "-" + s : title + "-" + s);
        if (mType > 4) {
            mModel.getTransferInfo(this, getIntent().getStringExtra("id"));
        } else {
            mModel.getBusinessUserInfo(this);
        }
    }

    public void initListener() {
        mTip.mTvCancel.setOnClickListener(v -> mTip.dismiss());
        mBinding.tab.addOnTabSelectedListener(new OnZTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab == null || tab.getText() == null) {
                    return;
                }
                String trim = tab.getText().toString().trim();
                SpannableString spannableString = new SpannableString(trim);
                StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
                spannableString.setSpan(styleSpan, 0, trim.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                tab.setText(spannableString);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab == null || tab.getText() == null) {
                    return;
                }
                String trim = tab.getText().toString().trim();
                SpannableString spannableString = new SpannableString(trim);
                StyleSpan styleSpan = new StyleSpan(Typeface.NORMAL);
                spannableString.setSpan(styleSpan, 0, trim.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                tab.setText(spannableString);
            }
        });

        mBinding.ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void initObserve() {
        mModel.mListBean.observe(this, listBeans -> {
            if (listBeans == null || listBeans.size() == 0) {
                mTip.show();
            } else {
                PayDealActivity.start(this, mPayDetailBean.orderId);
            }
        });
        mModel.mUserBusinessInfoByCompanyEntity.observe(this, businessInfoByCompanyEntity -> {
            mRealName = businessInfoByCompanyEntity.realName;
            mModel.getBillPayOrderDetails(this, getIntent().getStringExtra("orderId"));
        });
        mModel.mPayDetailBean.observe(this, payDetailBean -> {
            mPayDetailBean = payDetailBean;
            mBinding.tab.setVisibility(View.VISIBLE);
            if (mType == 2) {
                if (payDetailBean.orderTypeName.contains("商品")) {
                    List<Pair<String, Fragment>> mList = new ArrayList<>();
                    mList.add(new Pair<>("支付信息", new BillPayDetailsFragment()));
                    mList.add(new Pair<>("商品信息", new BillShopDetailsFragment()));
                    mList.add(new Pair<>("合同信息", new BillContractDetailsFragment()));
                    mList.add(new Pair<>("操作日志", new BillLogDetailsFragment()));
                    mBinding.vpNeed.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), mList));
                    mBinding.tab.setupWithViewPager(mBinding.vpNeed);
                    mBinding.vpNeed.setOffscreenPageLimit(4);
                } else if (payDetailBean.orderTypeName.contains("在线转账")) {
                    List<Pair<String, Fragment>> mList = new ArrayList<>();
                    mList.add(new Pair<>("支付信息", new BillPayDetailsFragment()));
                    mBinding.vpNeed.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), mList));
                    mBinding.tab.setupWithViewPager(mBinding.vpNeed);
                    mBinding.tab.setVisibility(View.GONE);
                } else {
                    List<Pair<String, Fragment>> mList = new ArrayList<>();
                    mList.add(new Pair<>("支付信息", new BillPayDetailsFragment()));
                    mList.add(new Pair<>("操作日志", new BillLogDetailsFragment()));
                    mBinding.vpNeed.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), mList));
                    mBinding.tab.setupWithViewPager(mBinding.vpNeed);
                    mBinding.vpNeed.setOffscreenPageLimit(2);
                }
            } else {
                if (payDetailBean.orderTypeName.contains("商品")) {
                    List<Pair<String, Fragment>> mList = new ArrayList<>();
                    mList.add(new Pair<>("支付信息", new BillPayDetailsFragment()));
                    mList.add(new Pair<>("商品信息", new BillShopDetailsFragment()));
                    mList.add(new Pair<>("合同信息", new BillContractDetailsFragment()));
                    mBinding.vpNeed.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), mList));
                    mBinding.tab.setupWithViewPager(mBinding.vpNeed);
                    mBinding.vpNeed.setOffscreenPageLimit(3);
                } else {
                    List<Pair<String, Fragment>> mList = new ArrayList<>();
                    mList.add(new Pair<>("支付信息", new BillPayDetailsFragment()));
                    mBinding.vpNeed.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), mList));
                    mBinding.tab.setupWithViewPager(mBinding.vpNeed);
                    mBinding.tab.setVisibility(View.GONE);
                }
            }
        });
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .SB_JSON:
                String json = (String) event.data;
                BaseEntity baseEntity = new Gson().fromJson(json, BaseEntity.class);
                if (baseEntity.getMsg().contains("登录")) {
                    MToast.showToast(Utils.getContext(), baseEntity.getMsg());
                }
                break;
            case Constants
                    .EVENT_HIDE_ORDERID:
            case Constants
                    .EVENT_REFRESH_ORDER:
                finish();
                break;
            case Constants
                    .EVENT_CLICK:
                mModel.list(this);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


}