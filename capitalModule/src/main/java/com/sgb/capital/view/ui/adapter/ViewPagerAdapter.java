package com.sgb.capital.view.ui.adapter;

import java.util.List;

import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * 作者:张磊
 * 日期:2019/9/25
 * 说明:自定义广场界面tab
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    List<Pair<String, Fragment>> mList;

    public ViewPagerAdapter(FragmentManager fm, List<Pair<String, Fragment>> items) {
        super(fm);
        mList = items;
    }

    @Override
    public Fragment getItem(int position) {
        return mList.get(position).second;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mList.get(position).first;
    }

    /**
     * 获取当前list中最后一个对象
     * @return
     */
    public Fragment getLastItem() {
        return mList.get(getCount() <= 0 ? 0 : getCount() - 1).second;
    }
}
