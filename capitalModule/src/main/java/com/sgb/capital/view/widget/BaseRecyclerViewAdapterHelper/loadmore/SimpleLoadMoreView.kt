package com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.loadmore

import android.view.View
import android.view.ViewGroup
import com.sgb.capital.R
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.loadmore.BaseLoadMoreView
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.util.getItemView
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder

class SimpleLoadMoreView : BaseLoadMoreView() {

    override fun getRootView(parent: ViewGroup): View =
            parent.getItemView(R.layout.brvah_quick_view_load_more)

    override fun getLoadingView(holder: BaseViewHolder): View =
            holder.getView(R.id.load_more_loading_view)

    override fun getLoadComplete(holder: BaseViewHolder): View =
            holder.getView(R.id.load_more_load_complete_view)

    override fun getLoadEndView(holder: BaseViewHolder): View =
            holder.getView(R.id.load_more_load_end_view)

    override fun getLoadFailView(holder: BaseViewHolder): View =
            holder.getView(R.id.load_more_load_fail_view)
}