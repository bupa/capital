package com.sgb.capital.view.widget.xrecyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Description:
 * Author zhengkewen
 * Time 2017/12/13
 */
public class ZDescHolder extends RecyclerView.ViewHolder {

    private ViewDataBinding mBinding;
    private View item;

    public ZDescHolder(View v) {
        super(v);
        this.item = v;
        mBinding = DataBindingUtil.bind(v);
    }

    public static ZDescHolder get(Context context, ViewGroup viewGroup, int layoutId) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ZDescHolder(itemView);
    }

    public View getItem() {
        return item;
    }

    public ZDescHolder setBinding(int variableId, Object object) {
        mBinding.setVariable(variableId, object);
        mBinding.executePendingBindings();
        return this;
    }

    public ViewDataBinding getmBinding() {
        return mBinding;
    }
}
