package com.sgb.capital.view.ui.adapter;

import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.model.LogBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 日志
 */
public class OrderLogAdapter extends BaseQuickAdapter<LogBean, BaseViewHolder> {


    public OrderLogAdapter(List<LogBean> beans) {
        super(R.layout.orderlog_adapter, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final LogBean item) {
        if (getData().size() == 0) {
            return;
        }
        helper
                .setText(R.id.tv_operType, item.operType)
                .setText(R.id.tv_created, Utils.getTime(item.created))
                .setText(R.id.tv_creatorName, item.creatorName);
        helper.getView(R.id.view1).setVisibility(helper.getLayoutPosition() == 0 ? View.INVISIBLE : View.VISIBLE);
        helper.getView(R.id.view2).setVisibility(helper.getLayoutPosition() == getData().size() - 1 ? View.INVISIBLE : View.VISIBLE);
    }
}


























/*BaseRecycleAdapter<LogBean> {

    public OrderLogAdapter(Context context, List<LogBean> beans) {
        super(context, R.layout.orderlog_adapter, beans);
    }


    @Override
    public void convert(ItemViewHolder holder, LogBean item) {
        holder.setBinding(BR.viewModel, item);
        if (getDatas().size()==0){
            return;
        }
        holder
                .setText(R.id.tv_operType, item.operType)
                .setText(R.id.tv_created, Utils.getTime(item.created))
                .setText(R.id.tv_creatorName, item.creatorName)
                .setVisible(R.id.view1, holder.getLayoutPosition() != 0)
                .setVisible(R.id.view2, holder.getLayoutPosition()==getDatas().size()-1)
        ;

        holder.getView(R.id.view1).setVisibility(holder.getBindingAdapterPosition()==0? View.INVISIBLE:View.VISIBLE);
        holder.getView(R.id.view2).setVisibility(holder.getBindingAdapterPosition()==getDatas().size()-1?View.INVISIBLE:View.VISIBLE);


        System.out.println("索引1=="+ holder.getAbsoluteAdapterPosition());
        System.out.println("索引2=="+ holder.getBindingAdapterPosition());
        System.out.println("索引3=="+ holder.getPosition());
        System.out.println("索引4=="+ holder.getOldPosition());
        System.out.println("索引5=="+ holder.getAdapterPosition());
        System.out.println("索引6=="+ holder.getLayoutPosition());


    }
}
*/