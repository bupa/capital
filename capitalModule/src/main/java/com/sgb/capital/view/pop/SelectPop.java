package com.sgb.capital.view.pop;

import android.content.Context;

import com.sgb.capital.R;
import com.sgb.capital.base.BasePopWindow;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.AdapterOnItemClick;
import com.sgb.capital.view.ui.adapter.SelectZItemAdapter;

import java.util.List;

import androidx.annotation.ColorRes;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 作者:张磊
 * 日期:2019/10/24
 * 说明:提示弹框
 */
public class SelectPop extends BasePopWindow {
    public int mType = 1;
    public SelectZItemAdapter mAdapter;
    private RecyclerView recyclerView;

    public Bean mBean;
    public boolean mIsClick;
    public int mPos1;
    public int mPos2;
    public int mPos3;

    public SelectPop(Context context) {
        super(context);
    }

    public SelectPop(Context context, int dir) {
        super(context, dir);
    }

    @Override
    protected int popLayout() {
        return R.layout.select_popwindow;
    }

    @Override
    protected void initView(PopViewHolder holder) {
        recyclerView = holder.getPop().findViewById(R.id.goods_type_list);
        mAdapter = new SelectZItemAdapter(mContext, null);
        mAdapter.getCheckDatas();
        mAdapter.setItemSelect(true);
        mAdapter.setDefItem(0);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClick(new AdapterOnItemClick<Bean>() {
            @Override
            public void onItemClick(Bean bean, int position) {
                mBean = bean;
                mIsClick = true;
                switch (mType) {
                    case 1:
                        mPos1 = position;
                        break;
                    case 2:
                        mPos2 = position;
                        break;
                    case 3:
                        mPos3 = position;
                        break;
                }
                if (onResultClick != null)
                    onResultClick.result(bean, position, null);
                dissMiss();
            }
        });

        holder.getPop().findViewById(R.id.bg_pop_gray_layout).setOnClickListener(view -> dissMiss());
    }

    public void setData(List<Bean> list) {
        mAdapter.setDatas(list);
    }

    public void setBackground(@ColorRes int color) {
        recyclerView.setBackgroundResource(color);
    }

}