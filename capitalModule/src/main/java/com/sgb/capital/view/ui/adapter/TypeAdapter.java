package com.sgb.capital.view.ui.adapter;


import com.sgb.capital.R;
import com.sgb.capital.model.Bean;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;


/**
 * 作者:张磊
 * 日期:2021/1/21 0021
 * 说明:
 */
public class TypeAdapter extends BaseQuickAdapter<Bean, BaseViewHolder> {
    public TypeAdapter(List<Bean> data) {
        super(R.layout.adapter_type, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Bean item) {
        helper
                .setText(R.id.tv_name, item.name)
                .setImageResource(R.id.iv_icon, item.type == 0 ? R.mipmap.withdraw_icon
                        : item.type == 1 ? R.mipmap.account_icon : item.type == 2 ? R.mipmap.water_icon
                        : item.type == 3 ? R.mipmap.charge_icon : item.type == 4 ?
                        R.mipmap.card_icon : item.type == 5 ? R.mipmap.chongzhi :
                        item.type == 6 ? R.mipmap.yifuzhangdan : item.type == 7 ? R.mipmap.yingshouzhangdan : R.mipmap.yishouzhangdan
                );
    }
}