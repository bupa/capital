package com.sgb.capital.view.pop;

import android.content.Context;
import android.text.Editable;
import android.view.View;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.MyZTextWatcher;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.DataAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:选择银行卡列表
 */
public class CardsListPop extends BasePopupWindow {


    public List<BankEntity> mDatas = new ArrayList<>();
    public List<BankEntity> mSsDatas = new ArrayList<>();
    private DataAdapter mAdapter;
    private BankEntity mBankEntity;
    public int mLastIndex = 0;
    private EditText mEtSs;
    private View mIvNull;

    public CardsListPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.cardslist_pop);
        RecyclerView rv = v.findViewById(R.id.rv);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> dismiss());
        rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mDatas = new ArrayList<>();
        mAdapter = new DataAdapter(mDatas);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            mBankEntity = mAdapter.getData().get(position);
            BankEntity bean2 = mAdapter.getData().get(mLastIndex);
            if (mLastIndex != position) {
                mBankEntity.isSelect = true;
                bean2.isSelect = false;
            }
            mLastIndex = position;
            mAdapter.notifyDataSetChanged();
        });
        rv.setAdapter(mAdapter);


        TextView tv_sure = v.findViewById(R.id.tv_sure);
        tv_sure.setOnClickListener(v1 -> {
            dismiss();
            if (mIvNull.getVisibility() == View.VISIBLE) return;
            if (mSsDatas.size() != 0) {
                for (BankEntity bankEntity : mSsDatas) {
                    if (bankEntity.isSelect) {
                        mBankEntity = bankEntity;
                        break;
                    }
                }
            }
            Utils.sendMsg(Constants.EVENT_SELECT, mBankEntity == null ? mAdapter.getData().get(0) : mBankEntity);
        });

        mEtSs = v.findViewById(R.id.et_ss);
        mIvNull = v.findViewById(R.id.iv_null);

        mEtSs.addTextChangedListener(new MyZTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                mSsDatas.clear();
                if (Utils.isEmpty(mEtSs.getText().toString())) {
                    for (int i = 0; i < mDatas.size(); i++) {
                        if (mDatas.get(i).isSelect) {
                            mLastIndex = i;
                        }
                    }
                    mAdapter.setNewInstance(mDatas);
                    mIvNull.setVisibility(mDatas.size() == 0 ? View.VISIBLE : View.GONE);
                } else {
                    for (BankEntity bankEntity : mDatas) {
                        bankEntity.isSelect = false;
                        if (bankEntity.name.contains(mEtSs.getText().toString())) {
                            mSsDatas.add(bankEntity);
                        }
                    }
                    if (mSsDatas.size() != 0) {
                        mLastIndex = 0;
                        mSsDatas.get(0).isSelect = true;
                    }
                    mIvNull.setVisibility(mSsDatas.size() == 0 ? View.VISIBLE : View.GONE);
                    mAdapter.setNewInstance(mSsDatas);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });

        mEtSs.setOnEditorActionListener((v1, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                // TODO: 2021/5/31 0031
                // TODO: 2021/5/31 0031
            /*      mSsDatas.clear();
                if (Utils.isEmpty(mEtSs.getText().toString())) {
                    mAdapter.setNewInstance(mDatas);
                    mIvNull.setVisibility(mDatas.size() == 0 ? View.VISIBLE : View.GONE);
                } else {
                    for (BankEntity bankEntity : mDatas) {
                        if (bankEntity.name.contains(mEtSs.getText().toString())) {
                            mSsDatas.add(bankEntity);
                        }
                    }
                    if (mSsDatas.size() != 0) {
                        mSsDatas.get(0).isSelect = true;
                    }
                    mIvNull.setVisibility(mSsDatas.size() == 0 ? View.VISIBLE : View.GONE);
                    mAdapter.setNewInstance(mSsDatas);
                    mAdapter.notifyDataSetChanged();
                }*/
            }
            return false;
        });

        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }


    public void show() {
        mEtSs.setText("");
        mAdapter.setNewInstance(mDatas);
        mAdapter.notifyDataSetChanged();
        showPopupWindow();
    }
}