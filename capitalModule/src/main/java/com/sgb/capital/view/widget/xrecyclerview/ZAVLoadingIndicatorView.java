package com.sgb.capital.view.widget.xrecyclerview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.sgb.capital.R;

import androidx.annotation.IntDef;


public class ZAVLoadingIndicatorView extends View {
    //indicators
    public static final int BallPulse = 0;
    public static final int BallGridPulse = 1;
    public static final int BallClipRotate = 2;
    public static final int BallClipRotatePulse = 3;
    public static final int SquareSpin = 4;
    public static final int BallClipRotateMultiple = 5;
    public static final int BallPulseRise = 6;
    public static final int BallRotate = 7;
    public static final int CubeTransition = 8;
    public static final int BallZigZag = 9;
    public static final int BallZigZagDeflect = 10;
    public static final int BallTrianglePath = 11;
    public static final int BallScale = 12;
    public static final int LineScale = 13;
    public static final int LineScaleParty = 14;
    public static final int BallScaleMultiple = 15;
    public static final int BallPulseSync = 16;
    public static final int BallBeat = 17;
    public static final int LineScalePulseOut = 18;
    public static final int LineScalePulseOutRapid = 19;
    public static final int BallScaleRipple = 20;
    public static final int BallScaleRippleMultiple = 21;
    public static final int BallSpinFadeLoader = 22;
    public static final int LineSpinFadeLoader = 23;
    public static final int TriangleSkewSpin = 24;
    public static final int Pacman = 25;
    public static final int BallGridBeat = 26;
    public static final int SemiCircleSpin = 27;
    //Sizes (with defaults in DP)
    public static final int DEFAULT_SIZE = 30;
    //attrs
    int mIndicatorId;
    int mIndicatorColor;
    Paint mPaint;
    ZBaseIndicatorController mIndicatorController;
    private boolean mHasAnimation;

    public ZAVLoadingIndicatorView(Context context) {
        super(context);
        init(null, 0);
    }

    public ZAVLoadingIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ZAVLoadingIndicatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ZAVLoadingIndicatorView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs, defStyleAttr);
    }

    public void destroy() {
        mHasAnimation = true;
        if (mIndicatorController != null) {
            mIndicatorController.destroy();
            mIndicatorController = null;
        }
        mPaint = null;
    }

    private void init(AttributeSet attrs, int defStyle) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.Xrecyclerview_AVLoadingIndicatorView);
        mIndicatorId = a.getInt(R.styleable.Xrecyclerview_AVLoadingIndicatorView_indicator, BallPulse);
        mIndicatorColor = a.getColor(R.styleable.Xrecyclerview_AVLoadingIndicatorView_indicator_color, Color.WHITE);
        a.recycle();
        mPaint = new Paint();
        mPaint.setColor(mIndicatorColor);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAntiAlias(true);
        applyIndicator();
    }

    public void setIndicatorId(int indicatorId) {
        mIndicatorId = indicatorId;
        applyIndicator();
    }

    public void setIndicatorColor(int color) {
        mIndicatorColor = color;
        mPaint.setColor(mIndicatorColor);
        this.invalidate();
    }

    private void applyIndicator() {
        switch (mIndicatorId) {
            case BallPulse:
                mIndicatorController = new ZBallPulseIndicator();
                break;
            case BallGridPulse:
                mIndicatorController = new ZBallGridPulseIndicator();
                break;
            case BallClipRotate:
                mIndicatorController = new ZBallClipRotateIndicator();
                break;
            case BallClipRotatePulse:
                mIndicatorController = new ZBallClipRotatePulseIndicator();
                break;
            case SquareSpin:
                mIndicatorController = new ZSquareSpinIndicator();
                break;
            case BallClipRotateMultiple:
                mIndicatorController = new ZBallClipRotateMultipleIndicator();
                break;
            case BallPulseRise:
                mIndicatorController = new ZBallPulseRiseIndicator();
                break;
            case BallRotate:
                mIndicatorController = new ZBallRotateIndicator();
                break;
            case CubeTransition:
                mIndicatorController = new ZCubeTransitionIndicator();
                break;
            case BallZigZag:
                mIndicatorController = new ZBallZigZagIndicator();
                break;
            case BallZigZagDeflect:
                mIndicatorController = new ZBallZigZagDeflectIndicator();
                break;
            case BallTrianglePath:
                mIndicatorController = new ZBallTrianglePathIndicator();
                break;
            case BallScale:
                mIndicatorController = new ZBallScaleIndicator();
                break;
            case LineScale:
                mIndicatorController = new ZLineScaleIndicator();
                break;
            case LineScaleParty:
                mIndicatorController = new ZLineScalePartyIndicator();
                break;
            case BallScaleMultiple:
                mIndicatorController = new ZBallScaleMultipleIndicator();
                break;
            case BallPulseSync:
                mIndicatorController = new ZBallPulseSyncIndicator();
                break;
            case BallBeat:
                mIndicatorController = new ZBallBeatIndicator();
                break;
            case LineScalePulseOut:
                mIndicatorController = new ZLineScalePulseOutIndicator();
                break;
            case LineScalePulseOutRapid:
                mIndicatorController = new ZLineScalePulseOutRapidIndicator();
                break;
            case BallScaleRipple:
                mIndicatorController = new ZBallScaleRippleIndicator();
                break;
            case BallScaleRippleMultiple:
                mIndicatorController = new ZBallScaleRippleMultipleIndicator();
                break;
            case BallSpinFadeLoader:
                mIndicatorController = new ZBallSpinFadeLoaderIndicator();
                break;
            case LineSpinFadeLoader:
                mIndicatorController = new ZLineSpinFadeLoaderIndicator();
                break;
            case TriangleSkewSpin:
                mIndicatorController = new ZTriangleSkewSpinIndicator();
                break;
            case Pacman:
                mIndicatorController = new ZPacmanIndicator();
                break;
            case BallGridBeat:
                mIndicatorController = new ZBallGridBeatIndicator();
                break;
            case SemiCircleSpin:
                mIndicatorController = new ZSemiCircleSpinIndicator();
                break;
        }
        mIndicatorController.setTarget(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = measureDimension(dp2px(DEFAULT_SIZE), widthMeasureSpec);
        int height = measureDimension(dp2px(DEFAULT_SIZE), heightMeasureSpec);
        setMeasuredDimension(width, height);
    }

    private int measureDimension(int defaultSize, int measureSpec) {
        int result = defaultSize;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else if (specMode == MeasureSpec.AT_MOST) {
            result = Math.min(defaultSize, specSize);
        } else {
            result = defaultSize;
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawIndicator(canvas);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (!mHasAnimation) {
            mHasAnimation = true;
            applyAnimation();
        }
    }

    @Override
    public void setVisibility(int v) {
        if (getVisibility() != v) {
            super.setVisibility(v);
            if (mIndicatorController == null)
                return;
            if (v == GONE || v == INVISIBLE) {
                mIndicatorController.setAnimationStatus(ZBaseIndicatorController.AnimStatus.END);
            } else {
                mIndicatorController.setAnimationStatus(ZBaseIndicatorController.AnimStatus.START);
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mIndicatorController == null)
            return;
        mIndicatorController.setAnimationStatus(ZBaseIndicatorController.AnimStatus.CANCEL);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mIndicatorController == null)
            return;
        mIndicatorController.setAnimationStatus(ZBaseIndicatorController.AnimStatus.START);
    }

    void drawIndicator(Canvas canvas) {
        if (mIndicatorController == null)
            return;
        mIndicatorController.draw(canvas, mPaint);
    }

    void applyAnimation() {
        if (mIndicatorController == null)
            return;
        mIndicatorController.initAnimation();
    }

    private int dp2px(int dpValue) {
        return (int) getContext().getResources().getDisplayMetrics().density * dpValue;
    }

    @IntDef(flag = true,
            value = {
                    BallPulse,
                    BallGridPulse,
                    BallClipRotate,
                    BallClipRotatePulse,
                    SquareSpin,
                    BallClipRotateMultiple,
                    BallPulseRise,
                    BallRotate,
                    CubeTransition,
                    BallZigZag,
                    BallZigZagDeflect,
                    BallTrianglePath,
                    BallScale,
                    LineScale,
                    LineScaleParty,
                    BallScaleMultiple,
                    BallPulseSync,
                    BallBeat,
                    LineScalePulseOut,
                    LineScalePulseOutRapid,
                    BallScaleRipple,
                    BallScaleRippleMultiple,
                    BallSpinFadeLoader,
                    LineSpinFadeLoader,
                    TriangleSkewSpin,
                    Pacman,
                    BallGridBeat,
                    SemiCircleSpin
            })
    public @interface Indicator {
    }


}
