package com.sgb.capital.view.ui.adapter;

import android.content.Context;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.InvoiceFeesBean;
import com.sgb.capital.utils.ItemViewHolder;

import java.util.List;

/**
 * 开票流水适配器
 * @author cyj
 */
public class InvoiceFlowAdapter extends BaseRecycleAdapter<InvoiceFeesBean> {

    private OnClickItemListener onClickItemListener = null;

    public InvoiceFlowAdapter(Context context, List<InvoiceFeesBean> beans) {
        super(context, R.layout.invoice_flow_adapter, beans);
    }

//    @BindingAdapter("bindAdapter")
//    public static void recycleViewBindAdapter(ZXRecyclerView recyclerView, ZXRecyclerView.Adapter<ItemViewHolder> adapter) {
//        recyclerView.setAdapter(adapter);
//    }

    @Override
    public void convert(ItemViewHolder holder, InvoiceFeesBean bean) {
        holder.setBinding(BR.viewModel, bean);
        if(null != onClickItemListener) {
            holder.getItem().setOnClickListener(v -> {
                onClickItemListener.onItemClick(holder.getItemId());
            });
        }
    }

//    /**
//     * 设置适配器布局中的 CheckBox 控件的选中状态
//     * desc：当 hasCheckedAll 为true时，认为用户选中全选，则所有 checkBox 控件的选中状态为true
//     * @param checkBox    配器布局中的 CheckBox 控件
//     * @param hasCheckedAll    全选标志
//     */
//    @BindingAdapter("setCheckBoxCheckedStatus")
//    public void setCheckBoxCheckedStatus(CheckBox checkBox, boolean hasCheckedAll) {
//        if(hasCheckedAll) {
//            checkBox.setChecked(true);
//        }
//    }

    public void setOnClickItemListener(OnClickItemListener onClickItemListener) {

    }

    public interface OnClickItemListener {

        void onItemClick(long index);
    }
}
