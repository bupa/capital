package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;

import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.utils.Utils;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:快捷登录
 */
public class UserPop extends BasePopupWindow {


    private String mTip = "企业商户";

    public UserPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.user_pop);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> dismiss());
        v.findViewById(R.id.tv_zj).setOnClickListener(v1 -> {
            dismiss();
            Utils.sendMsg(Constants.CLICK_ZJ, null);
        });
        v.findViewById(R.id.tv_pxt).setOnClickListener(v1 -> {
            dismiss();
            Utils.sendMsg(Constants.CLICK_PXT, null);
        });
        v.findViewById(R.id.tv_ht).setOnClickListener(v1 -> {
            dismiss();
            Utils.sendMsg(Constants.CLICK_HT, null);
        });
        v.findViewById(R.id.tv_ll).setOnClickListener(v1 -> {
            dismiss();
            Utils.sendMsg(Constants.CLICK_LL, null);
        });
        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }


    public void show() {
        showPopupWindow();
    }
}