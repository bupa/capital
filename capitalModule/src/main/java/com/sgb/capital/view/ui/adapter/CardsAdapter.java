package com.sgb.capital.view.ui.adapter;

import com.sgb.capital.R;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:
 */
public class CardsAdapter extends BaseQuickAdapter<Bean, BaseViewHolder> {


    public CardsAdapter(List<Bean> beans) {
        super(R.layout.cards_adapter, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final Bean item) {
        helper.setText(R.id.tv_name, item.name.split("-")[0]).setText(R.id.tv_value, item.name.split("-")[1]);
        helper.setVisible(R.id.tv_show, item.name.contains("show"));
        helper.setTextColor(R.id.tv_value, Utils.getColor((item.name.split("-")[0].contains("金额") || item.name.split("-")[0].contains("手续费")) ? R.color.c_FF4B10 : R.color.color_333333));
        addChildClickViewIds(R.id.tv_show);
    }
}