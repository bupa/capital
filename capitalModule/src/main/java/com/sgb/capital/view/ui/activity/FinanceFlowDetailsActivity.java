package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.google.gson.Gson;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.FinanceflowdetailsActivityBinding;
import com.sgb.capital.model.BalanceDetailsEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.RecordsBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.BalanceDetailsAdapter;
import com.sgb.capital.viewmodel.FinanceFlowModel;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/4/19 0019
 * 说明:财务流水详情
 */


public class FinanceFlowDetailsActivity extends BaseActivity {
    private FinanceflowdetailsActivityBinding mBinding;
    private List<Bean> mBeans = new ArrayList<>();
    private BalanceDetailsAdapter mAdapter;
    private RecordsBean mPayFlowBean;
    FinanceFlowModel mModel;
    private BalanceDetailsEntity mDetailsEntity;

    public static void start(Context context, String data) {
        Intent intent = new Intent(context, FinanceFlowDetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("data", data);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.financeflowdetails_activity);
        String data = getIntent().getStringExtra("data");
        mPayFlowBean = new Gson().fromJson(data, RecordsBean.class);
        mBinding.rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mAdapter = new BalanceDetailsAdapter(null);
        mBinding.rv.setAdapter(mAdapter);
        mBinding.ivBack.setOnClickListener(v -> finish());
    }


    @Override
    public void initObserve() {
        mModel.mData.observe(this, balanceDetailsEntity -> {
            mDetailsEntity = balanceDetailsEntity;
            mBinding.ivShow.setVisibility(balanceDetailsEntity.repeatPayStatus == 2 ? View.VISIBLE : View.GONE);
            if (balanceDetailsEntity.repeatPayStatus == 3) {
                mBinding.ivShow.setImageResource(R.mipmap.yichangdingdan);
                mBinding.ivShow.setVisibility(View.VISIBLE);
            }
            mBinding.ivIcon.setImageResource(mPayFlowBean.tradeStatus == 1 ? R.mipmap.succeed_icon : R.mipmap.djz);
            mBinding.tvTip.setText(mPayFlowBean.tradeStatus == 1 ? "已完成" : "冻结中");
            // 	交易类型 1.订单交易,2.转账汇款,3.账户提现,4.订单退款
            mBeans.add(new Bean("交易类型",
                    mPayFlowBean.sourceType.contains("1") ? "订单交易" :
                            mPayFlowBean.sourceType.contains("2") ? "转账汇款" :
                                    mPayFlowBean.sourceType.contains("3") ? "账户提现" : "订单退款"
            ));
            if (!mPayFlowBean.sourceType.contains("2")) {
                mBeans.add(new Bean("业务类型", mPayFlowBean.sourceType.contains("3") ? "账户提现" : mPayFlowBean.sourceType.contains("2") ? "在线转账" : mPayFlowBean.tradeType));
            }
            if (!Utils.isEmpty(mPayFlowBean.payType)) {
                mBeans.add(new Bean("支付方式", mPayFlowBean.payType));
            }
            mBeans.add(new Bean((mPayFlowBean.sourceType.contains("1") || mPayFlowBean.sourceType.contains("2")) ? "交易金额" : "订单金额", (mDetailsEntity.tradeFlowType == 1 ? "+" : "-") + Utils.getDecimalFormat(Float.parseFloat(mDetailsEntity.tradeAmount))));
            if (!mPayFlowBean.payType.contains("余额支付") && !mPayFlowBean.sourceType.contains("2")) {
                mBeans.add(new Bean("交易手续费", "-" + Utils.getDecimalFormat(Float.parseFloat(mPayFlowBean.tradeFees))));
            } else {
                if (mDetailsEntity.tradeFlowType == 1) {
                    mBeans.add(new Bean("交易手续费",
                            Float.parseFloat(mPayFlowBean.tradeFees) == 0 ? "/" : "-" + Utils.getDecimalFormat(Float.parseFloat(mPayFlowBean.tradeFees))));
                }
            }
            if (mPayFlowBean.tradeType.contains("余额充值")) {
                if (!Utils.isEmpty(mDetailsEntity.balance)) {
                    mBeans.add(new Bean("账户余额", Utils.getDecimalFormat(Float.parseFloat(mDetailsEntity.balance))));
                } else {
                    mBeans.add(new Bean("账户余额", "/"));
                }
                mBeans.add(new Bean("交易状态", (mPayFlowBean.tradeStatus == 1 ? "已完成" : "冻结中")));
                if (mDetailsEntity.tradeFlowType != 1 && !Utils.isEmpty(mPayFlowBean.payChannels)) {
                    mBeans.add(new Bean("付款渠道", mPayFlowBean.payChannels));
                }
                if (!Utils.isEmpty(mPayFlowBean.flowNo)) {
                    mBeans.add(new Bean("交易流水号", mPayFlowBean.flowNo));
                }
                if (!Utils.isEmpty(mPayFlowBean.orderId)) {
                    mBeans.add(new Bean("订单编号", mPayFlowBean.paymentId));
                }
                mBeans.add(new Bean("余额充值时间", Utils.getTime(mPayFlowBean.created)));
                if (!Utils.isEmpty(mDetailsEntity.retMsg)) {
                    mBeans.add(new Bean("余额充值时间", Utils.getTime(mPayFlowBean.created)));
                    mBeans.add(new Bean("失败原因", mDetailsEntity.retMsg));
                    mBeans.add(new Bean("退款渠道", Utils.isEmpty(mDetailsEntity.withdrawBankAccount) ? "原路退回" : "原路退回(" + mDetailsEntity.withdrawBankAccount + ")"));
                }
            } else if (mPayFlowBean.payType.contains("余额支付")) {
                mBeans.add(new Bean("交易状态", (mPayFlowBean.tradeStatus == 1 ? "已完成" : "冻结中")));
                if (!Utils.isEmpty(mPayFlowBean.payName)) {
                    mBeans.add(new Bean("付款方", mPayFlowBean.payName.length() >= 12 ? mPayFlowBean.payName.substring(12) + "..." : mPayFlowBean.payName));
                }
                if (!Utils.isEmpty(mPayFlowBean.payeeName)) {
                    mBeans.add(new Bean("收款方", mPayFlowBean.payeeName.length() >= 12 ? mPayFlowBean.payeeName.substring(12) + "..." : mPayFlowBean.payeeName));
                }
                if (!Utils.isEmpty(mPayFlowBean.flowNo)) {
                    mBeans.add(new Bean("交易流水号", mPayFlowBean.flowNo));
                }
                if (!Utils.isEmpty(mDetailsEntity.paymentId)) {
                    mBeans.add(new Bean("订单编号", mDetailsEntity.paymentId));
                }
                if (!Utils.isEmpty(mDetailsEntity.orderId) && !mPayFlowBean.sourceType.contains("2")) {
                    mBeans.add(new Bean("业务支付号", mDetailsEntity.orderId));
                }
                mBeans.add(new Bean("创建时间", Utils.getTime(mPayFlowBean.created)));
                if (mPayFlowBean.payTime != 0) {
                    mBeans.add(new Bean("付款时间", Utils.getTime(mPayFlowBean.payTime)));
                }
                if (!Utils.isEmpty(mPayFlowBean.finishTime)) {
                    mBeans.add(new Bean("解冻时间", Utils.getTime(Long.parseLong(mPayFlowBean.finishTime))));
                }
            } else {
                mBeans.add(new Bean("交易状态", (mPayFlowBean.tradeStatus == 1 ? "已完成" : "冻结中")));
                if (!Utils.isEmpty(mPayFlowBean.payName)) {
                    mBeans.add(new Bean("付款方", mPayFlowBean.payName));
                }
                if (mDetailsEntity.tradeFlowType != 1 && !Utils.isEmpty(mPayFlowBean.payChannels) && (mPayFlowBean.payType.contains("快捷") || mPayFlowBean.payType.contains("B2B"))) {
                    mBeans.add(new Bean("付款渠道", mPayFlowBean.payChannels));
                }
                if (!Utils.isEmpty(mPayFlowBean.payeeName)) {
                    mBeans.add(new Bean("收款方", mPayFlowBean.payeeName));
                }
                if (!Utils.isEmpty(mPayFlowBean.payAccountName)) {
                    mBeans.add(new Bean("付款账户", mPayFlowBean.payAccountName));
                }
                if (!Utils.isEmpty(mPayFlowBean.payAccountNumber)) {
                    mBeans.add(new Bean("付款账号", mPayFlowBean.payAccountNumber));
                }
                if (!Utils.isEmpty(mPayFlowBean.flowNo)) {
                    mBeans.add(new Bean("交易流水号", mPayFlowBean.flowNo));
                }
                if (!Utils.isEmpty(mDetailsEntity.paymentId)) {
                    mBeans.add(new Bean("订单编号", mDetailsEntity.paymentId));
                }
                if (!Utils.isEmpty(mDetailsEntity.orderId) && !mPayFlowBean.sourceType.contains("2")) {
                    mBeans.add(new Bean("业务支付号", mDetailsEntity.orderId));
                }
                mBeans.add(new Bean("创建时间", Utils.getTime(mPayFlowBean.created)));
                if (mPayFlowBean.payTime != 0) {
                    mBeans.add(new Bean("付款时间", Utils.getTime(mPayFlowBean.payTime)));
                }
                if (!Utils.isEmpty(mPayFlowBean.finishTime)) {
                    mBeans.add(new Bean("解冻时间", Utils.getTime(Long.parseLong(mPayFlowBean.finishTime))));
                }
            }
            mAdapter.setNewInstance(mBeans);
            mBinding.view.setVisibility(View.VISIBLE);
        });
    }

    @Override
    public void initData() {
        mModel = new ViewModelProvider(this).get(FinanceFlowModel.class);
        mModel.getPayFlowDetails(this, mPayFlowBean.flowNo);
    }
}