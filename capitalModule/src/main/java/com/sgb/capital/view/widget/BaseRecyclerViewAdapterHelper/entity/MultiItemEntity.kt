package com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.entity

/**
 * 多布局类型
 */
interface MultiItemEntity {
    val itemType: Int
}
