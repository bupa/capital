package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;

import com.sgb.capital.R;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:选择支付类型
 */
public class PaySelectPop extends BasePopupWindow {
    public View mTvBalancePayBtn;
    public View mTvQuickPayBtn;

    public PaySelectPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.pay_select_pop);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> dismiss());
        mTvBalancePayBtn = v.findViewById(R.id.tv_balance_pay_btn);
        mTvQuickPayBtn = v.findViewById(R.id.tv_quick_pay_btn);
        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }

    public void show(View view) {
        setPopupGravity(Gravity.TOP);
        showPopupWindow(view);
    }
}