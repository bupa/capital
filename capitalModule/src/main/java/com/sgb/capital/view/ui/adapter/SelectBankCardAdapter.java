package com.sgb.capital.view.ui.adapter;

import android.content.Context;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.BindBankCardListEntity;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.ItemViewHolder;

import java.util.List;

public class SelectBankCardAdapter extends BaseRecycleAdapter<BindBankCardListEntity> {
    public SelectBankCardAdapter(Context context, List<BindBankCardListEntity> datas, int tag) {
        super(context, tag, datas);
    }

    @Override
    public void convert(ItemViewHolder holder, final BindBankCardListEntity entity) {
        holder.setBinding(BR.viewModel, entity);

        // TODO: 2021/6/16 0016 解密
        String bankAccount = CryptoUtils.decryptAES_ECB(entity.bankAccount);
        holder.setText(R.id.tv_name, entity.bankName + "(" + bankAccount.substring(0, 4) +
                " **** **** " + bankAccount.substring(bankAccount.length() - 4) + ")");
        if (holder.getLayoutPosition() == this.datas.size() - 1) {
            holder.setVisible(R.id.line1, true);
        } else {
            holder.setVisible(R.id.line1, false);
        }
    }
}
