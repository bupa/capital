package com.sgb.capital.view.ui.adapter;

import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.model.SelBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

public class ZSelect1Adapter extends BaseQuickAdapter<SelBean, BaseViewHolder> {
    public ZSelect1Adapter(List<SelBean> data) {
        super(R.layout.zselect_adapter, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SelBean item) {
        TextView item_name = helper.getView(R.id.tv_zname);
        item_name.setText(Utils.isEmpty(item.name) ? "全部" : item.name);
        item_name.setBackgroundColor(Utils.getColor(item.isSel ? R.color.color_10EF4033 : R.color.white));
        item_name.setTextColor(Utils.getColor(item.isSel ? R.color.color_EF4033 : R.color.color_333333));
    }
}