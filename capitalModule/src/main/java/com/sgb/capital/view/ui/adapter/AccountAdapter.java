package com.sgb.capital.view.ui.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.widget.TextView;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.AccountBean;
import com.sgb.capital.utils.ItemViewHolder;
import com.sgb.capital.utils.Utils;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:
 */
public class AccountAdapter extends BaseRecycleAdapter<AccountBean> {

    private final String mAccType;

    public AccountAdapter(Context context, List<AccountBean> beans, String accType) {
        super(context, R.layout.account_adapter, beans);
        mAccType = accType;
    }

    @Override
    public void convert(ItemViewHolder holder, AccountBean item) {
        String balance = (mAccType.contains("108") ? "账户余额：" : "待解冻金额：")
                + Utils.getDecimalFormat(Float.parseFloat(item.balance));
        SpannableString payType = Utils.getKeyWordSpan(Utils.getColor(R.color.color_333333), balance, Utils.getDecimalFormat(Float.parseFloat(item.balance)));
        holder.setBinding(BR.viewModel, item);
        holder.setText(R.id.tv_balance, payType);


        holder.setText(R.id.tv_tip, item.transTypeName.contains("余额支付")?"余额支付": item.transTypeName.contains("退") ? "退款" : !Utils.isEmpty(item.transTypeName) ? item.transTypeName.contains("充值") ? "余额充值" :
                mAccType.contains("108") ? item.direct.contains("C") ? "冻结解除" : "提现" : item.direct.contains("C") ? "资金冻结" : "冻结解除" : mAccType.contains("108") ? item.direct.contains("C") ? "冻结解除" : "提现" : item.direct.contains("C") ? "资金冻结" : "冻结解除");
        holder.setText(R.id.tv_orderId, item.orderId);
        holder.setText(R.id.tv_amount, (item.direct.contains("C") ? "+" : "-") + Utils.getDecimalFormat(Float.parseFloat(item.amount)));
        holder.setText(R.id.tv_time, item.orderDate.substring(0, 4) + "-" + item.orderDate.substring(4, 6) + "-" + item.orderDate.substring(6));
        TextView tv_amount = holder.getView(R.id.tv_amount);
        tv_amount.setTextColor(Utils.getColor(item.direct.contains("C") ? R.color.color_000000 : R.color.color_FF4B10));
    }

}
