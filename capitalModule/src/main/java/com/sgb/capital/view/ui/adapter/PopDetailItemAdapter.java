package com.sgb.capital.view.ui.adapter;

import android.content.Context;
import android.widget.TextView;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.ItemViewHolder;
import com.sgb.capital.utils.Utils;

import java.util.List;

public class PopDetailItemAdapter extends BaseRecycleAdapter<Bean> {


    public PopDetailItemAdapter(Context context, List<Bean> datas) {
        super(context, R.layout.item_pop_pay_detail, datas);
    }

    @Override
    public void convert(ItemViewHolder holder, Bean detailEntity) {
        holder.setBinding(BR.viewModel, detailEntity);
        if (holder.getLayoutPosition() == datas.size() - 1) {
            holder.setVisible(R.id.last_line, true);
        } else {
            holder.setVisible(R.id.last_line, false);
        }
        TextView tv_amount = holder.getView(R.id.tv_value);
        tv_amount.setTextColor(Utils.getColor(detailEntity.no.contains("交易金额") ? R.color.color_FF4B10 : R.color.color_333333));
    }
}
