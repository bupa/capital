package com.sgb.capital.view.ui.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.BankcardsFragmentBinding;
import com.sgb.capital.model.AddAndEditCardsBean;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.CardsPop;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.ui.activity.AddCompanyBackActivity;
import com.sgb.capital.view.ui.activity.BindCompanyBackActivity;
import com.sgb.capital.view.ui.activity.BindUserBackActivity;
import com.sgb.capital.view.ui.activity.CompanyBankCardsActivity;
import com.sgb.capital.view.ui.activity.EditCompanyBackActivity;
import com.sgb.capital.view.ui.activity.OpenAccountFirstActivity;
import com.sgb.capital.view.ui.activity.PhoneNumberActivity;
import com.sgb.capital.view.ui.adapter.BankCardsItemAdapter;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.BankCardsModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:企业银行
 */
public class BankCardsFragment extends AppLazyFragment {


    BankCardsModel mModel;
    private BankcardsFragmentBinding mBinding;
    private BankCardsItemAdapter mBankCardsItemAdapter;
    private List<BankBean> mBeans = new ArrayList<>();
    private CardsPop mCardsPop;
    private TipPop mTipPop;
    private String mData;
    private BankBean mDel;
    private BankBean mBankBean;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.bankcards_fragment, container, false);
        return mBinding.getRoot();
    }

    @Override
    protected void initView() {
        // TODO: 2021/6/17 0017
        mBinding.rvList.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mCardsPop = new CardsPop(getActivity());
        mTipPop = new TipPop(getActivity(), Utils.getString(R.string.capital_tip), "解绑后，支付时无法快捷选择银行卡", "解绑", "取消");
        mTipPop.mTvSure.setTextColor(Utils.getColor(R.color.color_FF2928));
        mTipPop.mTvCancel.setTextColor(Utils.getColor(R.color.color_5792FD));
        mModel = new ViewModelProvider(this).get(BankCardsModel.class);
        mBankCardsItemAdapter = new BankCardsItemAdapter(mBeans, false);
    }


    @Override
    public void initObserve() {
        mModel.mBusinessInfoByCompanyEntity.observe(this, businessInfoByCompanyEntity -> {
            if (businessInfoByCompanyEntity == null || businessInfoByCompanyEntity.orderCount == 0) {
                MToast.showToast(Utils.getContext(), "数据异常");
                return;
            }
            if (businessInfoByCompanyEntity.subMerType.contains("3")) {
                BindCompanyBackActivity.start(mActivity);
            } else {
                BindUserBackActivity.start(mActivity, new Gson().toJson(businessInfoByCompanyEntity));
            }
        });
        mModel.mListMutableLiveData.observe(this, bankBeans -> payBankCardList(bankBeans));
        mModel.mBindBankCardLists.observe(this, bankBeans -> getBindBankCardLists(bankBeans));
    }

    public void initListener() {
        mTipPop.mTvSure.setOnClickListener(v -> {
            mTipPop.dismiss();
            mModel.payBankCard(this, mDel.id);
        });
        mBankCardsItemAdapter.setOnItemClickListener((adapter, view, position) -> {
            mBankBean = mBankCardsItemAdapter.getData().get(position);
            if (TextUtils.isEmpty(mBankBean.openingBank)) {
                mCardsPop.mBean = mBankBean;
                mCardsPop.show();
            }
        });
        mBinding.llBtn.setOnClickListener(v -> {
            CompanyBankCardsActivity activity = (CompanyBankCardsActivity) getActivity();
            if (activity.mPosition == 0) {
                AddCompanyBackActivity.start(getActivity());
            } else {
                CompanyBankCardsActivity companyBankCardsActivity = (CompanyBankCardsActivity) mActivity;
                int position = companyBankCardsActivity.mProcess;
                if (position == 0 || position == 3 || position == 1) {
                    TipPop tipPop = new TipPop(mActivity);
                    tipPop.initStr(Utils.getString(R.string.capital_tip),
                            position == 1 ? ("企业钱包开通审核中\n通过后可使用此功能") : "开通企业钱包后\n可使用此功能", (position == 1 || position == 2) ? null : "前往开通", "知道了");
                    tipPop.show();
                    tipPop.mTvSure.setOnClickListener(v1 -> {
                        tipPop.dismiss();
                        OpenAccountFirstActivity.start(mActivity, false);
                    });
                    return;
                }
                mModel.getBusinessInfoByCompany(mActivity);
            }
        });
    }

    @Override
    protected void onLazyLoad() {
        Bundle arguments = getArguments();
        mData = arguments.getString("data");
        mBankCardsItemAdapter.mIsPay = mData.contains("支付");
        mBinding.rvList.setAdapter(mBankCardsItemAdapter);
        if (mData.contains("支付")) {
            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this);
            }
            mModel.payBankCardList(this);
        } else {
            mModel.getBindBankCardLists(this);
        }
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants.MODIFY_THE_PHONE_NUMBER:
                mCardsPop.dismiss();
                BankBean data = (BankBean) event.data;
                PhoneNumberActivity.start(getActivity(), data.mobileId, data.id);
                break;
            case Constants
                    .EVENT_UNBIND:
                mDel = (BankBean) event.data;
                mTipPop.show();
                break;
            case Constants
                    .EVENT_ALTER_COMPANY:
                BankBean bankBean = (BankBean) event.data;
                AddAndEditCardsBean bean = new AddAndEditCardsBean();
                bean.type = bankBean.type == 0 ? "0" : "1";
                bean.accountName = bankBean.accountName;
                bean.phone = bankBean.phone;
                bean.bankCard = bankBean.bankCard;
                bean.gateId = bankBean.gateId;
                if (bean.type.contains("0")) {
                    bean.idCard = bankBean.idCard;
                }
                bean.openingBank = bankBean.openingBank;
                bean.id = bankBean.id;
                EditCompanyBackActivity.start(getActivity(), new Gson().toJson(bean));
                break;
            case Constants
                    .EVENT_REFRESH:
                mModel.payBankCardList(this);
                break;
        }
    }

    public void payBankCardList(List<BankBean> data) {
        mBankCardsItemAdapter.setNewInstance(data);
    }

    public void getBindBankCardLists(List<BankBean> data) {
        mBankCardsItemAdapter.setNewInstance(data);
        mBankCardsItemAdapter.notifyDataSetChanged();
    }


    public void payBankCard(boolean b) {
        if (b) {
            mModel.payBankCardList(this);
        }
    }
}