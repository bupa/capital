package com.sgb.capital.view.pop;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.callback.OnClickPopListener;

import razerdp.basepopup.BasePopupWindow;


/**
 * 作者:张磊
 * 日期:2019/10/24
 * 说明:提示弹框
 */
public class TipInputPop extends BasePopupWindow {

    private String mSureStr;
    private String mCancelStr;
    public TextView mTvSure;
    public TextView mTvCancel;
    public EditText mTvTip;
    private View mLine;
    private String mTitle;
    private String mTip;
    private TextView mTvTitle;
    private OnClickPopListener mOnClickListener;

    public TipInputPop(Context context) {
        super(context);
    }

    /**
     * @param context   上下文
     * @param title     标题
     * @param tip       提示
     * @param sureStr   确定文案
     * @param CancelStr 取消文案
     */
    public TipInputPop(Context context, String title, String tip, String sureStr, String CancelStr) {
        super(context);
        mTitle = title;
        mTip = tip;
        mSureStr = sureStr;
        mCancelStr = CancelStr;
    }


    public TipInputPop(Context context, String title, String tip, String sureStr, String cancelStr, OnClickPopListener onClickListener) {
        super(context);
        mTitle = title;
        mTip = tip;
        mSureStr = sureStr;
        mCancelStr = cancelStr;
        mOnClickListener = onClickListener;
    }


    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.tipinput_pop);
        mTvSure = v.findViewById(R.id.tv_sure);
        mTvSure.setTypeface(Typeface.DEFAULT_BOLD);
        mTvCancel = v.findViewById(R.id.tv_cancel);
        mTvTip = v.findViewById(R.id.tv_tip);
        mTvTitle = v.findViewById(R.id.tv_title);
        mLine = v.findViewById(R.id.line);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> dismiss());
        initListener();
        return v;
    }

    private void initListener() {
        if (mOnClickListener == null) return;
        mTvSure.setOnClickListener(v -> mOnClickListener.onClickSure());
        mTvCancel.setOnClickListener(v -> mOnClickListener.onClickCancel());
    }


    public void show() {
        showPopupWindow();
    }


    // 设置点击回调
    public void setOnClickListener(OnClickPopListener onClickListener) {
        mOnClickListener = onClickListener;
    }
}
