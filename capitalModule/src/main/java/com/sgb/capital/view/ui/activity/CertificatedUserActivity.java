package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.CertificateduserActivityBinding;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.QrCodeModel;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

/**
 * 作者:张磊
 * 日期:2021/4/19 0019
 * 说明:个人认证资料
 */
public class CertificatedUserActivity extends BaseActivity {
    CertificateduserActivityBinding mBinding;
    QrCodeModel mModel;

    public static void start(Context context) {
        Intent intent = new Intent(context, CertificatedUserActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        Utils.setStatusBarLightMode(this, true);
        mBinding = DataBindingUtil.setContentView(this, R.layout.certificateduser_activity);
    }

    @Override
    public void initData() {
        mModel = new ViewModelProvider(this).get(QrCodeModel.class);
        mModel.getBusinessInfoByUser(this);
    }

    @Override
    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void initObserve() {
        mModel.mBusinessInfoByCompanyEntity.observe(this, businessInfoByCompanyEntity -> {
            if (businessInfoByCompanyEntity == null) return;
            try {
                mBinding.llShow.setVisibility(View.VISIBLE);
                mBinding.tvName.setText(businessInfoByCompanyEntity.custName);
                mBinding.tvCard.setText(businessInfoByCompanyEntity.identityCode.substring(0, 6) + "********" + businessInfoByCompanyEntity.
                        identityCode.substring(businessInfoByCompanyEntity.identityCode.length() - 4));
                mBinding.tvPhone.setText(businessInfoByCompanyEntity.mobileId);
                mBinding.tvUserId.setText(businessInfoByCompanyEntity.userId);
            } catch (Exception e) {
                MToast.showToast(Utils.getContext(), "木好意思呀,请先开通!");
                Utils.postDelay(() -> finish(),2000);
            }

        });
    }
}