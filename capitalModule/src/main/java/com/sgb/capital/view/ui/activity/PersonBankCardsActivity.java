package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.PersonbankcardsActivityBinding;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.CardsPop;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.ui.adapter.PersonBankCardsItemAdapter;
import com.sgb.capital.viewmodel.PersonBankCardsModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:个人银行卡
 */
public class PersonBankCardsActivity extends BaseActivity {


    PersonBankCardsModel mModel;
    private PersonbankcardsActivityBinding mBinding;
    private PersonBankCardsItemAdapter mPersonBankCardsItemAdapter;
    private CardsPop mCardsPop;
    private TipPop mTipPop;
    private BankBean mDel;

    public static void start(Context context) {
        Intent intent = new Intent(context, PersonBankCardsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


    @Override
    protected void initView() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Utils.setStatusBarLightMode(this, true);
        mTipPop = new TipPop(this, "温馨提示", "解绑后,该银行卡将无法用于付款", "解绑", "取消");
        mBinding = DataBindingUtil.setContentView(this, R.layout.personbankcards_activity);
        mBinding.rvList.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mPersonBankCardsItemAdapter = new PersonBankCardsItemAdapter(null);
        mBinding.rvList.setAdapter(mPersonBankCardsItemAdapter);
        mBinding.ivBack.setOnClickListener(v -> finish());
        mCardsPop = new CardsPop(this);
        mModel = new ViewModelProvider(this).get(PersonBankCardsModel.class);
    }

    @Override
    public void initData() {
        mModel.personalList(this);
    }

    @Override
    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
        mModel.mPersonalList.observe(this, bankBeans -> personalList(bankBeans));
        mTipPop.mTvSure.setOnClickListener(v -> {
            mTipPop.dismiss();
            mModel.payBankCard(this, mDel.id);
        });

        mPersonBankCardsItemAdapter.setOnItemClickListener((adapter, view, position) -> {
            BankBean bankBean = mPersonBankCardsItemAdapter.getData().get(position);
            if (bankBean.bindBank) {
                mCardsPop.mBean = bankBean;
                mCardsPop.show();
            }
        });
        mBinding.llBtn.setOnClickListener(v -> {
            AddPersonBankCardActivity.start(this);
        });
    }

    public void personalList(List<BankBean> data) {
        mPersonBankCardsItemAdapter.setNewInstance(data);
    }

    public void payBankCard(boolean ok) {
        if (ok) {
            mModel.personalList(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.sendMsg(Constants.EVENT_REFRESH, null);
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_UNBIND:
                mDel = (BankBean) event.data;
                mTipPop.show();
                break;
            case Constants
                    .EVENT_ALTER:
                BankBean data = (BankBean) event.data;
                EditPersonBankActivity.start(this, new Gson().toJson(data));
                break;
            case Constants
                    .EVENT_REFRESH:
                mModel.personalList(this);
        }
    }
}