package com.sgb.capital.view.widget.xrecyclerview;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author zkw
 * @describe
 * @date 2018/11/21
 */
public class ZSecondaryEntity<D> {
    @SerializedName(value = "proList", alternate = {"depart", "children","childList","projects","data","category", "employee", "itemCatDTOList", "groupItem"})
    private List<D> datas;

    public List<D> getDatas() {
        return datas;
    }

    public void setDatas(List<D> datas) {
        this.datas = datas;
    }
}
