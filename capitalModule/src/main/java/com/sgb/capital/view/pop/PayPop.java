package com.sgb.capital.view.pop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.base.BasePopWindow;
import com.sgb.capital.callback.MyZTextWatcher;
import com.sgb.capital.model.PayCardEntity;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.PayTimerUtils;
import com.sgb.capital.utils.Utils;

public class PayPop extends BasePopWindow {

    private TextView back_order;
    private ImageView iv_close;
    private TextView pay_price_tv, new_get, phone_number, pay_button, tip;
    private TextView pass_1, pass_2, pass_3, pass_4, pass_5, pass_6;
    private boolean clock_2, clock_3, clock_4, clock_5, clock_6;//输入框焦点锁
    private LinearLayout pay_layout, pay_success_layout;
    private OnPopClickListener clickListener;
    private PayTimerUtils payTimerUtils;
    public String orderNo;
    public boolean isCreate = true;

    public PayPop(Context context, String orderNo, OnPopClickListener clickListener) {
        super(context, DIR_DOWN_UP);
        this.orderNo = orderNo;
        this.clickListener = clickListener;
    }

    @Override
    protected int popLayout() {
        return R.layout.pop_pay;
    }

    @Override
    protected void initView(PopViewHolder holder) {
        pay_layout = holder.getPop().findViewById(R.id.pay_layout);
        iv_close = holder.getPop().findViewById(R.id.close);
        pay_price_tv = holder.getPop().findViewById(R.id.pay_price_tv);
        phone_number = holder.getPop().findViewById(R.id.phone_number);
        pass_1 = holder.getPop().findViewById(R.id.pass_1);
        pass_2 = holder.getPop().findViewById(R.id.pass_2);
        pass_3 = holder.getPop().findViewById(R.id.pass_3);
        pass_4 = holder.getPop().findViewById(R.id.pass_4);
        pass_5 = holder.getPop().findViewById(R.id.pass_5);
        pass_6 = holder.getPop().findViewById(R.id.pass_6);
        tip = holder.getPop().findViewById(R.id.tip);
        new_get = holder.getPop().findViewById(R.id.new_get);
        pay_button = holder.getPop().findViewById(R.id.pay_button);
        pay_success_layout = holder.getPop().findViewById(R.id.pay_success_layout);
        back_order = holder.getPop().findViewById(R.id.back_order);
        iv_close.setOnClickListener(v -> dismiss());
        pay_button.setOnClickListener(v -> {
            if (TextUtils.isEmpty(pass_1.getText()) && TextUtils.isEmpty(pass_2.getText()) && TextUtils.isEmpty(pass_3.getText())
                    && TextUtils.isEmpty(pass_4.getText()) && TextUtils.isEmpty(pass_5.getText()) && TextUtils.isEmpty(pass_6.getText())) {
                setTipText("请输入验证码");
                return;
            } else if (TextUtils.isEmpty(pass_1.getText()) || TextUtils.isEmpty(pass_2.getText()) || TextUtils.isEmpty(pass_3.getText())
                    || TextUtils.isEmpty(pass_4.getText()) || TextUtils.isEmpty(pass_5.getText()) || TextUtils.isEmpty(pass_6.getText())) {
                setTipText("验证码错误");
                return;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(pass_1.getText());
            stringBuffer.append(pass_2.getText());
            stringBuffer.append(pass_3.getText());
            stringBuffer.append(pass_4.getText());
            stringBuffer.append(pass_5.getText());
            stringBuffer.append(pass_6.getText());
            pay_button.setEnabled(false);
            Utils.postDelay(() -> pay_button.setEnabled(true), 3000);
            clickListener.onClickPayButton(stringBuffer.toString());
        });
        new_get.setOnClickListener(v -> clickListener.onClickNewGet());
        back_order.setOnClickListener(v -> {
            dismiss();
            Intent intent = new Intent();
            intent.putExtra("orderNo", orderNo);
            ((Activity) mContext).setResult(Activity.RESULT_OK, intent);
            ((Activity) mContext).finish();
        });
        pass_2.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                if (pass_2.getText().toString().length() == 0) {
                    pass_1.requestFocus();
                    return true;
                } else {
                    return false;
                }

            }
            return false;
        });
        pass_3.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                if (pass_3.getText().toString().length() == 0) {
                    pass_2.requestFocus();
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        });
        pass_4.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                if (pass_4.getText().toString().length() == 0) {
                    pass_3.requestFocus();
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        });
        pass_5.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                if (pass_5.getText().toString().length() == 0) {
                    pass_4.requestFocus();
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        });
        pass_6.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                if (pass_6.getText().toString().length() == 0) {
                    pass_5.requestFocus();
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        });
        pass_1.addTextChangedListener(new MyZTextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    pass_1.clearFocus();
                    pass_2.requestFocus();
                    clock_2 = true;
                    tip.setVisibility(View.INVISIBLE);
                } else {
                    pass_1.requestFocus();
                    if (isCreate) {
                        tip.setVisibility(View.VISIBLE);
                        tip.setText("请输入手机验证码");
                        tip.setTextColor(Utils.getColor(R.color.color_969696));
                        isCreate = false;
                    } else {
                        setTipText("请输入验证码");
                    }

                }
            }
        });
        pass_2.addTextChangedListener(new MyZTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tip.setVisibility(View.INVISIBLE);
                if (clock_2) {
                    if (s.length() > 0) {
                        pass_2.clearFocus();
                        pass_3.requestFocus();
                        clock_3 = true;
                    } else {
                        pass_2.clearFocus();
                        pass_1.requestFocus();
                        clock_2 = false;
                    }
                }
            }
        });
        pass_3.addTextChangedListener(new MyZTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tip.setVisibility(View.INVISIBLE);
                if (clock_3) {
                    if (s.length() > 0) {
                        pass_3.clearFocus();
                        pass_4.requestFocus();
                        clock_4 = true;
                    } else {
                        clock_2 = true;
                        clock_3 = false;
                        pass_3.clearFocus();
                        pass_2.requestFocus();
                    }
                }
            }
        });
        pass_4.addTextChangedListener(new MyZTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tip.setVisibility(View.INVISIBLE);
                if (clock_4) {
                    if (s.length() > 0) {
                        pass_4.clearFocus();
                        pass_5.requestFocus();
                        clock_5 = true;
                    } else {
                        clock_3 = true;
                        clock_4 = false;
                        pass_4.clearFocus();
                        pass_3.requestFocus();
                        tip.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
        pass_5.addTextChangedListener(new MyZTextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tip.setVisibility(View.INVISIBLE);
                if (clock_5) {
                    if (s.length() > 0) {
                        clock_6 = true;
                        pass_5.clearFocus();
                        pass_6.requestFocus();
                    } else {
                        clock_4 = true;
                        pass_5.clearFocus();
                        pass_4.requestFocus();
                        clock_5 = false;
                    }
                }
            }
        });

        pass_6.addTextChangedListener(new MyZTextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tip.setVisibility(View.INVISIBLE);
                if (clock_6) {
                    clock_6 = false;
                    if (s.length() == 0) {
                        pass_6.clearFocus();
                        pass_5.requestFocus();
                        clock_5 = true;
                    }
                }
            }

        });
    }

    public void showPopType(boolean isPay, View view, String price, PayCardEntity entity) {
        if (isPay) {
            this.bgView.setClickable(false);
            this.bgView.setEnabled(false);
            pay_layout.setVisibility(View.VISIBLE);
            pay_success_layout.setVisibility(View.GONE);
            pay_price_tv.setText(price);
            String phone = CryptoUtils.decryptAES_ECB(entity.phone);
            if (!TextUtils.isEmpty(phone) && phone.length() == 11) {
                String pho1 = phone.substring(0, 3);
                String pho2 = phone.substring(phone.length() - 4);
                phone_number.setText(String.format(mContext.getString(R.string.capital_phone_info), pho1, pho2));
            }
            clickListener.onClickNewGet();
        } else {
            this.setFocusable(false);
            this.setOutsideTouchable(false);
            this.bgView.setClickable(true);
            pay_layout.setVisibility(View.GONE);
            pay_success_layout.setVisibility(View.VISIBLE);
            this.showDownPopwindow(view, true);
        }
    }

    public void setData(String price) {
        pay_price_tv.setText(price);
    }

    public void clearData() {
        pass_6.setText("");
        pass_5.setText("");
        pass_4.setText("");
        pass_3.setText("");
        pass_2.setText("");
        pass_1.setText("");
    }

    public void setTipText(String s) {
        tip.setVisibility(View.VISIBLE);
        tip.setTextColor(Utils.getColor(R.color.color_FF2928));
        tip.setText(s);
    }

    public void setStartTimer() {
        if (payTimerUtils != null) {
            payTimerUtils.cancel();
        }
        payTimerUtils = new PayTimerUtils(new_get, 60000, 1000); //倒计时1分钟
        payTimerUtils.start();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (onDisMiss != null)
            onDisMiss.disMiss();
    }

    public interface OnPopClickListener {
        void onClickNewGet();

        void onClickPayButton(String vCode);
    }
}
