package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sgb.capital.R;
import com.sgb.capital.base.BasePopWindow;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.utils.AdapterOnItemClick;
import com.sgb.capital.view.ui.adapter.BankAdapter;

import java.util.List;

/**
 * 说明信息弹窗
 */
public class ExplanatoryInfoPop extends BasePopWindow {
    private TextView okView;

    public ExplanatoryInfoPop(Context context) {
        super(context, DIR_DOWN_UP);
    }

    @Override
    protected int popLayout() {
        return R.layout.pop_explanatory_info;
    }

    @Override
    protected void initView(PopViewHolder holder) {
        okView = holder.getPop().findViewById(R.id.tv_ok);

        okView.setOnClickListener(v -> {
            dismiss();
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (onDisMiss != null)
            onDisMiss.disMiss();
    }
}
