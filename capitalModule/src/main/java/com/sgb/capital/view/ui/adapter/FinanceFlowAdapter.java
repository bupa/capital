package com.sgb.capital.view.ui.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.view.View;
import android.widget.TextView;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.RecordsBean;
import com.sgb.capital.utils.ItemViewHolder;
import com.sgb.capital.utils.Utils;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:财务流水
 */
public class FinanceFlowAdapter extends BaseRecycleAdapter<RecordsBean> {

    public FinanceFlowAdapter(Context context, List<RecordsBean> beans) {
        super(context, R.layout.financeflow_adapter, beans);
    }

    @Override
    public void convert(ItemViewHolder holder, RecordsBean item) {
        holder.setBinding(BR.viewModel, item);
        String payTypeStr = Utils.isEmpty(item.payType) ? "/" : item.payType;
        String payNameStr = Utils.isEmpty(item.payName) ? "/" : item.payName;
        String payeeNameStr = Utils.isEmpty(item.payeeName) ? "/" : item.payeeName;
        SpannableString payType = Utils.getKeyWordSpan(Utils.getColor(R.color.color_333333), "支付方式：" + payTypeStr, payTypeStr);
        SpannableString payeeName = Utils.getKeyWordSpan(Utils.getColor(R.color.color_333333), "收款方：" + payeeNameStr, payeeNameStr);
        SpannableString payName = Utils.getKeyWordSpan(Utils.getColor(R.color.color_333333), "付款方：" + payNameStr, payNameStr);
        SpannableString created = Utils.getKeyWordSpan(Utils.getColor(R.color.color_333333), "入账时间：" + Utils.getTime(item.payTime), Utils.getTime(item.payTime));
        SpannableString tradeStatus = Utils.getKeyWordSpan(Utils.getColor(R.color.color_333333), "交易状态：" + (item.tradeStatus == 1 ?
                "已完成" : item.tradeStatus == 2 ? "已失败" : item.tradeStatus == 3 ? "冻结中" : item.tradeType.contains("提现") ? "提现中" :  item.sourceType.contains("4")?"退款中": "确认中"), item.tradeStatus == 1 ?
                "已完成" : item.tradeStatus == 2 ? "已失败" : item.tradeStatus == 3 ? "冻结中" : item.tradeType.contains("提现") ? "提现中" :item.sourceType.contains("4")?"退款中": "确认中");
        TextView tv_tradeAmount = holder.getView(R.id.tv_tradeAmount);
        View ll_show = holder.getView(R.id.ll_show);
        ll_show.setVisibility(item.tradeType.contains("余额充值") ? View.GONE : View.VISIBLE);
        tv_tradeAmount.setTextColor(Utils.getColor(item.tradeFlowType == 1 ? R.color.color_000000 : R.color.color_FF4B10));
        holder
                .setText(R.id.tv_tradeType, item.sourceType.contains("4") ? "订单退款" : item.sourceType.contains("3") ? "账户提现" : item.sourceType.contains("2") ? "转账汇款" : item.tradeType)
                .setText(R.id.tv_flowNo, item.flowNo)
                .setText(R.id.tv_tradeAmount, (item.tradeFlowType == 1 ? "+" : "-") + Utils.getDecimalFormat(item.tradeAmount))
                .setText(R.id.tv_payType, payType).setText(R.id.tv_payeeName, payeeName).setText(R.id.tv_payName, payName).setText(R.id.tv_created, created).setText(R.id.tv_tradeStatus, tradeStatus);

    }

}
