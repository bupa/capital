package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.AsiarecorddetailsActivityBinding;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.InvoiceDetailBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.ui.adapter.AsiaRecordDetailsAdapter;
import com.sgb.capital.view.ui.adapter.WaterAdapter;
import com.sgb.capital.viewmodel.AsiaRecordModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import razerdp.basepopup.BasePopupWindow;

/**
 * 作者:张磊
 * 日期:2021/7/21 0021
 * 说明:发票详情
 */
public class AsiaRecordDetailsActivity extends BaseActivity {

    private AsiarecorddetailsActivityBinding mBinding;
    private AsiaRecordModel mModel;
    private AsiaRecordDetailsAdapter mAdapter;
    private List<Bean> mInvoiceInformation;
    private List<Bean> mApplyInformation;
    private List<Bean> mLogisticsInformation;
    private int mIndex = 1;
    private WaterAdapter mWaterAdapter;
    private InvoiceDetailBean mData;
    private TipPop mTipPop;
    private String mInvoiceId;

    public static void start(Context context, String invoiceId) {
        Intent intent = new Intent(context, AsiaRecordDetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("invoiceId", invoiceId);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.asiarecorddetails_activity);
        mBinding.rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mBinding.rvWater.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mModel = new ViewModelProvider(this).get(AsiaRecordModel.class);
        mAdapter = new AsiaRecordDetailsAdapter(null);
        mWaterAdapter = new WaterAdapter(null);
        mBinding.rv.setAdapter(mAdapter);
        mBinding.rvWater.setAdapter(mWaterAdapter);
        mTipPop = new TipPop(this);
    }

    @Override
    public void initData() {
        mInvoiceId = getIntent().getStringExtra("invoiceId");
        mModel.getInvoiceDetail(this, mInvoiceId);
        mInvoiceInformation = new ArrayList<>();
        mApplyInformation = new ArrayList<>();
        mLogisticsInformation = new ArrayList<>();
    }


    @Override
    public void initObserve() {
        mModel.mCode.observe(this, s -> {
            if (s.contains("1")) {
                mTipPop.initStr("发票状态信息已发生变化", null, Utils.getString(R.string.capital_sureStr));
                mTipPop.show();
                return;
            }
            RepealActivity.start(this, mInvoiceId, mData.invoiceStatus);
        });

        mModel.mInvoiceDetailBean.observe(this, data -> {
            mBinding.tvRepeal.setVisibility((data.invoiceStatus == 1 || data.invoiceStatus == 6) ? View.VISIBLE : View.GONE);
            mBinding.tvInvoiceStatus.setTextColor(Utils.getColor((data.invoiceStatus == 1 || data.invoiceStatus == 2 || data.invoiceStatus == 6 || data.invoiceStatus == 5) ? R.color.color_333333 : R.color.color_06C764));
            mBinding.tvTip.setTextColor(Utils.getColor(data.invoiceStatus == 2 ? R.color.color_EF4033 : R.color.color_5792FD));
            mBinding.tvInvoiceStatus.setText(
                    (data.invoiceStatus == 1 || data.invoiceStatus == 2 || data.invoiceStatus == 6) ? "未开票" :
                            (data.invoiceStatus == 3 || data.invoiceStatus == 4 || data.invoiceStatus == 7) ? "已开票" : "已撤销");
            mBinding.tvTip.setText(
                    data.invoiceStatus == 1 ? "平台审核中" :
                            data.invoiceStatus == 2 ? "平台驳回申请" :
                                    data.invoiceStatus == 3 ? "等待平台寄出" :
                                            data.invoiceStatus == 4 ? "平台已寄出" : "");
            mBinding.tvTotalFees.setText("￥" + Utils.getDecimalFormat(data.totalFees));
            mBinding.tvInvoiceId.setText(data.invoiceId);
            mData = data;
            // 发票信息
            mInvoiceInformation.add(new Bean("开票类型", data.makeBy == 1 ? "企业" : "个人"));
            mInvoiceInformation.add(new Bean("发票类型", data.invoiceType == 1 ? "增值税专用发票(纸质)" : "增值税普通发票(纸质)"));
            mInvoiceInformation.add(new Bean(data.makeBy == 1 ? "单位名称" : "发票抬头", data.invoiceHead));
            if (data.makeBy == 1) {
                mInvoiceInformation.add(new Bean("税号", data.companyDutyNo));
            }
            if (!Utils.isEmpty(data.companyBankName) && data.invoiceType == 1) {
                mInvoiceInformation.add(new Bean("开户银行", data.companyBankName));
                mInvoiceInformation.add(new Bean("银行账号", data.companyBankAccount));
                mInvoiceInformation.add(new Bean("地址", data.companyAddress));
                mInvoiceInformation.add(new Bean("企业电话", data.companyPhone));
            }
            mInvoiceInformation.add(new Bean("发票号码", data.invoiceNumber));
            mInvoiceInformation.add(new Bean("发票代码", data.invoiceCode));
            // 申请信息   // 状态(1.待审核 2.已驳回 3.待寄出 4.已寄出 5.已撤销 6.未开票 7.已开票 )
            mApplyInformation.add(new Bean("发票状态",
                    data.invoiceStatus == 1 ? "未开票(待审核)" :
                            data.invoiceStatus == 2 ? "未开票(已驳回)" :
                                    data.invoiceStatus == 3 ? "已开票(待寄出)" :
                                            data.invoiceStatus == 4 ? "已开票(已寄出)" :
                                                    data.invoiceStatus == 5 ? "已撤销" :
                                                            data.invoiceStatus == 6 ? "未开票" : "已开票"));
            mApplyInformation.add(new Bean("申请人", data.creatorName));

            mAdapter.setNewInstance(mInvoiceInformation);
            mApplyInformation.add(new Bean("申请时间", Utils.getTime(data.created)));
            if (data.invoiceStatus == 5) {
                mApplyInformation.add(new Bean("撤销时间", Utils.getTime(data.revokeDate)));
            } else {
                if (!Utils.isEmpty(data.remark)) {
                    mApplyInformation.add(new Bean("驳回原因", data.remark));
                }
            }
            // 流水信息
            if (data.payFlow == null || data.payFlow.size() == 0) {
                mBinding.tvWater.setVisibility(View.GONE);
            } else {
                String str = "您的发票关联" + data.payFlow.size() + "条费用流水信息，合计开票金额为￥" + Utils.getDecimalFormat(data.totalFees);
                mBinding.tvWater.setText(str);
            }
            // 物流信息
            if (!Utils.isEmpty(data.mailNo)) {
                mLogisticsInformation.add(new Bean("运单号", data.mailNo));
                mLogisticsInformation.add(new Bean("物流公司", data.mailType));
                mLogisticsInformation.add(new Bean("收货人", data.mailName));
                mLogisticsInformation.add(new Bean("联系电话", data.mailPhone.substring(0, 3) + "****" + data.mailPhone.substring(data.mailPhone.length() - 4)));
                mLogisticsInformation.add(new Bean("地址", data.mailAddress));
            }

            mBinding.llTab1.setOnClickListener(v -> getShow(1, R.color.color_EF4033, View.VISIBLE, R.color.color_333333, View.INVISIBLE, R.color.color_333333, View.INVISIBLE, R.color.color_333333, View.INVISIBLE));
        });
    }


    @Override
    public void initListener() {
        mTipPop.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mModel.getInvoiceDetail(AsiaRecordDetailsActivity.this, mInvoiceId);
            }
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.tvRepeal.setOnClickListener(v -> mModel.verifyStatusChange(this, mData.invoiceId, mData.invoiceStatus + ""));
        mBinding.llTab1.setOnClickListener(v -> getShow(1, R.color.color_EF4033, View.VISIBLE, R.color.color_333333, View.INVISIBLE, R.color.color_333333, View.INVISIBLE, R.color.color_333333, View.INVISIBLE));
        mBinding.llTab2.setOnClickListener(v -> getShow(2, R.color.color_333333, View.INVISIBLE, R.color.color_EF4033, View.VISIBLE, R.color.color_333333, View.INVISIBLE, R.color.color_333333, View.INVISIBLE));
        mBinding.llTab3.setOnClickListener(v -> getShow(3, R.color.color_333333, View.INVISIBLE, R.color.color_333333, View.INVISIBLE, R.color.color_EF4033, View.VISIBLE, R.color.color_333333, View.INVISIBLE));
        mBinding.llTab4.setOnClickListener(v -> getShow(4, R.color.color_333333, View.INVISIBLE, R.color.color_333333, View.INVISIBLE, R.color.color_333333, View.INVISIBLE, R.color.color_EF4033, View.VISIBLE));
    }

    private void getShow(int i, int p, int invisible, int p2, int invisible2, int p3, int visible, int p4, int invisible3) {
        if (mIndex == i) return;
        mIndex = i;
        mBinding.tvName1.setTextColor(Utils.getColor(p));
        mBinding.view1.setVisibility(invisible);
        mBinding.tvName2.setTextColor(Utils.getColor(p2));
        mBinding.view2.setVisibility(invisible2);
        mBinding.tvName3.setTextColor(Utils.getColor(p3));
        mBinding.view3.setVisibility(visible);
        mBinding.tvName4.setTextColor(Utils.getColor(p4));
        mBinding.view4.setVisibility(invisible3);


        mBinding.tvName1.setTypeface(i == 1 ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
        mBinding.tvName2.setTypeface(i == 2 ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
        mBinding.tvName3.setTypeface(i == 3 ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
        mBinding.tvName4.setTypeface(i == 4 ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);

        mBinding.llShowWater.setVisibility(mIndex == 3 && (mData.payFlow != null && mData.payFlow.size() != 0) ? View.VISIBLE : View.GONE);
        mBinding.rv.setVisibility(mIndex == 1 || mIndex == 2 ? View.VISIBLE : View.GONE);
        mBinding.zErrorView.setVisibility((mIndex == 1 || mIndex == 2) ? View.GONE :
                mIndex == 3 ? mBinding.llShowWater.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE : Utils.isEmpty(mData.mailNo) ? View.VISIBLE : View.GONE);
        mAdapter.setNewInstance(mIndex == 1 ? mInvoiceInformation :
                mIndex == 2 ? mApplyInformation : mLogisticsInformation);
        if (mIndex == 4) {
            mBinding.rv.setVisibility(mLogisticsInformation.size() == 0 ? View.GONE : View.VISIBLE);
            mBinding.zErrorView.setVisibility(mLogisticsInformation.size() == 0 ? View.VISIBLE : View.GONE);
        }

        if (mIndex == 3) {
            mWaterAdapter.setNewInstance(mData.payFlow);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_REFRESH:
                if (event.data == null) {
                    finish();
                } else {
                    mModel.getInvoiceDetail(this, mInvoiceId);
                }
                break;
        }
    }
}