package com.sgb.capital.view.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sgb.capital.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.adapters.ListenerUtil;


/**
 * @CopyRight cfk
 * @Author einstein
 * @CreateDate 2017/9/5 10:14.
 * @Description 文字型输入型item
 */

public class ZItemInputTextView extends RelativeLayout {

    public static final int TYPE_DATE = 1;//日期时间类型
    public static final int TYPE_NAME = 2;//姓名，仅支持中文
    public static final int TYPE_DEFAULT = 3;//其他通用类型
    public static final int TYPE_INDO = 4;//身份证号
    public static final int TYPE_NUM = 5;//纯数字
    public static final int TYPE_PHONE = 6;//手机号 + 座机号
    private Context context;
    private TextView label_name_tv;
    private TextView right_tv;
    private TextView star_tv;
    private EditText content_edt;
    private ImageView arrow_right_img;
    private ImageView right_img, img_icon_right;
    private ImageView deliver_line_img;
    private boolean isSingleLine = false;

    private boolean canInput;
    private boolean arrowRightImgV;
    private OnWholeItemClickListener listener;
    private onTextChange onTextChange;
    private OnFocusChangedListener onFocusChangedListener;

    private int infilterType;
    private ImageView pick_right_img;

    public ZItemInputTextView(Context context) {
        this(context, null);
    }

    public ZItemInputTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ZItemInputTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
        perseAttrs(attrs);
    }


    private void init() {
        View view = LayoutInflater.from(context).inflate(R.layout.view_item_input_text_z, this);
        label_name_tv = view.findViewById(R.id.label_name_tv);
        star_tv = view.findViewById(R.id.star_tv);
        right_tv = view.findViewById(R.id.right_tv);
        content_edt = view.findViewById(R.id.content_edt);
        arrow_right_img = view.findViewById(R.id.arrow_right_img);
        right_img = view.findViewById(R.id.right_img);
        img_icon_right = view.findViewById(R.id.img_icon_right);
        deliver_line_img = view.findViewById(R.id.deliver_line_img);
        pick_right_img = view.findViewById(R.id.pick_right_img);
        if (isSingleLine) {
            content_edt.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
            content_edt.setMinLines(1);
        } else {

        }

        content_edt.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (onFocusChangedListener != null) {
                    onFocusChangedListener.onFocusChanged(ZItemInputTextView.this, b);
                }
            }
        });
        content_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String inputText = content_edt.getText().toString();
                String patterStr = "";
                if (infilterType == TYPE_DATE) {//日期时间类型，允许其他输入
                    return;
                } else if (infilterType == TYPE_NAME) {//姓名、只允许汉字
                    String regEx = "[^\u4E00-\u9FA5]";
                    patterStr = stringFilter(inputText, regEx);
                    if (!inputText.equals(patterStr)) {
                        content_edt.setText(patterStr);
                        content_edt.setSelection(patterStr.length());//设置新的光标所在位置
                    }
                } else if (infilterType == TYPE_DEFAULT) {//其他类型，去掉特殊符号
                    String regEx = "[^a-zA-Z0-9\u4E00-\u9FA5]";
                    patterStr = stringFilter(inputText, regEx);
                    if (!inputText.equals(patterStr)) {
                        content_edt.setText(patterStr);
                        content_edt.setSelection(patterStr.length());//设置新的光标所在位置
                    }
                } else if (infilterType == TYPE_INDO) {//身份证号，只允许输入字母数字
                    String regEx = "[^a-zA-Z0-9]";
                    patterStr = stringFilter(inputText, regEx);
                    if (!inputText.equals(patterStr)) {
                        content_edt.setText(patterStr);
                        content_edt.setSelection(patterStr.length());//设置新的光标所在位置
                    }
                } else if (infilterType == TYPE_NUM) {//只允许输入数字
                    String regEx = "[^0-9]";
                    patterStr = stringFilter(inputText, regEx);
                    if (!inputText.equals(patterStr)) {
                        content_edt.setText(patterStr);
                        content_edt.setSelection(patterStr.length());//设置新的光标所在位置
                    }
                } else if (infilterType == TYPE_PHONE) {//手机号 + 座机号
                    String regEx = "[^-0-9]";
                    patterStr = stringFilter(inputText, regEx);
                    if (!inputText.equals(patterStr)) {
                        content_edt.setText(patterStr);
                        content_edt.setSelection(patterStr.length());//设置新的光标所在位置
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (onTextChange != null) {
                    onTextChange.change(editable.toString());
                }
            }
        });
    }

    @SuppressLint("NewApi")
    public void setContent_edtGravity(int gravity) {
        if (Gravity.END == gravity) {
            content_edt.setGravity(Gravity.END | Gravity.CENTER_VERTICAL);
            content_edt.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        } else if (Gravity.START == gravity) {
            content_edt.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
            content_edt.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        }
    }

    public void setEditLength(int length) {
        content_edt.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(length)});
    }

    public void setFilters(InputFilter[] filters) {
        content_edt.setFilters(filters);
    }

    public void setLabelName(int nameId) {
        label_name_tv.setText(nameId);
    }

    public void setLabelName(String name) {
        label_name_tv.setText(name);
    }

    public void setLabelName(CharSequence name) {
        label_name_tv.setText(name);
    }

    public void setLabelRight(String label) {
        right_tv.setText(label);
        right_tv.requestLayout();
    }

    public void setLabelHint(String hint) {
        content_edt.setHint(hint);
    }

    public void setLabelName(int nameId, int hint) {
        setLabelName(nameId);
        content_edt.setHint(getResources().getString(hint));
    }

    /**
     * 只能输入数字和字母
     *
     * @param s
     */
    public void setNumberAndLetter(String s) {
        content_edt.setKeyListener(DigitsKeyListener.getInstance(s));
    }

    /**
     * 设置整数小数点限制
     *
     * @param z
     * @param x
     */
    public void setCheck(int z, int x) {
        content_edt.addTextChangedListener(new DecimalInputTextWatcher(content_edt, z, x));
    }

    /**
     * 设置整数小数点限制
     *
     * @param z
     * @param x
     */
    public void setCheck(int z, int x, boolean isInputFirstZero) {
        content_edt.addTextChangedListener(new DecimalInputTextWatcher(content_edt, z, x, isInputFirstZero));
    }

    /**
     * 只能输入中文
     */
    public void setChinese() {
        content_edt.setFilters(new InputFilter[]{new ChineseInputFilter()});
    }

    /**
     * 设置限定字符
     */
    public void setMaxSize(int max) {
        InputFilter[] filters = {new InputFilter.LengthFilter(max)};
        content_edt.setFilters(filters);
    }

    /**
     * 禁止输入符号
     */
    public void setNoSymbol() {
        content_edt.setFilters(new InputFilter[]{new NoSymbolputFilter()});
    }

    public String getContent() {
        return content_edt.getText().toString().trim();
    }

    public EditText getContent_edt() {
        return content_edt;
    }

    public void setContent(String content) {
        if (!TextUtils.isEmpty(content)) {
            content_edt.setText(content);
        } else {
            content_edt.setText("");
        }
    }

    public void setColor(int color) {
        content_edt.setTextColor(color);
    }

    public void setSelection(int len) {
        content_edt.setSelection(len);
    }

    public void setHint(String hint) {
        content_edt.setHint(hint);
    }

    public String getHint() {
        return content_edt.getHint().toString();
    }

    public void setInputType(int inputType) {
        if (content_edt != null) {
            content_edt.setInputType(inputType);
        }
    }

    public void setArrowVisible(boolean visible) {
        arrowRightImgV = visible;
        arrow_right_img.setVisibility(visible ? View.VISIBLE : GONE);
    }

    public void setArrowRightImage(int image) {
        arrow_right_img.setBackgroundResource(image);
    }

    public void setPickRightImage(int image) {
        pick_right_img.setVisibility(VISIBLE);
        pick_right_img.setBackgroundResource(image);
    }

    public void showImgVisible(int i) {
        right_img.setVisibility(VISIBLE);
        right_img.setImageResource(i);
    }


    public void setContentGravity(int one, int two) {
        content_edt.setGravity(one | two);
    }

    /**
     * 设置输入过滤的类型
     *
     * @param infilterType
     */
    public void setInfilterType(int infilterType) {
        this.infilterType = infilterType;
    }

    /**
     * 设置输入框是否可编辑
     */
    public void setEnable(boolean isEnable) {
        canInput = isEnable;
        content_edt.setFocusable(isEnable);
        content_edt.setFocusableInTouchMode(isEnable);
        if (!arrowRightImgV)
            label_name_tv.setTextColor(isEnable ? context.getResources().getColor(R.color.color_C0C4CC) : context.getResources().getColor(R.color.color_000000));
    }

    private void perseAttrs(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ItemInputTextView);
            //遍历AttributeSet中的XML属性
            //设置是否必填，显示隐藏星号
            if (typedArray.getBoolean(R.styleable.ItemInputTextView_itv_star_visible, false)) {
                star_tv.setVisibility(View.VISIBLE);
            } else {
                star_tv.setVisibility(View.GONE);
            }
            //设置label颜色
            label_name_tv.setTextColor(typedArray.getColor(R.styleable.ItemInputTextView_itv_label_text_color, getResources().getColor(R.color.color_333333)));
            //设置label名字
            label_name_tv.setText(typedArray.getString(R.styleable.ItemInputTextView_itv_label_name));
            //设置右边名字
            right_tv.setText(typedArray.getString(R.styleable.ItemInputTextView_itv_label_right));
            //设置最大字数限制
            content_edt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(typedArray.getInteger(R.styleable.ItemInputTextView_itv_max_text, 1000))});
            //设置hint内容
            content_edt.setHint(typedArray.getString(R.styleable.ItemInputTextView_itv_label_hint));
            content_edt.setText(typedArray.getString(R.styleable.ItemInputTextView_itv_content_text));
            //设置右侧箭头可见
            if (typedArray.getBoolean(R.styleable.ItemInputTextView_itv_arrow_visible, false)) {
                arrow_right_img.setVisibility(View.VISIBLE);
                arrowRightImgV = true;
            } else {
                arrow_right_img.setVisibility(View.GONE);
                arrowRightImgV = false;
            }
            //设置输入框输入类型
            if (typedArray.getBoolean(R.styleable.ItemInputTextView_itv_deliver_line_visible, true)) {
                deliver_line_img.setVisibility(View.VISIBLE);
            } else {
                deliver_line_img.setVisibility(View.GONE);
            }
            //设置输入框能否输入
            if (typedArray.getBoolean(R.styleable.ItemInputTextView_itv_can_input, true)) {
                content_edt.setFocusable(true);
                canInput = true;
            } else {
                content_edt.setFocusable(false);
                canInput = false;
                if (!arrowRightImgV)
                    label_name_tv.setTextColor(context.getResources().getColor(R.color.color_C0C4CC));
            }
            //设置文字大小
            int textSize = (int) typedArray.getDimension(R.styleable.ItemInputTextView_itv_input_text_size, 0);

            if (textSize != 0) {
                label_name_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                content_edt.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }
            int lableTextSize = (int) typedArray.getDimension(R.styleable.ItemInputTextView_itv_label_text_size, 0);
            if (lableTextSize != 0) {
                label_name_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, lableTextSize);
            }

            //设置输入框是否单行显示
            if (typedArray.getBoolean(R.styleable.ItemInputTextView_itv_single_line, false)) {
                content_edt.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
                content_edt.setMinLines(1);
            } else {

            }
            if (typedArray.getBoolean(R.styleable.ItemInputTextView_itv_right_img_visible, false)) {
                img_icon_right.setVisibility(View.VISIBLE);
            } else {
                img_icon_right.setVisibility(View.GONE);
            }
            int icon = typedArray.getResourceId(R.styleable.ItemInputTextView_itv_right_img, R.mipmap.right_arrow_gray);
            img_icon_right.setImageResource(icon);
            pick_right_img.setVisibility(View.GONE);
            content_edt.setTextColor(typedArray.getColor(R.styleable.ItemInputTextView_itv_content_color, getResources().getColor(R.color.color_333333)));
            typedArray.recycle();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!canInput) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_UP:
                    if (listener != null) {
                        setSoftWareHide();
                        listener.OnWholeItemClick(this);
                    }
                    break;
            }
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    public interface onTextChange {
        void change(String s);
    }

    public void onTextChange(onTextChange listener) {
        this.onTextChange = listener;
    }

    /**
     * 点击事件
     */
    public interface OnWholeItemClickListener {
        void OnWholeItemClick(View view);
    }

    /**
     * View焦点改变接口
     */
    public interface OnFocusChangedListener {
        void onFocusChanged(View view, boolean focused);
    }

    /**
     * 设置点击事件
     *
     * @param listener
     */
    public void setOnWholeItemClickListener(OnWholeItemClickListener listener) {
        this.listener = listener;
    }

    /**
     * 设置焦点改变的监听
     *
     * @param onFocusChangedListener
     */
    public void setViewOnFocusChangedListener(OnFocusChangedListener onFocusChangedListener) {
        this.onFocusChangedListener = onFocusChangedListener;
    }

    /**
     * 设置输入框仅支持 字母  数字  汉字
     *
     * @param str
     * @return
     * @throws PatternSyntaxException
     */
    private String stringFilter(String str, String regEx) throws PatternSyntaxException {
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }

    /**
     * 隐藏软键盘
     */
    private void setSoftWareHide() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(((Activity) getContext()).getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    public void setMemberStyle() {
        getContent_edt().setHintTextColor(getResources().getColor(R.color.color_input_text));
        getContent_edt().setTextColor(getResources().getColor(R.color.FF000000));
    }

    @BindingAdapter(value = {"itv_label_text"}, requireAll = false)
    public static void setText(ZItemInputTextView view, String text) {
        if (text == null) {
            return;
        } else if (text.equals(((EditText) view.findViewById(R.id.content_edt)).getText().toString())) {
            return;
        }
        ((EditText) view.findViewById(R.id.content_edt)).setText(text);
    }

    @BindingAdapter(value = {"itv_label_type"}, requireAll = false)
    public static void setType(ZItemInputTextView view, int type) {
        if (type == 3) {
            ((EditText) view.findViewById(R.id.content_edt)).setInputType(InputType.TYPE_CLASS_PHONE);
        }
    }

    @InverseBindingAdapter(attribute = "itv_label_text", event = "textAttrChanged")
    public static String getText(ZItemInputTextView view) {
        return ((EditText) view.findViewById(R.id.content_edt)).getText().toString();
    }

    public void setDeliverLineVG(boolean isShow) {
        if (isShow) {
            deliver_line_img.setVisibility(View.VISIBLE);
        } else {
            deliver_line_img.setVisibility(View.GONE);
        }
    }

    @BindingAdapter(value = {"textAttrChanged"}, requireAll = false)
    public static void setTextAttrChanged(ZItemInputTextView view, final InverseBindingListener inverseBindingListener) {
        TextWatcher newWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                inverseBindingListener.onChange();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        TextWatcher oldWatcher = ListenerUtil.trackListener(((EditText) view.findViewById(R.id.content_edt)), newWatcher, R.id.textWatcher);

        if (inverseBindingListener != null) {
            ((EditText) view.findViewById(R.id.content_edt)).addTextChangedListener(newWatcher);
        }

        if (oldWatcher != null) {
            ((EditText) view.findViewById(R.id.content_edt)).removeTextChangedListener(oldWatcher);
        }
    }
}
