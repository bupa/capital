package com.sgb.capital.view.ui.adapter;

import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.model.BankTradeLimitBean;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/9/18 0018
 * 说明:交易限额
 */
public class DealLimitAdapter extends BaseQuickAdapter<BankTradeLimitBean, BaseViewHolder> {

    public DealLimitAdapter(List<BankTradeLimitBean> beans) {
        super(R.layout.deallimit_adapter, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final BankTradeLimitBean item) {
        try {
            helper
                    .setText(R.id.tv_name, item.bankName)
                    .setText(R.id.tv_kj_num, item.singleQuickLimit + "万元")
                    .setText(R.id.tv_kj_day, item.dayQuickLimit + "万元")
                    .setText(R.id.tv_b2b_num, item.singleB2bLimit + "万元")
                    .setText(R.id.tv_b2b_day, item.dayB2bLimit + "万元")
            ;
            View ll_show = helper.getView(R.id.ll_show);
            helper.getView(R.id.ll_btn).setOnClickListener(v -> {
                ll_show.setVisibility(ll_show.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                helper.getView(R.id.iv_arrow).setRotation(ll_show.getVisibility() == View.VISIBLE ? 180 : 0);
            });
            if (getData().size() != 0) {
                helper.getView(R.id.view).setVisibility(helper.getLayoutPosition() == getData().size() - 1 ? View.GONE : View.VISIBLE);
            }
        } catch (Exception e) {
        }

    }
}