package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.BindcompanybackActivityBinding;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.model.BindEnterpriseBankBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.CardsListPop;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.BankModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import razerdp.basepopup.BasePopupWindow;


/**
 * 作者:张磊
 * 日期:2021/2/20 0020
 * 说明:绑定企业银行卡
 */

public class BindCompanyBackActivity extends BaseActivity {
    private BindcompanybackActivityBinding mBinding;
    private BankModel mModel;
    private CardsListPop mListPop;
    private TipPop mTipPop;
    private CountDownTimer mTimer;
    BindEnterpriseBankBean mBindEnterpriseBankBean = new BindEnterpriseBankBean();
    private TipPop mErrorTipPop;
    private TipPop mTipPop3;
    private TipPop mTipPopCode;
    private int mIndex;

    public static void start(Context context) {
        Intent intent = new Intent(context, BindCompanyBackActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    public void initData() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Utils.setStatusBarLightMode(this, true);
        mListPop = new CardsListPop(this);
        mTipPop = new TipPop(this, "温馨提示", "恭喜您,绑定支付银行卡成功", null, "知道了");
        mErrorTipPop = new TipPop(this, "温馨提示", "未查到开户行支行，请重新检查", null, "编辑资料");
        mTipPopCode = new TipPop(this, "温馨提示", "验证码错误次数超过3次", null, "知道了");
        mTipPop3 = new TipPop(this, "温馨提示", "确认绑卡前，请仔细核对银行卡号、银行名称、银行卡开户行等信息，若有误可能导致无法正常提现！",
                "确定", "取消");
        mModel = new ViewModelProvider(this).get(BankModel.class);
        mModel.getBusinessInfoByCompany(this);
        mModel.getPublicBank(this);
    }

    @Override
    public void initObserve() {
        mModel.mCode.observe(this, s -> {
            if (s.contains("0")) {
                mIndex++;
                if (mIndex > 3) {
                    mTipPopCode.show();
                } else {
                    Utils.showToast("验证码错误");
                }
            }
        });
        mModel.mBusinessInfoByCompanyEntity.observe(this, businessInfoByCompanyEntity -> {
            mBindEnterpriseBankBean.bankAccountName = businessInfoByCompanyEntity.subMerName;
            mBinding.tvBankAccountName.setText(businessInfoByCompanyEntity.subMerName);
        });

        mModel.mBank.observe(this, bankEntities -> {
            if (bankEntities != null && bankEntities.size() != 0) {
                bankEntities.get(0).isSelect = true;
                mListPop.mDatas = bankEntities;
            }
        });
        mModel.mBaseEntity.observe(this, baseEntity -> {
            if (baseEntity == null) {
                mErrorTipPop.show();
            } else {
                mTipPop.show();
            }
        });
    }

    @Override
    protected void initView() {
        Utils.setStatusBarLightMode(this, true);
        mBinding = DataBindingUtil.setContentView(this, R.layout.bindcompanyback_activity);
    }

    public void initListener() {
        mTipPop3.mTvSure.setOnClickListener(v -> {
            mTipPop3.dismiss();
            mBindEnterpriseBankBean.bankAccount = mBinding.etBackNumber.getText().toString();
            mBindEnterpriseBankBean.bankBranchName = mBinding.etOpen.getText().toString();
            mBindEnterpriseBankBean.mobileId = mBinding.etPhone.getText().toString();
            mBindEnterpriseBankBean.code = mBinding.etCode.getText().toString();
            mModel.bindEnterpriseBank(BindCompanyBackActivity.this, mBindEnterpriseBankBean);
        });
        mBinding.tvGetCode.setOnClickListener(v -> {
            mIndex = 0;
            if (Utils.isEmpty(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(Utils.getContext(), "请输入手机号!");
                return;
            } else if (!Utils.isChinaPhoneLegal(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(this, "手机号格式错误");
                return;
            }
            // 获取验证码
            mTimer = new CountDownTimer(60000, 1000) {
                public void onTick(long millisUntilFinished) {
                    mBinding.tvGetCode.setEnabled(false);
                    mBinding.tvGetCode.setText("重新获取" + "(" + (millisUntilFinished / 1000) + ")");
                }

                public void onFinish() {
                    mBinding.tvGetCode.setEnabled(true);
                    mBinding.tvGetCode.setText("获取验证码");
                }
            };
            mTimer.start();
            mModel.applyCode(this, mBinding.etPhone.getText().toString());
        });

        mBinding.llBankCard.setOnClickListener(v -> {
            Utils.hide(this);
            mListPop.show();
        });
        mTipPop.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                finish();
                Utils.sendMsg(Constants.EVENT_REFRESH, null);
            }
        });
        mBinding.tvSave.setOnClickListener(v -> {
            if (mBinding.tvBankName.getText().toString().contains("请选择银行")) {
                MToast.showToast(Utils.getContext(), "请选择开户银行");
                return;
            } else if (Utils.isEmpty(mBinding.etBackNumber.getText().toString())) {
                MToast.showToast(Utils.getContext(), "请输入银行卡号");
                return;
            } else if (mBinding.etBackNumber.getText().toString().length()<8) {
                MToast.showToast(this, "银行卡号格式错误");
                return;
            } else if (Utils.isEmpty(mBinding.etOpen.getText().toString())) {
                MToast.showToast(Utils.getContext(), "请输入银行卡开户支行全称");
                return;
            } else if (Utils.isEmpty(mBinding.etPhone.getText().toString())) {
                MToast.showToast(Utils.getContext(), "请输入银行预留手机号");
                return;
            } else if (!Utils.isChinaPhoneLegal(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(this, "手机号格式错误");
                return;
            } else if (Utils.isEmpty(mBinding.etCode.getText().toString())) {
                MToast.showToast(Utils.getContext(), "请输入验证码");
                return;
            }
            mTipPop3.show();

        });
        mBinding.ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.onFinish();
        }
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants.EVENT_SELECT:
                BankEntity data = (BankEntity) event.data;
                mBindEnterpriseBankBean.gateId = data.code;
                mBinding.tvBankName.setText(data.name);
                mBinding.tvGateId.setText(data.code);
                break;
        }
    }
}
