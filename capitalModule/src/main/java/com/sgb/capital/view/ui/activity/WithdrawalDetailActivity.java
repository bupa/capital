package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.ActivityWithDrawDetailBinding;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.viewmodel.WithdrawalDetailModel;

import java.text.DecimalFormat;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

/**
 * 提现流水详情
 * chenTao 2021/4/29
 */
public class WithdrawalDetailActivity extends BaseActivity {
    WithdrawalDetailModel mModel;
    private ActivityWithDrawDetailBinding mBinding;
    private String flowNo;

    public static void start(Context context, String flowNo) {
        Intent intent = new Intent();
        intent.setClass(context, WithdrawalDetailActivity.class);
        intent.putExtra("flowNo", flowNo);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_with_draw_detail);
        Utils.setStatusBarLightMode(this, true);
        mModel = new ViewModelProvider(this).get(WithdrawalDetailModel.class);
        flowNo = getIntent().getStringExtra("flowNo");
        mBinding.ivBack.setOnClickListener(v -> finish());
        mModel.getPayFlowDetails(this, flowNo);
        mModel.mData.observe(this, balanceDetailsEntity -> setData());
    }

    private void setData() {
        mBinding.withdrawalImg.setVisibility(View.VISIBLE);
        if (mModel.mData.getValue() == null) return;
        mBinding.setEntity(mModel.mData.getValue());
        String endTime = Utils.getTime(mModel.mData.getValue().finishTime);
        String applyTime = Utils.getTime(mModel.mData.getValue().created);
        if (mModel.mData.getValue().tradeStatus == 1) {
            mBinding.withdrawalImg.setImageResource(R.mipmap.icon_withdraw_success);
            mBinding.withdrawalStatus.setText("已完成");
            mBinding.tvStatus.setText("已完成");
            mBinding.view1.setBackgroundColor(Utils.getColor(R.color.color_0286DF));
            mBinding.view2.setBackgroundResource(R.drawable.bg_corner999_blue4);
            mBinding.accountTime.setVisibility(View.VISIBLE);
            mBinding.accountTime.setText(endTime);
            mBinding.accountTime.setGravity(Gravity.RIGHT);
            mBinding.tvIntoTimeTip.setText("提现完成时间");
            mBinding.tvIntoTime.setText(endTime);
        } else if (mModel.mData.getValue().tradeStatus == 4) {
            mBinding.withdrawalImg.setImageResource(R.mipmap.icon_withdraw_load);
            mBinding.withdrawalStatus.setText("提现中");
            mBinding.tvStatus.setText("提现中");
            mBinding.view1.setBackgroundColor(Utils.getColor(R.color.color_line));
            mBinding.view2.setBackgroundResource(R.drawable.bg_corner999_ededed4);
            mBinding.accountTime.setVisibility(View.VISIBLE);
            mBinding.accountTime.setText("预计" + Utils.getTime2Day(mModel.mData.getValue().created) + "到账");
            mBinding.accountTime.setGravity(Gravity.CENTER);
            mBinding.tvIntoTimeTip.setText("预计到账时间");
            mBinding.tvIntoTime.setText(Utils.getTime2Day(mModel.mData.getValue().created));
        } else if (mModel.mData.getValue().tradeStatus == 2) {
            mBinding.withdrawalImg.setImageResource(R.mipmap.icon_withdraw_fail);
            mBinding.withdrawalStatus.setText("已失败");
            mBinding.tvStatus.setText("已失败");
            mBinding.view1.setBackgroundColor(Utils.getColor(R.color.color_FF2928));
            mBinding.view2.setBackgroundResource(R.drawable.bg_corner999_ff2928);
            mBinding.layoutWithdrawalCommission.setVisibility(View.GONE);
            mBinding.layoutIntoTime.setVisibility(View.GONE);
            mBinding.view3.setVisibility(View.GONE);
            mBinding.view4.setVisibility(View.GONE);
        }
        mBinding.tvType.setText("账户提现");
        String priceTv = "";
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        if (!TextUtils.isEmpty(mModel.mData.getValue().tradeAmount)) {
            priceTv = "-" + decimalFormat.format(Double.valueOf(mModel.mData.getValue().tradeAmount));
        }
        String tradeFeesTv = "";
        if (!TextUtils.isEmpty(mModel.mData.getValue().tradeFees)) {
            tradeFeesTv = "-" + decimalFormat.format(Double.valueOf(mModel.mData.getValue().tradeFees));
        }
        String balance = "";
        if (!TextUtils.isEmpty(mModel.mData.getValue().balance)) {
            balance = decimalFormat.format(Double.valueOf(mModel.mData.getValue().balance));
        }
        // TODO: 2021/6/16 0016 解密
        if (Utils.isEmpty(mModel.mData.getValue().balance)) {
            mBinding.llShow.setVisibility(View.GONE);
        }
        mBinding.tvWithdrawalMoney.setText(priceTv);
        mBinding.tvWithdrawalCommission.setText(tradeFeesTv);
        mBinding.tvReceiptAccount.setText(mModel.mData.getValue().withdrawBankAccountName);
        String s = CryptoUtils.decryptAES_ECB(mModel.mData.getValue().withdrawBankAccount);
        mBinding.tvReceiptNum.setText(TextUtils.isEmpty(mModel.mData.getValue().withdrawBankName) ? "" : mModel.mData.getValue().withdrawBankName + "(" + s.substring(0, 4) + "********" + s.substring(s.length() - 4) + ")");
        mBinding.tvApplyTime.setText(applyTime);
        mBinding.tvBalance.setText(balance);
        mBinding.tvTransactionNumber.setText(mModel.mData.getValue().flowNo);
    }
}