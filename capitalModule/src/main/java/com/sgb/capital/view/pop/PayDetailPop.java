package com.sgb.capital.view.pop;

import android.content.Context;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.base.BasePopWindow;
import com.sgb.capital.model.Bean;
import com.sgb.capital.view.ui.adapter.PopDetailItemAdapter;
import com.sgb.capital.view.widget.MaxHeightRecyclerView;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * chentao 2021/5/11
 * 订单详情弹框
 */
public class PayDetailPop extends BasePopWindow {
    public PayDetailPop(Context context) {
        super(context, DIR_DOWN_UP);
    }

    private List<Bean> data = new ArrayList<>();
    private PopDetailItemAdapter adapter;

    @Override
    protected int popLayout() {
        return R.layout.pop_pay_detail_z;
    }

    @Override
    protected void initView(PopViewHolder holder) {
        TextView iv_close = holder.getPop().findViewById(R.id.close);
        MaxHeightRecyclerView maxHeightRecyclerView = holder.getPop().findViewById(R.id.recyclerView);
        iv_close.setOnClickListener(v -> this.dismiss());
        adapter = new PopDetailItemAdapter(mContext,null);
        maxHeightRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        maxHeightRecyclerView.setAdapter(adapter);
    }

    public void setData(List<Bean> list) {
        this.data = list;
        adapter.setDatas(data);
    }
}
