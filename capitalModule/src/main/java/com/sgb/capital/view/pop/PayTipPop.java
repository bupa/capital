package com.sgb.capital.view.pop;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.sgb.capital.R;

import razerdp.basepopup.BasePopupWindow;


/**
 * 作者:陈涛
 * 日期:2021/5/20
 * 说明:提示弹框重复支付
 */
public class PayTipPop extends BasePopupWindow {

    public TextView mTvSure;
    public TextView tvCall;
    public TextView mTip1,mTip2;
    private TextView mTvTitle;


    public PayTipPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.dialog_pay_pop);
        mTvSure = v.findViewById(R.id.tv_sure);
        mTvSure.setTypeface(Typeface.DEFAULT_BOLD);
        tvCall = v.findViewById(R.id.tv_call);
        mTip1 = v.findViewById(R.id.tv_tip1);
        mTip2 = v.findViewById(R.id.tv_tip2);
        mTvTitle = v.findViewById(R.id.tv_title);
        return v;
    }


    public void show() {
        showPopupWindow();
    }
}
