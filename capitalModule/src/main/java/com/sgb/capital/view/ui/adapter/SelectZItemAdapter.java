package com.sgb.capital.view.ui.adapter;

import android.content.Context;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.ItemViewHolder;

import java.util.List;

public class SelectZItemAdapter extends BaseRecycleAdapter<Bean> {


    public SelectZItemAdapter(Context context, List<Bean> datas) {
        super(context, R.layout.selectitem_adapter_z, datas);
    }

    @Override
    public void convert(ItemViewHolder holder, Bean ipPickSelectEntity) {
        holder.setBinding(BR.viewModel, ipPickSelectEntity);
    }
}
