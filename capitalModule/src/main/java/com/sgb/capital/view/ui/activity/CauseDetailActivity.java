package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import com.google.gson.Gson;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.CausedetailActivityBinding;
import com.sgb.capital.model.RecordsBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.viewmodel.FinanceFlowModel;

import java.text.DecimalFormat;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

/**
 * 流水退款详情
 * chenTao 2021/4/29
 */
public class CauseDetailActivity extends BaseActivity {
    private CausedetailActivityBinding mBinding;
    FinanceFlowModel mModel;
    private RecordsBean mPayFlowBean;

    public static void start(Context context, String data) {
        Intent intent = new Intent();
        intent.setClass(context, CauseDetailActivity.class);
        intent.putExtra("data", data);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.causedetail_activity);
        Utils.setStatusBarLightMode(this, true);
        mModel = new ViewModelProvider(this).get(FinanceFlowModel.class);
    }

    @Override
    public void initData() {
        String data = getIntent().getStringExtra("data");
        mPayFlowBean = new Gson().fromJson(data, RecordsBean.class);
        mModel.getPayFlowDetails(this, mPayFlowBean.flowNo);
    }

    @Override
    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void initObserve() {
        mModel.mData.observe(this, data -> {
            mBinding.ivShow.setVisibility(data.repeatPayStatus == 2 ? View.VISIBLE : View.GONE);
            mBinding.tvStatus.setText(data.refundStatus == 3 ? "已失败" : data.refundStatus == 2 ? "已完成" : "退款中");
            mBinding.tvRebateStatus.setText(data.refundStatus == 3 ? "已失败" : data.refundStatus == 2 ? "已完成" : "退款中");
            mBinding.tvApplyTime.setText(Utils.getTime(data.created));
            mBinding.tvTip.setText(data.refundStatus == 3 ? "已失败" : "到账");
            if (data.repeatPayStatus==3){
                mBinding.ivShow.setImageResource(R.mipmap.yichangdingdan);
                mBinding.ivShow.setVisibility(View.VISIBLE);
            }
            String endTime = Utils.getTime(mModel.mData.getValue().finishTime);
            DecimalFormat decimalFormat = new DecimalFormat("#0.00");
            String priceTv = "";
            if (!TextUtils.isEmpty(mModel.mData.getValue().tradeAmount)) {
                priceTv = (data.tradeFlowType == 1 ? "+" : "-") + decimalFormat.format(Double.valueOf(mModel.mData.getValue().tradeAmount));
            }
            mBinding.tvWithdrawalMoney.setText(priceTv);
            // 原订单支付方式
            mBinding.tvReceiptAccount.setText(data.refundOriPayType);
            if (data.tradeFlowType == 1) {
                if (data.refundOriPayType.contains("余额")) {
                    mBinding.tvRefundType.setText("退款至余额账户");
                    mBinding.tvRefundType2.setText("退款至余额账户");
                    mBinding.llRefundType.setVisibility(View.GONE);
                    mBinding.llRefundType2.setVisibility(View.VISIBLE);
                } else if (data.refundOriPayType.contains("快捷支付")) {
                    mBinding.llRefundType.setVisibility(View.VISIBLE);
                    mBinding.llRefundType2.setVisibility(View.GONE);
                    String withdrawBankAccount = data.refundOriBankAccount.substring(0, 4) + "********" + data.refundOriBankAccount.substring(data.refundOriBankAccount.length() - 4);
                    String refundType = "退款至" + data.refundOriBankName + "\n(" + withdrawBankAccount + ")";
                    mBinding.tvRefundType.setText(refundType);
                } else if (data.refundOriPayType.contains("支付宝")) {
                    mBinding.llRefundType.setVisibility(View.GONE);
                    mBinding.llRefundType2.setVisibility(View.VISIBLE);
                    mBinding.tvRefundType.setText("退款至支付宝");
                    mBinding.tvRefundType2.setText("退款至支付宝");
                } else if (data.refundOriPayType.contains("微信")) {
                    mBinding.llRefundType.setVisibility(View.GONE);
                    mBinding.llRefundType2.setVisibility(View.VISIBLE);
                    mBinding.tvRefundType.setText("退款至微信");
                    mBinding.tvRefundType2.setText("退款至微信");
                } else {
                    mBinding.llRefundType.setVisibility(View.GONE);
                    mBinding.llRefundType2.setVisibility(View.VISIBLE);
                    mBinding.tvRefundType.setText("退款至网银账户");
                    mBinding.tvRefundType2.setText("退款至网银账户");
                }
            } else {
                mBinding.llRefundType.setVisibility(View.GONE);
                mBinding.llRefundType2.setVisibility(View.GONE);
            }


            // 交易流水号
            mBinding.tvWater.setText(data.flowNo);
            // 原订单号
            mBinding.tvMediaNum.setText(data.refundOriOrderId);
            // 退费单号
            mBinding.tvReceiptNum.setText(data.orderId);
            if (data.refundStatus == 2) {
                mBinding.withdrawalImg.setImageResource(R.mipmap.icon_withdraw_success);
                mBinding.view1.setBackgroundColor(Utils.getColor(R.color.color_0286DF));
                mBinding.view2.setBackgroundResource(R.drawable.bg_corner999_blue4);
                mBinding.accountTime.setVisibility(View.VISIBLE);
                mBinding.accountTime.setText(endTime);
                mBinding.accountTime.setGravity(Gravity.RIGHT);
                mBinding.tvIntoTime.setText(endTime);
            } else if (data.refundStatus == 3) {
                mBinding.llShowSucceed.setVisibility(View.GONE);
                mBinding.llShowError.setVisibility(View.VISIBLE);
                mBinding.llShow.setVisibility(View.VISIBLE);
                mBinding.withdrawalImg.setImageResource(R.mipmap.icon_withdraw_fail);
                mBinding.view1.setBackgroundColor(Utils.getColor(R.color.color_FF2928));
                mBinding.view2.setBackgroundResource(R.drawable.bg_corner999_ff2928);
                mBinding.layoutIntoTime.setVisibility(View.GONE);
                if (!data.refundRetMsg.contains("格式")) {
                    mBinding.tvCause.setText(data.refundRetMsg);
                }
            } else {
                mBinding.withdrawalImg.setImageResource(R.mipmap.withdraw_load);
                mBinding.view1.setBackgroundColor(Utils.getColor(R.color.color_line));
                mBinding.view2.setBackgroundResource(R.drawable.bg_corner999_ededed4);
                mBinding.accountTime.setVisibility(View.VISIBLE);
                mBinding.accountTime.setText("预计" + Utils.getTime1Day(mModel.mData.getValue().created) + "到账");
                mBinding.accountTime.setGravity(Gravity.CENTER);
                mBinding.tvIntoTime.setText(Utils.getTime1Day(data.refundFinishTime));
                mBinding.layoutIntoTime.setVisibility(View.GONE);
            }
            mBinding.withdrawalImg.setVisibility(View.VISIBLE);
        });
    }
}