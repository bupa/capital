package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.OpenuserActivityBinding;
import com.sgb.capital.model.PersonalBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.QrCodeModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import razerdp.basepopup.BasePopupWindow;

/**
 * 作者:张磊
 * 日期:2021/4/19 0019
 * 说明:开通个人账户
 */
public class OpenUserActivity extends BaseActivity {
    OpenuserActivityBinding mBinding;
    private CountDownTimer mTimer;
    QrCodeModel mModel;
    private TipPop mTipPop;
    private TipPop mTipPopCode;
    int mIndex = 0;


    public static void start(Context context) {
        Intent intent = new Intent(context, OpenUserActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mBinding = DataBindingUtil.setContentView(this, R.layout.openuser_activity);
        mTipPop = new TipPop(this, "温馨提示", "恭喜您，个人钱包开通成功\n" +
                "请绑定提现银行卡", "立即绑卡", "知道了");
        mTipPopCode = new TipPop(this, "温馨提示", "验证码错误次数超过3次", null, "知道了");
    }

    @Override
    public void initData() {
        mModel = new ViewModelProvider(this).get(QrCodeModel.class);
        mModel.getRealNameInfo(this);
    }

    @Override
    public void initListener() {
        mTipPop.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                Utils.sendMsg(Constants.EVENT_REFRESH, null);
            }
        });
        mTipPop.mTvSure.setOnClickListener(v -> {
            mTipPop.dismiss();
            Utils.sendMsg(Constants.EVENT_HIDE, null);
            finish();
            AddPersonBankCardActivity.start(this);
        });
        mBinding.tvOpen.setOnClickListener(v -> {
            if (Utils.isEmpty(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(Utils.getContext(), "请输入手机号!");
                return;
            } else if (!Utils.isChinaPhoneLegal(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(this, "手机号格式错误");
                return;
            } else if (Utils.isEmpty(mBinding.etCode.getText().toString())) {
                MToast.showToast(Utils.getContext(), "请输入验证码!");
                return;
            }
            mBinding.tvOpen.setEnabled(false);
            mModel.personal(this, new PersonalBean(mBinding.tvName.getText().toString(), mBinding.tvCard.getText().toString(), mBinding.etPhone.getText().toString(), mBinding.etCode.getText().toString()));
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.tvGetCode.setOnClickListener(v -> {
            mIndex = 0;
            if (Utils.isEmpty(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(Utils.getContext(), "请输入手机号!");
                return;
            } else if (!Utils.isChinaPhoneLegal(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(this, "手机号格式错误");
                return;
            }
            // 获取验证码
            mTimer = new CountDownTimer(60000, 1000) {
                public void onTick(long millisUntilFinished) {
                    mBinding.tvGetCode.setEnabled(false);
                    mBinding.tvGetCode.setText("重新获取" + "(" + (millisUntilFinished / 1000) + ")");
                }

                public void onFinish() {
                    mBinding.tvGetCode.setEnabled(true);
                    mBinding.tvGetCode.setText("获取验证码");
                }
            };
            mTimer.start();
            mModel.send2Code(this, mBinding.etPhone.getText().toString().trim());
        });
    }

    @Override
    public void initObserve() {
        mModel.mBaseEntity.observe(this, baseEntity ->
                {
                    if (baseEntity == null) {
                        mIndex++;
                        if (mIndex > 3) {
                            mTipPopCode.show();
                        } else {
                            MToast.showToast(this, "验证码错误");
                        }
                    } else {
                        mTipPop.show();
                    }
                }
        );
        mModel.mUserInfoEntity.observe(this, userInfoEntity -> {
            mBinding.tvCard.setText(userInfoEntity.identityCard);
            mBinding.tvName.setText(userInfoEntity.realName);
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.onFinish();
        }
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .ACTIVATE:
                mBinding.tvOpen.setEnabled(true);
                break;
        }
    }
}