package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.databinding.AccountActivityBinding;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.ViewPagerAdapter;
import com.sgb.capital.view.ui.fragment.AccountFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.core.util.Pair;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

/**
 * 作者:张磊
 * 日期:2021/4/19 0019
 * 说明:账户明细
 */
public class AccountActivity extends BaseActivity {
    private AccountActivityBinding mBinding;

    public static void start(Context context) {
        Intent intent = new Intent(context, AccountActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.account_activity);
        mBinding.ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void initData() {
        initTab(new String[]{"余额账户", "冻结账户"});
    }

    private void initTab(String[] titls) {
        List<Pair<String, Fragment>> mList = new ArrayList<>();
        for (String s : titls) {
            Bundle bundle = new Bundle();
            AccountFragment couponFragment = new AccountFragment();
            bundle.putString("data", s);
            couponFragment.setArguments(bundle);
            mList.add(new Pair<>(s, couponFragment));
        }
        mBinding.vpNeed.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), mList));
        mBinding.vpNeed.setOffscreenPageLimit(titls.length);
        mBinding.tab.setupWithViewPager(mBinding.vpNeed);
    }

    @Override
    protected void onDestroy() {
        Utils.sendMsg(Constants.EVENT_REFRESH,null);
        super.onDestroy();
    }
}