package com.sgb.capital.view.ui.adapter;

import com.sgb.capital.R;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:选择银行卡
 */
public class DataAdapter extends BaseQuickAdapter<BankEntity, BaseViewHolder> {


    public DataAdapter(List<BankEntity> beans) {
        super(R.layout.data_adapter, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final BankEntity item) {
        helper.setText(R.id.tv_name, item.name)
                .setTextColor(R.id.tv_name, Utils.getColor(item.isSelect ? R.color.color_5792FD : R.color.color_666666))
                .setVisible(R.id.iv_sel, item.isSelect);
    }
}