package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.Gravity;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.ConfirmInvoiceActivityBinding;
import com.sgb.capital.utils.SpannableUtils;
import com.sgb.capital.view.pop.SimplePop;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.ConfirmInvoiceModel;

import java.util.Objects;

/**
 * 确认发票及地址
 * @author cyj
 */
public class ConfirmInvoiceAndAddressActivity extends BaseActivity {

    private ConfirmInvoiceActivityBinding mBinding;
    private ConfirmInvoiceModel mModel;
    private SimplePop simplePop;
    private final String TAG_INVOICE_TYPE = "invoiceType";
    private final String TAG_COMPANY_NAME = "companyName";
    private final String TAG_ADDRESSEE = "addressee";

    /** 跳转 开票流水列表 页面 */
    public static void start(Context context, String invoiceTotalMoney, String[] ids) {
        Intent intent = new Intent(context, ConfirmInvoiceAndAddressActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("invoiceTotalMoney", invoiceTotalMoney);
        intent.putExtra("ids", ids);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        // viewModel绑定
        mBinding = DataBindingUtil.setContentView(this, R.layout.confirm_invoice_activity);
        mModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(ConfirmInvoiceModel.class);
        mBinding.setConfirmInvoiceModel(mModel);

        //设置开票流水id数组赋值
        mModel.ids = getIntent().getStringArrayExtra("ids");
        //设置开票金额
        String content = "开票金额：￥" + getIntent().getStringExtra("invoiceTotalMoney");
        mModel.invoiceTotalMoney.set(SpannableUtils.changePartTextColor(content,"#FF4B10", 5, content.length(), true));
        // 设置Pop弹窗
        simplePop = new SimplePop(this);
        simplePop.setTitle("选择发票类型");
        // 获取开票公司信息列表
        mModel.getInvoiceCompanyInfoList();
        // 获取收件人列表
        mModel.getAddresseeList();
    }

    @Override
    public void initListener() {
        super.initListener();
        mBinding.ivBack.setOnClickListener(v -> finish());
        simplePop.setRightClickListener((item, position, tag) -> {
            // 设置弹窗回调数据对应的UI界面数据显示
            if(TAG_INVOICE_TYPE.equals(tag)) {
                mModel.invoiceType.set(String.valueOf(item.getSecond()));
                mModel.hasNoShowPersonal.set(Objects.equals(item.getSecond(), mModel.allInvoiceTypeList[1]));
                mModel.isShowCompanyOtherInfoView.set(Objects.equals(item.getSecond(), mModel.allInvoiceTypeList[1]));
            } else if(TAG_COMPANY_NAME.equals(tag)) {
                // 刷新公司名称信息
                mBinding.tvCompanyName.setText(String.valueOf(item.getSecond()));
                // 刷新公司名称的数据信息
                mModel.companyNameInfoItemId.set(Long.parseLong(item.getFirst()));
                mModel.isShowCompanyInfoView.set(true);
            } else if(TAG_ADDRESSEE.equals(tag)) {
                // 刷新当前收件人信息
                mBinding.tvAddresseeName.setText(String.valueOf(item.getSecond()));
                // 弹出收件人列表其他信息
                mModel.isShowAddresseeView.set(true);
                // 刷新收件人的数据信息
                mModel.addresseeItemId.set(Long.valueOf(item.getFirst()));
            }
        });
        mBinding.llInvoiceType.setOnClickListener(v -> {
            if(mModel.hasCheckedPersonal.get()) {
                return;
            }
            simplePop.setTitle("选择发票类型")
                    .setTag(TAG_INVOICE_TYPE)
                    .setData(mModel.allInvoiceTypeList)
                    .setCurrentSelectItem(mModel.invoiceType.get())
                    .show();
        });
        mBinding.llCompanyName.setOnClickListener(v -> {
            // 判断是否存在单位信息
            if(null == mModel.companyNameMap || mModel.companyNameMap.size() <= 0) {
                MToast.showLongToast(getApplication().getApplicationContext(),
                        "暂无开票信息，请前往PC端网站新增后再进行操作", Gravity.CENTER);
                return;
            }
            simplePop.setTitle("选择单位名称")
                    .setTag(TAG_COMPANY_NAME)
                    .setData(mModel.companyNameMap)
                    .setCurrentSelectItem(mBinding.tvCompanyName.getText().toString().trim())
                    .show();
        });
        mBinding.llAddressee.setOnClickListener(v -> {
            // 判断是否存在收件人
            if(null == mModel.addresseeMap || mModel.addresseeMap.size() <= 0) {
                MToast.showLongToast(getApplication().getApplicationContext(),
                        "暂无邮寄信息，请前往PC端网站新增后再进行操作", Gravity.CENTER);
                return;
            }
            simplePop.setTitle("选择收件人")
                    .setTag(TAG_ADDRESSEE)
                    .setData(mModel.addresseeMap)
                    .setCurrentSelectItem(mBinding.tvAddresseeName.getText().toString().trim())
                    .show();
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
