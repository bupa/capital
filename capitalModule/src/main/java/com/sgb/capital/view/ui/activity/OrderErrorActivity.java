package com.sgb.capital.view.ui.activity;

import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.ActivityErrorOrderBinding;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.PayOrderBean;
import com.sgb.capital.model.PayOrderEntity;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.SelectPop;
import com.sgb.capital.view.ui.adapter.OrderPayAdapter;
import com.sgb.capital.view.widget.xrecyclerview.ZXRecyclerView;
import com.sgb.capital.viewmodel.ErrorOrderModel;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * chent 2021/5/6 异常订单
 */

public class OrderErrorActivity extends BaseActivity {

    private ActivityErrorOrderBinding mBinding;
    private PayOrderBean mOrderBean = new PayOrderBean();
    private ErrorOrderModel mModel;
    private OrderPayAdapter mErrorOrderAdapter;
    private boolean isLoadMore = false;
    private List<Bean> mTimeBeans = new ArrayList<>();
    private List<Bean> mOrderTypeBeans = new ArrayList<>();
    private SelectPop mSelectPop;
    private String mTime;

    @Override
    protected void initView() {
        Utils.setStatusBarLightMode(this, true);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_error_order);
        mModel = new ViewModelProvider(this).get(ErrorOrderModel.class);
        mErrorOrderAdapter = new OrderPayAdapter(this, null, null);
        mErrorOrderAdapter.mType =7;
        mBinding.rvList.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvList.setAdapter(mErrorOrderAdapter);

        mSelectPop = new SelectPop(this);
        mSelectPop.setBgView(mBinding.grayLayout);
        mOrderBean.limit = 10;
        mOrderBean.page = 1;
        mOrderBean.state = "";
        mOrderBean.dateType = "";
        mOrderBean.orderType = "";
        isLoadMore = false;
        mModel.getAbnormalOrderList(OrderErrorActivity.this, mOrderBean);
        mModel.mErrorOrderList.observe(this, accountBeans -> getDataList(accountBeans, isLoadMore));
    }


    private void setPopData(int type) {
        mSelectPop.mType = type;
        mBinding.ivArrow1.setRotation(180);
        mBinding.ivArrow1.setImageResource(R.mipmap.ic_down_arrow_n);
        mBinding.ivArrow2.setRotation(180);
        mBinding.ivArrow2.setImageResource(R.mipmap.ic_down_arrow_n);
        if (type == 1) {
            mBinding.tvType.setTextColor(Utils.getColor(R.color.color_333333));
            if (mOrderTypeBeans.size() == 0) {
                mModel.getOrderType(OrderErrorActivity.this);
            } else {
                if (mSelectPop.mAdapter != null) {
                    mSelectPop.mAdapter.clearDatas();
                }
                mSelectPop.setData(mOrderTypeBeans);
                mSelectPop.showPopwindow(mBinding.view, 0, 0);
            }
        } else {
            mTimeBeans.clear();
            mBinding.tvApplyTime.setTextColor(Utils.getColor(R.color.color_333333));
            Utils.setBeans(mTimeBeans, new String[]{"全部", "今日", "最近一周", "最近一个月", "最近三个月", "最近半年", "最近一年"});
            mSelectPop.setData(mTimeBeans);
            mSelectPop.mAdapter.setDefItem(mSelectPop.mPos2);
            mSelectPop.showPopwindow(mBinding.view, 0, 0);
        }
    }

    public void getDataList(List<PayOrderEntity> list, boolean isLoadMore) {
        if (isLoadMore) {
            mBinding.rvList.loadMoreComplete();
            if (list.size() != 0) {
                mErrorOrderAdapter.addDatas(list);
            } else {
                mBinding.rvList.setNoMore(true);
            }

        } else {
            if (list.size() == 0) {
                mBinding.errorView.setVisibility(View.VISIBLE);
            } else {
                mBinding.errorView.setVisibility(View.GONE);
            }
            mBinding.rvList.refreshComplete();
            mBinding.rvList.setLoadingMoreEnabled(true);
            mErrorOrderAdapter.setDatas(list);
        }
    }


    @Override
    public void initListener() {
        mBinding.rvList.setLoadingListener(new ZXRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                isLoadMore = false;
                mOrderBean.page = 1;
                mModel.getAbnormalOrderList(OrderErrorActivity.this, mOrderBean);
            }

            @Override
            public void onLoadMore() {
                isLoadMore = true;
                mOrderBean.page++;
                mModel.getAbnormalOrderList(OrderErrorActivity.this, mOrderBean);
            }
        });
        mSelectPop.setOnDismissListener(() -> {
            if (mSelectPop.mType == 1) {
                mBinding.ivArrow1.setRotation(0);
                if (mSelectPop.mBean != null) {
                    mOrderBean.orderType = mSelectPop.mBean.value;
                }
                mBinding.tvType.setTextColor(Utils.getColor(mSelectPop.mPos1 != 0 ? R.color.color_main : R.color.color_tab_two));
                if (mSelectPop.mPos1 == 0) {
                    mBinding.tvType.setText("订单类型");
                    mBinding.ivArrow1.setImageResource(R.mipmap.ic_down_arrow_n);
                } else {
                    mBinding.tvType.setText(mSelectPop.mBean.name);
                    mBinding.ivArrow1.setImageResource(R.mipmap.down);
                }
            } else {
                mBinding.ivArrow2.setRotation(0);
                if (mSelectPop.mBean != null) {
                    mTime = mSelectPop.mBean.name.contains("今日") ? Utils.getStartAndEndTime(0, 0, "今日")
                            : mSelectPop.mBean.name.contains("最近一周") ? Utils.getStartAndEndTime(1, 7, "最近一周")
                            : mSelectPop.mBean.name.contains("最近一个月") ? Utils.getStartAndEndTime(2, 1, "最近一个月")
                            : mSelectPop.mBean.name.contains("最近三个月") ? Utils.getStartAndEndTime(2, 3, "最近三个月")
                            : mSelectPop.mBean.name.contains("最近半年") ? Utils.getStartAndEndTime(2, 6, "最近六个月")
                            : Utils.getStartAndEndTime(3, 1, "最近一年");
                    mOrderBean.startTime = mTime.split("-")[0];
                    mOrderBean.endTime = mTime.split("-")[1];
                    mBinding.tvApplyTime.setText(mSelectPop.mBean.name);
                }
                mBinding.tvApplyTime.setTextColor(Utils.getColor(mSelectPop.mPos2 != 0 ? R.color.color_main : R.color.color_tab_two));
                if (mSelectPop.mPos2 == 0) {
                    mOrderBean.startTime = null;
                    mOrderBean.endTime = null;
                    mBinding.tvApplyTime.setText("申请时间");
                    mBinding.ivArrow2.setImageResource(R.mipmap.ic_down_arrow_n);
                } else {
                    mBinding.ivArrow2.setImageResource(R.mipmap.down);
                }
            }
            mOrderBean.limit = 10;
            mOrderBean.page = 1;
            if (mSelectPop.mIsClick) {
                mSelectPop.mIsClick = false;
                isLoadMore = false;
                mModel.getAbnormalOrderList(this, mOrderBean);
            }
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.orderType.setOnClickListener(v -> setPopData(1));
        mBinding.applyTime.setOnClickListener(v -> setPopData(2));


        mBinding.etSs.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mOrderBean.page = 1;
                mOrderBean.orderId = mBinding.etSs.getText().toString().trim();
                mModel.getAbnormalOrderList(this, mOrderBean);
            }
            return false;
        });
    }

    @Override
    public void initObserve() {
        mModel.mBean.observe(this, beans -> {
            Bean bean = new Bean("全部");
            bean.value = "";
            mOrderTypeBeans.add(bean);
            mOrderTypeBeans.addAll(beans);
            mSelectPop.mType = 1;
            mSelectPop.setData(mOrderTypeBeans);
            mSelectPop.mAdapter.setDefItem(mSelectPop.mPos1);
            mSelectPop.showPopwindow(mBinding.view, 0, 0);
        });
    }
}
