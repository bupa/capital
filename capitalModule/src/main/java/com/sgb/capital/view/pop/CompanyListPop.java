package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.DataAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:公司列表
 */
public class CompanyListPop extends BasePopupWindow {


    public List<BankEntity> mDatas = new ArrayList<>();
    private DataAdapter mAdapter;
    private BankEntity mBankEntity;
    public int mLastIndex = 0;

    public CompanyListPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.companylist_pop);
        RecyclerView rv = v.findViewById(R.id.rv);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> dismiss());
        rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mDatas = new ArrayList<>();
        mAdapter = new DataAdapter(mDatas);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            mBankEntity = mAdapter.getData().get(position);
            BankEntity bean2 = mAdapter.getData().get(mLastIndex);
            if (mLastIndex != position) {
                mBankEntity.isSelect = true;
                bean2.isSelect = false;
            }
            mLastIndex = position;
            mAdapter.notifyDataSetChanged();
        });
        rv.setAdapter(mAdapter);


        TextView tv_sure = v.findViewById(R.id.tv_sure);
        tv_sure.setOnClickListener(v1 -> {
            dismiss();
            Utils.sendMsg(Constants.EVENT_SELECT, mBankEntity == null ? mAdapter.getData().get(0) : mBankEntity);
        });

        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }


    public void show() {
        mAdapter.setNewInstance(mDatas);
        mAdapter.notifyDataSetChanged();
        showPopupWindow();
    }
}