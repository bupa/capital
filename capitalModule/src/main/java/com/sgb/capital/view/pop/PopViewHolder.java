package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Description:
 * Author zhengkewen
 * Time 2017/12/13
 */
public class PopViewHolder extends RecyclerView.ViewHolder {

    private ViewDataBinding mBinding;
    private View pop;

    public PopViewHolder(View v) {
        super(v);
        this.pop = v;
        mBinding = DataBindingUtil.bind(v);
    }

    public static PopViewHolder get(Context context, ViewGroup viewGroup, int layoutId) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new PopViewHolder(itemView);
    }

    public View getPop() {
        return pop;
    }

    public PopViewHolder setBinding(int variableId, Object object) {
        mBinding.setVariable(variableId, object);
        mBinding.executePendingBindings();
        return this;
    }

    public ViewDataBinding getmBinding() {
        return mBinding;
    }
}
