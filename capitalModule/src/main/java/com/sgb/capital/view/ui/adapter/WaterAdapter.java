package com.sgb.capital.view.ui.adapter;

import com.sgb.capital.R;
import com.sgb.capital.model.InvoiceDetailBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;
/**
 * 作者:张磊
 * 日期:2021/1/21 0021
 * 说明:流水item
 */
public class WaterAdapter extends BaseQuickAdapter<InvoiceDetailBean.PayFlowDTO, BaseViewHolder> {
    public WaterAdapter(List<InvoiceDetailBean.PayFlowDTO> data) {
        super(R.layout.water_dapter, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, InvoiceDetailBean.PayFlowDTO item) {
        helper
                .setText(R.id.tv_tradeType,"手续费-"+item.tradeType)
                .setText(R.id.tv_paymentId,item.flowNo)
                .setText(R.id.tv_payTime, Utils.getTime(item.payTime))
                .setText(R.id.tv_payType, item.payType)
                .setText(R.id.tv_tradeFees, Utils.getDecimalFormat(item.tradeFees));
    }
}