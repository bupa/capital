package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.CapitalTobepaidbilldetailsActivityBinding;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.ui.adapter.BalanceDetailsAdapter;
import com.sgb.capital.viewmodel.DetailsModel;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/9/1 0001
 * 说明:待付订单详情
 */
public class ToBePaidBillDetailsActivity extends BaseActivity {

    private List<Bean> mBeans = new ArrayList<>();
    private BalanceDetailsAdapter mAdapter;
    private BillOrderDetailsActivity mActivity;
    private TipPop mTipPop;
    private String mCode = "0";
    private CapitalTobepaidbilldetailsActivityBinding mBinding;
    private DetailsModel mModel;
    private int mType;

    public static void start(Context context, int type, String orderId) {
        Intent intent = new Intent(context, ToBePaidBillDetailsActivity.class);
        intent.putExtra("orderId", orderId);
        intent.putExtra("type", type);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


    @Override
    protected void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.capital_tobepaidbilldetails_activity);
        mModel = new ViewModelProvider(this).get(DetailsModel.class);
        mBinding.rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mTipPop = new TipPop(mActivity);
        mAdapter = new BalanceDetailsAdapter(null);
    }

    public void initData() {
        mType = getIntent().getIntExtra("type", 1);
        mBinding.tvTitle.setText(mType==1?"待付订单详情":mType==2?"已付订单详情":mType==3?"应收订单详情":mType==4?"已收账单详情":"待付订单详情");
        mModel.getBillPayOrderDetails(this, getIntent().getStringExtra("orderId"));
    }

    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
    }


    @Override
    public void initObserve() {
        mModel.mPayDetailBean.observe(this, data -> {
            mBinding.tvBtn.setVisibility(mType > 2 ? View.GONE : View.VISIBLE);
            mBinding.tvBtn.setEnabled(mType == 2);
            mBinding.tvBtn.setText(mType == 1 ? "企业用户目前不支持在移动端进行支付" : "资金解冻");
            mBinding.tvBtn.setBackground(Utils.getResources().getDrawable(mType == 1 ? R.drawable.button_false : R.drawable.button));
            mBinding.tvBtn.setTextColor(Utils.getColor(mType == 1 ? R.color.color_FFC000 : R.color.white));
            mBeans.add(new Bean("业务类型", data.matters));
            mBeans.add(new Bean("订单号", data.orderId));
            mBeans.add(new Bean("业务支付号", data.paymentId));
            if (mType < 2) {
                mBeans.add(new Bean("交易流水号", data.number));
            }
            mBeans.add(new Bean("付款方", data.account));
            mBeans.add(new Bean("收款方", data.payeeName));
            mBeans.add(new Bean("订单金额", Utils.getDecimalFormat(data.money)));
            if (mType == 2) {
                mBeans.add(new Bean("交易手续费", Utils.getDecimalFormat(data.fees)));
            }
            mBeans.add(new Bean("付款方式", data.method));
            //付款状态 0 审批中 1 审批通过 3 审批未通过 4 确认中 5 支付成功
            if (mType == 2) {
                mBeans.add(new Bean("支付类型", data.payType));
                mBeans.add(new Bean("付款渠道", data.channel));
                mBeans.add(new Bean("付款账户", data.account));
                mBeans.add(new Bean("付款账号", data.accountNumber));
            }
            mBeans.add(new Bean("付款状态", data.state == 0 ? "审批中" : data.state == 1 ? "审批通过" :
                    data.state == 3 ? "审批未通过" :
                            data.state == 4 ? "冻结中" : data.state == 5 ? "支付成功" : "支付失败"));
            //交易状态 0 未付款 1 冻结中 2 支付成功 3 支付失败
            if (mType == 2) {
                mBeans.add(new Bean("资金状态", data.payState == 0 ? "未付款" : data.payState == 1 ? "冻结中" : data.payState == 2 ? "支付成功" : "支付失败"));
            }
            mBeans.add(new Bean("创建订单时间", Utils.getTime(data.created)));
            if (mType != 1) {
                mBeans.add(new Bean("付款时间", mType == 3 ? "/" : Utils.getTime(data.completionTime)));
            }
            if (mType == 2) {
                mBeans.add(new Bean("交易备注", data.remark));
            }
            mAdapter.setNewInstance(mBeans);
        });
    }
}