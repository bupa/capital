package com.sgb.capital.view.pop;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.callback.OnClickPopListener;
import com.sgb.capital.utils.Utils;

import razerdp.basepopup.BasePopupWindow;


/**
 * 作者:张磊
 * 日期:2019/10/24
 * 说明:提示弹框
 */
public class TipPop extends BasePopupWindow {

    private String mSureStr;
    private String mCancelStr;
    public TextView mTvSure;
    public TextView mTvCancel;
    private TextView mTvTip;
    private View mLine;
    private String mTitle = "温馨提示";
    private String mTip;
    private TextView mTvTitle;
    private OnClickPopListener onClickPopListener;

    public TipPop(Context context) {
        super(context);
    }

    /**
     * @param context   上下文
     * @param title     标题
     * @param tip       提示
     * @param sureStr   确定文案
     * @param CancelStr 取消文案
     */
    public TipPop(Context context, String title, String tip, String sureStr, String CancelStr) {
        this(context, title, tip, sureStr, CancelStr, null);
    }


    public TipPop(Context context, String title, String tip, String sureStr, String cancelStr, OnClickPopListener onClickListener) {
        super(context);
        mTitle = title;
        mTip = tip;
        mSureStr = sureStr;
        mCancelStr = cancelStr;
        this.onClickPopListener = onClickListener;
    }


    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.dialog_tip_pop);
        mTvSure = v.findViewById(R.id.tv_sure);
        mTvSure.setTypeface(Typeface.DEFAULT_BOLD);
        mTvCancel = v.findViewById(R.id.tv_cancel);
        mTvTip = v.findViewById(R.id.tv_tip);
        mTvTitle = v.findViewById(R.id.tv_title);
        mLine = v.findViewById(R.id.line);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> {
            dismiss();
            if(null != this.onClickPopListener) {
                this.onClickPopListener.onClickCancel();
            }
        });
        mTvSure.setOnClickListener(v12 -> {
            if(null != this.onClickPopListener) {
                this.onClickPopListener.onClickSure();
            }
        });
        return v;
    }




    public void initStr(String title, String tip, String sureStr, String cancelStr) {
        mTitle = title;
        mTip = tip;
        mSureStr = sureStr;
        mCancelStr = cancelStr;
    }

    public void initStr(String tip, String sureStr, String cancelStr) {
        mTip = tip;
        mSureStr = sureStr;
        mCancelStr = cancelStr;
    }

    public void show() {
        mTvSure.setText(mSureStr);
        if (Utils.isEmpty(mTitle)) {
            mTvTitle.setVisibility(View.GONE);
        }
        mTvTitle.setText(mTitle);
        mTvTip.setText(mTip);
        mTvCancel.setText(mCancelStr);
        mTvCancel.setTypeface(Utils.isEmpty(mSureStr) ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
        mTvCancel.setTextColor(Utils.isEmpty(mSureStr) ? Utils.getColor(R.color.color_5792FD) : Utils.getColor(R.color.color_666666));
        mTvSure.setVisibility(Utils.isEmpty(mSureStr) ? View.GONE : View.VISIBLE);
        mLine.setVisibility(Utils.isEmpty(mSureStr) ? View.GONE : View.VISIBLE);
        showPopupWindow();
    }

}
