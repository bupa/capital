package com.sgb.capital.view.ui.adapter;

import android.content.Context;
import android.widget.TextView;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.ContractDetailBean;
import com.sgb.capital.utils.ItemViewHolder;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.MToast;

import java.util.List;

/**
 * 合同信息
 */
public class OrderContractAdapter extends BaseRecycleAdapter<ContractDetailBean.ListDTO> {

    public OrderContractAdapter(Context context, List<ContractDetailBean.ListDTO> beans) {
        super(context, R.layout.ordercontract_adapter, beans);
    }


    @Override
    public void convert(ItemViewHolder holder, ContractDetailBean.ListDTO item) {
        //合同状态 1:拟定中，2：签署中，3：已完成，4：已过期，5，已作废
        holder.setBinding(BR.viewModel, item);
        holder.setText(R.id.tv_title, Utils.isEmpty(item.title) ? "/" : item.title)
                .setText(R.id.tv_status, item.status == 1 ? "拟定中" : item.status == 2 ? item.signTag == 6 ? "待对方签署" : "待我方签署" : item.status == 3 ? "已完成" : item.status == 4 ? "已过期" : "已作废")
                .setText(R.id.tv_no, item.no)
                .setText(R.id.tv_typeName, item.typeName)
                .setText(R.id.tv_companyName1, item.involves.size() == 0 ? "/" : item.involves.get(0).companyName)
                .setText(R.id.tv_companyName2, item.involves.size() == 0 ? "/" : item.involves.get(1).companyName)
        ;
        holder.getView(R.id.tv_btn).setOnClickListener(v -> MToast.showToast(Utils.getContext(), "开发中.."));
        TextView tv_status = holder.getView(R.id.tv_status);
        tv_status.setTextColor(Utils.getColor(
                item.status == 1 ? R.color.color_0286DF : item.status == 2 ? item.signTag == 6 ? R.color.color_FFC000 : R.color.color_FF9A3C :
                        item.status == 3 ? R.color.color_06C764 : item.status == 4 ? R.color.color_C0C4CC : R.color.color_969696
        ));
    }
}
