package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sgb.capital.R;
import com.sgb.capital.base.BasePopWindow;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.model.BindBankCardListEntity;
import com.sgb.capital.utils.AdapterOnItemClick;
import com.sgb.capital.view.ui.adapter.BankAdapter;

import java.util.List;

/**
 * chentao 2021/5/11
 * 选择银行弹框
 */
public class SelectBankPop extends BasePopWindow {
    private RecyclerView recyclerView;
    private TextView okView;
    private TextView cancelView;
    private BankAdapter bankAdapter;
    private List<BankEntity> bankList;
    public BankEntity bankEntity;
    private String id = "";

    public SelectBankPop(Context context) {
        super(context, DIR_DOWN_UP);
    }

    @Override
    protected int popLayout() {
        return R.layout.pop_select_bank;
    }

    @Override
    protected void initView(PopViewHolder holder) {
        recyclerView = holder.getPop().findViewById(R.id.rlv_list);
        cancelView = holder.getPop().findViewById(R.id.tv_cancel);
        okView = holder.getPop().findViewById(R.id.tv_ok);

        cancelView.setOnClickListener(v -> {
            bankEntity = null;
            if (onResultClick != null)
                onResultClick.result(bankEntity, null, null);
            dismiss();
        });
        okView.setOnClickListener(v -> {
            if (onResultClick != null)
                onResultClick.result(bankEntity, null, null);
            dismiss();
        });
        bankAdapter = new BankAdapter(mContext, null, R.layout.item_bank);
        bankAdapter.setItemSelect(true);
        bankAdapter.setDefItem(-1);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(bankAdapter);
        bankAdapter.setOnItemClick(new AdapterOnItemClick<BankEntity>() {
            @Override
            public void onItemClick(BankEntity s, int position) {
                bankEntity = s;
            }
        });
    }

    public void setData(List<BankEntity> bankList, String name) {
        this.bankList = bankList;
        bankAdapter.setDatas(bankList);
        //选中默认
        for (int i = 0; i < bankList.size(); i++) {
            if (name.equals(bankList.get(i).name)) {
                bankAdapter.setDefItem(i);
                bankAdapter.checkDatas(i);
            }

        }
    }


    public void setVisibleList(boolean visible) {
        recyclerView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    public void setDefData(int position) {
        bankAdapter.setDefItem(position);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (onDisMiss != null)
            onDisMiss.disMiss();
    }
}
