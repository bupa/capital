package com.sgb.capital.view.pop;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.utils.WithdrawalTimerUtils;

import java.util.HashMap;
import java.util.Map;

import razerdp.basepopup.BasePopupWindow;

public class WithdrawalPop extends BasePopupWindow {
    private static boolean clock_2, clock_3, clock_4, clock_5, clock_6;//输入框焦点锁
    private static EditText pass_1, pass_2, pass_3, pass_4, pass_5, pass_6;
    private static TextView tv_timer, tv_tip, tv_tip2, tv_money, tv_message;
    private static WithdrawalTimerUtils withdrawalTimerUtils;
    private String money, mobileId, pagreementId;
    private boolean mIsUser;
    private DialogButtonListener listener;

    public WithdrawalPop(Context context, DialogButtonListener listener) {
        super(context);
        this.listener = listener;
    }

    public void clearData() {
        pass_6.setText("");
        pass_5.setText("");
        pass_4.setText("");
        pass_3.setText("");
        pass_2.setText("");
        pass_1.setText("");
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.balance_pop);
        tv_message = v.findViewById(R.id.tv_message);
        TextView confirm = v.findViewById(R.id.confirm);
        TextView cancel = v.findViewById(R.id.cancel);
        pass_1 = v.findViewById(R.id.pass_1);
        pass_2 = v.findViewById(R.id.pass_2);
        pass_3 = v.findViewById(R.id.pass_3);
        pass_4 = v.findViewById(R.id.pass_4);
        pass_5 = v.findViewById(R.id.pass_5);
        pass_6 = v.findViewById(R.id.pass_6);
        tv_timer = v.findViewById(R.id.tv_timer);
        tv_tip = v.findViewById(R.id.tv_tip);
        tv_tip2 = v.findViewById(R.id.tv_tip2);
        tv_money = v.findViewById(R.id.tv_money);
        pass_2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (pass_2.getText().toString().length() == 0) {
                        pass_1.requestFocus();
                        return true;
                    } else {
                        return false;
                    }

                }
                return false;
            }
        });
        pass_3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (pass_3.getText().toString().length() == 0) {
                        pass_2.requestFocus();
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });
        pass_4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (pass_4.getText().toString().length() == 0) {
                        pass_3.requestFocus();
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });
        pass_5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (pass_5.getText().toString().length() == 0) {
                        pass_4.requestFocus();
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });
        pass_6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (pass_6.getText().toString().length() == 0) {
                        pass_5.requestFocus();
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });
        pass_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    pass_1.clearFocus();
                    pass_2.requestFocus();
                    clock_2 = true;
                    setTvtip(4);
                } else {
                    pass_1.requestFocus();
                    setTvtip(2);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        pass_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (clock_2) {
                    if (s.length() > 0) {
                        pass_2.clearFocus();
                        pass_3.requestFocus();
                        clock_3 = true;
                    } else {
                        pass_2.clearFocus();
                        pass_1.requestFocus();
                        clock_2 = false;
                    }
                }
                setTvtip(4);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        pass_3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (clock_3) {
                    if (s.length() > 0) {
                        pass_3.clearFocus();
                        pass_4.requestFocus();
                        clock_4 = true;
                    } else {
                        clock_2 = true;
                        clock_3 = false;
                        pass_3.clearFocus();
                        pass_2.requestFocus();
                    }
                }
                setTvtip(4);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        pass_4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (clock_4) {
                    if (s.length() > 0) {
                        pass_4.clearFocus();
                        pass_5.requestFocus();
                        clock_5 = true;
                    } else {
                        clock_3 = true;
                        clock_4 = false;
                        pass_4.clearFocus();
                        pass_3.requestFocus();
                    }
                }
                setTvtip(4);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        pass_5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (clock_5) {
                    if (s.length() > 0) {
                        clock_6 = true;
                        pass_5.clearFocus();
                        pass_6.requestFocus();
                    } else {
                        clock_4 = true;
                        pass_5.clearFocus();
                        pass_4.requestFocus();
                        clock_5 = false;
                    }
                }
                setTvtip(4);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        pass_6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (clock_6) {
                    clock_6 = false;
                    if (s.length() == 0) {
                        pass_6.clearFocus();
                        pass_5.requestFocus();
                        clock_5 = true;
                    }
                }
                setTvtip(4);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tv_timer.setOnClickListener(view -> {
            listener.applyCode(mobileId);
            clearData();
        });
        //确定
        confirm.setOnClickListener(view -> {
            if (TextUtils.isEmpty(pass_1.getText()) && TextUtils.isEmpty(pass_2.getText()) && TextUtils.isEmpty(pass_3.getText())
                    && TextUtils.isEmpty(pass_4.getText()) && TextUtils.isEmpty(pass_5.getText()) && TextUtils.isEmpty(pass_6.getText())) {
                setTvtip(2);
                return;
            } else if (TextUtils.isEmpty(pass_1.getText()) || TextUtils.isEmpty(pass_2.getText()) || TextUtils.isEmpty(pass_3.getText())
                    || TextUtils.isEmpty(pass_4.getText()) || TextUtils.isEmpty(pass_5.getText()) || TextUtils.isEmpty(pass_6.getText())) {
                setTvtip(1);
                return;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(pass_1.getText());
            stringBuffer.append(pass_2.getText());
            stringBuffer.append(pass_3.getText());
            stringBuffer.append(pass_4.getText());
            stringBuffer.append(pass_5.getText());
            stringBuffer.append(pass_6.getText());
            confirm.setEnabled(false);
            Utils.postDelay(() -> confirm.setEnabled(true),3000);
            Map map = new HashMap();
            map.put("amount", money);
            map.put("mobile", mobileId);
            map.put("code", stringBuffer.toString());
            map.put("pagreementId", pagreementId);
            listener.applyWithdrawal(map);
        });
        //取消
        cancel.setOnClickListener(view -> {
            this.dismiss();
            clearData();
        });
        return v;
    }

    public interface DialogButtonListener {
        void applyCode(String phone);

        void applyWithdrawal(Map map);
    }

    public void setData(String money, boolean user, String mobileId, String pagreementId) {
        this.money = money;
        this.mIsUser = user;
        this.mobileId = mobileId;
        this.pagreementId = pagreementId;
        tv_message.setText(mIsUser ? "提现手续费：¥0.40/笔" : "提现手续费：¥2.00/笔");
        tv_money.setText("¥" + money);
        tv_tip2.setText("验证码已发送至您手机" + mobileId.substring(0, 3) + "****" + mobileId.substring(mobileId.length() - 4));
        this.showPopupWindow();
    }

    public static void setStartTimer() {
        if (withdrawalTimerUtils != null) {
            withdrawalTimerUtils.cancel();
        }
        withdrawalTimerUtils = new WithdrawalTimerUtils(tv_timer, 60000, 1000); //倒计时1分钟
        withdrawalTimerUtils.start();
    }

    public static void setTvtip(int type) {
        if (type == 1) {
            tv_tip.setVisibility(View.VISIBLE);
            tv_tip.setText("验证码错误");
            tv_tip.setTextColor(Utils.getColor(R.color.color_FF2928));
        } else if (type == 2) {
            tv_tip.setVisibility(View.VISIBLE);
            tv_tip.setText("请输入验证码");
            tv_tip.setTextColor(Utils.getColor(R.color.color_FF2928));
        } else {
            tv_tip.setText("请输入手机验证码");
            tv_tip.setTextColor(Utils.getColor(R.color.color_969696));
            tv_tip.setVisibility(View.INVISIBLE);
        }
    }
}
