package com.sgb.capital.view.ui.adapter;


import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/1/12 0008
 * 说明:流水详情
 */
public class BalanceDetailsAdapter extends BaseQuickAdapter<Bean, BaseViewHolder> {


    public BalanceDetailsAdapter(List<Bean> beans) {
        super(R.layout.balancedetails_adapter_item, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final Bean item) {
        helper
                .setText(R.id.tv_name, item.no)
                .setText(R.id.tv_value, item.no.contains("失败原因") || item.no.contains("交易备注") ? "" : item.name)
                .setText(R.id.tv_tip_show, item.name)
                .setTextColor(R.id.tv_value, Utils.getColor(
                        item.name.contains("/") ? R.color.color_000000 :
                                (item.no.contains("金额") || item.no.contains("手续费")) ? R.color.c_FF4B10 : R.color.color_000000));
        helper.getView(R.id.tv_tip_show).setVisibility(item.no.contains("失败原因") || item.no.contains("交易备注") ? View.VISIBLE : View.GONE);
    }
}
