package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.PhonenumberActivityBinding;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.QrCodeModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

/**
 * 作者:张磊
 * 日期:2021/4/19 0019
 * 说明:预留手机号
 */
public class PhoneNumberActivity extends BaseActivity {
    PhonenumberActivityBinding mBinding;
    private CountDownTimer mTimer;

    QrCodeModel mModel;
    private String mPhone;
    private String mId;


    public static void start(Context context, String phone, String id) {
        Intent intent = new Intent(context, PhoneNumberActivity.class);
        intent.putExtra("phone", phone);
        intent.putExtra("id", id);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        Utils.setStatusBarLightMode(this, true);
        mBinding = DataBindingUtil.setContentView(this, R.layout.phonenumber_activity);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void initData() {
        mPhone = getIntent().getStringExtra("phone");
        mId = getIntent().getStringExtra("id");
        mBinding.tvTip.setText("更换提现银行卡预留手机后，下次申请提现时，提现" +
                "验证码将向该手机号发送。当前手机号：" + mPhone);
        mModel = new ViewModelProvider(this).get(QrCodeModel.class);
    }


    @Override
    public void initListener() {
        mBinding.tvSave.setOnClickListener(v -> {
            if (Utils.isEmpty(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(Utils.getContext(), "请输入手机号!");
                return;
            } else if (!Utils.isChinaPhoneLegal(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(this, "手机号格式错误");
                return;
            } else if (Utils.isEmpty(mBinding.etCode.getText().toString())) {
                MToast.showToast(Utils.getContext(), mBinding.etCode.getHint().toString());
                return;
            }
            mBinding.tvSave.setEnabled(false);
            mModel.modifyCardPhone(this, new Bean(mBinding.etPhone.getText().toString(), mBinding.etCode.getText().toString(), mId));
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.tvGetCode.setOnClickListener(v -> {
            if (Utils.isEmpty(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(Utils.getContext(), "请输入手机号!");
                return;
            } else if (!Utils.isChinaPhoneLegal(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(this, "手机号格式错误");
                return;
            }
            // 获取验证码
            mTimer = new CountDownTimer(60000, 1000) {
                public void onTick(long millisUntilFinished) {
                    mBinding.tvGetCode.setEnabled(false);
                    mBinding.tvGetCode.setText("重新获取" + "(" + (millisUntilFinished / 1000) + ")");
                }

                public void onFinish() {
                    mBinding.tvGetCode.setEnabled(true);
                    mBinding.tvGetCode.setText("获取验证码");
                }
            };
            mTimer.start();
            mModel.modifyCode(mBinding.etPhone.getText().toString().trim());
        });
    }

    @Override
    public void initObserve() {
        super.initObserve();
        mModel.mBaseEntity.observe(this, baseEntity -> {
            MToast.showToast(Utils.getContext(), "修改成功");
            Utils.sendMsg(Constants.EVENT_REFRESH, null);
            finish();
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.onFinish();
        }
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants.ACTIVATE:
                mBinding.tvSave.setEnabled(true);
                break;
        }
    }
}