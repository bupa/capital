package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.databinding.BankcardsActivityBinding;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.ViewPagerAdapter;
import com.sgb.capital.view.ui.fragment.BankCardsFragment;
import com.sgb.capital.viewmodel.CapitalModel;

import java.util.ArrayList;
import java.util.List;

import androidx.core.util.Pair;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:企业银行卡
 */
public class CompanyBankCardsActivity extends BaseActivity {

    private BankcardsActivityBinding mBinding;
    public int mPosition;
    public int mProcess;

    private CapitalModel mModel;
    public static void start(Context context,int process) {
        Intent intent = new Intent(context, CompanyBankCardsActivity.class);
        intent.putExtra("process",process);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        Utils.setStatusBarLightMode(this, true);
        mBinding = DataBindingUtil.setContentView(this, R.layout.bankcards_activity);
        mBinding.ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void initData() {
        mProcess =  getIntent().getIntExtra("process",0);
        mModel = new ViewModelProvider(this).get(CapitalModel.class);
        initTab(new String[]{"支付账户", "提现银行卡"});
        mBinding.ivBack.setOnClickListener(v -> finish());
        mModel.getProcess(this);
    }

    private void initTab(String[] titls) {
        List<Pair<String, Fragment>> mList = new ArrayList<>();
        for (String s : titls) {
            Bundle bundle = new Bundle();
            BankCardsFragment couponFragment = new BankCardsFragment();
            bundle.putString("data", s);
            couponFragment.setArguments(bundle);
            mList.add(new Pair<>(s, couponFragment));
        }
        mBinding.vpNeed.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), mList));
        mBinding.vpNeed.setOffscreenPageLimit(titls.length);
        mBinding.tab.setupWithViewPager(mBinding.vpNeed);


        mBinding.tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void initObserve() {
        mModel.mCapitalEntity.observe(this, capitalEntity -> mProcess = capitalEntity.process);
    }

    @Override
    protected void onDestroy() {
        Utils.sendMsg(Constants.EVENT_REFRESH,null);
        super.onDestroy();
    }
}