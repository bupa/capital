package com.sgb.capital.view.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sgb.capital.R;
import com.sgb.capital.databinding.AccountFragmentBinding;
import com.sgb.capital.model.AccountBean;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.SelectPop;
import com.sgb.capital.view.ui.adapter.AccountAdapter;
import com.sgb.capital.view.widget.xrecyclerview.ZXRecyclerView;
import com.sgb.capital.viewmodel.AccountModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:账户明细
 */
public class AccountFragment extends AppLazyFragment {
    private AccountBean mBean = new AccountBean();
    AccountModel mModel;
    private AccountFragmentBinding mBinding;
    private AccountAdapter mAdapter;
    private SelectPop mSelectPop;
    private List<Bean> mTimeBeans;
    private String mTime;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.account_fragment, container, false);
        return mBinding.getRoot();
    }

    @Override
    protected void initView() {
        mBinding.rvList.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mSelectPop = new SelectPop(getActivity());
        mSelectPop.setBgView(mBinding.grayLayout);
        mSelectPop.mType = 1;
        mTime = Utils.getStartAndEndTimeYMD(2, 1, "最近一个月");
        mBean.startDate = mTime.split("-")[0];
        mBean.endDate = mTime.split("-")[1];
        mModel = new ViewModelProvider(this).get(AccountModel.class);
    }

    @Override
    public void initObserve() {
        mModel.mTradingQuery.observe(this, accountBeans -> tradingQuery(accountBeans));
    }

    @Override
    public void initListener() {
        mSelectPop.setOnDismissListener(() -> {
            mBinding.img.setRotation(0);
            mBinding.tvName.setText(mSelectPop.mBean != null ? mSelectPop.mBean.name : "入账时间");
            mBinding.tvName.setTextColor(Utils.getColor(mSelectPop.mBean != null ? R.color.color_EF4033 : R.color.color_333333));
            mBinding.img.setImageResource(mSelectPop.mBean != null ? R.mipmap.down : R.mipmap.ic_down_arrow_n);
            if (mSelectPop.mPos1 == 0) {
                mTime = Utils.getStartAndEndTimeYMD(2, 1, "最近一个月");
            } else {
                mTime = mSelectPop.mBean.name.contains("入账时间") ? Utils.getStartAndEndTimeYMD(0, 0, "今日")
                        : mSelectPop.mBean.name.contains("最近一周") ? Utils.getStartAndEndTimeYMD(1, 7, "最近一周")
                        : mSelectPop.mBean.name.contains("最近一个月") ? Utils.getStartAndEndTimeYMD(2, 1, "最近一个月")
                        : Utils.getStartAndEndTimeYMD(3, 0, "本年");
            }
            mBean.startDate = mTime.split("-")[0];
            mBean.endDate = mTime.split("-")[1];
            if (mSelectPop.mIsClick) {
                mSelectPop.mIsClick = false;
                mModel.tradingQuery(this, mBean);
            }
        });

        mBinding.llView.setOnClickListener(v -> {
            Utils.hide(mActivity);
            mBinding.img.setRotation(180);
            mSelectPop.showPopwindow(mBinding.view, 0, 0);
        });
        mBinding.rvList.setLoadingListener(new ZXRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                mBean.pageNum = 1;
                mModel.tradingQuery(AccountFragment.this, mBean);
            }

            @Override
            public void onLoadMore() {
                mBean.pageNum++;
                mModel.tradingQuery(AccountFragment.this, mBean);
            }
        });

    }

    @Override
    protected void onLazyLoad() {
        Bundle arguments = getArguments();
        String data = arguments.getString("data");
        mBean.accType = data.contains("余额") ? "108" : "105";
        mTimeBeans = new ArrayList<>();
        mTimeBeans.add(new Bean("入账时间"));
        mTimeBeans.add(new Bean("今日"));
        mTimeBeans.add(new Bean("最近一周"));
        mTimeBeans.add(new Bean("最近一个月"));
        mSelectPop.setData(mTimeBeans);
        mAdapter = new AccountAdapter(getActivity(), null, mBean.accType);
        mBinding.rvList.setAdapter(mAdapter);
        mModel.tradingQuery(AccountFragment.this, mBean);
    }


    public void tradingQuery(List<AccountBean> data) {
        mBinding.rvList.refreshComplete();
        if (mBean.pageNum == 1) {
            mBinding.rvList.setVisibility(data.size() != 0 ? View.VISIBLE : View.GONE);
            mBinding.zErrorView.setVisibility(data.size() != 0 ? View.GONE : View.VISIBLE);
            mBinding.rvList.setLoadingMoreEnabled(true);
            mBinding.rvList.refreshComplete();
            mAdapter.setDatas(data);
        } else {
            if (data.size() != 0) {
                mBinding.rvList.loadMoreComplete();
                mAdapter.addDatas(data);
            } else {
                mBinding.rvList.setNoMore(true);
            }
        }
        mAdapter.notifyDataSetChanged();
    }
}