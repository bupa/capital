package com.sgb.capital.view.ui.adapter;

import android.graphics.Typeface;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:数据
 */
public class DataListAdapter extends BaseQuickAdapter<Bean, BaseViewHolder> {


    public DataListAdapter(List<Bean> beans) {
        super(R.layout.data_adapter, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final Bean item) {
        helper.setText(R.id.tv_name, item.no)
                .setTextColor(R.id.tv_name, Utils.getColor(item.select ? R.color.color_333333 : R.color.color_666666))
                .setVisible(R.id.iv_sel, item.select);
        TextView tv_name = helper.getView(R.id.tv_name);
        tv_name.setTypeface(item.select ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
    }
}