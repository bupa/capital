package com.sgb.capital.view.widget;

import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

public class NoSymbolputNewFilter implements InputFilter {
    private int max_size = -1;//针对最大长度不起作用做的限制
    public NoSymbolputNewFilter(){

    }

    public NoSymbolputNewFilter(int max_size){
        this.max_size = max_size;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        for (int i = start; i < end; i++) {
            if (!Character.isLetterOrDigit(source.charAt(i))
                    || Character.toString(source.charAt(i)).equals("_")
                    || Character.toString(source.charAt(i)).equals("-")) {
                return "";
            }
        }
        Log.e("source:",source.length()+"");
        Log.e("dest:",dest.length()+"");
        Log.e("dend:",dend+"");

        if(max_size!=-1&&dest.length()>=max_size){
            return "";
        }
        return null;

    }
}
