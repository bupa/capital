package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.CapitalPaidbillshopdetailsActivityBinding;

import androidx.databinding.DataBindingUtil;

/**
 * 作者:张磊
 * 日期:2021/9/1 0001
 * 说明:已付订单商品详情
 */
public class PaidBillShopDetailsActivity extends BaseActivity {


    private CapitalPaidbillshopdetailsActivityBinding mBinding;

    public static void start(Context context) {
        Intent intent = new Intent(context, PaidBillShopDetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


  @Override
  protected void initView() {
      mBinding = DataBindingUtil.setContentView(this, R.layout.capital_paidbillshopdetails_activity);
      initData();
      initListener();
  }

  public void initData() {

  }

  public void initListener() {
    mBinding.ivBack.setOnClickListener(v -> finish());
  }
}