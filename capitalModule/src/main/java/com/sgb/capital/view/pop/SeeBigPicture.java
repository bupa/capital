package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.sgb.capital.R;
import com.sgb.capital.base.BasePopWindow;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.utils.AdapterOnItemClick;
import com.sgb.capital.view.ui.adapter.BankAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * chentao 2021/5/11
 * 查看大图弹框
 */
public class SeeBigPicture extends BasePopWindow {

    public SeeBigPicture(Context context) {
        super(context, DIR_DOWN_UP_MATCH);
    }
    private ImageView imageView;
    private ViewPager viewPager;
    private LinearLayout linearLayout;
    private TextView tip,imgNum;
    @Override
    protected int popLayout() {
        return R.layout.pop_see_big_picture;
    }

    @Override
    protected void initView(PopViewHolder holder) {
        imageView = holder.getPop().findViewById(R.id.img_view);
        viewPager = holder.getPop().findViewById(R.id.view_page);
        linearLayout = holder.getPop().findViewById(R.id.ll_tip);
        tip = holder.getPop().findViewById(R.id.tip_tv);
        imgNum = holder.getPop().findViewById(R.id.img_num);
        FrameLayout v = holder.getPop().findViewById(R.id.view);
        v.setOnClickListener(v1 -> dismiss());
    }

    public void showPop(int type,View grayView){
        if (type == 1){
            linearLayout.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            ImageView imageView1 = new ImageView(mContext);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            imageView1.setLayoutParams(params);
            imageView1.setImageResource(R.mipmap.icon_shili4);
            ImageView imageView2 = new ImageView(mContext);
            imageView2.setLayoutParams(params);
            imageView2.setImageResource(R.mipmap.icon_shili3);
            List<View> views = new ArrayList<>();
            views.add(imageView1);
            views.add(imageView2);
            viewPager.setAdapter(new MyViewPagerAdapter(views));
            viewPager.setCurrentItem(0);
            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (position==0){
                        tip.setText("基本存款账户证明图片");
                        imgNum.setText("1/2");
                    }else {
                        tip.setText("开户许可证图片");
                        imgNum.setText("2/2");
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }else if (type == 2){
            linearLayout.setVisibility(View.GONE);
            viewPager.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(R.mipmap.icon_shili2);
        }else {
            linearLayout.setVisibility(View.GONE);
            viewPager.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(R.mipmap.icon_shili1);
        }
        this.showPopwindow(grayView,true);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (onDisMiss != null)
            onDisMiss.disMiss();
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private List<View> mListViews;
        public MyViewPagerAdapter(List<View> mListViews) {
            this.mListViews = mListViews;//构造方法，参数是我们的页卡，这样比较方便。
        }
        //直接继承PagerAdapter，至少必须重写下面的四个方法，否则会报错
        @Override
        public void destroyItem(ViewGroup container, int position, Object object)  {
            container.removeView(mListViews.get(position));//删除页卡
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position){
            //这个方法用来实例化页卡
            container.addView(mListViews.get(position), 0);//添加页卡
            return mListViews.get(position);
        }
        @Override
        public int getCount() {
            return  mListViews.size();//返回页卡的数量
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0==arg1;//官方提示这样写
        }
    }
}
