package com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.provider

import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseNodeAdapter
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.entity.node.BaseNode


abstract class BaseNodeProvider : BaseItemProvider<BaseNode>() {

    override fun getAdapter(): BaseNodeAdapter? {
        return super.getAdapter() as? BaseNodeAdapter
    }

}