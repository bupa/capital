package com.sgb.capital.view.ui.adapter;

import android.content.Context;
import android.text.SpannableString;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.CommoditiesBean;
import com.sgb.capital.utils.ItemViewHolder;
import com.sgb.capital.utils.Utils;

import java.util.List;

/**
 * 商品信息
 */
public class OrderShopAdapter extends BaseRecycleAdapter<CommoditiesBean> {

    public OrderShopAdapter(Context context, List<CommoditiesBean> beans) {
        super(context, R.layout.ordershop_adapter, beans);
    }


    @Override
    public void convert(ItemViewHolder holder, CommoditiesBean item) {

        String s = Utils.isEmpty(item.details) ? "属性:\t/" : "属性:\t" + item.details;
        SpannableString keyWordSpan = Utils.getKeyWordSpan(Utils.getColor(R.color.color_333333), s, "属性:");
        holder.setBinding(BR.viewModel, item);
        holder
                .setText(R.id.tv_name, Utils.isEmpty(item.name) ? "\t/" : "\t" + item.name)
                .setText(R.id.tv_brand, Utils.isEmpty(item.brand) ? "\t/" : "\t" + item.brand)
                .setText(R.id.tv_details, keyWordSpan)
                .setText(R.id.tv_count, "\t" + item.count)
                .setText(R.id.tv_money, Utils.isEmpty(item.money) ? "\t/" : "\t" + item.money)
        ;
    }
}
