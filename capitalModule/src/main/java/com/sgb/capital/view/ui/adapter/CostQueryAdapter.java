package com.sgb.capital.view.ui.adapter;

import android.content.Context;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.CostQueryBean;
import com.sgb.capital.utils.ItemViewHolder;
import com.sgb.capital.utils.Utils;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:费用查询
 */
public class CostQueryAdapter extends BaseRecycleAdapter<CostQueryBean> {

    public CostQueryAdapter(Context context, List<CostQueryBean> beans) {
        super(context, R.layout.costquery_adapter, beans);
    }

    @Override
    public void convert(ItemViewHolder holder, CostQueryBean item) {
        holder.setBinding(BR.viewModel, item);

        holder.setText(R.id.tv_tradeType, "手续费-" + (item.tradeType.contains("提现")?"账户提现":item.tradeType)).setText(R.id.tv_flowNo, item.flowNo)
                .setText(R.id.tv_orderId, item.paymentId)
                .setText(R.id.tv_title, item.tradeType.contains("提现") ? "/" : "业务订单号:")
                .setText(R.id.tv_created,item.tradeType.contains("提现") ? "/" :  item.payType)
                .setText(R.id.tv_payType, Utils.getTime(item.payTime))
                .setText(R.id.tv_tradeFees, "-" + Utils.getDecimalFormat(Float.parseFloat(item.tradeFees)))
                .setText(R.id.tv_otherCompanyName, Utils.isEmpty(item.otherCompanyName) ? "/" : item.otherCompanyName)
                .setVisible(R.id.ll_show,!item.tradeType.contains("提现"))
        ;
    }
}
