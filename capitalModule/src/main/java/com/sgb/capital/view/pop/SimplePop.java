package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import com.bigkoo.pickerview.adapter.ArrayWheelAdapter;
import com.contrarywind.view.WheelView;
import com.sgb.capital.R;
import com.sgb.capital.utils.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import kotlin.Pair;
import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

/**
 * 简单的Pop弹窗
 * @author cyj
 */
public class SimplePop extends BasePopupWindow {

    private final List<String> data = new ArrayList<>();
    private Map<Long, String> tempMap;
    private TextView tvTitle;
    private WheelView wheelView;
    private boolean isItemSelected;
    /** 当前滚动选中的item */
    private String currentItem = "";
    /** 当前滚动选中的位置 */
    private int currentPosition;
    private String tag = "default";
    private OnRightClickListener onRightClickListener;
    private Pair<String, String> pair = new Pair<>("", "");

    public SimplePop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.pop_simple);

        // wheelView 此控件局限性太大，只支持 IPickerViewData，int，String 内容。
        wheelView = v.findViewById(R.id.wv);
        wheelView.setCurrentItem(0);
        wheelView.setCyclic(false);
        wheelView.setAdapter(new ArrayWheelAdapter(new ArrayList()));
        wheelView.setOnItemSelectedListener(position -> {
            isItemSelected = true;
            this.setCurrentItem(position);
        });

        v.findViewById(R.id.tv_left).setOnClickListener(v1 -> dismiss());

        //右边按钮事件
        TextView tvRight = v.findViewById(R.id.tv_right);
        tvRight.setOnClickListener(v1 -> {
            dismiss();
            if(!isItemSelected) {
                setCurrentItem(Math.max(wheelView.getCurrentItem(), 0));
            }
            if(null != onRightClickListener) {
                onRightClickListener.onClick(pair, currentPosition, tag);
            }
        });

        tvTitle = v.findViewById(R.id.tv_center);
        return v;
    }

    /**
     * 滚动选中某一项
     * @param position
     */
    private void setCurrentItem(int position) {
        if(data.size() > 0) {
            currentItem = data.get(position);
        }
        currentPosition = position;
        String bizKey = null == tempMap ? "" : tempMap.get(Long.parseLong(position+""));
        pair = new Pair<>(null == bizKey ? "" : bizKey, currentItem);
    }

    /** 设置标题 */
    public SimplePop setTitle(String title) {
        tvTitle.setText(title);
        return this;
    }

    public SimplePop setRightClickListener(OnRightClickListener onRightClickListener) {
        this.onRightClickListener = onRightClickListener;
        return this;
    }

    /**
     * 设置数据
     * 注意：调用setData之前需先调用setCurrentSelectItem设置默认项，否则默认集合第一个显示
     * @param list
     * @return
     */
    public SimplePop setData(List<String> list) {
        this.data.clear();
        this.data.addAll(list);
        wheelView.setAdapter(new ArrayWheelAdapter(data));
        return this;
    }

    /**
     * 设置数据
     * 注意：调用setData之前需先调用setCurrentSelectItem设置默认项，否则默认集合第一个显示
     * @param array
     * @return
     */
    public SimplePop setData(String[] array) {
        return setData(Arrays.asList(array));
    }

    /**
     * 设置数据
     * 注意：调用setData之前需先调用setCurrentSelectItem设置默认项，否则默认集合第一个显示
     * @param map
     * @return
     */
    public SimplePop setData(Map<String, String> map) {
        long index = -1;
        if(null == tempMap) {
            tempMap = new LinkedHashMap<>();
        } else {
            this.tempMap.clear();
        }
        for(String value : map.keySet().toArray(new String[0])) {
            this.tempMap.put(++index, value);
        }
        return setData(Arrays.asList(map.values().toArray(new String[0])));
    }

    /**
     * 设置当前默认选中的item项
     * @param item
     * @return
     */
    public SimplePop setCurrentSelectItem(String item) {
        int index = null == item ? 0 : data.indexOf(item);
        // wheelView.setCurrentItem代码【totalScrollY = 0;】若每次不滚动，默认第一项问题
        wheelView.setCurrentItem(Math.max(index, 0));
        return this;
    }

    /**
     * 设置Tag
     * @param tag
     * @return
     */
    public SimplePop setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public interface OnRightClickListener {
        void onClick(Pair<String, String> item, int position, String tag);
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig().from(Direction.BOTTOM)).toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig().to(Direction.BOTTOM)).toDismiss();
    }


    public void show() {
        if(this.data.size() > 0) {
            this.isItemSelected = false;
            showPopupWindow();
        } // else {
            // 无任何显示意义
        //}
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }
}
