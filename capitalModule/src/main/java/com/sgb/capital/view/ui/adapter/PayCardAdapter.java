package com.sgb.capital.view.ui.adapter;

import android.content.Context;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.databinding.ItemCheckoutCounterBinding;
import com.sgb.capital.model.PayCardEntity;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.ItemViewHolder;

import java.util.List;


public class PayCardAdapter extends BaseRecycleAdapter<PayCardEntity> {
    private Context context;
    private String cardNum;
    private boolean isFirst = true;
    private int mPosition = -1;
    private OnClickItemListener mOnClickItemListener;

    public PayCardAdapter(Context context, List<PayCardEntity> datas, OnClickItemListener listener) {
        super(context, R.layout.item_checkout_counter, datas);
        this.context = context;
        this.mOnClickItemListener = listener;
    }

    @Override
    public void convert(ItemViewHolder holder, final PayCardEntity entity) {
        holder.setBinding(BR.viewModel, entity);
        cardNum = "";
        // TODO: 2021/6/16 0016 解密
        // TODO: 2021/6/16 0016 解密
        // TODO: 2021/6/16 0016 解密
        String bankCard = CryptoUtils.decryptAES_ECB(entity.bankCard);
        if (bankCard.length() > 4) {
            cardNum = bankCard.substring(bankCard.length() - 4);
        }
        ((ItemCheckoutCounterBinding) holder.getmBinding()).cardType.setText(entity.type == 0 ? "个人" : "企业");
        ((ItemCheckoutCounterBinding) holder.getmBinding()).tvCardName.setText(entity.openingBank);
        if (isFirst) {
            holder.setImageResource(R.id.select_card, entity.enabledDefault ? R.mipmap.icon_check_select : R.mipmap.icon_check_normal);
            isFirst = false;
        } else {
            holder.setImageResource(R.id.select_card, entity.isCheck ? R.mipmap.icon_check_select : R.mipmap.icon_check_normal);
        }

        ((ItemCheckoutCounterBinding) holder.getmBinding()).cardInfo.setText(String.format(context.getResources().getString(R.string.capital_card_info), cardNum, entity.accountName));
        ((ItemCheckoutCounterBinding) holder.getmBinding()).editBtn.setOnClickListener(v -> mOnClickItemListener.onItemClick(holder.getPosition()));
        ((ItemCheckoutCounterBinding) holder.getmBinding()).itemLayout.setOnClickListener(v -> {
            if (datas.get(holder.getPosition()).isCheck) {
                mPosition = -1;
                datas.get(holder.getPosition()).isCheck = false;
            } else {
                for (int i = 0; i < datas.size(); i++) {
                    if (i == holder.getPosition()) {
                        datas.get(holder.getPosition()).isCheck = true;
                        mPosition = holder.getPosition();
                    } else {
                        datas.get(i).isCheck = false;
                    }
                }
            }
            notifyDataSetChanged();
        });
    }

    public int getSelectPosition() {
        return mPosition;
    }

    public interface OnClickItemListener {
        void onItemClick(int position);
    }

}