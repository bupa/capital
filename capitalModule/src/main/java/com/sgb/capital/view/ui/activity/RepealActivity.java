package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.RepealActivityBinding;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.InvoicePop;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.AsiaRecordModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import razerdp.basepopup.BasePopupWindow;

/**
 * 作者:张磊
 * 日期:2021/4/19 0019
 * 说明:撤销申请
 */
public class RepealActivity extends BaseActivity {
    private RepealActivityBinding mBinding;
    private InvoicePop mInvoicePop;
    private AsiaRecordModel mModel;
    private String mInvoiceId;
    private TipPop mTipPop;
    private int mInvoiceStatus;

    public static void start(Context context, String invoiceId, int invoiceStatus) {
        Intent intent = new Intent(context, RepealActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("invoiceId", invoiceId);
        intent.putExtra("invoiceStatus", invoiceStatus);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.repeal_activity);
        mInvoicePop = new InvoicePop(this);
        mModel = new ViewModelProvider(this).get(AsiaRecordModel.class);
        mTipPop = new TipPop(this);
    }

    @Override
    public void initData() {
        mInvoiceId = getIntent().getStringExtra("invoiceId");
        mInvoiceStatus = getIntent().getIntExtra("invoiceStatus", 0);
    }


    @Override
    public void initListener() {
        mTipPop.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                finish();
                Utils.sendMsg(Constants.EVENT_REFRESH, "0");
            }
        });
        mBinding.tvBtn.setOnClickListener(v -> {
            if (mBinding.tvBankName.getText().toString().contains("原因")) {
                MToast.showToast(Utils.getContext(), "请确保信息填写完整,再进行提交");
                return;
            }
            mModel.verifyStatusChange(this, mInvoiceId, mInvoiceStatus + "");
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.llSelect.setOnClickListener(v -> {
            mInvoicePop.show(mBinding.tvBankName.getText().toString().contains("原因") ? 1 :
                    mBinding.tvBankName.getText().toString().contains("类型") ? 0 :
                            mBinding.tvBankName.getText().toString().contains("信息") ? 1 : 2
            );
        });
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    @Override
    public void initObserve() {
        mModel.mCode.observe(this, s -> {
            if (s.contains("1")) {
                mTipPop.initStr("发票状态信息已发生变化", null, Utils.getString(R.string.capital_sureStr));
                mTipPop.show();
                return;
            }
            HashMap map = new HashMap();
            map.put("invoiceId", mInvoiceId);
            map.put("remark", mBinding.etContent.getText().toString());
            map.put("revokeType",
                    mBinding.tvBankName.getText().toString().contains("类型") ? 1 :
                            mBinding.tvBankName.getText().toString().contains("信息") ? 2 : 3);
            mModel.revoke(this, map);

        });


        super.initObserve();
    }

    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_SELECT:
                String index = (String) event.data;
                mBinding.tvBankName.setText(index);
                mBinding.tvBankName.setTextColor(Utils.getColor(R.color.color_000000));
                break;
        }
    }
}