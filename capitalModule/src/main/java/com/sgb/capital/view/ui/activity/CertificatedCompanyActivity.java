package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.bumptech.glide.Glide;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.CertificatedcompanyActivityBinding;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.QrCodeModel;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

/**
 * 作者:张磊
 * 日期:2021/4/19 0019
 * 说明:认证资料
 */
public class CertificatedCompanyActivity extends BaseActivity {
    CertificatedcompanyActivityBinding mBinding;
    QrCodeModel mModel;

    public static void start(Context context) {
        Intent intent = new Intent(context, CertificatedCompanyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        Utils.setStatusBarLightMode(this, true);
        mBinding = DataBindingUtil.setContentView(this, R.layout.certificatedcompany_activity);
    }

    @Override
    public void initData() {
        mModel = new ViewModelProvider(this).get(QrCodeModel.class);
        mModel.getBusinessInfoByCompany(this);
    }

    @Override
    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void initObserve() {
        mModel.mBusinessInfoByCompanyEntity.observe(this, businessInfoByCompanyEntity -> {
            try {
                mBinding.nsView.setVisibility(View.VISIBLE);
                mBinding.tvUserId.setText(businessInfoByCompanyEntity.userId);
                mBinding.tvType.setText(businessInfoByCompanyEntity.subMerType.contains("3") ? "企业商户" : "个体工商户");
                mBinding.llShow.setVisibility(businessInfoByCompanyEntity.subMerType.contains("3") ? View.VISIBLE : View.GONE);
                mBinding.tvNameCompany.setText(businessInfoByCompanyEntity.subMerName);
                mBinding.tvBusinessAddress.setText(businessInfoByCompanyEntity.address);
                mBinding.tvCreditCode.setText(businessInfoByCompanyEntity.certId);
                mBinding.tvLegalName.setText(businessInfoByCompanyEntity.legalName);
                mBinding.tvIdentityId.setText(businessInfoByCompanyEntity.identityId.substring(0, 6) + "******" + businessInfoByCompanyEntity
                        .identityId.substring(businessInfoByCompanyEntity.identityId.length() - 4));
                mBinding.tvContactsName.setText(businessInfoByCompanyEntity.contactsName);
                mBinding.tvMobileId.setText(businessInfoByCompanyEntity.mobileId);
                Glide.with(mContext).load(businessInfoByCompanyEntity.bussinessLicense).into(mBinding.ivBusinessLicense);
                Glide.with(mContext).load(businessInfoByCompanyEntity.bankAccountLicence).into(mBinding.ivLicenceOpenAccounts);
                Glide.with(mContext).load(businessInfoByCompanyEntity.idCardFront).into(mBinding.ivIdCardFront);
                Glide.with(mContext).load(businessInfoByCompanyEntity.idCardBack).into(mBinding.ivIdCardBack);
            } catch (Exception e) {
                Utils.postDelay(() -> {
                    MToast.showToast(Utils.getContext(), "未获取到相关信息,请重新选择!Σ(☉▽☉");
                    finish();
                }, 1000);
            }
        });
    }
}