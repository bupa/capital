package com.sgb.capital.view.ui.adapter;

import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.model.SelBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

public class ZSelect2Adapter extends BaseQuickAdapter<SelBean, BaseViewHolder> {
    public ZSelect2Adapter(List<SelBean> data) {
        super(R.layout.zselect_2adapter, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SelBean item) {
        TextView item_name = helper.getView(R.id.tv_zname);
        View iv_show = helper.getView(R.id.iv_show);
        item_name.setText(Utils.isEmpty(item.name) ? "全部" : item.name);
        item_name.setTextColor(Utils.getColor(item.isSel ? R.color.color_000000 : R.color.color_666666));
        iv_show.setVisibility(item.isSel ? View.VISIBLE : View.GONE);
        item_name.setTypeface(item.isSel ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
    }
}