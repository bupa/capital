package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.MainActivityBinding;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.CompanyEntity;
import com.sgb.capital.utils.SPreferenceUtil;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.CompanyListPop;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.pop.UserPop;
import com.sgb.capital.view.ui.adapter.CapitalListAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.animation.AlphaInAnimation;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.CapitalModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/4/19 0019
 * 说明:sdk首页
 */
public class MainActivity extends BaseActivity {
    private MainActivityBinding mBinding;

    public List<BankEntity> mDatas = new ArrayList<>();
    private CapitalModel mModel;
    private UserPop mUserPop;
    private CompanyListPop mCompanyListPop;
    private int mType;
    private boolean mLogin;
    private List<Bean> mCompanyBeans = new ArrayList<>();
    private CapitalListAdapter mAdapter;
    private int mProcess;
    private TipPop mTipPop;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        Utils.setStatusBarLightMode(this, false);
        mBinding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        mBinding.ivBack.setOnClickListener(v -> finish());
        mUserPop = new UserPop(this);
        mCompanyListPop = new CompanyListPop(this);
        mTipPop = new TipPop(this);
        mBinding.rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mAdapter = new CapitalListAdapter(mCompanyBeans);
        mBinding.rv.setAdapter(mAdapter);
    }

    @Override
    public void initData() {
        // zjkj0719
        EventBus.getDefault().register(this);
        mModel = new ViewModelProvider(this).get(CapitalModel.class);
        SPreferenceUtil.getInstance().saveData(SPreferenceUtil.resourcePCToken, "");
        SPreferenceUtil.getInstance().saveData(SPreferenceUtil.resourceAPPToken, "");
    }


    @Override
    public void initObserve() {
        mModel.mCompanyEntity.observe(this, companyEntities -> {
            mCompanyListPop.mDatas.clear();
            mCompanyListPop.mLastIndex = 0;
            for (CompanyEntity entity : companyEntities) {
                mCompanyListPop.mDatas.add(new BankEntity(entity.getName(), entity.getCompNo(), entity.getType()));
            }
            if (mCompanyListPop.mDatas.size() != 0) {
                mCompanyListPop.mDatas.get(0).isSelect = true;
            }
            mCompanyListPop.show();
        });

        mModel.mIsCompany.observe(this, aBoolean -> {
            mLogin = true;
            mModel.getCompanyList(this);
        });
        mModel.mCapitalEntity.observe(this, capitalEntity -> {
            mProcess = capitalEntity.process;
            mBinding.tvType.setText((capitalEntity.process == 0 ? "未开户" : capitalEntity.process == 1 ? "审核中" : capitalEntity.process == 3 ? "开户失败" : "已开户"));
            mCompanyBeans.clear();
            mCompanyBeans.add(new Bean("首页"));
            if (capitalEntity.process == 0 || capitalEntity.process == 1) {
                mCompanyBeans.add(new Bean("开通账户"));
            }
            if (mType != 0) {
                mCompanyBeans.add(new Bean("业务管理"));
            } else {
                mCompanyBeans.add(new Bean("交易限额"));
            }
            mCompanyBeans.add(new Bean("账务流水"));
            mCompanyBeans.add(new Bean("费用查询"));
            mCompanyBeans.add(new Bean("账户明细"));
            mCompanyBeans.add(new Bean("银行卡"));
            mCompanyBeans.add(new Bean("认证资料"));
            mCompanyBeans.add(new Bean("余额充值"));
            mCompanyBeans.add(new Bean("开票记录"));
            mAdapter.setNewInstance(mCompanyBeans);
            mAdapter.setAdapterAnimation(new AlphaInAnimation());
            mAdapter.notifyDataSetChanged();
            mBinding.rv.setVisibility(View.VISIBLE);
            mBinding.tvType.setVisibility(View.VISIBLE);
            MToast.showToast(Utils.getContext(), "信息更新完毕");
        });
    }


    @Override
    public void initListener() {
        mTipPop.mTvSure.setOnClickListener(v -> {
            if (mProcess == 4) {
                if (mType == 0) {
                    PersonBankCardsActivity.start(MainActivity.this);
                } else {
                    CompanyBankCardsActivity.start(MainActivity.this, mProcess);
                }
            } else {
                OpenAccountFirstActivity.start(MainActivity.this, mType == 0);
            }
        });

        mBinding.tvList.setOnClickListener(v -> {
            if (!mLogin) {
                MToast.showToast(Utils.getContext(), "请先登录");
                return;
            }
            if (mCompanyListPop.mDatas.size() == 0) {
                mModel.getCompanyList(this);
            } else {
                mCompanyListPop.show();
            }
        });
        // 登录
        mBinding.tvLogin.setOnClickListener(v -> mUserPop.show());
    }

    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_REFRESH:
                if (null == event.data) {
                    return;
                }
                TextView textView = (TextView) event.data;
                String type = textView.getText().toString();
                if (type.contains("首页")) {
                    CapitalActivity.start(this, mType == 0);
                } else if (type.contains("开通账户")) {
                    OpenAccountFirstActivity.start(this, mType == 0);
                } else if (type.contains("交易限额")) {
                    DealLimitActivity.start(this);
                } else if (type.contains("业务管理")) {
                    if (mType == 0) {
                        MToast.showToast(Utils.getContext(), "个人账户无待付订单");
                        return;
                    }
                    BusinessManagementActivity.start(this, 1);
                } else if (type.contains("账务流水")) {
                    FinanceFlowAndCostQueryActivity.start(this, mType == 0, 0);
                } else if (type.contains("费用查询")) {
                    FinanceFlowAndCostQueryActivity.start(this, mType == 0, 1);
                } else if (type.contains("开票记录")) {
                    AsiaRecordActivity.start(this);
                } else if (type.contains("账户明细")) {
                    AccountActivity.start(this);
                } else if (type.contains("余额充值")) {
                    if (verification()) return;
                    PayActivity.start(this, false, mType == 0);
                } else if (type.contains("银行卡")) {
                    if (mType == 0) {
                        PersonBankCardsActivity.start(this);
                    } else {
                        CompanyBankCardsActivity.start(this, 2);
                    }
                } else if (type.contains("认证资料")) {
                    if (verification()) return;
                    if (mType == 0) {
                        CertificatedUserActivity.start(this);
                    } else {
                        CertificatedCompanyActivity.start(this);
                    }
                }
                break;
            case Constants
                    .CLICK_PXT:
                mCompanyListPop.mDatas.clear();
                mModel.login(this, "18780832941", "qqq123");
                break;
            case Constants
                    .CLICK_ZJ:
                mCompanyListPop.mDatas.clear();
                mModel.login(this, "18349250409", "qwer123");
                break;
            case Constants
                    .CLICK_HT:
                mCompanyListPop.mDatas.clear();
                mModel.login(this, "15892272092", "qqq123");
                break;
            case Constants
                    .CLICK_LL:
                mCompanyListPop.mDatas.clear();
                mModel.login(this, "18191037226", "123qwer");
                break;
            case Constants
                    .EVENT_SELECT:
                BankEntity data = (BankEntity) event.data;
                mType = data.type;
                CompanyEntity companyEntity = new CompanyEntity();
                companyEntity.setType(data.type);
                companyEntity.setCompNo(data.no);
                mModel.settingMainCompany(this, companyEntity);
                break;
        }
    }

    private boolean verification() {
        if (mProcess == 0 || mProcess == 3 || mProcess == 1) {
            mTipPop.initStr("温馨提示",
                    mProcess == 1 ? ("企业钱包开通审核中\n通过后可使用此功能") : (mType == 0 ? "开通个人钱包后\n" : "开通企业钱包后\n") +
                            "可使用此功能", (mProcess == 1 || mProcess == 2) ? null : "前往开通", "知道了");
            mTipPop.show();
            return true;
        }
        return false;
    }


    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}