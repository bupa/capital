package com.sgb.capital.view.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.MyZTextWatcher;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.ActivityAddEditBankCarBinding;
import com.sgb.capital.model.AddAndEditCardsBean;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.CardsListPop;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.AddAndEditCardsModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;




public class AddAndEditCardsActivity extends BaseActivity {

    private ActivityAddEditBankCarBinding mBinding;
    private AddAndEditCardsModel mModel;
    private String id;
    private AddAndEditCardsBean mAndEditCardsBean = new AddAndEditCardsBean();
    private CardsListPop mListPop;

    public static void start(Activity context, String id) {
        Intent intent = new Intent();
        intent.putExtra("id", id);
        intent.setClass(context, AddAndEditCardsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Utils.setStatusBarLightMode(this, true);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_edit_bank_car);
        mModel = new ViewModelProvider(this).get(AddAndEditCardsModel.class);
        id = getIntent().getStringExtra("id");
        if (TextUtils.isEmpty(id)) {
            mBinding.tvTitle.setText("添加银行卡");
        } else {
            mBinding.tvTitle.setText("编辑银行卡");
            mModel.getCarInfo(this, id);
            mBinding.bankName.setTextColor(Utils.getColor(R.color.color_000000));
        }
        mListPop = new CardsListPop(this);
        mModel = new ViewModelProvider(this).get(AddAndEditCardsModel.class);
    }


    @Override
    public void initObserve() {
        mModel.mCarInfo.observe(this, andEditCardsBean -> getCarInfoSuccess());
        mModel.saveSuccess.observe(this, s -> {
            finish();
            Utils.sendMsg(Constants.EVENT_REFRESH, null);
        });
        mModel.mBank.observe(this, bankEntities -> {
            mListPop.mDatas = bankEntities;
            if (mBinding.bankName.getText().toString().contains("请选择银行")) {
                if (mListPop.mDatas.size() != 0) {
                    mListPop.mDatas.get(0).isSelect = true;
                }
            } else {
                for (int i = 0; i < mListPop.mDatas.size(); i++) {
                    if (mBinding.bankName.getText().toString().contains(mListPop.mDatas.get(i).name)) {
                        mListPop.mDatas.get(i).isSelect = true;
                        break;
                    }
                }
            }
            mListPop.show();
        });

    }

    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.bankName.setOnClickListener(v -> {
            if (mBinding.bankName.getText().toString().contains("请选择银行") || mListPop.mDatas.size() == 0) {
                mModel.getQpBank(this);
                return;
            }
            mListPop.show();
        });

        mBinding.saveButton.setOnClickListener(view -> saveCarInfo());
        mBinding.accountName.addTextChangedListener(new MyZTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 60) {
                    MToast.showToast(AddAndEditCardsActivity.this, "最多可输入60个文字");
                    mBinding.accountName.setText(s.subSequence(0, 60));
                    mBinding.accountName.setSelection(60);
                }
            }
        });
        mBinding.IDNumber.addTextChangedListener(new MyZTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 22) {
                    MToast.showToast(AddAndEditCardsActivity.this, "最多可输入22位");
                    mBinding.IDNumber.setText(s.subSequence(0, 22));
                    mBinding.IDNumber.setSelection(22);
                }
            }
        });
        mBinding.carNum.addTextChangedListener(new MyZTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 35) {
                    MToast.showToast(AddAndEditCardsActivity.this, "最多可输入35位");
                    mBinding.carNum.setText(s.subSequence(0, 35));
                    mBinding.carNum.setSelection(35);
                }
            }
        });
    }

    public void getCarInfoSuccess() {
        String idCard = CryptoUtils.decryptAES_ECB(mModel.mCarInfo.getValue().idCard);
        mBinding.accountName.setText(mModel.mCarInfo.getValue().accountName);
        mBinding.accountName.setEnabled(false);
        mBinding.accountName.setTextColor(Utils.getColor(R.color.color_c0c4cc));
        mBinding.IDNumber.setText(idCard.substring(0, 4) + "********" + idCard.substring(idCard.length() - 4));
        mBinding.IDNumber.setTextColor(Utils.getColor(R.color.color_c0c4cc));
        mBinding.IDNumber.setEnabled(false);
        mBinding.bankName.setText(mModel.mCarInfo.getValue().openingBank);
        mBinding.carNum.setText(CryptoUtils.decryptAES_ECB(mModel.mCarInfo.getValue().bankCard));
        mBinding.carPhone.setText(CryptoUtils.decryptAES_ECB(mModel.mCarInfo.getValue().phone));
    }

    private void saveCarInfo() {
        if (TextUtils.isEmpty(mBinding.accountName.getText().toString())) {
            MToast.showToast(this, "请输入账户名称");
            return;
        }
        if (TextUtils.isEmpty(mBinding.IDNumber.getText().toString())) {
            MToast.showToast(this, "请输入身份证号");
            return;
        }
        if (mBinding.bankName.getText().toString().contains("请选择银行")) {
            MToast.showToast(this, "请选择开户银行");
            return;
        }
        if (TextUtils.isEmpty(mBinding.carNum.getText().toString())) {
            MToast.showToast(this, "请输入银行卡号");
            return;
        }
        if (TextUtils.isEmpty(mBinding.carPhone.getText().toString())) {
            MToast.showToast(this, "请输入预留手机号");
            return;
        }
        if (!mBinding.tvTitle.getText().toString().contains("编辑银行卡")) {
            if (!Utils.personIdValidation(mBinding.IDNumber.getText().toString())) {
                MToast.showToast(this, "身份证号格式错误");
                return;
            }
        }
        if (mBinding.carNum.getText().toString().length() < 8) {
            MToast.showToast(this, "银行卡号格式错误");
            return;
        }
        if (!Utils.isChinaPhoneLegal(mBinding.carPhone.getText().toString())) {
            MToast.showToast(this, "手机号格式错误");
            return;
        }
        // TODO: 2021/6/16 0016  加密 bankCard idCard phone
        mBinding.saveButton.setEnabled(false);
        mAndEditCardsBean.accountName = mBinding.accountName.getText().toString();
        mAndEditCardsBean.idCard = mBinding.tvTitle.getText().toString().contains("编辑银行卡") ? mModel.mCarInfo.getValue().idCard : CryptoUtils.encryptAES_ECB(mBinding.IDNumber.getText().toString());
        mAndEditCardsBean.openingBank = mBinding.bankName.getText().toString();
        mAndEditCardsBean.bankCard = CryptoUtils.encryptAES_ECB(mBinding.carNum.getText().toString());
        mAndEditCardsBean.phone = CryptoUtils.encryptAES_ECB(mBinding.carPhone.getText().toString());
        mAndEditCardsBean.type = "0";
        mAndEditCardsBean.cardType = "1";
        mAndEditCardsBean.id = mBinding.tvTitle.getText().toString().contains("编辑银行卡") ? id : null;
        if (TextUtils.isEmpty(mAndEditCardsBean.gateId) && !TextUtils.isEmpty(id) && mModel.mCarInfo.getValue() != null) {
            mAndEditCardsBean.gateId = mModel.mCarInfo.getValue().gateId;
        }
        mModel.payBankCardList(AddAndEditCardsActivity.this, mAndEditCardsBean);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_SELECT:
                BankEntity data = (BankEntity) event.data;
                mBinding.bankName.setText(data.name);
                mBinding.bankName.setTextColor(Utils.getColor(R.color.color_000000));
                mAndEditCardsBean.gateId = data.code;
                break;
            case Constants.ACTIVATE:
                mBinding.saveButton.setEnabled(true);
                break;
        }
    }
}
