package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import com.bigkoo.pickerview.adapter.ArrayWheelAdapter;
import com.contrarywind.view.WheelView;
import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:切换的弹框
 */
public class CutPop extends BasePopupWindow {


    public String mTip="企业商户";
    public List<String> mList;

    public CutPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.cut_pop);

        WheelView wheelView = v.findViewById(R.id.wv);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> dismiss());
        mList = new ArrayList<>();
        mList.add("企业商户");
        mList.add("个体工商户");
        wheelView.setCurrentItem(0);
        wheelView.setCyclic(false);
        wheelView.setAdapter(new ArrayWheelAdapter(mList));

        wheelView.setOnItemSelectedListener(index -> {
            mTip = mList.get(index);
        });
        TextView tv_sure = v.findViewById(R.id.tv_sure);
        tv_sure.setOnClickListener(v1 -> {
            dismiss();
            Utils.sendMsg(Constants.EVENT_SELECT,mTip);
        });


        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }


    public void show() {
        showPopupWindow();
    }
}