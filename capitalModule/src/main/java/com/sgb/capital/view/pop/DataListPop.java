package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.DataListAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:数据集合
 */
public class DataListPop extends BasePopupWindow {


    public List<Bean> mDatas = new ArrayList<>();
    private DataListAdapter mAdapter;
    public Bean mDataBean;
    public int mLastIndex = 0;
    private View mIvNull;

    public DataListPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.datalist_pop);
        RecyclerView rv = v.findViewById(R.id.rv);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> dismiss());
        rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mDatas = new ArrayList<>();
        mAdapter = new DataListAdapter(mDatas);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            mDataBean = mAdapter.getData().get(position);
            Bean bean2 = mAdapter.getData().get(mLastIndex);
            if (mLastIndex != position) {
                mDataBean.select = true;
                bean2.select = false;
            }
            mLastIndex = position;
            mAdapter.notifyDataSetChanged();
        });
        rv.setAdapter(mAdapter);


        TextView tv_sure = v.findViewById(R.id.tv_sure);
        tv_sure.setOnClickListener(v1 -> {
            if (mIvNull.getVisibility() == View.VISIBLE) return;
            Utils.sendMsg(Constants.EVENT_SELECT, mDataBean == null ? mAdapter.getData().get(0) : mDataBean);
            dismiss();
        });
        mIvNull = v.findViewById(R.id.iv_null);
        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }


    public void show() {
        if (mDatas.size() == 0) {
            mIvNull.setVisibility(View.VISIBLE);
            return;
        }
        mAdapter.setNewInstance(mDatas);
        mAdapter.notifyDataSetChanged();
        showPopupWindow();
    }
}