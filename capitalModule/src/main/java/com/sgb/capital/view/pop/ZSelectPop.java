package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BasePopWindow;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.SelBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.ZSelect1Adapter;
import com.sgb.capital.view.ui.adapter.ZSelect2Adapter;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 作者:张磊
 * 日期:2019/10/24
 * 说明:二级筛选
 */
public class ZSelectPop extends BasePopWindow {

    public Bean mName1 = new Bean(null);
    public Bean mName2 = new Bean(null);
    public String name;
    public String strKey;
    public int mlastIndex1;
    public int mlastIndex2;
    public boolean mIsClick;
    private RecyclerView mRv1;
    private RecyclerView mRv2;
    private ZSelect1Adapter mSelect1Adapter;
    public ZSelect2Adapter mSelect2Adapter;
    private View mTvOk;
    private View mTvReSet;
    public List<SelBean> mDatas = new ArrayList<>();
    private boolean mIsAll = true;

    public ZSelectPop(Context context) {
        super(context);
    }

    @Override
    protected int popLayout() {
        return R.layout.zselect_pop;
    }

    @Override
    protected void initView(PopViewHolder holder) {
        // 办理
        mRv1 = holder.getPop().findViewById(R.id.rv_1);
        mRv2 = holder.getPop().findViewById(R.id.rv_2);
        mTvOk = holder.getPop().findViewById(R.id.tv_ok);
        mTvReSet = holder.getPop().findViewById(R.id.tv_re_set);
        mRv1.setLayoutManager(new GridLayoutManager(mContext, 1));
        mRv2.setLayoutManager(new GridLayoutManager(mContext, 1));
        mSelect1Adapter = new ZSelect1Adapter(null);
        mSelect2Adapter = new ZSelect2Adapter(null);
        mRv1.setAdapter(mSelect1Adapter);
        mRv2.setAdapter(mSelect2Adapter);

        mSelect1Adapter.setOnItemClickListener((adapter, view, position) -> {
            SelBean selBean = mSelect1Adapter.getData().get(position);
            SelBean selLastBean = mSelect1Adapter.getData().get(mlastIndex1);
            if (selBean.isSel) {
                return;
            }
            selBean.isSel = true;
            selLastBean.isSel = false;
            mlastIndex1 = position;
            mSelect1Adapter.notifyDataSetChanged();
            List<SelBean> children = mSelect1Adapter.getData().get(position).children;
            mlastIndex2 = 0;
            if (children == null || children.size() == 0) {
                mName2.name = null;
            } else {
                for (SelBean bean : children) {
                    bean.isSel = false;
                }
                if (children.get(0).name != null) {
                    if (mIsAll) {
                        children.add(0, new SelBean());
                    }
                }
                children.get(0).isSel = true;
            }
            mSelect2Adapter.setNewInstance(children);
            mName1.name = mSelect1Adapter.getData().get(position).name;
            if (!mIsAll) {
                name = mName1.name + "-" + children.get(0).name;
                strKey = children.get(0).strKey;
            }
            mSelect2Adapter.notifyDataSetChanged();
        });
        mSelect2Adapter.setOnItemClickListener((adapter, view, position) -> {
            SelBean selBean = mSelect2Adapter.getData().get(position);
            SelBean selLastBean = mSelect2Adapter.getData().get(mlastIndex2);
            if (selBean.isSel) {
                return;
            }
            selBean.isSel = true;
            selLastBean.isSel = false;
            mlastIndex2 = position;
            if (!Utils.isEmpty(mSelect2Adapter.getData().get(position).name)) {
                mName2.name = mSelect2Adapter.getData().get(position).name;
            } else {
                mName2.name = null;
            }
            if (!mIsAll) {
                name = mName1.name + "-" + selBean.name;
                strKey = selBean.strKey;
            }
            mSelect2Adapter.notifyDataSetChanged();
        });

        mTvReSet.setOnClickListener(v -> {
            mlastIndex1 = mlastIndex2 = 0;
            List<SelBean> selBean = mSelect1Adapter.getData();
            for (SelBean bean : selBean) {
                bean.isSel = false;
                if (bean.children != null && bean.children.size() != 0) {
                    for (SelBean children : bean.children) {
                        children.isSel = false;
                    }
                }
            }
            selBean.get(0).isSel = true;
            mName1.name = null;
            if (!mIsAll) {
                mSelect1Adapter.getData().get(0).children.get(0).isSel = true;
                name = mSelect1Adapter.getData().get(0).name + "-" + mSelect1Adapter.getData().get(0).children.get(0).name;
                strKey = mSelect1Adapter.getData().get(0).children.get(0).strKey;
                mSelect2Adapter.setNewInstance(mSelect1Adapter.getData().get(0).children);
            } else {
                mSelect2Adapter.setNewInstance(null);
            }
            mSelect1Adapter.notifyDataSetChanged();
        });
        mTvOk.setOnClickListener(v -> {
            mIsClick = true;
            dissMiss();
        });
    }

    public void setData(List<SelBean> list) {
        mDatas = list;
        list.add(0, new SelBean());
        list.get(this.mlastIndex1).isSel = true;
        mSelect1Adapter.setNewInstance(list);
        List<SelBean> children = mSelect1Adapter.getData().get(this.mlastIndex1).children;
        if (children == null || children.size() == 0) return;
        mSelect2Adapter.getData().clear();
        children.add(new SelBean());
        children.get(mlastIndex2).isSel = true;
        mSelect2Adapter.setNewInstance(children);

    }

    public void setData(List<SelBean> list, boolean isAll) {
        mIsAll = isAll;
        mDatas = list;
        list.get(this.mlastIndex1).isSel = true;
        mSelect1Adapter.setNewInstance(list);
        List<SelBean> children = mSelect1Adapter.getData().get(this.mlastIndex1).children;
        if (children == null || children.size() == 0) return;
        mSelect2Adapter.getData().clear();
        children.get(mlastIndex2).isSel = true;
        mName1.name = list.get(this.mlastIndex1).name;
        name = list.get(this.mlastIndex1).name + "-" + children.get(mlastIndex2).name;
        strKey = children.get(mlastIndex2).strKey;
        mSelect2Adapter.setNewInstance(children);
    }


    public void setNewInstance() {
        List<SelBean> selBean = mSelect1Adapter.getData();
        for (SelBean bean : selBean) {
            bean.isSel = false;
            if (bean.children != null && bean.children.size() != 0) {
                for (SelBean children : bean.children) {
                    children.isSel = false;
                }
            }
        }
        mDatas.get(this.mlastIndex1).isSel = true;
        List<SelBean> children = mSelect1Adapter.getData().get(this.mlastIndex1).children;
        if (children == null || children.size() == 0) return;
        children.get(mlastIndex2).isSel = true;
        mName1.name = mDatas.get(this.mlastIndex1).name;
        name = mDatas.get(this.mlastIndex1).name + "-" + children.get(mlastIndex2).name;
        strKey = children.get(mlastIndex2).strKey;
        mSelect2Adapter.setNewInstance(children);
        mSelect1Adapter.notifyDataSetChanged();
        mSelect2Adapter.notifyDataSetChanged();
    }
}