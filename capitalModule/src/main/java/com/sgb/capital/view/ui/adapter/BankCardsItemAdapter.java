package com.sgb.capital.view.ui.adapter;

import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:银行卡
 */
public class BankCardsItemAdapter extends BaseQuickAdapter<BankBean, BaseViewHolder> {

    private final boolean mUser;
    public boolean mIsPay;

    public BankCardsItemAdapter(List<BankBean> beans, boolean isUser) {
        super(R.layout.personbankcardsitem_adapter, beans);
        mUser = isUser;
    }

    @Override
    protected void convert(BaseViewHolder helper, final BankBean item) {
        try {
            if (mIsPay) {
                // TODO: 2021/6/15 0015 解密
                // bankCard idCard phone
                String bankCard = CryptoUtils.decryptAES_ECB(item.bankCard);
                item.bankCard = bankCard;
                item.phone = CryptoUtils.decryptAES_ECB(item.phone);
                item.idCard = CryptoUtils.decryptAES_ECB(item.idCard);

                helper
                        .setBackgroundResource(R.id.fl_bg, item.openingBank.contains("工商银行") || item.openingBank.contains("中国银行") ? R.mipmap.gs_bg : item.openingBank.contains("建设银行") ?
                                R.mipmap.jh_bg : item.openingBank.contains("农业银行") ? R.mipmap.lh_bg : R.mipmap.lh_bg)
                        .setVisible(R.id.tv_zf, mUser)
                        .setVisible(R.id.tv_tx, mUser)
                        .setVisible(R.id.ll_pay, mIsPay)
                        .setText(R.id.tv_openingBank, item.openingBank)
                        .setText(R.id.tv_accountName, item.accountName)
                        .setText(R.id.tv_type, item.type == 0 ? "个人" : "企业")
                        .setText(R.id.tv_cardType, item.cardType == 0 ? "借记卡" : "贷记卡")
                        .setText(R.id.tv_bankCard, bankCard.substring(0, 4) + " **** "
                                + "**** " + bankCard.substring(bankCard.length() - 4));
            } else {
                // TODO: 2021/4/25 0025 提现
                String bankAccount = CryptoUtils.decryptAES_ECB(item.bankAccount);
                item.bankAccount = bankAccount;
                item.mobileId = CryptoUtils.decryptAES_ECB(item.mobileId);
                helper
                        .setBackgroundResource(R.id.fl_bg, item.bankName.contains("工商银行") || item.bankName.contains("中国银行") ? R.mipmap.gs_bg : item.bankName.contains("建设银行") ?
                                R.mipmap.jh_bg : item.bankName.contains("农业银行") ? R.mipmap.lh_bg : R.mipmap.lh_bg)
                        .setVisible(R.id.tv_zf, false)
                        .setVisible(R.id.tv_tx, false)
                        .setVisible(R.id.tv_cardType, false)
                        .setVisible(R.id.ll_pay, false)
                        .setText(R.id.tv_openingBank, item.bankName)
                        .setText(R.id.tv_accountName, item.bankAccountName)
                        .setText(R.id.tv_type, item.userType == 3 ? "企业" : "个体工商户")
                        .setText(R.id.tv_bankCard, bankAccount.substring(0, 4) + " **** "
                                + "**** " + bankAccount.substring(bankAccount.length() - 4));
            }
        } catch (Exception e) {

        }

        helper.getView(R.id.tv_btn_unbind).setOnClickListener(v -> Utils.sendMsg(Constants.EVENT_UNBIND, item));
        helper.getView(R.id.tv_btn_alter).setOnClickListener(v -> Utils.sendMsg(Constants.EVENT_ALTER_COMPANY, item));
    }

}