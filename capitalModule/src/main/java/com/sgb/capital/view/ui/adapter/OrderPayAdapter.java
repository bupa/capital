package com.sgb.capital.view.ui.adapter;

import android.content.Context;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.PayOrderEntity;
import com.sgb.capital.utils.ItemViewHolder;
import com.sgb.capital.utils.Utils;

import java.util.List;

public class OrderPayAdapter extends BaseRecycleAdapter<PayOrderEntity> {


    public int mType = 1;
    private OnClickAdapterListener listener;

    public OrderPayAdapter(Context context, List<PayOrderEntity> beans, OnClickAdapterListener onClickAdapterListener) {
        super(context, R.layout.item_order_pay_error, beans);
        this.listener = onClickAdapterListener;
    }

    @Override
    public void convert(ItemViewHolder holder, PayOrderEntity item) {
        holder.setBinding(BR.viewModel, item);
        holder.setText(R.id.order_type_z, item.orderTypeName)
                .setText(R.id.order_num_z, item.orderId)
                .setText(R.id.tv_order_time_z,
                        Utils.getTime(mType == 7 ? item.created : mType > 4 ? item.payTime : (mType == 2 || mType == 4) ? item.completionTime : item.created))
                .setText(R.id.pay_type_z, mType > 4 ? "在线转账" : item.method)
                .setText(R.id.tv_contact_z, item.contacts)
                .setText(R.id.tv_money_z, "¥" + Utils.getDecimalFormat(item.money))
                .setText(R.id.tv_state_z, mType == 3 ? "付款方:" : mType == 4 ? "交易状态:" : "付款状态:")
                .setVisible(R.id.ll_show_z, mType == 1 || mType == 4)
        ;


        switch (item.state) {
            case 1:
                holder.setText(R.id.tv_pay_success_z, "未付款");
                break;
            case 4:
                holder.setText(R.id.tv_pay_success_z, "确认中");
                break;
            case 5:
                holder.setText(R.id.tv_pay_success_z, "支付成功");
                break;
            default:
                holder.setText(R.id.tv_pay_success_z, "支付失败");
                break;
        }
        if (mType == 3) {
            holder.setText(R.id.tv_pay_success_z, item.payerCompanyName);
        } else if (mType == 4) {
            holder.setText(R.id.tv_name_z, "付款状态:")
                    .setVisible(R.id.ll_show_z, true)
                    .setVisible(R.id.ll_show2_z, true)
                    .setText(R.id.tv_type_z, item.method)
                    .setText(R.id.tv_contact_z, item.contacts)
                    .setText(R.id.tv_state_z, "交易状态:")
            ;
        } else {
            holder.setText(R.id.tv_name_z, "业务联系人:");
            if (mType == 5) {
                holder
                        .setText(R.id.order_type_z, "在线转账")
                        .setText(R.id.tv_money_z, "¥" + Utils.getDecimalFormat(item.transferMoney))
                        .setText(R.id.pay_type_z, "在线支付")
                        .setText(R.id.order_num_z, item.orderId)
                        .setText(R.id.tv_state_z, "收款方:")
                        .setText(R.id.tv_pay_success_z, item.payeeCompanyName.length() > 4 ? item.payeeCompanyName.substring(0, 4) + "..." : item.payeeCompanyName)
                        .setVisible(R.id.ll_show2_z, true)
                        .setVisible(R.id.ll_show_z, false);
            }
            if (mType == 6) {
                holder
                        .setText(R.id.order_type_z, "在线转账")
                        .setText(R.id.order_num_z, item.orderId)
                        .setText(R.id.tv_money_z, "¥" + Utils.getDecimalFormat(item.transferMoney))
                        .setVisible(R.id.ll_show_z, false)
                        .setVisible(R.id.ll_show2_z, false)
                ;
            }
            if (mType == 7) {
                holder.setText(R.id.tv_state_z, "业务联系人:")
                        .setText(R.id.tv_pay_success_z, item.contacts)
                        .setVisible(R.id.ll_show_z, false)
                ;
            }
            notifyDataSetChanged();
        }

    }

    public interface OnClickAdapterListener {
        void OnItemClickPay(int index);

        void OnItemClickDetail(int index);
    }
}
