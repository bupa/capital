package com.sgb.capital.view.widget;

import android.text.InputFilter;
import android.text.Spanned;

public class ChineseInputFilter implements InputFilter {
    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        for (int i = start; i < end; i++) {
            if (!isChinese(source.charAt(i))) {
                return "";
            }
        }
        return null;
    }

    public static boolean isChinese(char c) {
        String regex = "[\\u4e00-\\u9fa5]";
        return String.valueOf(c).matches(regex);
    }
}
