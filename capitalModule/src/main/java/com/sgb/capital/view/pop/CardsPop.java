package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;

import com.sgb.capital.R;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.CardsAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

import static com.sgb.capital.callback.Constants.MODIFY_THE_PHONE_NUMBER;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:银行卡详情
 */
public class CardsPop extends BasePopupWindow {


    public List<Bean> mDatas;
    public BankBean mBean;
    private CardsAdapter mAdapter;

    public CardsPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.cards_pop);
        RecyclerView rv = v.findViewById(R.id.rv);
        v.findViewById(R.id.tv_btn).setOnClickListener(v1 -> dismiss());
        rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mDatas = new ArrayList<>();
        mAdapter = new CardsAdapter(mDatas);

        mAdapter.setOnItemChildClickListener((adapter1, view, position) -> {
            Utils.sendMsg(MODIFY_THE_PHONE_NUMBER, mBean);
        });
        rv.setAdapter(mAdapter);
        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }


    public void show() {
        mDatas.clear();
        mDatas.add(new Bean("账户类型-" + (mBean.bindBank? "个人账户" : (mBean.userType == 3 ? "对公账户" : mBean.userType == 1 ? "个人账户" : "个体工商户"))) );
        mDatas.add(new Bean("银行账户名称-" + (mBean.bindBank ? mBean.accountName : mBean.bankAccountName)));
        String bankCard = CryptoUtils.decryptAES_ECB(mBean.bankCard);
        mDatas.add(new Bean("银行卡号码-" + (mBean.bindBank ?
                bankCard.substring(0, 4) + "**********" + bankCard.substring(bankCard.length() - 4) :
                mBean.bankAccount.substring(0, 4) + "**********" + mBean.bankAccount.substring(mBean.bankAccount.length() - 4)
        )));
        mDatas.add(new Bean("银行名称-" + mBean.bankName));
        if (!mBean.bindBank) {
            if (!Utils.isEmpty(mBean.bankBranchName)) {
                mDatas.add(new Bean("银行编码-" + mBean.gateId));
                mDatas.add(new Bean("开户行-" + mBean.bankBranchName));
                mDatas.add(new Bean("银行预留手机号-" + mBean.mobileId + "-show"));
            } else {
                mDatas.add(new Bean("银行预留手机号-" + mBean.mobileId));
            }
        } else {
            String phone = CryptoUtils.decryptAES_ECB(mBean.phone);
            mDatas.add(new Bean("银行预留手机号-" + phone.substring(0, 3) + "****" + phone.substring(phone.length() - 4)));
        }
        mAdapter.setNewInstance(mDatas);
        mAdapter.notifyDataSetChanged();
        showPopupWindow();
    }
}