package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.utils.KeyboardUtils;
import com.sgb.capital.utils.PayTimerUtils;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.LastInputEditText;
import com.sgb.capital.view.widget.VerificationInputView;

import java.util.List;

import razerdp.basepopup.BasePopupWindow;

/**
 * 作者:张磊
 * 日期:2021/6/28 0028
 * 说明:余额充值
 */
public class NewPayPop extends BasePopupWindow {

    public View mPayLayout;
    public TextView mPayPriceTv;
    public TextView mPhoneNumber;

    public TextView mTip;
    public TextView mPayButton;
    public View mPaySuccessLayout;
    public TextView mBackOrder;
    public OnPopClickListener mClickListener;
    public boolean isCreate = true;
    private VerificationInputView mVerificationInputView;
    private int mDoneNum;
    private String mCode;
    private TextView mNewGet;
    public PayTimerUtils mPayTimerUtils;
    public TextView mTvTitle;
    public TextView mTvOk;
    public TextView mTvTip;
    public boolean mIsTimeOut;

    public NewPayPop(Context context, OnPopClickListener clickListener) {
        super(context);
        mClickListener = clickListener;
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.newpay_pop);
        mPayLayout = v.findViewById(R.id.pay_layout);
        mPayPriceTv = v.findViewById(R.id.pay_price_tv);
        mPhoneNumber = v.findViewById(R.id.phone_number);
        mTvTitle = v.findViewById(R.id.tv_title);
        mTvTip = v.findViewById(R.id.tv_tip);
        mTvOk = v.findViewById(R.id.tv_ok);
        mVerificationInputView = v.findViewById(R.id.verification_input_view);
        mTip = v.findViewById(R.id.tip);
        mPayButton = v.findViewById(R.id.pay_button);
        mPaySuccessLayout = v.findViewById(R.id.pay_success_layout);
        mBackOrder = v.findViewById(R.id.back_order);
        mNewGet = v.findViewById(R.id.new_get);
        v.findViewById(R.id.close).setOnClickListener(v1 -> dismiss());
        mNewGet.setOnClickListener(v1 -> mClickListener.onClickNewGet());
        initListener();
        return v;
    }

    private void initListener() {
        mVerificationInputView.setListener(new VerificationInputView.Listener() {
            @Override
            public void onChange(String[] strings, List<LastInputEditText> editTextList) {
                mDoneNum = 0;
                for (int m = 0; m < strings.length; m++) {
                    if (!strings[m].isEmpty()) {
                        mDoneNum++;
                    }
                }
                if (mDoneNum == 6) {
                    mPayPriceTv.setFocusable(true);
                    mPayPriceTv.requestFocus();
                    mPayPriceTv.setFocusableInTouchMode(true);
                    KeyboardUtils.hideSoftInput(mPayPriceTv);
                    mTip.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onComplete(String string) {
                mCode = string;
            }
        });

        mPayButton.setOnClickListener(v -> {
         /*   if (mIsTimeOut){
                Utils.sendMsg(Constants.TIMEOUT, null);
                return;
            }*/
            if (Utils.isEmpty(mCode)) {
                mTip.setText("请输入验证码");
                mTip.setVisibility(View.VISIBLE);
                return;
            }
            mTip.setVisibility(View.INVISIBLE);
            mPayButton.setEnabled(false);
            Utils.postDelay(() -> mPayButton.setEnabled(true), 1000);
            mClickListener.onClickPayButton(mCode);
        });
    }


    public void show() {
        mTip.setVisibility(View.INVISIBLE);
        mVerificationInputView.clear();
        mCode = null;
        mPayLayout.setVisibility(View.VISIBLE);
        mPaySuccessLayout.setVisibility(View.GONE);
        if (mPayTimerUtils != null) {
            mPayTimerUtils.onFinish();
        }
        setPopupGravity(GravityMode.RELATIVE_TO_ANCHOR, Gravity.BOTTOM);
        showPopupWindow();
    }

    public void show(View view) {
        mTip.setVisibility(View.INVISIBLE);
        mVerificationInputView.clear();
        mCode = null;
        mPayLayout.setVisibility(View.VISIBLE);
        mPaySuccessLayout.setVisibility(View.GONE);
        if (mPayTimerUtils != null) {
            mPayTimerUtils.onFinish();
        }
        setPopupGravity(GravityMode.RELATIVE_TO_ANCHOR, Gravity.TOP);
        showPopupWindow(view);
    }

    public void setTipText(String s) {
        // TODO: 2021/9/28 0028
        mTip.setVisibility(View.VISIBLE);
        mTip.setTextColor(Utils.getColor(R.color.color_FF2928));
        mTip.setText(s);
    }

    public interface OnPopClickListener {
        void onClickNewGet();

        void onClickPayButton(String vCode);
    }

    public void setStartTimer() {
        if (mPayTimerUtils != null) {
            mPayTimerUtils.cancel();
        }
        mPayTimerUtils = new PayTimerUtils(mNewGet, 60000, 1000); //倒计时1分钟
        mPayTimerUtils.start();
    }


}