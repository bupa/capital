package com.sgb.capital.view.ui.adapter;


import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:个人银行卡
 */
public class PersonBankCardsItemAdapter extends BaseQuickAdapter<BankBean, BaseViewHolder> {


    public PersonBankCardsItemAdapter(List<BankBean> beans) {
        super(R.layout.personbankcardsitem_adapter, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final BankBean item) {
        // TODO: 2021/6/16 0016 解密

        try{
            String bankCard = CryptoUtils.decryptAES_ECB(item.bankCard);
            helper
                    .setBackgroundResource(R.id.fl_bg, item.bankName.contains("中国银行") || item.bankName.contains("工商银行") ? R.mipmap.gs_bg : item.bankName.contains("建设银行") ?
                            R.mipmap.jh_bg : item.bankName.contains("农业银行") || item.bankName.contains("邮政") ? R.mipmap.lh_bg : R.mipmap.jh_bg)
                    .setText(R.id.tv_openingBank, item.bankName)
                    .setText(R.id.tv_type, "个人")
                    .setText(R.id.tv_accountName, item.accountName)
                    .setText(R.id.tv_bankCard, bankCard.substring(0, 4) + " **** "
                            + "**** " + bankCard.substring(bankCard.length() - 4))
                    .setVisible(R.id.ll_pay, item.payBank && !item.bindBank)
                    .setVisible(R.id.tv_tx, item.bindBank);
            helper.getView(R.id.tv_zf).setVisibility(item.payBank? View.VISIBLE:View.GONE);
            helper.getView(R.id.tv_btn_unbind).setOnClickListener(v -> Utils.sendMsg(Constants.EVENT_UNBIND, item));
            helper.getView(R.id.tv_btn_alter).setOnClickListener(v -> Utils.sendMsg(Constants.EVENT_ALTER, item));
        }catch (Exception e){

        }


    }
}