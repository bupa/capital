package com.sgb.capital.view.ui.adapter;

import android.view.View;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.model.Bean;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

public class AsiaRecordDetailsAdapter extends BaseQuickAdapter<Bean, BaseViewHolder> {
    public AsiaRecordDetailsAdapter(List<Bean> data) {
        super(R.layout.asiarecorddetails_adapter, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Bean item) {
        helper
                .setText(R.id.tv_type, item.no)
                .setText(R.id.tv_value, item.no.contains("原因") ? "" : item.name);

        TextView tv_show = helper.getView(R.id.tv_show);
        View view = helper.getView(R.id.view);
        if (item.no.contains("原因")) {
            tv_show.setText(item.name);
            tv_show.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
        } else {
            tv_show.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
        }
    }
}