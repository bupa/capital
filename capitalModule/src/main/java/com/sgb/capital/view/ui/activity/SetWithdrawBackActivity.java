package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;

import com.google.gson.Gson;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.databinding.SetwithdrawbackActivityBinding;
import com.sgb.capital.model.BankBean;
import com.sgb.capital.model.PhoneBean;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.BankModel;

import java.util.HashMap;
import java.util.Map;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import razerdp.basepopup.BasePopupWindow;


/**
 * 作者:张磊
 * 日期:2021/2/20 0020
 * 说明:设置为提现银行卡
 */

public class SetWithdrawBackActivity extends BaseActivity {
    private SetwithdrawbackActivityBinding mBinding;
    private BankModel mModel;
    private TipPop mTipPop;
    private CountDownTimer mTimer;
    private BankBean mBankBean;

    public static void start(Context context, String json) {
        Intent intent = new Intent(context, SetWithdrawBackActivity.class);
        intent.putExtra("data", json);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    public void initData() {
        Utils.setStatusBarLightMode(this, true);
        String data = getIntent().getStringExtra("data");
        mBankBean = new Gson().fromJson(data, BankBean.class);
        mBinding.tvBankAccountName.setText(mBankBean.accountName);
        mBinding.tvIdentityId.setText(mBankBean.idCard.substring(0, 4) + "**********" + mBankBean.idCard.substring(mBankBean.idCard.length() - 4));
        mBinding.tvBankName.setText(mBankBean.bankName);
        mBinding.tvBackNumber.setText(mBankBean.bankCard);
        mBinding.tvPhone.setText(mBankBean.phone);
        mModel = new ViewModelProvider(this).get(BankModel.class);
    }

    @Override
    public void initObserve() {
        mModel.mIsWithdraw.observe(this, s -> {
            if (s.contains("ok")) {
                mTipPop.show();
            } else if (s.contains("6")) {
                // 获取验证码
                mTimer = new CountDownTimer(60000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        mBinding.tvGetCode.setEnabled(false);
                        mBinding.tvGetCode.setText("重新获取" + "(" + (millisUntilFinished / 1000) + ")");
                    }

                    public void onFinish() {
                        mBinding.tvGetCode.setEnabled(true);
                        mBinding.tvGetCode.setText("获取验证码");
                    }
                };
                mTimer.start();
            }
        });
        mModel.mBank.observe(this, bankEntities -> {
            if (bankEntities != null && bankEntities.size() != 0) {
                bankEntities.get(0).isSelect = true;
            }
        });
    }

    @Override
    protected void initView() {
          CryptoUtils.decryptAES_ECB("");
        Utils.setStatusBarLightMode(this, true);
        mBinding = DataBindingUtil.setContentView(this, R.layout.setwithdrawback_activity);
        mTipPop = new TipPop(this, "", "恭喜您,绑定提现银行卡成功", null, "知道了");
    }

    public void initListener() {
        mBinding.tvCancel.setOnClickListener(v -> finish());
        mBinding.tvSave.setOnClickListener(v -> {
            if (Utils.isEmpty(mBinding.etCode.getText().toString())) {
                MToast.showToast(Utils.getContext(), mBinding.etCode.getHint().toString());
                return;
            }
            Map map = new HashMap();
            map.put("cardId", mBankBean.bankCard);
            map.put("verifyCode", mBinding.etCode.getText().toString());
            mModel.confirmPersonalBank(SetWithdrawBackActivity.this, map);
        });
        mBinding.tvGetCode.setOnClickListener(v -> {
            PhoneBean phoneBean = new PhoneBean();
            phoneBean.bankAccountName = mBankBean.accountName;
            phoneBean.cardId = mBankBean.bankCard;
            phoneBean.mobileId = mBankBean.phone;
            phoneBean.userType = "1";
            mModel.orderPersonalBank(this, phoneBean);
        });

        mTipPop.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                finish();
                Utils.sendMsg(Constants.EVENT_HIDE, null);
            }
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.onFinish();
        }
    }
}
