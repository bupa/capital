package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import com.sgb.capital.R;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:绑定提示弹框
 */
public class CardsTipPop extends BasePopupWindow {

    public CardsTipPop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.cardstip_pop);
        TextView tv_sure = v.findViewById(R.id.tv_sure);
        tv_sure.setOnClickListener(v1 -> {
            dismiss();
        });
        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }


    public void show() {
        showPopupWindow();
    }
}