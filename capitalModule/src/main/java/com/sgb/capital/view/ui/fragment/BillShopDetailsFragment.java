package com.sgb.capital.view.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.TobepaidbillshopdetailsFragmentBinding;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.Cards2Pop;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.ui.activity.BillOrderDetailsActivity;
import com.sgb.capital.view.ui.activity.OrderErrorActivity;
import com.sgb.capital.view.ui.adapter.OrderShopAdapter;
import com.sgb.capital.viewmodel.DetailsModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:商品信息
 */
public class BillShopDetailsFragment extends AppLazyFragment {

    private TobepaidbillshopdetailsFragmentBinding mBinding;
    private DetailsModel mModel;
    private OrderShopAdapter mAdapter;
    private int mType;
    private TipPop mTipPop;
    private BillOrderDetailsActivity mActivity;
    private Cards2Pop mCards2Pop;
    private Double mCode;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.tobepaidbillshopdetails_fragment, container, false);
        return mBinding.getRoot();
    }

    @Override
    protected void initView() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mBinding.rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mTipPop = new TipPop(getActivity());
        mModel = new ViewModelProvider(this).get(DetailsModel.class);
        mAdapter = new OrderShopAdapter(getActivity(), null);
        mBinding.rv.setAdapter(mAdapter);
        mCards2Pop = new Cards2Pop(getActivity());
    }


    @Override
    public void initListener() {
        mTipPop.mTvSure.setOnClickListener(v -> {
            mTipPop.dismiss();
            Map map = new HashMap();
            map.put("operName", mActivity.mRealName);
            map.put("orderId", mActivity.mPayDetailBean.orderId);
            mModel.finish(mActivity, map);
        });
        mTipPop.mTvCancel.setOnClickListener(v -> {
            mTipPop.dismiss();
            if (mCode == null) return;
            if (mCode == 6) {
                startActivity(new Intent(mActivity, OrderErrorActivity.class));
            }
        });
        mBinding.tvBtn.setOnClickListener(v -> {
            if (mActivity.mType == 1) {
                mModel.getOrderPayStatus(mActivity, mActivity.mPayDetailBean.orderId);
            }else{
                mModel.getPayStatus(mActivity,mActivity.mPayDetailBean.orderId);
            }
        });
    }

    @Override
    protected void onLazyLoad() {
        mActivity = (BillOrderDetailsActivity) getActivity();
        mType = mActivity.mType;
        mBinding.tvBtn.setVisibility(mType == 2 ? View.VISIBLE : View.GONE);
        if (mActivity.mPayDetailBean.payState == 2) {
            mBinding.tvBtn.setVisibility(View.GONE);
        }
        mBinding.tvBtn.setText(mType == 1 ? "余额支付" : "资金解冻");
        mModel.commodities(mActivity, mActivity.mPayDetailBean.paymentId);
    }

    @Override
    public void initObserve() {
        mModel.mCode.observe(this, s -> {
            mCode = s;
            if (mActivity.mType == 1) {
                if (s == 6) {
                    mTipPop.initStr("温馨提示", " 订单异常,无法付款", null, "知道了");
                    mTipPop.show();
                } else if (s < 4) {
                    mCards2Pop.mBean = mActivity.mPayDetailBean;
                    mCards2Pop.show();
                } else {
                    mTipPop.initStr("温馨提示", " 订单状态已发生变化,请刷新页面后再进行操作", null, "知道了");
                    mTipPop.show();
                }
            } else {
                if (s == 7) {
                    mBinding.tvBtn.setVisibility(View.GONE);
                    Utils.sendMsg(Constants.EVENT_REFRESH_ORDER, null);
                }else if (s == 1) {
                    mTipPop.initStr("是否确认解冻该订单资金？", "解冻后平台将解除订单资金担保，资金进入对方账户", "确定", "取消");
                    mTipPop.show();
                } else if (mActivity.mPayDetailBean.payStatus != s) {
                    mTipPop.initStr("温馨提示", " 订单状态已发生变化，请刷新页面后再进行操作", null, "知道了");
                    mTipPop.show();
                }
            }
        });
        mModel.mDatas.observe(this, commoditiesBeans -> {
            if (commoditiesBeans == null || commoditiesBeans.size() == 0) {
                mBinding.errorView.setVisibility(View.VISIBLE);
                mBinding.tvBtn.setVisibility(View.GONE);
            } else {
                mAdapter.setDatas(commoditiesBeans);
            }
        });
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_REFRESH_ORDER:
                mBinding.tvBtn.setVisibility(View.GONE);
                break;
        }
    }
}