package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.text.method.NumberKeyListener;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.AddcompanybackActivityBinding;
import com.sgb.capital.model.AddAndEditCardsBean;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.utils.CryptoUtils;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.CardsListPop;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.BankModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import razerdp.basepopup.BasePopupWindow;

import static android.view.View.VISIBLE;


/**
 * 作者:张磊
 * 日期:2021/2/20 0020
 * 说明:企业添加银行卡
 */

public class AddCompanyBackActivity extends BaseActivity {
    private AddcompanybackActivityBinding mBinding;
    private BankModel mModel;
    private CardsListPop mListPop;
    private TipPop mTipPop;
    AddAndEditCardsBean mAndEditCardsBean = new AddAndEditCardsBean();


    public static void start(Context context) {
        Intent intent = new Intent(context, AddCompanyBackActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    public void initData() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Utils.setStatusBarLightMode(this, true);
        mListPop = new CardsListPop(this);
        mTipPop = new TipPop(this, "温馨提示", "恭喜您,绑定支付银行卡成功", null, "知道了");
        mModel = new ViewModelProvider(this).get(BankModel.class);
    }

    @Override
    public void initObserve() {
        mModel.mBank.observe(this, bankEntities -> {
            if (mBinding.tvBankName.getText().toString().contains("请选择银行")) {
                if (bankEntities != null && bankEntities.size() != 0) {
                    mListPop.mLastIndex = 0;
                    bankEntities.get(0).isSelect = true;
                    mListPop.mDatas = bankEntities;
                }
            } else {
                mListPop.mDatas = bankEntities;
                for (int i = 0; i < bankEntities.size(); i++) {
                    if (bankEntities.get(i).name.contains(mBinding.tvBankName.getText().toString())) {
                        mListPop.mLastIndex = i;
                        bankEntities.get(i).isSelect = true;
                        break;
                    }
                }
            }
            mListPop.show();
        });
        mModel.mBaseEntity.observe(this, baseEntity -> mTipPop.show());
    }

    @Override
    protected void initView() {
        Utils.setStatusBarLightMode(this, true);
        mBinding = DataBindingUtil.setContentView(this, R.layout.addcompanyback_activity);
    }

    public void initListener() {
        mBinding.llBankCard.setOnClickListener(v -> {
            Utils.hide(this);
            if (mAndEditCardsBean.type.contains("0")) {
                mModel.getQpBank(this);
            } else {
                mModel.getPublicBank(this);
            }
        });
        mBinding.tvType0.setOnClickListener(v -> {
            if (mAndEditCardsBean.type.contains("0")) {
                return;
            }
            mBinding.llCard.setVisibility(View.VISIBLE);
            mAndEditCardsBean.type = "0";
            mBinding.tvType0.setBackgroundResource(R.drawable.corner_true);
            mBinding.tvType0.setTextColor(Utils.getColor(R.color.white));
            mBinding.tvType1.setBackgroundResource(R.drawable.corner_false);
            mBinding.tvType1.setTextColor(Utils.getColor(R.color.color_333333));
        });
        mBinding.tvType1.setOnClickListener(v -> {
            if (mAndEditCardsBean.type.contains("1")) {
                return;
            }
            mBinding.llCard.setVisibility(View.GONE);
            mBinding.etCard.setText("");
            mAndEditCardsBean.type = "1";
            mBinding.tvType0.setBackgroundResource(R.drawable.corner_false);
            mBinding.tvType0.setTextColor(Utils.getColor(R.color.color_333333));
            mBinding.tvType1.setBackgroundResource(R.drawable.corner_true);
            mBinding.tvType1.setTextColor(Utils.getColor(R.color.white));
        });

        mTipPop.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                finish();
                Utils.sendMsg(Constants.EVENT_REFRESH, null);
            }
        });
        mBinding.tvSave.setOnClickListener(v -> {
            if (Utils.isEmpty(mBinding.etName.getText().toString())) {
                MToast.showToast(Utils.getContext(), "请输入账户名称");
                return;
            } else if (mBinding.llCard.getVisibility() == VISIBLE && Utils.isEmpty(mBinding.etCard.getText().toString())) {
                MToast.showToast(Utils.getContext(), "请输入银行卡账户身份证号");
                return;
            } else if (mBinding.llCard.getVisibility() == VISIBLE && !Utils.idCardValidate(mBinding.etCard.getText().toString())) {
                MToast.showToast(Utils.getContext(), "身份证号格式错误");
                return;
            } else if (mBinding.tvBankName.getText().toString().contains("请选择银行")) {
                MToast.showToast(Utils.getContext(), "请选择开户银行");
                return;
            } else if (Utils.isEmpty(mBinding.etBackNumber.getText().toString())) {
                MToast.showToast(Utils.getContext(), "请输入银行卡号");
                return;
            } else if (mBinding.etBackNumber.getText().toString().length()<8) {
                MToast.showToast(this, "银行卡号格式错误");
                return;
            } else if (Utils.isEmpty(mBinding.etPhone.getText().toString())) {
                MToast.showToast(Utils.getContext(), "请输入银行预留手机号");
                return;
            } else if (!Utils.isChinaPhoneLegal(mBinding.etPhone.getText().toString().trim())) {
                MToast.showToast(this, "手机号格式错误");
                return;
            }
            // TODO: 2021/6/16 0016 加密
            mAndEditCardsBean.accountName = mBinding.etName.getText().toString();
            mAndEditCardsBean.idCard = CryptoUtils.encryptAES_ECB(mBinding.etCard.getText().toString());
            mAndEditCardsBean.openingBank = mBinding.tvBankName.getText().toString();
            mAndEditCardsBean.bankCard = CryptoUtils.encryptAES_ECB(mBinding.etBackNumber.getText().toString());
            mAndEditCardsBean.phone = CryptoUtils.encryptAES_ECB(mBinding.etPhone.getText().toString());
            mModel.save(this, mAndEditCardsBean);
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.etCard.setKeyListener(new NumberKeyListener() {
            @Override
            public int getInputType() {
                return android.text.InputType.TYPE_CLASS_PHONE;
            }

            @Override
            protected char[] getAcceptedChars() {
                return new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'x'};
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_SELECT:
                BankEntity data = (BankEntity) event.data;
                mAndEditCardsBean.gateId = data.code;
                mBinding.tvBankName.setText(data.name);
                mBinding.tvBankName.setTextColor(Utils.getColor(R.color.color_000000));
                break;
        }
    }
}
