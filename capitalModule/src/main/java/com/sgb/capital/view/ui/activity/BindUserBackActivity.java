package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;

import com.google.gson.Gson;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.databinding.BinduserbackActivityBinding;
import com.sgb.capital.model.BusinessInfoByCompanyEntity;
import com.sgb.capital.model.PhoneBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.CardsTipPop;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.view.widget.MToast;
import com.sgb.capital.viewmodel.BankModel;

import java.util.HashMap;
import java.util.Map;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import razerdp.basepopup.BasePopupWindow;

/**
 * 作者:张磊
 * 日期:2021/5/31 0031
 * 说明:绑定企业个人账户提现银行卡
 */
public class BindUserBackActivity extends BaseActivity {

    private BinduserbackActivityBinding mBinding;
    private CardsTipPop mPop;
    BankModel mModel;
    private CountDownTimer mTimer;
    private BusinessInfoByCompanyEntity mBusinessInfoByCompanyEntity;
    private TipPop mTipPop;
    private long mIndex;
    private TipPop mTipPopCode;

    public static void start(Context context, String data) {
        Intent intent = new Intent(context, BindUserBackActivity.class);
        intent.putExtra("data", data);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.binduserback_activity);
        mPop = new CardsTipPop(this);
        mModel = new ViewModelProvider(this).get(BankModel.class);
        mTipPop = new TipPop(this, "", "恭喜您,绑定提现银行卡成功", null, "知道了");
        mTipPopCode = new TipPop(this, "温馨提示", "验证码错误次数超过3次", null, "知道了");
    }


    @Override
    public void initData() {
        String data = getIntent().getStringExtra("data");
        mBusinessInfoByCompanyEntity = new Gson().fromJson(data, BusinessInfoByCompanyEntity.class);
        mBinding.itemTvAccountName.setHint(mBusinessInfoByCompanyEntity.legalName);
        mBinding.itemTvIdNumber.setHint(mBusinessInfoByCompanyEntity.identityId.substring(0, 6) + "******" + mBusinessInfoByCompanyEntity.identityId.substring(mBusinessInfoByCompanyEntity.identityId.length() - 4));
    }

    @Override
    public void initObserve() {

        mModel.mCode.observe(this, s -> {
            if (s.contains("1")) {
                mTipPop.show();
            } else if (s.contains("0")) {
                mIndex++;
                if (mIndex > 3) {
                    mTipPopCode.show();
                } else {
                    MToast.showToast(this, Utils.getString(R.string.capital_code_error));
                }
            }
        });

        mModel.mIsWithdraw.observe(this, s -> {
            if (s.contains("6")) {
                // 获取验证码
                mTimer = new CountDownTimer(60000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        mBinding.tvGetCode.setEnabled(false);
                        mBinding.tvGetCode.setText("重新获取" + "(" + (millisUntilFinished / 1000) + ")");
                    }

                    public void onFinish() {
                        mBinding.tvGetCode.setEnabled(true);
                        mBinding.tvGetCode.setText(Utils.getString(R.string.capital_get_code));
                    }
                };
                mTimer.start();
            }
        });
    }

    @Override
    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
        mTipPop.setOnDismissListener(new BasePopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                finish();
                Utils.sendMsg(Constants.EVENT_REFRESH, null);
            }
        });
        mBinding.tvGetCode.setOnClickListener(v -> {
            if (Utils.isEmpty(mBinding.itemTvCarNum.getText().toString())) {
                MToast.showToast(Utils.getContext(), Utils.getString(R.string.capital_input_bank));
                return;
            }
            if (Utils.isEmpty(mBinding.itemTvCarPhone.getContent())) {
                MToast.showToast(Utils.getContext(), "请输入银行卡预留手机号");
                return;
            } else if (!Utils.isChinaPhoneLegal(mBinding.itemTvCarPhone.getContent())) {
                MToast.showToast(this, Utils.getString(R.string.capital_phone_error));
                return;
            } else if (mBinding.itemTvCarNum.getText().toString().length() < 8) {
                MToast.showToast(this, Utils.getString(R.string.capital_bank_error));
                return;
            }

            PhoneBean phoneBean = new PhoneBean();
            phoneBean.bankAccountName = mBusinessInfoByCompanyEntity.subMerName;
            phoneBean.cardId = mBinding.itemTvCarNum.getText().toString();
            phoneBean.mobileId = mBinding.itemTvCarPhone.getContent();
            phoneBean.userType = "2";
            mModel.orderPersonalBank(this, phoneBean);
        });
        mBinding.ivInfo.setOnClickListener(v -> mPop.show());
        mBinding.okView.setOnClickListener(v -> {
            if (Utils.isEmpty(mBinding.itemTvCarNum.getText().toString())) {
                MToast.showToast(Utils.getContext(), Utils.getString(R.string.capital_input_bank));
                return;
            }
            if (Utils.isEmpty(mBinding.itemTvCarPhone.getContent())) {
                MToast.showToast(Utils.getContext(), Utils.getString(R.string.capital_input_phone));
                return;
            } else if (!Utils.isChinaPhoneLegal(mBinding.itemTvCarPhone.getContent())) {
                MToast.showToast(this, Utils.getString(R.string.capital_phone_error));
                return;
            } else if (Utils.isEmpty(mBinding.itemTvCarNum.getText().toString())) {
                MToast.showToast(Utils.getContext(), Utils.getString(R.string.capital_input_bank));
                return;
            } else if (mBinding.itemTvCarNum.getText().toString().length() < 8) {
                MToast.showToast(this, Utils.getString(R.string.capital_bank_error));
                return;
            } else if (Utils.isEmpty(mBinding.etCode.getText().toString())) {
                MToast.showToast(Utils.getContext(), mBinding.etCode.getHint().toString());
                return;
            }
            Map map = new HashMap();
            map.put("cardId", mBinding.itemTvCarNum.getText().toString());
            map.put("verifyCode", mBinding.etCode.getText().toString());
            mModel.confirmPersonalBank(this, map);
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.onFinish();
        }
    }
}