package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.sgb.capital.R;
import com.sgb.capital.base.BasePopWindow;
import com.sgb.capital.model.BindBankCardListEntity;
import com.sgb.capital.utils.AdapterOnItemClick;
import com.sgb.capital.view.ui.adapter.SelectBankCardAdapter;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 资金中心提现
 * 银行卡popwindow
 */
public class SelectBankCardPopWindow extends BasePopWindow {
    private RecyclerView recyclerView;
    private TextView okView;
    private TextView cancelView;
    private SelectBankCardAdapter bankCardAdapter;
    private List<BindBankCardListEntity> bankList;
    public BindBankCardListEntity mBankCardListEntity;
    private String id = "";

    public SelectBankCardPopWindow(Context context) {
        super(context, DIR_DOWN_UP);
    }

    @Override
    protected int popLayout() {
        return R.layout.popwindow_bank_no_z;
    }

    @Override
    protected void initView(PopViewHolder holder) {
        recyclerView = holder.getPop().findViewById(R.id.rlv_list);
        cancelView = holder.getPop().findViewById(R.id.tv_cancel);
        okView = holder.getPop().findViewById(R.id.tv_ok);

        cancelView.setOnClickListener(v -> {
            mBankCardListEntity = null;
            if (onResultClick != null)
                onResultClick.result(mBankCardListEntity, null, null);
            dismiss();
        });
        bankCardAdapter = new SelectBankCardAdapter(mContext, null, R.layout.item_select_bank_card);
        bankCardAdapter.setItemSelect(true);
        bankCardAdapter.setDefItem(-1);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(bankCardAdapter);
        bankCardAdapter.setOnItemClick(new AdapterOnItemClick<BindBankCardListEntity>() {
            @Override
            public void onItemClick(BindBankCardListEntity s, int position) {
                mBankCardListEntity = s;
                if (onResultClick != null)
                    onResultClick.result(mBankCardListEntity, null, null);
                dismiss();
            }
        });
    }

    public void setData(List<BindBankCardListEntity> bankList) {
        this.bankList = bankList;
        bankCardAdapter.setDatas(bankList);
    }


    public void setVisibleList(boolean visible) {
        recyclerView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    public void setDefData(int position) {
        bankCardAdapter.setDefItem(position);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (onDisMiss != null)
            onDisMiss.disMiss();
    }
}
