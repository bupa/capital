package com.sgb.capital.view.ui.adapter;

import android.content.Context;

import com.sgb.capital.BR;
import com.sgb.capital.R;
import com.sgb.capital.base.BaseRecycleAdapter;
import com.sgb.capital.model.BankEntity;
import com.sgb.capital.utils.ItemViewHolder;

import java.util.List;

public class BankAdapter extends BaseRecycleAdapter<BankEntity> {
    public BankAdapter(Context context, List<BankEntity> datas, int tag) {
        super(context, tag, datas);

    }

    @Override
    public void convert(ItemViewHolder holder, final BankEntity entity) {
        holder.setBinding(BR.viewModel, entity);
        holder.setText(R.id.tv_name, entity.name);
    }
}
