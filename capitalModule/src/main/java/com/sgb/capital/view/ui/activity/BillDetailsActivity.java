package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.CapitalBilldetailsActivityBinding;

import androidx.databinding.DataBindingUtil;

/**
 * 作者:张磊
 * 日期:2021/9/1 0001
 * 说明:应收/已收账单详情
 */
public class BillDetailsActivity extends BaseActivity {


    private CapitalBilldetailsActivityBinding mBinding;

    public static void start(Context context, int type) {
        Intent intent = new Intent(context, BillDetailsActivity.class);
        intent.putExtra("type", type);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


  @Override
  protected void initView() {
      mBinding = DataBindingUtil.setContentView(this, R.layout.capital_billdetails_activity);
      initData();
      initListener();
  }

  public void initData() {
      int type = getIntent().getIntExtra("type", 0);
      mBinding.tvTitle.setText(type==0?"应收账单详情":"已收账单详情");
  }

  public void initListener() {
    mBinding.ivBack.setOnClickListener(v -> finish());
  }
}