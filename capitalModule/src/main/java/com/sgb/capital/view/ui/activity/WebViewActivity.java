package com.sgb.capital.view.ui.activity;

import android.net.http.SslError;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.databinding.DataBindingUtil;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.ActivityWebViewBinding;
import com.sgb.capital.utils.Utils;

public class WebViewActivity extends BaseActivity {

    private ActivityWebViewBinding mBinding;

    @Override
    protected void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);
        Utils.setStatusBarLightMode(this, true);
        mBinding.ivBack.setOnClickListener(view -> finish());
        String url = getIntent().getStringExtra("url");
        if (!TextUtils.isEmpty(url)){
            showWebView(url);
        }
        mBinding.webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                view.getSettings().setJavaScriptEnabled(true);
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError
                    error) {
                super.onReceivedError(view, request, error);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Log.i("详情页加载异常-----", request.getUrl() + "");
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Log.i("详情页加载异常-----", error.getErrorCode() + "");
                }
            }

            // 这行代码一定加上否则效果不会出现

        });
    }

    private void showWebView(String link) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WebSettings webSettings = mBinding.webView.getSettings();

                if (Build.VERSION.SDK_INT >= 19) {
                    webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                }
                webSettings.setJavaScriptEnabled(true);
                webSettings.setDomStorageEnabled(true);
                webSettings.setAppCacheEnabled(true);
                mBinding.webView.loadUrl(link);
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}


