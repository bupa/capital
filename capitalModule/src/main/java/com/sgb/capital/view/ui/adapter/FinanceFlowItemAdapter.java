package com.sgb.capital.view.ui.adapter;

import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.model.Bean;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/5 0008
 * 说明:财务流水
 */
public class FinanceFlowItemAdapter extends BaseQuickAdapter<Bean, BaseViewHolder> {


    public FinanceFlowItemAdapter(List<Bean> beans) {
        super(R.layout.financeflowitem2adapter, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final Bean item) {
        helper.setText(R.id.tv_name, item.name);
        helper.getView(R.id.ll_select).setVisibility(!item.select ? View.VISIBLE : View.INVISIBLE);
    }
}