package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.MyZTextWatcher;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.PayActivityBinding;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.TipPop;
import com.sgb.capital.viewmodel.CheckoutCounterModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

/**
 * 作者:张磊
 * 日期:2021/6/28 0028
 * 说明:余额充值
 */
public class PayActivity extends BaseActivity {

    private PayActivityBinding mBinding;
    private CheckoutCounterModel mModel;
    private HashMap mMap;
    private String mMoney;
    private TipPop mPop;

    public static void start(Context context, boolean isMain, boolean isUser) {
        Intent intent = new Intent(context, PayActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("isMain", isMain);
        intent.putExtra("isUser", isUser);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.pay_activity);
        mPop = new TipPop(this);
        mMap = new HashMap();
    }


    @Override
    public void initObserve() {
        mModel.mCode.observe(this, s -> {
            PayCounterActivity.start(this, mMoney, s, getIntent().getBooleanExtra("isMain", false), getIntent().getBooleanExtra("isUser", false));
            finish();
        });
    }

    @Override
    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.tvBtn.setOnClickListener(v -> {
            String s = mBinding.etMoney.getText().toString();
            if (mBinding.tvTip.getVisibility() == View.VISIBLE)
                return;
            if (s.length() == 0) {
                mPop.initStr("请填写充值金额后再进行提交", null, "知道了");
                mPop.show();
                return;
            }
            boolean contains = s.contains(".");
            if (contains) {
                String[] split = s.split("\\.");
                if (split.length == 0) {
                    mMoney = s.substring(0, s.length() - 1) + ".00";
                } else {
                    if (split[1].length() == 1) {
                        mMoney = split[0] + "." + (split[1] + "0");
                    } else {
                        mMoney = s;
                    }
                }
            } else {
                mMoney = s + ".00";
            }

            mMap.put("amount", mMoney);
            mModel.apply(this, mMap);
        });
    }

    @Override
    public void initData() {
        boolean isUser = getIntent().getBooleanExtra("isUser", false);
        mBinding.llShow.setVisibility(isUser ? View.VISIBLE : View.GONE);
        mModel = new ViewModelProvider(this).get(CheckoutCounterModel.class);
        mBinding.etMoney.addTextChangedListener(new MyZTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().contains(".")) {
                    if (s.length() > 11) {
                        mBinding.etMoney.setText(s.subSequence(0, 11));
                        mBinding.etMoney.setSelection(s.length() - 1);
                    }
                }

                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                        s = s.toString().subSequence(0,
                                s.toString().indexOf(".") + 3);
                        mBinding.etMoney.setText(s);
                        mBinding.etMoney.setSelection(s.length());
                    }
                }
                if (s.toString().trim().substring(0).equals(".")) {
                    s = "0" + s;
                    mBinding.etMoney.setText(s);
                    mBinding.etMoney.setSelection(2);
                }
                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        mBinding.etMoney.setText(s.subSequence(0, 1));
                        mBinding.etMoney.setSelection(1);
                        return;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int num;
                    String temp = s.toString();
                    if (temp.length() == 0) {
                        mBinding.tvTip.setVisibility(View.GONE);
                    }
                    num = Integer.parseInt(s.toString().contains(".") ? s.toString().split(".")[0] : s.toString());
                    mBinding.tvTip.setVisibility(num < 100 ? View.VISIBLE : View.GONE);
                } catch (Exception e) {

                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.sendMsg(Constants.EVENT_REFRESH, null);
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_HIDE:
                Utils.sendMsg(Constants.EVENT_REFRESH, null);
                finish();
                break;
        }
    }
}