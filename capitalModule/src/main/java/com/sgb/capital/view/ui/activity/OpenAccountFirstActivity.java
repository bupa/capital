package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.ActivityOpenAccountFirstBinding;
import com.sgb.capital.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.databinding.DataBindingUtil;


public class OpenAccountFirstActivity extends BaseActivity {


    private ActivityOpenAccountFirstBinding mBinding;

    private boolean isAgree = false; //默认不勾选


    public static void start(Context context, boolean isUser) {
        Intent intent = new Intent(context, OpenAccountFirstActivity.class);
        intent.putExtra("isUser", isUser);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


    @Override
    protected void initView() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Utils.setStatusBarLightMode(this, true);
        boolean isUser = getIntent().getBooleanExtra("isUser", false);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_open_account_first);
        if (isUser){
            mBinding.tvTitle.setText("开通我的钱包");
            mBinding.tvTip1.setText("我的钱包服务介绍");
            mBinding.tvTip2.setText("\t\t开通我的钱包后，您在经营帮平台的业务可以实现在线收款，本服务由联动优势支付提供，资金全程央行监管，保障您的每一笔交易资金安全。");
            mBinding.tvTip3.setText("\t\t认证时请如实填写您的个人信息，认证通过后即可实现在线收款");
            mBinding.llShow1.setVisibility(View.GONE);
            mBinding.llShow2.setVisibility(View.VISIBLE);
            mBinding.llShow3.setVisibility(View.GONE);
            mBinding.view.setVisibility(View.VISIBLE);
            mBinding.tvSureBtn.setText("立即开通");
        }
        mBinding.businessLicense.setOnClickListener(v ->
                startActivity(new Intent(OpenAccountFirstActivity.this, WebViewActivity.class).putExtra("url", Constants.PAY_LICENCE)));
        mBinding.agree.setOnClickListener(v -> {
            isAgree = !isAgree;
            if (isAgree) {
                mBinding.agree.setImageResource(R.mipmap.icon_check_select);
                mBinding.tvSureBtn.setBackgroundResource(R.drawable.button5);
                mBinding.tvSureBtn.setEnabled(true);
            } else {
                mBinding.agree.setImageResource(R.mipmap.icon_check_normal);
                mBinding.tvSureBtn.setBackgroundResource(R.drawable.bg_shap_color_change_2corner_50);
                mBinding.tvSureBtn.setEnabled(false);
            }
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.tvSureBtn.setOnClickListener(v -> {
            if (!isUser) {
                OpenAccountActivity.start(OpenAccountFirstActivity.this);
            } else {
                OpenUserActivity.start(OpenAccountFirstActivity.this);
            }
        });
    }

    @Override
    public void initData() {


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_HIDE:
                finish();
                break;
        }
    }
    public static void main(String args[]) {
        String h= null;
        "".equals(h);
    }
}