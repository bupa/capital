package com.sgb.capital.view.ui.adapter;

import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:列表
 */
public class CapitalListAdapter extends BaseQuickAdapter<Bean, BaseViewHolder> {


    public CapitalListAdapter(List<Bean> beans) {
        super(R.layout.capital_list_adapter, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final Bean item) {
        helper.setText(R.id.tv_name,item.name);
        helper.getView(R.id.tv_name).setOnClickListener(v -> Utils.sendMsg(Constants.EVENT_REFRESH, v));
    }
}