package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.DeallimitActivityBinding;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.DealLimitAdapter;
import com.sgb.capital.viewmodel.DealLimitModel;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/9/18 0018
 * 说明:交易限额
 */
public class DealLimitActivity extends BaseActivity {

    private DeallimitActivityBinding mBinding;
    private DealLimitModel mModel;
    private DealLimitAdapter mAdapter;

    public static void start(Context context) {
        Intent intent = new Intent(context, DealLimitActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


    @Override
    protected void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.deallimit_activity);
        mModel = new ViewModelProvider(this).get(DealLimitModel.class);
        mBinding.rv.setLayoutManager(new GridLayoutManager(Utils.getContext(), 1));
        mAdapter = new DealLimitAdapter(null);
        mBinding.rv.setAdapter(mAdapter);

    }

    public void initData() {
        mModel.bankTradeLimitList(this);
    }

    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void initObserve() {
        mModel.mDatas.observe(this, payDealBean -> {
            if (payDealBean == null || payDealBean.size() == 0) {
                mBinding.ivNull.setVisibility(View.VISIBLE);
                return;
            }
            mAdapter.setNewInstance(payDealBean);
        });
    }
} 