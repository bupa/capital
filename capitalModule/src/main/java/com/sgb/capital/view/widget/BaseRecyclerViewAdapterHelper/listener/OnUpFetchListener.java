package com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.listener;

/**
 * @author: limuyang
 * @date: 2019-12-03
 * @Description:
 */
public interface OnUpFetchListener {
    void onUpFetch();
}
