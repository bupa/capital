package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.databinding.NeworderpayActivityBinding;
import com.sgb.capital.view.ui.adapter.ViewPagerAdapter;
import com.sgb.capital.view.ui.fragment.OrderFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.core.util.Pair;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

/**
 * 作者:张磊
 * 日期:2021/4/7 0007
 * 说明:待付订单
 */
public class NewOrderPayActivity extends BaseActivity {

    private NeworderpayActivityBinding mBinding;
    public boolean mIsPayOrder; // 是否只需要展示未付款 订单

    public static void start(Context context, boolean isPayOrder) {
        Intent intent = new Intent(context, NewOrderPayActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("isPayOrder", isPayOrder);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.neworderpay_activity);
        mIsPayOrder = getIntent().getBooleanExtra("isPayOrder", false);
    }


    @Override
    public void initData() {
        if (mIsPayOrder) {
            initTab(new String[]{"待支付"});
            mBinding.tab.setVisibility(View.GONE);
            mBinding.rightTv.setVisibility(View.GONE);
        } else {
            initTab(new String[]{"待支付", "已支付"});
        }
    }


    @Override
    public void initListener() {
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.rightTv.setOnClickListener(v -> startActivity(new Intent(this, OrderErrorActivity.class)));
    }

    private void initTab(String[] title) {
        List<Pair<String, Fragment>> mList = new ArrayList<>();
        for (String s : title) {
            Bundle bundle = new Bundle();
            OrderFragment flowFragment = new OrderFragment();
            bundle.putString("data", s);
            flowFragment.setArguments(bundle);
            mList.add(new Pair<>(s, flowFragment));
        }
        mBinding.vpNeed.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), mList));
        mBinding.vpNeed.setOffscreenPageLimit(title.length);
        mBinding.tab.setupWithViewPager(mBinding.vpNeed);
    }
}
