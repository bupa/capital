package com.sgb.capital.view.ui.adapter;


import com.sgb.capital.R;
import com.sgb.capital.model.Bean;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.BaseQuickAdapter;
import com.sgb.capital.view.widget.BaseRecyclerViewAdapterHelper.viewholder.BaseViewHolder;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/5 0008
 * 说明:财务流水
 */
public class FinanceFlowListAdapter extends BaseQuickAdapter<Bean, BaseViewHolder> {
    public FinanceFlowListAdapter(List<Bean> beans) {
        super(R.layout.financeflowitem_adapter, beans);
    }

    @Override
    protected void convert(BaseViewHolder helper, final Bean item) {
        helper.setText(R.id.tv_name, item.name);
        helper.getView(R.id.img).setRotation(item.select ? 180 : 0);
    }
}