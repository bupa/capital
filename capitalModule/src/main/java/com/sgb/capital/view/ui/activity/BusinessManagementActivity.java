package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.CapitalBusinessmanagementActivityBinding;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.PayOrderBean;
import com.sgb.capital.model.PayOrderEntity;
import com.sgb.capital.model.SelBean;
import com.sgb.capital.utils.AdapterOnItemClick;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.SelectPop;
import com.sgb.capital.view.pop.ZSelectPop;
import com.sgb.capital.view.ui.adapter.OrderPayAdapter;
import com.sgb.capital.view.widget.xrecyclerview.ZXRecyclerView;
import com.sgb.capital.viewmodel.PayOrderModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/9/2 0002
 * 说明:业务管理
 */
public class BusinessManagementActivity extends BaseActivity {

    private CapitalBusinessmanagementActivityBinding mBinding;
    private OrderPayAdapter mAdapter;
    private PayOrderBean mOrderBean = new PayOrderBean();
    private List<Bean> mTimeBeans = new ArrayList<>();
    private SelectPop mSelectPop;
    private String mStr = "商品";
    PayOrderModel mModel;
    private String mTime;
    private ZSelectPop mZSelectPop;
    private List<SelBean> mBeanList;

    public static void start(Context context, int code) {
        Intent intent = new Intent(context, BusinessManagementActivity.class);
        intent.putExtra("code", code);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.capital_businessmanagement_activity);
        mSelectPop = new SelectPop(this);
        mZSelectPop = new ZSelectPop(this);

        mSelectPop.setBgView(mBinding.grayLayout);
        mZSelectPop.setBgView(mBinding.grayLayout);
        mBinding.rvList.setLayoutManager(new GridLayoutManager(Utils.getContext(), 1));
        mAdapter = new OrderPayAdapter(this, null, null);
        mBinding.rvList.setAdapter(mAdapter);
        mModel = new ViewModelProvider(this).get(PayOrderModel.class);
    }

    public void initData() {
        int code = getIntent().getIntExtra("code", 1);
        mBinding.tvType.setText(code == 1 ? "待支付-商品材料" : code == 5 ? "已支付-商品材料" : code == 6 ? "应收-商品材料" : "已收-商品材料");
        mAdapter.mType = mBinding.tvType.getText().toString().contains("待支付") ? 1 : mBinding.tvType.getText().toString().contains("已支付") ? 2 :
                mBinding.tvType.getText().toString().contains("应收") ? 3 : 4;
        Utils.setBeans(mTimeBeans, new String[]{"全部", "今日", "最近一周", "最近一个月", "最近三个月", "最近半年", "最近一年"});
        mBinding.tvName.setText((code == 1 || code == 5) ? "创建时间" : "付款时间");
        if (code == 1) {
            mModel.getToBePayOrderList(this, mOrderBean);
        } else if (code == 5) {
            mModel.getPay2OrderList(this, mOrderBean);
        } else if (code == 6) {
            mModel.getReceivableOrderList(this, mOrderBean);
        } else {
            mModel.getReceivedOrderList(this, mOrderBean);
        }
    }

    public void initListener() {

        mBinding.rvList.setLoadingListener(new ZXRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                mOrderBean.page = 1;
                refresh();
            }

            @Override
            public void onLoadMore() {
                mOrderBean.page++;
                refresh();
            }
        });

        mBinding.tvRight.setOnClickListener(v -> startActivity(new Intent(this, OrderErrorActivity.class)));
        mAdapter.setOnItemClick(new AdapterOnItemClick<PayOrderEntity>() {
            @Override
            public void onItemClick(PayOrderEntity payOrderEntity, int position) {
                BillOrderDetailsActivity.start(BusinessManagementActivity.this, mAdapter.mType, payOrderEntity.orderId, payOrderEntity.id, mStr);
            }
        });
        mZSelectPop.setOnDismissListener(() -> {
            mBinding.ivArrow.setRotation(0);
            if (mZSelectPop.mIsClick) {
                mOrderBean.page = 1;
                mOrderBean.orderType = mZSelectPop.strKey;
                if (mZSelectPop.name.contains("待付账单")) {
                    mAdapter.mType = 1;
                    mModel.getToBePayOrderList(this, mOrderBean);
                    mBinding.tvType.setText("待支付" + "-" + mZSelectPop.name.split("-")[1]);
                } else if (mZSelectPop.name.contains("已付账单")) {
                    mBinding.tvType.setText("已支付" + "-" + mZSelectPop.name.split("-")[1]);
                    mAdapter.mType = mZSelectPop.name.split("-")[1].contains("转账") ? 5 : 2;
                    if (mAdapter.mType == 2) {
                        mModel.getPay2OrderList(this, mOrderBean);
                    } else {
                        mModel.getTransferInfoList(this, mOrderBean);
                    }
                } else if (mZSelectPop.name.contains("应收账单")) {
                    mAdapter.mType = 3;
                    mModel.getReceivableOrderList(this, mOrderBean);
                    mBinding.tvType.setText("应收" + "-" + mZSelectPop.name.split("-")[1]);
                } else {
                    mBinding.tvType.setText("已收" + "-" + mZSelectPop.name.split("-")[1]);
                    mAdapter.mType = mZSelectPop.name.split("-")[1].contains("转账") ? 6 : 4;
                    if (mAdapter.mType == 4) {
                        mModel.getReceivedOrderList(this, mOrderBean);
                    } else {
                        mModel.getReceivedTransferOrderList(this, mOrderBean);
                    }
                }
                mBinding.tvName.setText(mSelectPop.mPos1 == 0 ?
                        (mAdapter.mType == 1 || mAdapter.mType == 3) ? "创建时间" : "付款时间" : mSelectPop.mBean.name);
                String s = mZSelectPop.name.split("-")[1];
                if (mAdapter.mType > 4) {
                    mStr = "转账";
                } else {
                    mStr = s.contains("商品") ? "商品" : s;
                }
            }
        });
        mBinding.rlSelect.setOnClickListener(v -> {
            if (mZSelectPop.mDatas.size() == 0) {
                mModel.getSelectTree(this);
                return;
            }
            Utils.hide(this);
            mBinding.ivArrow.setRotation(180);
            mZSelectPop.showPopwindow(mBinding.view2, 0, 0);
        });
        mSelectPop.setOnDismissListener(() -> {
            mBinding.ivImage.setRotation(0);
            mBinding.tvName.setText(mSelectPop.mPos1 == 0 ? (mAdapter.mType == 1 || mAdapter.mType == 3) ? "创建时间" : "付款时间" : mSelectPop.mBean.name);
            mBinding.tvName.setTextColor(Utils.getColor(mSelectPop.mPos1 == 0 ? R.color.color_333333 : R.color.color_EF4033));
            mBinding.ivImage.setImageResource(mSelectPop.mPos1 == 0 ? R.mipmap.ic_down_arrow_n : R.mipmap.down);
            if (mSelectPop.mPos1 == 0) {
                mOrderBean.startTime = null;
                mOrderBean.endTime = null;
            } else {
                mTime = mSelectPop.mBean.name.contains("今日") ? Utils.getStartAndEndTime(0, 0, "今日")
                        : mSelectPop.mBean.name.contains("最近一周") ? Utils.getStartAndEndTime(1, 7, "最近一周")
                        : mSelectPop.mBean.name.contains("最近一个月") ? Utils.getStartAndEndTime(2, 1, "最近一个月")
                        : mSelectPop.mBean.name.contains("最近三个月") ? Utils.getStartAndEndTime(2, 3, "最近三个月")
                        : mSelectPop.mBean.name.contains("最近半年") ? Utils.getStartAndEndTime(2, 6, "最近六个月")
                        : Utils.getStartAndEndTime(3, 1, "最近一年");
                mOrderBean.startTime = mTime.split("-")[0];
                mOrderBean.endTime = mTime.split("-")[1];
            }

            if (mSelectPop.mIsClick) {
                mSelectPop.mIsClick = false;
                if (mAdapter.mType == 1) {
                    mModel.getToBePayOrderList(this, mOrderBean);
                } else if (mAdapter.mType == 2) {
                    mModel.getPay2OrderList(this, mOrderBean);
                } else if (mAdapter.mType == 3) {
                    mModel.getReceivableOrderList(this, mOrderBean);
                } else if (mAdapter.mType == 4) {
                    mModel.getReceivedOrderList(this, mOrderBean);
                } else if (mAdapter.mType == 5) {
                    mModel.getTransferInfoList(this, mOrderBean);
                } else {
                    mModel.getReceivedTransferOrderList(this, mOrderBean);
                }
            }
        });

        mBinding.llView.setOnClickListener(v -> {
            mBinding.ivImage.setRotation(180);
            mSelectPop.setData(mTimeBeans);
            mSelectPop.showPopwindow(mBinding.view, 0, 0);
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.etSs.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mOrderBean.page = 1;
                mOrderBean.orderId = mBinding.etSs.getText().toString().trim();
                mModel.getPayOrderList(this, mOrderBean);
            }
            return false;
        });
    }

    private void refresh() {
        if (mAdapter.mType == 1) {
            mModel.getToBePayOrderList(BusinessManagementActivity.this, mOrderBean);
        } else if (mAdapter.mType == 2) {
            mModel.getPay2OrderList(BusinessManagementActivity.this, mOrderBean);
        } else if (mAdapter.mType == 3) {
            mModel.getReceivableOrderList(BusinessManagementActivity.this, mOrderBean);
        } else if (mAdapter.mType == 4) {
            mModel.getReceivedOrderList(BusinessManagementActivity.this, mOrderBean);
        } else if (mAdapter.mType == 5) {
            mModel.getTransferInfoList(BusinessManagementActivity.this, mOrderBean);
        } else {
            mModel.getReceivedTransferOrderList(BusinessManagementActivity.this, mOrderBean);
        }
    }

    @Override
    public void initObserve() {
        mModel.mPayOrderList.observe(this, payOrderEntities -> {
            if (mOrderBean.page != 1) {
                mBinding.rvList.loadMoreComplete();
                if (payOrderEntities.size() != 0) {
                    mAdapter.addDatas(payOrderEntities);
                } else {
                    mBinding.rvList.setNoMore(true);
                }
            } else {
                mBinding.errorView.setVisibility(payOrderEntities.size() != 0 ? View.GONE : View.VISIBLE);
                mBinding.rvList.refreshComplete();
                mBinding.rvList.setLoadingMoreEnabled(true);
                mBinding.rvList.scrollToPosition(0);
                mAdapter.setDatas(payOrderEntities);
                mAdapter.notifyDataSetChanged();
            }
        });
        mModel.mSelBean.observe(this, selBeans -> {
            mBeanList = selBeans;
            mZSelectPop.setData(selBeans, false);
            mZSelectPop.showPopwindow(mBinding.view2, 0, 0);
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants.EVENT_HIDE_ORDERID:
                test();
                break;
        }
    }

    private void test() {
        mBinding.tvType.setText("已支付" + "-" + (mStr.contains("转账") ? "在线转债" : mStr));
        mAdapter.mType = 2;
        mZSelectPop.mlastIndex1 = 1;
        mOrderBean.page = 1;
        mZSelectPop.mlastIndex2 = mStr.contains("商品") ? 0 :
                mStr.contains("物流") ? 1 :
                        mStr.contains("租赁") ? 2 :
                                mStr.contains("企业服务") ? 4 :
                                        mStr.contains("食住行") ? 3 : 5;
        mZSelectPop.setNewInstance();
        Utils.postDelay(() -> refresh(),500);
    }
}