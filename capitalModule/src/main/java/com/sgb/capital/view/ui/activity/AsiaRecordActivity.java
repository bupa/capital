package com.sgb.capital.view.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.sgb.capital.R;
import com.sgb.capital.base.BaseActivity;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.callback.MyZTextWatcher;
import com.sgb.capital.callback.ZMessageEvent;
import com.sgb.capital.databinding.AsiarecordActivityBinding;
import com.sgb.capital.model.AsiaRecordBean;
import com.sgb.capital.model.Bean;
import com.sgb.capital.utils.AdapterOnItemClick;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.pop.SelectPop;
import com.sgb.capital.view.pop.ZSelectPop;
import com.sgb.capital.view.ui.adapter.AsiaRecordAdapter;
import com.sgb.capital.view.widget.xrecyclerview.ZXRecyclerView;
import com.sgb.capital.viewmodel.AsiaRecordModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 作者:张磊
 * 日期:2021/7/21 0021
 * 说明:开票记录
 */
public class AsiaRecordActivity extends BaseActivity {
    private AsiarecordActivityBinding mBinding;
    private SelectPop mSelectPop;
    private ZSelectPop mZSelectPop;
    private AsiaRecordModel mModel;
    private int mIndex = 1;
    private AsiaRecordAdapter mAdapter;
    private HashMap mMap;
    private String mTime;
    private List<Bean> mTimeBeans;

    public static void start(Context context) {
        Intent intent = new Intent(context, AsiaRecordActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.asiarecord_activity);
        mBinding.rvList.setLayoutManager(new LinearLayoutManager(Utils.getContext()));

        mSelectPop = new SelectPop(this);
        mSelectPop.setBgView(mBinding.grayLayout);
        mSelectPop.mType = 1;
        mZSelectPop = new ZSelectPop(this);
        mZSelectPop.setBgView(mBinding.grayLayout);
        mAdapter = new AsiaRecordAdapter(Utils.getContext(), null);
        mBinding.rvList.setAdapter(mAdapter);
        mModel = new ViewModelProvider(this).get(AsiaRecordModel.class);
    }

    @Override
    public void initData() {
        mTimeBeans = new ArrayList<>();
        Utils.setBeans(mTimeBeans, new String[]{"全部", "今日", "最近一周", "最近一月", "最近一年"});
        mSelectPop.setData(mTimeBeans);
        mMap = new HashMap();
        mMap.put("limit", 20);
        mMap.put("page", mIndex);
        mModel.invoiceList(this, mMap);
        mModel.getSelectOption(this);
    }


    @Override
    public void initObserve() {
        mModel.mAsiaRecordBean.observe(this, data -> {
            mBinding.rvList.refreshComplete();
            if (mIndex == 1) {
                mBinding.rvList.setVisibility(data.size() != 0 ? View.VISIBLE : View.GONE);
                mBinding.zErrorView.setVisibility(data.size() != 0 ? View.GONE : View.VISIBLE);
                mBinding.rvList.setLoadingMoreEnabled(true);
                mBinding.rvList.refreshComplete();
                mAdapter.setDatas(data);
            } else {
                if (data.size() != 0) {
                    mBinding.rvList.loadMoreComplete();
                    mAdapter.addDatas(data);
                } else {
                    mBinding.rvList.setNoMore(true);
                }
            }
            mAdapter.notifyDataSetChanged();
        });
        mModel.mSelBean.observe(this, selBeans -> {
            mZSelectPop.setData(selBeans);
        });
    }


    @Override
    public void initListener() {
        mAdapter.setOnItemClick(new AdapterOnItemClick<AsiaRecordBean.ListDTO>() {
            @Override
            public void onItemClick(AsiaRecordBean.ListDTO listDTO, int position) {
                AsiaRecordDetailsActivity.start(AsiaRecordActivity.this, listDTO.invoiceId);
            }
        });
        mBinding.etSs.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mIndex = 1;
                mMap.put("page", mIndex);
                mMap.put("flowNo", mBinding.etSs.getText().toString().trim());
                mModel.invoiceList(AsiaRecordActivity.this, mMap);
            }
            return false;
        });
        mBinding.etSs.addTextChangedListener(new MyZTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String editable = mBinding.etSs.getText().toString();
                String str = Utils.stringFilter(mBinding.etSs.getText().toString());
                if (!editable.equals(str)) {
                    mBinding.etSs.setText(str);
                    mBinding.etSs.setSelection(str.length());
                }
            }
        });


        mBinding.rvList.setLoadingListener(new ZXRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                mIndex = 1;
                mMap.put("page", mIndex);
                mModel.invoiceList(AsiaRecordActivity.this, mMap);
            }

            @Override
            public void onLoadMore() {
                mIndex++;
                mMap.put("page", mIndex);
                mModel.invoiceList(AsiaRecordActivity.this, mMap);
            }
        });
        mBinding.ivBack.setOnClickListener(v -> finish());
        mBinding.llTimeBtn.setOnClickListener(v -> {
            mBinding.tvName.setTextColor(Utils.getColor(R.color.color_333333));
            Utils.hide(this);
            mBinding.img.setRotation(180);
            mBinding.img.setImageResource(R.mipmap.ic_down_arrow_n);
            mSelectPop.showPopwindow(mBinding.view, 0, 0);
        });
        mBinding.llStateBtn.setOnClickListener(v -> {
            mBinding.tvState.setTextColor(Utils.getColor(R.color.color_333333));
            Utils.hide(this);
            mBinding.imgState.setRotation(180);
            mBinding.imgState.setImageResource(R.mipmap.ic_down_arrow_n);
            mZSelectPop.mIsClick = false;
            mZSelectPop.showPopwindow(mBinding.view, 0, 0);
        });

        mZSelectPop.setOnDismissListener(() -> {
            mBinding.imgState.setRotation(0);
            if (mZSelectPop.mIsClick) {
                String name = mZSelectPop.mName1.name != null ? mZSelectPop.mName2.name == null ? mZSelectPop.mName1.name : mZSelectPop.mName2.name : "发票状态";
                mBinding.tvState.setText(name);
                mBinding.tvState.setTextColor(Utils.getColor(!name.contains("发票状态") ? R.color.color_EF4033 : R.color.color_333333));
                mBinding.imgState.setImageResource(!name.contains("发票状态") ? R.mipmap.down : R.mipmap.ic_down_arrow_n);
                // 状态(1.待审核 2.已驳回 3.待寄出 4.已寄出 5.已撤销 6.未开票 7.已开票 )
                mMap.put("invoiceStatus", name.contains("发票状态") ? null :
                        name.contains("待审核") ? 1 :
                                name.contains("已驳回") ? 2 :
                                        name.contains("待寄出") ? 3 :
                                                name.contains("已寄出") ? 4 :
                                                        name.contains("已撤销") ? 5 :
                                                                name.contains("未开票") ? 6 : 7);
                mModel.invoiceList(this, mMap);
            }
            if (mBinding.tvState.getText().toString().contains("发票状态")) {
                mBinding.tvState.setTextColor(Utils.getColor(R.color.color_666666));
            }
        });

        mSelectPop.setOnDismissListener(() -> {
            mBinding.img.setRotation(0);
            mBinding.tvName.setText(mSelectPop.mPos1 != 0 ? mSelectPop.mBean.name : "查询时间");
            mBinding.tvName.setTextColor(Utils.getColor(mSelectPop.mPos1 != 0 ? R.color.color_EF4033 : R.color.color_333333));
            if (mSelectPop.mPos1 == 0) {
                mBinding.tvName.setTextColor(Utils.getColor(R.color.color_666666));
            }
            mBinding.img.setImageResource(mSelectPop.mPos1 != 0 ? R.mipmap.down : R.mipmap.ic_down_arrow_n);
            if (mSelectPop.mPos1 == 0) {
                mTime = null;
                mMap.put("startTime", null);
                mMap.put("endTime", null);
            } else {
                mTime = mSelectPop.mBean.name.contains("今日") ? Utils.getStartAndEndTime(0, 0, "今日")
                        : mSelectPop.mBean.name.contains("最近一周") ? Utils.getStartAndEndTime(1, 1, "最近一周")
                        : mSelectPop.mBean.name.contains("最近一月") ? Utils.getStartAndEndTime(2, 1, "最近一月")
                        : mSelectPop.mBean.name.contains("最近一年") ? Utils.getStartAndEndTime(3, 1, "最近一年")
                        : Utils.getStartAndEndTime(3, 3, "最近三年");
                mMap.put("startTime", mTime.split("-")[0]);
                mMap.put("endTime", mTime.split("-")[1]);
            }

            if (mSelectPop.mIsClick) {
                mSelectPop.mIsClick = false;
                mModel.invoiceList(this, mMap);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void getCode(ZMessageEvent event) {
        switch (event.code) {
            case Constants
                    .EVENT_REFRESH:
                mIndex = 1;
                mMap.put("page", mIndex);
                mModel.invoiceList(AsiaRecordActivity.this, mMap);
                break;
        }
    }
}