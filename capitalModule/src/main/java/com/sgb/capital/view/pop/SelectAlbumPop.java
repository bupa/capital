package com.sgb.capital.view.pop;

import android.content.Context;
import android.widget.TextView;


import com.sgb.capital.R;
import com.sgb.capital.base.BasePopWindow;

/**
 * 选择手机相册弹框
 */
public class SelectAlbumPop extends BasePopWindow<String> {
    private TextView tvCancel;
    private TextView tvPhoneAlbum;

    public SelectAlbumPop(Context context) {
        super(context, DIR_DOWN_UP);
    }

    @Override
    protected int popLayout() {
        return R.layout.pop_select_album;
    }

    @Override
    protected void initView(PopViewHolder holder) {
        tvCancel = holder.getPop().findViewById(R.id.tv_cancel);
        tvPhoneAlbum = holder.getPop().findViewById(R.id.tv_phone_album);
        tvPhoneAlbum.setOnClickListener(v -> {
            onResultClick.result(0, null, null);
        });
        tvCancel.setOnClickListener(v -> {
            dismiss();
        });
    }
}
