package com.sgb.capital.view.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;

import com.sgb.capital.R;
import com.sgb.capital.callback.Constants;
import com.sgb.capital.model.Bean;
import com.sgb.capital.model.PayDetailBean;
import com.sgb.capital.utils.Utils;
import com.sgb.capital.view.ui.adapter.CardsAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.Direction;
import razerdp.util.animation.TranslationConfig;

import static com.sgb.capital.callback.Constants.MODIFY_THE_PHONE_NUMBER;

/**
 * 作者:张磊
 * 日期:2021/4/16 0016
 * 说明:账单详情
 */
public class Cards2Pop extends BasePopupWindow {


    public List<Bean> mDatas;
    public PayDetailBean mBean;
    private CardsAdapter mAdapter;

    public Cards2Pop(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View v = createPopupById(R.layout.cards2pop);
        RecyclerView rv = v.findViewById(R.id.rv);
        v.findViewById(R.id.tv_btn).setOnClickListener(v1 -> dismiss());
        rv.setLayoutManager(new LinearLayoutManager(Utils.getContext()));
        mDatas = new ArrayList<>();
        mAdapter = new CardsAdapter(mDatas);
        mAdapter.setOnItemChildClickListener((adapter1, view, position) -> Utils.sendMsg(MODIFY_THE_PHONE_NUMBER, mBean));
        rv.setAdapter(mAdapter);
        v.findViewById(R.id.tv_cancel).setOnClickListener(v1 -> dismiss());

        v.findViewById(R.id.tv_btn).setOnClickListener(v1 -> {
            Utils.sendMsg(Constants.EVENT_CLICK, null);
            dismiss();
        });
        return v;
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .from(Direction.BOTTOM))
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withTranslation(new TranslationConfig()
                        .to(Direction.BOTTOM))
                .toDismiss();
    }


    public void show() {
        mDatas.clear();
        mDatas.add(new Bean("业务类型-" + mBean.orderTypeName));
        mDatas.add(new Bean("订单号-" + mBean.paymentId));
        mDatas.add(new Bean("付款金额-" + Utils.getDecimalFormat(mBean.money)));
        mDatas.add(new Bean("收款方-" + (Utils.isEmpty(mBean.payeeName) ? "/" : mBean.payeeName)));
        mAdapter.setNewInstance(mDatas);
        mAdapter.notifyDataSetChanged();
        showPopupWindow();
    }
}