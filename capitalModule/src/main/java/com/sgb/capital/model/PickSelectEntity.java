package com.sgb.capital.model;

import com.contrarywind.interfaces.IPickerViewData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PickSelectEntity<T> implements Serializable, IPickerViewData {
    @SerializedName(value = "no", alternate = {"value", "key", "code", "id","categoryType","count","supervisorType","type"})
    private String no;

    @SerializedName(value = "projectscale", alternate = {"payWay", "simpleName", "name", "text", "val", "area_name", "areaName", "label", "ranges", "typeName","supervisorTypeName","desc", "enquiryType"})
    private String projectscale;

    @SerializedName(value = "children", alternate = {"valList", "vals", "child", "honorLevelTwoList", "childrenVO"})
    T children;

    @SerializedName(value = "isCheck")
    private boolean isCheck = false;  //false未选中  true选中

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getProjectscale() {
        return projectscale;
    }

    public void setProjectscale(String projectscale) {
        this.projectscale = projectscale;
    }

    public T getChildren() {
        return children;
    }

    public void setChildren(T children) {
        this.children = children;
    }

    @Override
    public String getPickerViewText() {
        return projectscale;
    }

    public PickSelectEntity() {
    }

    public PickSelectEntity(String no, String projectscale) {
        this.no = no;
        this.projectscale = projectscale;
    }

    @Override
    public String toString() {
        return "PickSelectEntity{" +
                "no='" + no + '\'' +
                ", projectscale='" + projectscale + '\'' +
                ", children=" + children +
                ", isCheck=" + isCheck +
                '}';
    }
}
