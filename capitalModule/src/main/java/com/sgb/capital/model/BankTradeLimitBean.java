package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/9/18 0018
 * 说明:交易限额
 */
public class BankTradeLimitBean {
   /* bankName	银行名称	string
    bankType	银行卡类型 1.借记卡	integer(int32)
    created	创建日期	string(date-time)
    creator	创建人	string
    creatorName	创建人名称	string
    dayB2bLimit	b2b网银(单日)	string
    dayQuickLimit	快捷支付(单日)	string
    id	ID	string
    isDeleted	是否删除	boolean
    remark	备注	string
    singleB2bLimit	b2b网银(单笔)	string
    singleQuickLimit	快捷支付(单笔)	string
    updater	更新人*/
    public String bankName;
    public int bankType;
    public String created;
    public String creator;
    public String creatorName;
    public String dayB2bLimit;
    public String dayQuickLimit;
    public String id;
    public Boolean isDeleted;
    public String remark;
    public String singleB2bLimit;
    public String singleQuickLimit;
    public String updater;
}