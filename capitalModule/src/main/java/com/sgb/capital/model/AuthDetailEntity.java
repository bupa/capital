package com.sgb.capital.model;

public class AuthDetailEntity {
//	          "balance": "",余额
//            "companyNo": "",企业编号
//            "created": "",创建日期
//            "creator": "",创建人
//            "creatorName": "",创建人名称
//            "deleted": true,
//            "finishTime": "",完成时间
//            "flowNo": "",	交易流水号
//            "id": "",ID
//            "orderId": "",订单编号
//            "otherCompanyName": "",	对方企业名称
//            "payAccountName": "",	付款账户名称
//            "payAccountNumber": "",付款账号
//            "payChannels": "",付款渠道
//            "payName": "",付款方
//            "payTime": "",付款时间
//            "payType": "",支付类型
//            "payeeName": "",收款方
//            "tradeAmount": "",交易金额
//            "tradeFees": "",交易手续费
//            "tradeFlowType": 0,交易流水类型（1：入账，2：出账）
//            "tradeStatus": 0,交易状态（1：.已成功，2.已失败，3.冻结中,4.确认中）
//            "tradeType": "",交易类型 比如：商品采购、商品销售
//            "updater": "",更新人
//            "userId": "",商户号 联动用户编码
//            "withdrawBankAccount": "",提现银行账户编号
//            "withdrawBankAccountName": "",提现企业账户名称
//            "withdrawBankName": ""提现银行名称


    	public String balance;
    	public String companyNo;
    	public String created;
    	public String creator;
    	public String creatorName;
    	public String deleted;
    	public String finishTime;
    	public String flowNo;
    	public String id;
    	public String orderId;
    	public String otherCompanyName;
    	public String payAccountName;
    	public String payAccountNumber;
    	public String payChannels;
    	public String payName;
    	public String payTime;
    	public String payType;
    	public String payeeName;
    	public String tradeAmount;
    	public String tradeFees;
    	public String tradeFlowType;
    	public String tradeStatus;
    	public String tradeType;
    	public String updater;
    	public String userId;
    	public String withdrawBankAccount;
    	public String withdrawBankAccountName;
    	public String withdrawBankName;
}
