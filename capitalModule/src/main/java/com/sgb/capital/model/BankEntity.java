package com.sgb.capital.model;

public class BankEntity {
    public String code;
    public String name;
    public String no;
    public int type;
    public boolean isSelect;

    public BankEntity() {
    }

    public BankEntity(String name, String no, int type) {
        this.name = name;
        this.no = no;
        this.type = type;
    }
}
