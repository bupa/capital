package com.sgb.capital.model;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/9/6 0006
 * 说明:
 */
public class ContractDetailBean {

    public Boolean lastPage;
    public List<ListDTO> list;

    public static class ListDTO {
        public String companyNo;
        public String contractNum;
        public String createTime;
        public String deadline;
        public Integer id;
        public List<InvolvesDTO> involves;
        public Boolean isTaker;
        public String localUrl;
        public String no;
        public Integer signStatus;
        public Integer signTag;
        public String signingTime;
        public Integer status;
        public String statusName;
        public String title="/";
        public Integer type;
        public String typeName;

        public static class InvolvesDTO {
            public String companyName="/";
            public String companyNo;
            public String contractPosition;
            public Integer isTaker;
            public Integer signCase;
            public Integer signStatus;
            public String signTime;
            public Integer sort;
        }
    }
}