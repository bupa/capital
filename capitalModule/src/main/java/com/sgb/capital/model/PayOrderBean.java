package com.sgb.capital.model;

import java.io.Serializable;

public class PayOrderBean implements Serializable {
    //    dateType	时间范围筛选：1 今天，2 本周 3 本月 4 本年
//    limit   integer(int32)	分页条数		false
//    orderId string	订单id		false
//    orderType  string	订单类型		商品采购 物流运费 租赁订单false
//    page	integer(int32) 页码		false
//    state integer(int32)	付款状态 1 未付款 4 确认中 5 支付成功		false
    public String dateType;
    public int limit = 10;
    public String paymentId;
    public String orderId;
    public String orderType="0001";
    public String startTime;
    public String endTime;
    public int page=1;
    public String state;

}
