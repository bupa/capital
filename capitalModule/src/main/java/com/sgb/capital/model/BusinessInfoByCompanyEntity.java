package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/1/26 0026
 * 说明:企业联动商户信息
 */
public class BusinessInfoByCompanyEntity {


   /* address	商户地址	string
    assBal	冻结金额	number
    balance	余额	number
    bankAccountLicence	开户许可证	string
    bussinessLicense	营业执照	string
    certId	统一社会信用代码	string
    certType	证件类型，1：营业执照 2：统一社会信用代码	string
    contactsName	业务联系人姓名	string
    custName	真实姓名	string
    idCardBack	法人身份证（反面）	string
    idCardFront	法人身份证（正面）	string
    identityCode	证件号	string
    identityId	法人证件号	string
    legalName	法人姓名	string
    mobileId	业务联系人的手机号/预留手机号	string
    retMsg	该笔交易的返回描述信息	string
    status	审核状态 0-初始、1-待审批、2-审批通过、3-审批不通过	integer
    subMerAlias	商户名称的业务别名,比如店铺名称	string
    subMerName	商户名称	string
    subMerType	商户类型，3（企业商户）、2（个体工商户）	string
    userId	商户编号	string
    orderCount	订单数量
    userType	用户类型*/

    public String address;
    public String realName;
    public float assBal;
    public float balance;
    public int orderCount;
    public String bankAccountLicence;
    public String bussinessLicense;
    public String license;
    public String certId;
    public String certType;
    public String contactsName;
    public String custName;
    public String idCardBack;
    public String idCardFront;
    public String identityCode;
    public String identityId;
    public String legalName;
    public String mobileId;
    public String retMsg;
    public int status;
    public String subMerAlias;
    public String subMerName;
    public String subMerType;
    public String userId;
    public String userType;
    public String name;
    public String creditCode;
    public UserDTO user;
    public float todayOrderAmount;


    public static class UserDTO {
        public String accountName;
        public Integer authTime;
        public String cType;
        public String channel;
        public String code;
        public Integer companyAuth;
        public Long createTime;
        public String createUser;
        public Integer driverAuth;
        public String email;
        public String grantIdentifying;
        public Boolean guidanceOpenCompanyInfo;
        public String headImg;
        public Integer id;
        public String identityCard;
        public String identityCertification;
        public String imAccount;
        public String imToken;
        public Integer isDelete;
        public Integer isValidate;
        public Integer lastLoginTime;
        public String memo;
        public String no;
        public String password;
        public Integer passwordStrength;
        public Integer passwordStrengthScore;
        public String qq;
        public Integer realAuth;
        public String realName;
        public String reason;
        public String repassword;
        public Integer sex;
        public Integer teamAuth;
        public Integer teamAuthStatus;
        public String telPhone;
        public String type;
        public Long updateTime;
        public String updateUser;
        public Integer userRole;
        public String username;
        public String verifyText;
        public String weChat;
    }
}