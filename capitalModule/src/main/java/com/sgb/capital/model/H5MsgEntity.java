package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/1/29 0029
 * 说明:原生调h5
 */
public class H5MsgEntity {
    /**
     * msgType : xx
     * data : xx
     * msgId : xx
     */

    public String msgType;
    public Object data;
    public String msgId;
}