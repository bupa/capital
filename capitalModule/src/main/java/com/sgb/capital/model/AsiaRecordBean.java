package com.sgb.capital.model;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/7/22 0022
 * 说明:
 */
public class AsiaRecordBean {
    /*    created	申请时间	string
        invoiceCode	发票代码	string
        invoiceHead	单位名称(发票抬头)	string
        invoiceId	服务单号	string
        invoiceMakeDate	发票开具时间	string
        invoiceNumber	发票号码	string
        invoiceStatus	状态(1.待审核 2.已驳回 3.待寄出 4.已寄出 5.已撤销 6.未开票 7.已开票 )	integer
        invoiceType	发票类型 1.增值税专用发票 2.增值税普通发票	integer
        totalFees	合计金额*/
    public List<ListDTO> list;

    public static class ListDTO {
        public long created;
        public String invoiceCode;
        public String invoiceHead;
        public String invoiceId;
        public String invoiceMakeDate;
        public String invoiceNumber;
        public Integer invoiceStatus;
        public Integer invoiceType;
        public float totalFees;
    }
}