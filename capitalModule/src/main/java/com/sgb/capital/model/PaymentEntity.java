package com.sgb.capital.model;

/**
 * 作者:陈涛
 * 日期:2021/5/10
 * 说明:付款的实体类
 */
public class PaymentEntity {
    /**
     * account : 新疆金正大工贸有限公司
     * auditStatus : 0
     * created : 1611289226000
     * creator : 201907187391201
     * id : 1352471158301528064
     * matters : 采购商品
     * method : 在线付款
     * money : 0.1
     * orderId : 1352469941353582592
     * orderType : 0001
     * payApplyState : 2
     * payState : 0
     * payeeAccountName : 中钧科技有限公司
     * payeeCompany : 201909024695838
     * payeeCompanyName : 中钧科技有限公司
     * payeeUserId : 00123484
     * payerAccountName : 新疆金正大工贸有限公司
     * payerCompany : 201907185069098
     * state : 1
     * tradeNo : 2101221220381150
     */

    public String account;
    public int auditStatus;
    public long created;
    public String creator;
    public String id;
    public String matters;
    public String method;
    public double money;
    public String orderId;
    public String orderType;
    public int payApplyState;
    public int payState;
    public String payeeAccountName;
    public long payeeCompany;
    public String payeeCompanyName;
    public String payeeUserId;
    public String payerAccountName;
    public long payerCompany;
    public int state;
    public String tradeNo;
}