package com.sgb.capital.model;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/23 0023
 * 说明:账户明细
 */
public class AccountBean {

    /**
     * balance : 0.08
     * orderId : 1032021032317530961
     * orderDate : 20210323
     * direct : C
     * amount : 0.01
     */
    public String balance;
    public String orderId;
    public String orderDate;
    public String direct;
    public String amount;
    public String accType="108";
    public String endDate;
    public String transTypeName;
    public int pageNum = 1;
    public int pageSize = 20;
    public String startDate;

    public List<AccountBean> list;
}