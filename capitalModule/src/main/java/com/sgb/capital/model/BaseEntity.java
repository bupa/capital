package com.sgb.capital.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Description:网络请求实体泛型基类
 * Author zhengkewen
 * Time 2017/12/11
 */
public class BaseEntity<T> implements Serializable {
    protected String code;
    protected String msg;
    @SerializedName(value = "state", alternate = {"status"})
    protected String state;



    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @SerializedName(value = "data", alternate = {"result", "records"})
    private T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
