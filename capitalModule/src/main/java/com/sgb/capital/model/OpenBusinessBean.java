package com.sgb.capital.model;

public class OpenBusinessBean {
//    address	商户的联系地址		false
//    string
//    bankAccountLicence	开户许可证		false
//    string
//    bussinessLicense	营业执照		false
//    string
//    certId	营业执照号码		false
//    string
//    contactsName	业务联系人姓名		false
//    string
//    id	id		false
//    integer(int64)
//    idCardBack	法人身份证（反面）		false
//    string
//    idCardFront	法人身份证（正面）		false
//    string
//    identityId	法人证件号		false
//    string
//    legalName	法人姓名		false
//    string
//    mobileId	业务联系人的手机号		false
//    string
//    subMerAlias	商户名称的业务别名,比如店铺名称		false
//    string
//    subMerName	营业执照上的商户名称		false
//    string
    public String address;
    public String bankAccountLicence;
    public String bussinessLicense;
    public String certId;
    public String contactsName;
    public String id;
    public String idCardBack;
    public String idCardFront;
    public String identityId;
    public String legalName;
    public String mobileId;
    public int payType=1;
    public int clientType=3;
    public String subMerAlias;
    public String subMerName;
    public String subMerType;
}
