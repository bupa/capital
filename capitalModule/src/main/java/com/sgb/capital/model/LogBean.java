package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/9/7 0007
 * 说明:日志
 */
public class LogBean {

    public String content;
    public long created;
    public String creator;
    public String creatorName;
    public Boolean deleted;
    public String id;
    public String operType;
    public String orderId;
    public String remark;
    public String updater;
}