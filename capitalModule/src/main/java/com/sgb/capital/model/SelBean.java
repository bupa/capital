package com.sgb.capital.model;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/23 0023
 * 说明:账户明细
 */
public class SelBean {

    public Integer key;
    public String strKey;
    public Integer parent;
    public String name;
    public boolean isSel;
    public List<SelBean> children;
    public String extend;


}