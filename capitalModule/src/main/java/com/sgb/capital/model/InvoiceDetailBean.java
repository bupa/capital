package com.sgb.capital.model;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/7/23 0023
 * 说明:发票详情
 */
public class InvoiceDetailBean {
   /* companyAddress	企业地址	string
    companyBankAccount	企业开户银行帐号	string
    companyBankName	企业开户银行	string
    companyDutyNo	企业税号	string
    companyNo	申请企业编号	string
    companyPhone	企业联系方式	string
    created	申请时间	string(date-time)
    creatorName	申请人	string
    invoiceCode	发票代码	string
    invoiceContent	发票内容	string
    invoiceHead	单位名称(发票抬头)	string
    invoiceId	服务单号	string
    invoiceMakeDate	发票开具时间	string(date-time)
    invoiceNumber	发票号码	string
    invoiceStatus	状态(1.待审核 2.已驳回 3.待寄出 4.已寄出 5.已撤销)	integer(int32)
    invoiceType	发票类型 1.增值税专用发票 2.增值税普通发票	integer(int32)
    logs	操作日志	array	ZjInvoiceLog对象
    content	操作内容	string
    created	创建日期	string
    creator	创建人	string
    creatorName	操作人名称	string
    deleted		boolean
    id	ID	string
    invoiceId	服务单号	string
    remark	备注	string
    updater	更新人	string
    mailAddress	收件人地址	string
    mailAreaName	收件人地区	string
    mailName	收件人	string
    mailNo	运单号	string
    mailPhone	收件人手机号	string
    mailType	物流公司	string
    makeBy	开票类型 1.企业 2.个人	integer(int32)
    payFlow	流水信息	array	InvoicePayFlowDTO
    flowNo	交易流水号	string
    id		string
    payTime	支付时间	string
    paymentId	业务订单编号	string
    tradeFees	交易手续费	string
    tradeType	交易类型 比如：商品采购、商品销售	string
    remark	备注(驳回原因)	string
    revokeType	撤销类型 1.发票信息错误 2.发票类型错误 3.其他	integer(int32)
    totalFees	合计金额*/

    public String companyAddress;
    public String companyBankAccount;
    public String companyBankName;
    public String companyDutyNo;
    public String companyNo;
    public String companyPhone;
    public long created;
    public String creatorName;
    public String invoiceCode="/";
    public String invoiceContent;
    public String invoiceHead="";
    public String invoiceId;
    public String invoiceMakeDate;
    public String invoiceNumber="/";
    public int invoiceStatus;
    public Integer invoiceType;
    public List<LogsDTO> logs;
    public String mailAddress;
    public String mailAreaName;
    public String mailName;
    public String mailNo;
    public String mailPhone;
    public String mailType;
    public Integer makeBy;
    public List<PayFlowDTO> payFlow;
    public String remark;
    public long revokeDate;
    public Integer revokeType;
    public float totalFees;

    public static class LogsDTO {
        public String content;
        public String created;
        public String creator;
        public String creatorName;
        public Boolean deleted;
        public String id;
        public String invoiceId;
        public String remark;
        public String updater;
    }

    public static class PayFlowDTO {
        public String flowNo;
        public String id;
        public String payType="";
        public long payTime;
        public String paymentId;
        public float tradeFees;
        public String tradeType;
    }
}