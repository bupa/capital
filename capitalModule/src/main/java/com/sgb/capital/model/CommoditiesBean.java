package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/9/7 0007
 * 说明:商品信息
 */
public class CommoditiesBean {
    public String brand;
    public String commodityId;
    public String count="";
    public String details;
    public String money;
    public String name="/";
    public String orderId;
    public String unit;
}