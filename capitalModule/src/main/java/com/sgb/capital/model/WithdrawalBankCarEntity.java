package com.sgb.capital.model;

/**
 * 资金中心1.4.0
 * chent 提现银行卡信息
 */
public class WithdrawalBankCarEntity {
//
//            "bankAccount": "",银行卡号码
//            "bankAccountName": "",银行账户名称
//            "bankBranchName": "",开户行
//            "bankName": "",银行名称
//            "gateId": "",	银行编码
//            "mobileId": "",预留手机号
//            "userId": "",联动 商户编号
//            "userType": 0子商户类型：1（个人商户），2 （个体工商户），3（企业商户）
    public String bankAccount;
    public String bankAccountName;
    public String bankBranchName;
    public String bankName;
    public String gateId;
    public String mobileId;
    public String userId;
    public int userType;
}
