package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/5/17 0017
 * 说明:绑定企业提现银行卡
 */
public class BindEnterpriseBankBean {

    /**
     * bankAccount :
     * bankAccountName :
     * bankBranchName :
     * code :
     * gateId :
     * mobileId :
     * userType :
     */

    public String bankAccount;
    public String bankAccountName;
    public String bankBranchName;
    public String code;
    public String gateId;
    public String mobileId;
    public String userType="3";
}