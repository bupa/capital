package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/3/15 0015
 * 说明:二维码返回与上传实体类
 */
public class QRcodeEntity {
    /**
     * amount : 0.01
     * isFeePayer : 1
     * orderChannel : 2
     * orderId : 1364058882352658888
     * orderType : 2
     * payType : WX
     * payeeName : 胡思露
     * payerCompany : 201907185069098
     * payerCompanyName : 新疆金正大工贸有限公司
     * remark : 收款二维码测试
     * sellerId : 00123627
     * tradeType : 1
     * merTrace : 流水号
     * paymentLink : 链接
     *
     */
    public String merTrace;
    public String paymentLink;
    public String amount="0.0";
    public String isFeePayer;
    public String orderChannel;
    public String orderId;
    public String orderType;
    public String payType="WX";
    public String payeeName;
    public String payerCompany;
    public String payerCompanyName;
    public String remark;
    public String sellerId;
    public String tradeType;
    public String tradeNo;
}