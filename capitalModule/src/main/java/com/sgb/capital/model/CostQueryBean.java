package com.sgb.capital.model;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/4/23 0023
 * 说明:账户明细
 */
public class CostQueryBean {


    public String endTime;
    public String flowNo;
    public int limit = 20;
    public int page = 1;
    public String startTime;


    public long created;
    public long payTime;
    public String orderId="";
    public String paymentId="";
    public String otherCompanyName;
    public String payType="";
    public String tradeFees;
    public String tradeType;

    public List<CostQueryBean> list;
    public String searchValue;
}