package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/1/28 0028
 * 说明:绑定企业对公结算账户
 */
public class BindEnterpriseBankEntity {
    /*  bankAccount	银行账户编号		false
      string
      bankAccountName	企业账户名称/持卡人名		false
      string
      bankBranchName	按照开户许可证上的开户行支行全称		false
      string
      cardId	卡号		false
      string
      gateId	银行编码		false
      string
      mobileId	提现到账通知手机号码/当前银行卡开户时在银行预留的手机号		false
      string
      userType	子商户类型:1（个人商户），2 （个体工商户），3（企业商户）		false*/
    public String bankAccount;
    public String bankAccountName;
    public String bankBranchName;
    public String cardId;
    public String gateId;
    public String mobileId;
    public String userType;
}