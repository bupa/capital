package com.sgb.capital.model;

import com.google.gson.annotations.SerializedName;

public class LoginEntity {
    @SerializedName(value = "access_token", alternate = {"token"})
    private String token;
    @SerializedName("msg")
    private String msg;
    @SerializedName("phone")
    private String phone;
    @SerializedName("token_type")
    private String token_type;
    @SerializedName("expires_in")
    private String expires_in;
    @SerializedName("scope")
    private String scope;
    @SerializedName("license")
    private String license;
    @SerializedName("remember_me")
    private String remember_me;
    @SerializedName("jti")
    private String jti;
    @SerializedName("code")
    private String code;
    @SerializedName("needRegister")
    private boolean isNeedRegister;
    @SerializedName("imAccount")
    private String imAccount;
    @SerializedName("imToken")
    private String imToken;
    @SerializedName("resourcePCToken")
    private String resourcePCToken;
    @SerializedName("resourceAPPToken")
    private String resourceAPPToken;

    public String getResourcePCToken() {
        return resourcePCToken;
    }

    public void setResourcePCToken(String resourcePCToken) {
        System.out.println("resourcePCToken=="+ resourcePCToken);
        this.resourcePCToken = resourcePCToken;
    }

    public String getResourceAPPToken() {
        return resourceAPPToken;
    }

    public void setResourceAPPToken(String resourceAPPToken) {
        System.out.println("resourceAPPToken=="+ resourcePCToken);
        this.resourceAPPToken = resourceAPPToken;
    }

    public boolean isNeedRegister() {
        return isNeedRegister;
    }

    public void setNeedRegister(boolean needRegister) {
        isNeedRegister = needRegister;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getRemember_me() {
        return remember_me;
    }

    public void setRemember_me(String remember_me) {
        this.remember_me = remember_me;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public String getImAccount() {
        return imAccount;
    }

    public void setImAccount(String imAccount) {
        this.imAccount = imAccount;
    }

    public String getImToken() {
        return imToken;
    }

    public void setImToken(String imToken) {
        this.imToken = imToken;
    }
}
