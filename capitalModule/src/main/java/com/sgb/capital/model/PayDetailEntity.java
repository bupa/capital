package com.sgb.capital.model;

import java.io.Serializable;

public class PayDetailEntity implements Serializable {

//            "account": "",付款账户
//            "accountNumber": "",付款账号
//            "channel": "",付款渠道
//            "collectionVoucher": "",收款凭证
//             "created" ,创建时间
//            "completionTime": "",完成时间
//            "matters": "",付款实事项
//            "method": "",付款方式
//            "money": 0,付款金额
//            "number": "",交易流水号
//            "orderId": "",订单编号
//            "orderType": "",订单类型
//            "orderTypeName": "",订单类型名
//            "payApplyState": 0,支付申请状态 0 待申请 1 审批中 2 审批通过 3 审批未通过
//            "payState": 0,付款状态 0 未付款 1 冻结中 2 支付成功 3 支付失败
//            "payType": "",支付类型
//            "proof": "",支付凭证
//            "state": 0 状态 0 审批中 1 审批通过 3 审批未通过 4 确认中 5 支付成功

    public String account="/";
    public String accountNumber="/";
    public String channel="/";
    public String collectionVoucher;
    public long completionTime;
    public long created;
    public String matters;
    public String method;
    public String money;
    public String number;
    public String orderId;
    public String paymentId="/";
    public String orderType;
    public String orderTypeName;
    public String payApplyState;
    public int payState;
    public String payType;
    public String proof;
    public int state;
}


