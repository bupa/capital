package com.sgb.capital.model;

import java.util.List;

/**
 * 作者:张磊
 * 日期:2021/1/27 0027
 * 说明:获取流水统计
 */
public class PayFlowStatisticsEntity {
   // payAmount	本月支出金额	number
   // payFlowList	支出流水列表	array	object
   // payeeAmount	本月收入金额	number
   // payeeFlowList	收入流水列表

    public float payAmount;
    public float payeeAmount;
    public List<Object> payFlowList;
    public List<Object> payeeFlowList;
}