package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/1/28 0028
 * 说明:绑定结算账户列表
 */
public class BindBankCardListEntity {
/*    bankAccount	银行账户编号	string
    bankAccountName	企业账户名称	string
    bankBranchName	按照开户许可证上的开户行支行全称	string
    bankMobileId	当前银行卡开户时在银行预留的手机号	string
    bankName	银行名称	string
    cardId	卡号	string
    createCompany	创建公司	string
    created	创建日期	string
    creator	创建人	string
    creatorName	创建人名称	string
    deleted		boolean
    gateId	银行编码	string
    id	ID	string
    isDefault	是否默认	boolean
    mobileId	提现到账通知手机号码	string
    pagreementId		string
    tradeNo	联动在绑定法人结算卡申请接口中返回的trade_no参数	string
    updater	更新人	string
    userId	子商户在联动链金平台注册成功后，联动返回的子商户唯一编号	string
    userType	子商户类型：1（个人商户），2 （个体工商户），3（企业商户）*/
    public String bankAccount;
    public String bankAccountName;
    public String bankBranchName;
    public String bankMobileId;
    public String bankName;
    public String cardId;
    public String createCompany;
    public String created;
    public String creator;
    public String creatorName;
    public boolean deleted;
    public String gateId;
    public String id;
    public boolean isDefault;
    public String mobileId;
    public String pagreementId;
    public String tradeNo;
    public String updater;
    public String userId;
    public int userType;
}