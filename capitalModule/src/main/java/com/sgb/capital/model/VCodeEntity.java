package com.sgb.capital.model;

public class VCodeEntity {
    /**
     * 持卡人名
     */
    public String bankAccountName;
    /**
     * 卡号
     */
    public String cardId;
    /**
     * 银行预留手机号
     */
    public String mobileId;
    /**
     * 子商户类型：1（个人商户），2 （个体工商户），3（企业商户）
     */
    public String userType;
}
