package com.sgb.capital.model;

public class PayInfoEntity {

//    	"amount": 0,金额
//                "created": "",	创建时间
//                "extra": "",额外信息，用于跳转
//                "name": "",	商户名称
//                "orderId": "",	订单编号
//                "sellerId": "",卖家联动商户编码
//                "tradeNo": "",联动交易订单
//                "tradeType": 0交易类型，1：材料 2:物流信息费 5:物流运费 6:租赁订单

    public double amount;
    public String created;
    public String extra;
    public String name;
    public String orderId;
    public String sellerId;
    public String tradeNo;
    public int tradeType;
    public int orderLimitTime;
}
