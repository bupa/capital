package com.sgb.capital.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 费用开票列表bean
 * @author cyj
 */
public class InvoiceCompanyInfoBean {

    public int limit = 20;
    public int page = 1;

    /** 	总条数 */
    public int totalRow;
    /** 	当前页 */
    public long pageNumber;
    /** 	总页数 */
    public int totalPage = 0;
    /** 	分页条数 */
    public int pageSize = 20;
    /** 是否第一页 */
    public boolean firstPage;
    /** 	是否最后一页 */
    public boolean lastPage;
    public List<InvoiceCompanyList> list;

    /**
     * 转换UI显示公司名数据
     * @return data
     */
    public Map<String, String> convertUIShowData() {
        LinkedHashMap<String, String> data = new LinkedHashMap<>();
        if(null == list || list.size() <= 0) {
            return data;
        }
        for (InvoiceCompanyInfoBean.InvoiceCompanyList icl : list) {
            data.put(String.valueOf(icl.id), icl.companyName);
        }
        return data;
    }

    /**
     * 根据id获取item数据
     * @param id
     * @return
     */
    public InvoiceCompanyInfoBean.InvoiceCompanyList getItem(long id) {
        if(null == list || list.size() <= 0) {
            return null;
        }
        for (InvoiceCompanyInfoBean.InvoiceCompanyList icl : list) {
            if(icl.id == id) {
                return icl;
            }
        }
        return null;
    }


    public static class InvoiceCompanyList {

        public String address = "";
        public String bank = "";
        public String bankAccount = "";
        public String bankNumber = "";
        public long companyInfoId;
        public String companyName = "";
        public String companyNo = "";
        public long createTime;
        public String createUser = "";
        public String creditCode = "";
        public int deleted;
        public long id;
        public int isDefault;
        public String phone = "";
        public String rememberMe = "";
        public long updateTime;
        public String updateUser = "";
    }
}