package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/4/23 0023
 * 说明:银行卡
 */
public class BankBean {
    /**
     * accountName :
     * bankCard :
     * bankCode :
     * cardType : 0
     * checkCode :
     * enabledDefault : true
     * gateId :
     * id :
     * idCard :
     * openingBank :
     * phone :
     * type : 0
     * validity :
     */

    public String accountName;
    public String bankAccountName;
    public String bankCard;
    public String bankAccount;
    public String bankCode;
    public String bankName;
    public int cardType;
    public int userType = 3;//1（个人商户），2 （个体工商户），3（企业商户）
    public String checkCode;
    public boolean enabledDefault;
    public String gateId;
    public String id;
    public String idCard;
    public boolean bindBank;
    public boolean payBank;
    public String openingBank;
    public String phone;
    public int type;
    public String validity;
    public String mobileId;
    public String bankBranchName;
}