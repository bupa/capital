package com.sgb.capital.model;

public class PayCardEntity {

//			accountName	账户名 string
//    bankCard	银行卡号	string
//    bankCode	银行码	string
//    cardType	卡类型(0支持借记/1贷记卡)	integer
//    checkCode	信用卡校验码	string
//    enabledDefault	是否默认	boolean
//    gateId	银行编码	string
//    id		string
//    idCard	身份证号	string
//    openingBank	开户行	string
//    phone	预留手机号	string
//    type	账户类型：[0个人、1企业]	integer
//    validity	信用卡有效期
    public String accountName;
    public String bankCard;
    public String bankCode;
    public Integer cardType;
    public String checkCode;
    public boolean enabledDefault;
    public String gateId;
    public String id;
    public String idCard;
    public String openingBank;
    public String phone;
    public Integer type;
    public String validity;
    public boolean isCheck;
}
