package com.sgb.capital.model;


import androidx.lifecycle.ViewModel;

/**
 * 作者:张磊
 * 日期:2020/11/28 0028
 * 说明:通用
 */

public class Bean extends ViewModel {

    public String url;
    public String no;
    public String id;
    public String name="";
    public String phone;
    public String code;
    public String dateType;
    public String minPrice;//1 最低价
    public String maxPrice; //0 最高价
    public String taxScale;// 纳税规模 0：小规模:1：一般纳税人:2：出口纳税人:3：未核税；

    public float money;
    public int type;
    public boolean select;
    public String title;
    public String bankAccountName;
    public String cardId;
    public String mobileId;
    public String userType;
    public String value;

    public Bean(String url, int type) {
        this.url = url;
        this.type = type;
    }

    public Bean(String bankAccountName, String cardId, String mobileId, String userType) {
        this.bankAccountName = bankAccountName;
        this.cardId = cardId;
        this.mobileId = mobileId;
        this.userType = userType;
    }

    public Bean(String no, String name) {
        this.no = no;
        this.name = name;
    }

    public Bean(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public Bean(String name) {
        this.name = name;
    }

    public Bean(String phone, String code, String id) {
        this.phone = phone;
        this.code = code;
        this.id = id;
    }
}
