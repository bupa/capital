package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/2/5 0005
 * 说明:
 */
public class BalanceDetailsEntity {

    public String balance;
    public String companyNo;
    public long created;
    public String creator;
    public String creatorName;
    public Boolean deleted;
    public long finishTime;
    public String flowNo;
    public String id;
    public String orderId;
    public String otherCompanyName;
    public String payAccountName;
    public String payAccountNumber;
    public String payChannels;
    public String payName;
    public String payTime;
    public String payType;
    public String payeeName;
    public String paymentId;
    public long refundFinishTime;
    public String refundOriBankAccount;
    public String refundOriBankName;
    public String refundOriOrderId;
    public String refundOriPayType;
    public String refundRetMsg;
    public Integer refundStatus;
    public String retMsg;
    public Integer sourceType;
    public String tradeAmount="0";
    public String tradeFees;
    public Integer tradeFlowType;
    public Integer tradeStatus;
    public String tradeType;
    public String updater;
    public int repeatPayStatus;
    public String userId;
    public String withdrawBankAccount;
    public String withdrawBankAccountName;
    public String withdrawBankName;
}