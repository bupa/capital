package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/4/13 0013
 * 说明:财务流水实体类
 */

import java.io.Serializable;
import java.util.List;


/**
 * clientsName :
 * endTime :
 * flowNo :
 * limit : 0
 * page : 0
 * startTime :
 * tradeFlowType : 0
 * tradeType :
 */


public class PayOrderPageEntity implements Serializable {

    public String firstPage;
    public String lastPage;
    public String pageNumber;
    public int pageSize ;
    public int totalPage ;
    public int totalRow;


    public List<PayOrderEntity> list;
}