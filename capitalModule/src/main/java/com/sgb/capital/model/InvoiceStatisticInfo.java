package com.sgb.capital.model;

import java.io.Serializable;

/**
 * 统计可开具流水数量和金额
 * @author cyj
 */
public class InvoiceStatisticInfo implements Serializable {

    /** 请求参数 */
    public String endTime = "";
    public String flowNo = "";
    public String searchValue = "";
    public String startTime = "";

    /** 返回参数 */
    public String money = "";
    public long number = 0;
}
