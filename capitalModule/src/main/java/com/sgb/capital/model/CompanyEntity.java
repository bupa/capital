package com.sgb.capital.model;

import com.contrarywind.interfaces.IPickerViewData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CompanyEntity implements Serializable, IPickerViewData {

    @SerializedName(value = "compName", alternate = "comName")
    private String compName;

    @SerializedName(value = "compLogo", alternate = "logo")
    private String compLogo;
    @SerializedName(value = "compNo", alternate = "no")
    private String compNo;
    @SerializedName("isMainCompany")
    private String isMainCompany;

    //个人在企业中的信息
    @SerializedName("administrators")
    private String administrators;
    @SerializedName(value = "name", alternate = "realName")
    private String name;
    @SerializedName(value = "phone", alternate = "telphone")
    private String phone;
    @SerializedName(value = "departName", alternate = "deptName")
    private String departName;
    @SerializedName("roleName")
    private String roleName;
    @SerializedName("authTime")
    private long authTime;
    @SerializedName(value = "companyId", alternate = "comId")
    private String companyId;
    @SerializedName("updateTime")
    private long updateTime;
    @SerializedName("remark")
    private String remark;
    @SerializedName("sComName")
    private String sComName;
    @SerializedName("createTime")
    private long createTime;
    //状态(1审核中2已加入3未通过)


    private int status;
    // 类型 0个人 1企业 2团队
    private int type;

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getsComName() {
        return sComName;
    }

    public void setsComName(String sComName) {
        this.sComName = sComName;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public long getAuthTime() {
        return authTime;
    }

    public void setAuthTime(long authTime) {
        this.authTime = authTime;
    }

    public String getCompLogo() {
        return compLogo;
    }

    public void setCompLogo(String compLogo) {
        this.compLogo = compLogo;
    }

    public String getCompNo() {
        return compNo;
    }

    public void setCompNo(String compNo) {
        this.compNo = compNo;
    }

    public String getIsMainCompany() {
        return isMainCompany;
    }

    public void setIsMainCompany(String isMainCompany) {
        this.isMainCompany = isMainCompany;
    }

    public String getAdministrators() {
        return administrators;
    }

    public void setAdministrators(String administrators) {
        this.administrators = administrators;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    @Override
    public String toString() {
        return "CompanyEntity{" +
                ", compName='" + compName + '\'' +
                ", compLogo='" + compLogo + '\'' +
                ", compNo='" + compNo + '\'' +
                ", isMainCompany='" + isMainCompany + '\'' +
                ", administrators='" + administrators + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", departName='" + departName + '\'' +
                ", roleName='" + roleName + '\'' +
                '}';
    }

    @Override
    public String getPickerViewText() {
        return name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
