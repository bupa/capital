package com.sgb.capital.model;

public class OpenAccountInfoEntity {
//    address	商户地址	string
//    amount	总金额	number
//    assBal	冻结金额	number
//    balance	余额	number
//    bankAccountLicence	开户许可证	string
//    bussinessLicense	营业执照	string
//    certId	统一社会信用代码	string
//    certType	证件类型，1：营业执照 2：统一社会信用代码	string
//    contactsName	业务联系人姓名	string
//    custName	真实姓名	string
//    idCardBack	法人身份证（反面）	string
//    idCardFront	法人身份证（正面）	string
//    identityCode	证件号	string
//    identityId	法人证件号	string
//    legalName	法人姓名	string
//    mobileId	业务联系人的手机号/预留手机号	string
//    orderCount	待付订单数量	integer(int32)
//    retMsg	该笔交易的返回描述信息	string
//    status	审核状态 0-初始、1-待审批、2-审批通过、3-审批不通过	integer(int32)
//    subMerAlias	商户名称的业务别名,比如店铺名称	string
//    subMerName	商户名称	string
//    subMerType	商户类型，3（企业商户）、2（个体工商户）	string
//    todayOrderAmount	当日订单金额	number
//    userId	商户编号	string
//    userType	用户类型	string
    public String address;
    public String amount;
    public String assBal;
    public String balance;
    public String bankAccountLicence;
    public String bussinessLicense;
    public String certId;
    public String certType;
    public String contactsName;
    public String custName;
    public String idCardBack;
    public String idCardFront;
    public String identityCode;
    public String identityId;
    public String legalName;
    public String mobileId;
    public String orderCount;
    public String retMsg;
    public String status;
    public String subMerAlias;
    public String subMerName;
    public String subMerType;
    public String todayOrderAmount;
    public String userId;
    public String userType;
}
