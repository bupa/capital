package com.sgb.capital.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户地址
 * @author cyj
 */
public class UserAddressBean implements Serializable {

    private List<Records> data;

    /**
     * 转换UI显示收件人数据
     * @return data
     */
    public Map<String, String> convertUIShowData() {
        LinkedHashMap<String, String> dataMap = new LinkedHashMap<>();
        if(data.size() <= 0) {
            return dataMap;
        }
        for (Records rd : data) {
            dataMap.put(String.valueOf(rd.id), rd.consigneeName);
        }
        return dataMap;
    }

    /**
     * 根据id获取item数据
     * @param id
     * @return
     */
    public Records getItem(long id) {
        if(null == data || data.size() <= 0) {
            return null;
        }
        for (Records rd : data) {
            if(rd.id == id) {
                return rd;
            }
        }
        return null;
    }

    public static class Records {

        public Long id;
        public String address = "";
        public String consigneeName = "";
        public String phone = "";
        public String provinceName = "";
        public String cityName = "";
        public String districtName = "";

        /** 获取所在地区 */
        public String getAreaDesc() {
            return provinceName + cityName + districtName;
        }

    }

    public List<Records> getData() {
        return data;
    }

    public UserAddressBean setData(List<Records> data) {
        this.data = data;
        return this;
    }
}
