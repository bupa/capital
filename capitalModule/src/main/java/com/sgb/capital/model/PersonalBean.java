package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/5/14 0014
 * 说明:提交个人注册
 */
public class PersonalBean {


    /**
     * custName :
     * identityCode :
     * mobileId :
     * verifyCode :
     */

    public String custName;
    public String identityCode;
    public String mobileId;
    public String verifyCode;

    public PersonalBean(String custName, String identityCode, String mobileId, String verifyCode) {
        this.custName = custName;
        this.identityCode = identityCode;
        this.mobileId = mobileId;
        this.verifyCode = verifyCode;
    }
}