package com.sgb.capital.model;

import java.io.Serializable;

public class PayOrderEntity implements Serializable {
//    account	付款账户	string
//    accountNumber	付款账号	string
//    auditStatus	审批状态	integer(int32)
//    channel	付款渠道	string
//    collectionVoucher	收款凭证	string
//    completionTime	完成时间	string(date-time)
//    contacts	业务联系人	string
//    created	创建日期	string(date-time)
//    creator	创建人	string
//    id	ID	string
//    isFeePayer	卖家是否是手续费承担方 1 是，0 否	string
//    matters	付款实事项	string
//    method	付款方式	string
//    money	付款金额	number
//    number	交易流水号	string
//    orderId	订单编号	string
//    orderType	订单类型	string
//    orderTypeName	订单类型名称	string
//    payApplyState	支付申请状态 0 待申请 1 审批中 2 审批通过 3 审批未通过	integer(int32)
//    payState	付款状态 0 未付款 1 冻结中 2 支付成功 3 支付失败	integer(int32)
//    payType	支付类型	string
//    payeeAccountBankName	收款企业开户银行	string
//    payeeAccountName	收款企业账户名称	string
//    payeeAccountNumber	收款企业账户号码	string
//    payeeCompany	收款企业编号	integer(int64)
//    payeeCompanyName	收款企业名称	string
//    payeeName	收款用户名称	string
//    payeeNo	收款用户编号	string
//    payeeUserId	收款联动商户号 ，用于在线支付	string
//    payerAccountName	付款企业账户名称	string
//    payerCompany	付款企业编号	integer(int64)
//    proof	支付凭证	string
//    state	状态 0 审批中 1 审批通过 3 审批未通过 4 确认中 5 支付成功 6 异常	integer(int32)
//    tradeFees	手续费	string
//    tradeNo	联动订单编号	string
//    tradeType	交易模式 2担保 3标准
    public String account;
    public String accountNumber;
    public String auditStatus;
    public String channel;
    public String collectionVoucher;
    public long completionTime;
    public String contacts;
    public long created;
    public String creator;
    public String id;
    public String isFeePayer;
    public String matters;
    public String method;
    public float money;
    public String number;
    public String orderId;
    public String paymentId;
    public String orderType;
    public float transferMoney;
    public String orderTypeName;
    public String payApplyState;
    public int payState;
    public String payType;
    public String payeeAccountBankName;
    public String payeeAccountName;
    public String payeeAccountNumber;
    public String payeeCompany;
    public String payeeCompanyName="/";
    public String payeeName;
    public int continuePay=1;
    public String payeeNo;
    public String payeeUserId;
    public String payerAccountName;
    public String payerCompany;
    public String proof;
    public int state;
    public String tradeFees;
    public String tradeNo;
    public String tradeType;
    public String payerCompanyName;
    public long payTime;
}
