package com.sgb.capital.model;

import com.google.gson.annotations.SerializedName;

public class H5ReEntity {
    @SerializedName("url")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
