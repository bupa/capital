package com.sgb.capital.model;

public class AddAndEditCardsBean {
//    accountName	string账户名		false
//    bankCard	string银行卡号		false
//    cardType	integer(int32)卡类型(1支持借记/2贷记卡)		false
//    checkCode string	信用卡校验码		false
//    enabledDefault boolean	是否默认		false
//    gateId string	银行编码（联动）		false
//    id		 string	false
//    idCard	  string身份证号		false
//    openingBank	string开户行		false
//    phone	  string 预留手机号		false
//    type	integer(int32) 账户类型：[0个人、1企业]		false
//    validity	  string 信用卡有效期		false

    public String accountName;
    public String bankCard;
    public String cardType="1";
    public String checkCode;
    public String enabledDefault;
    public String gateId;
    public String id;
    public String idCard;
    public String openingBank;
    public String phone;
    public String type="0";
    public String validity;
    public String code;

}
