package com.sgb.capital.model;

public class WithdrawalCompanyEntity {
//    bankAccount	string银行账户编号		true
//    bankAccountName	string企业账户名称/持卡人名		true
//    bankBranchName	string按照开户许可证上的开户行支行全称		true
//    code	string验证码		true
//    gateId	string银行编码，具体查询绑定企业提现卡支持银行列表接口		true
//    mobileId	string银行预留手机号		true
//    userType	string子商户类型:1（个人商户），2 （个体工商户），3（企业商户）		true
    public String bankAccount;
    public String bankAccountName;
    public String bankBranchName;
    public String code;
    public String gateId;
    public String mobileId;
    public String userType;
}
