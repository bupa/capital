package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/9/16 0016
 * 说明:支付详情
 */
public class PayDealBean {
  /*  amount	总金额	number
    created	创建时间	string(date-time)
    extra	额外信息，用于跳转	string
    fees	手续费	string
    isPayerFee	是否承担手续费	boolean
    name	商户名称	string
    orderAmount	订单金额	number
    orderId	订单编号	string
    orderLimitTime	倒计时剩余时间	integer(int64)
    paymentId	业务订单编号	string
    sellerId	卖家联动商户编码	string
    tradeNo	联动交易订单	string
    tradeType	交易类型，1：材料 2:物流信息费 5:物流运费 6:租赁订单	integer(int32)
    zjOrderId	联动订单id*/
    public float amount;
    public String created;
    public String extra;
    public String fees;
    public Boolean isPayerFee;
    public String name;
    public float orderAmount;
    public String orderId;
    public int orderLimitTime;
    public String paymentId;
    public String sellerId;
    public String tradeNo;
    public int tradeType;
    public String zjOrderId;
    public float balance;


  public String mobileId;
}