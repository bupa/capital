package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/4/21 0021
 * 说明:
 */
public class RecordsBean {
    /**
     * id : 1381515916398821376
     * created : 1618214016000
     * flowNo : 20220210412155338738
     * tradeType : 租赁支出
     * orderId : 1022021041215305070
     * tradeFlowType : 2
     * tradeStatus : 3
     * tradeAmount : 1
     * tradeFees : 0
     * payType : 微信扫码支付
     * payChannels : 微信
     * companyNo : 201907185069098
     * payName : 新疆金正大工贸有限公司
     * payeeName : 中钧科技有限公司
     * payTime : 1618214036000
     * deleted : false
     * retMsg  : false
     */

    public String id;
    public long created;
    public String flowNo;
    public String tradeType="订单交易";
    public String sourceType;
    public String orderId;
    public int tradeFlowType;
    public int tradeStatus = 1;
    public float tradeAmount;
    public String tradeFees="0";
    public String payType = "";
    public String payChannels;
    public String companyNo;
    public String payName;
    public String payeeName;
    public String paymentId;
    public long payTime;
    public boolean deleted;
    public String payAccountName;
    public String payAccountNumber;
    public String finishTime;
}