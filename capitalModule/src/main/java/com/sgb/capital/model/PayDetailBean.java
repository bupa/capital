package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/9/6 0006
 * 说明:
 */
public class PayDetailBean {
    public String account="/";
    public String accountNumber="/";
    public String channel="/";
    public String collectionVoucher;
    public long completionTime;
    public String contacts;
    public long created;
    public float fees;
    public String finishTime;
    public String flowNo="/";
    public String matters;
    public String method;
    public float money;
    public String number="/";
    public String orderId;
    public String orderType;
    public String orderTypeName="在线转账";
    public Integer payApplyState;
    public int payState;
    public String payType;
    public String payeeAccountBankName;
    public String payeeAccountName;
    public String payeeAccountNumber;
    public String payeeName;
    public String payerCompanyName="/";
    public String paymentId="/";
    public String proof;
    public String remark="/";
    public int state;
    public String payeeCompanyName="/";
    public int payStatus;
    public float transferMoney;
    public long payTime;
}