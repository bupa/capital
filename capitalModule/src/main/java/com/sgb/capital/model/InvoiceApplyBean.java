package com.sgb.capital.model;

/**
 * 开票申请bean
 * @author cyj
 */
public class InvoiceApplyBean {

    /** 企业地址 */
    public String companyAddress;
    /** 企业开户银行帐号 */
    public String companyBankAccount;
    /** 企业开户银行 */
    public String companyBankName;
    /** 企业税号 */
    public String companyDutyNo;
    /** 企业联系方式 */
    public String companyPhone;
    /** 开具发票流水ids */
    public String[] ids;
    /** 发票内容 */
    public String invoiceContent;
    /** 单位名称(发票抬头) */
    public String invoiceHead;
    /** 发票类型 1.增值税专用发票 2.增值税普通发票 */
    public int invoiceType = 2;
    /** 收件人地址 */
    public String mailAddress;
    /** 收件人地区 */
    public String mailAreaName;
    /** 收件人 */
    public String mailName;
    /** 收件人手机号 */
    public String mailPhone;
    /** 开票类型 1.企业 2.个人 */
    public int makeBy = 1;



}