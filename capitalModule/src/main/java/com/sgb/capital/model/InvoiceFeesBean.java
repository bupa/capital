package com.sgb.capital.model;

import androidx.databinding.ObservableBoolean;

import java.math.BigDecimal;
import java.util.List;

/**
 * 费用开票列表bean
 * @author cyj
 */
public class InvoiceFeesBean {

    // 请求参数
    /** 截止时间 */
    public String endTime;
    /** 流水号 */
    public String flowNo;
    /** 分页条数 */
    public int limit = 20;
    /** 页码 */
    public int page = 1;
    /** 起始时间 */
    public String startTime;
    /** 搜索参数 流水号或者订单号 */
    public String searchValue;

    // 响应参数
    /** id */
    public String id;
    /** 创建日期 */
    public long created;
    /** 交易流水号 */
    /** 发票开具状态 0.未开发票 1.已开发票 */
    public int invoiceStatus;
    /** 订单编号 */
    public String orderId = "";
    /** 对方企业名称 */
    public String otherCompanyName;
    /** 支付时间 */
    public long payTime;
    /** 支付类型 */
    public String payType="";
    /** 业务订单编号 */
    public String paymentId = "";
    /** 交易手续费 */
    public String tradeFees;
    /** 交易类型 比如：商品采购、商品销售 */
    public String tradeType;

    public List<InvoiceFeesBean> list;

    public String getTradeFees() {
        return new BigDecimal(tradeFees).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
    }

    /** 单选按钮是否选中 */
    public ObservableBoolean isChecked = new ObservableBoolean(false);
}