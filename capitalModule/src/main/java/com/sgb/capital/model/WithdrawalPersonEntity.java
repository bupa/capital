package com.sgb.capital.model;

public class WithdrawalPersonEntity {
//    bankAccountName	string持卡人名		true
//    cardId	string卡号		true
//    mobileId	string银行预留手机号		true
//    userType string 子商户类型：1（个人商户），2 （个体工商户），3（企业商户）	true
    public String bankAccountName;
    public String cardId;
    public String mobileId;
    public String userType;
}
