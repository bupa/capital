package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/4/13 0013
 * 说明:财务流水实体类
 */

import java.util.List;


/**
 * clientsName :
 * endTime :
 * flowNo :
 * limit : 0
 * page : 0
 * startTime :
 * tradeFlowType : 0
 * tradeType :
 */


public class PayFlowBean {

    public String clientsName;
    public String endTime;
    public String flowNo;
    public String dateType;
    public String searchValue;
    public int limit = 20;
    public int page = 1;
    public String startTime;
    public int tradeFlowType;  // 支出2  收入1 全部 0
    public String tradeType;

    public List<RecordsBean> list;
    public String sourceType;
    public String strKey;
}