package com.sgb.capital.model;

/**
 * 作者:张磊
 * 日期:2021/9/16 0016
 * 说明:复核人员名称
 */
public class ListBean {


  /*  companyNo	公司id	string
    created	创建日期	string(date-time)
    creator	创建人	string
    creatorName	创建人名称	string
    id	ID	string
    isDeleted	是否删除	boolean
    reviewerName	复核人员名称	string
    reviewerPhone	复核人员电话号码	string
    reviewerUserNo	复核人员id	string
    updater	更新人*/

    public String companyNo;
    public String created;
    public String creator;
    public String creatorName;
    public String id;
    public Boolean isDeleted;
    public String reviewerName;
    public String reviewerPhone;
    public String reviewerUserNo;
    public String updater;
}