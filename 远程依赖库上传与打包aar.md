![输入图片说明](app/34d9db64fa981ed55d0785415defc7d.jpg)
![输入图片说明](app/874f00f652d46049a9c825283ffbb05.jpg)
![输入图片说明](app/e81b44bdefb491afed658d04cda3e84.jpg)
![输入图片说明](app/0d38b8fea4d3216b8840c857d387a16.jpg)
![输入图片说明](app/dbc64327757dee78711af837e3b4f3f.jpg)
![输入图片说明](app/4e8f23e740fa89612673d65e95685ae.jpg)
![输入图片说明](app/216c7030677454a7938b166864cc76b.jpg)
11111111111
![输入图片说明](app/e70376f0aa549f844cd3540736e805d.jpg)
![输入图片说明](app/deba149e8376b6bdc4fd0c72241c444.jpg)
![输入图片说明](app/d52965b94cd37e544d15ea2501b6eec.jpg)
![输入图片说明](app/368358767a1a4db0198357b185d4178.jpg)
![输入图片说明](app/d5b5298a5dac52d502d67f3423b6194.jpg)
# 打包aar与上传依赖库


## **(一)打包aar**

1. 打开android studio 如图所示 选择Gradle的任务管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/160003_b89a364b_1065166.png "屏幕截图.png")
2. 选择需要执行的libs库可根据项目需求打包不同的环境下aar
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/160354_9bd816c2_1065166.png "屏幕截图.png")
![b771a634b744874e1f5a3d2d2e45d60d.png](en-resource://database/535:1)
3. 编译完成后aar包或apk会放在该lib的build/outputs文件夹下如图所示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/160442_214f9d66_1065166.png "屏幕截图.png")
## **(二)上传依赖库**
1. 将自己修改或创建的依赖库提交到远程仓库
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/160703_05fa3cf9_1065166.png "屏幕截图.png")
2. 用浏览器打开远程仓库地址(这里是gitee 当然githup也一样只是比较卡一点 )
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/160813_ac919c58_1065166.png "屏幕截图.png")
3. 创建发行版
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/160852_7ca0d06a_1065166.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/161043_85d7a440_1065166.png "屏幕截图.png")
4.创建完成后复制自己的仓库地址到jitPack如图所示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/161232_c8b8ea94_1065166.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/161452_fa43a3f2_1065166.png "屏幕截图.png")
5. 在app的gradle 添加对应版本的依赖就行了
dependencies {
    ...
    implementation 'com.gitee.bupa:capital:1.2.1'
}
